import tornado_opentracing
import logging

import tornado.web
import tornado.ioloop

from config import config
from handler import config_server
from handler.gateway.gateway_msg_handler import GatewayMessageHandler
from handler.groups_handler import GroupsHandler
from handler.gateway.gateway_integrations_handler import GatewayIntegrationsHandler
from handler.agent.query_nlu_handler import QueryNluHandler
from handler.integrations_handler import IntegrationsHandler
from handler.message_history_handler import ReceiveAgentMessageHistoryHandler, ReceiveGatewayMessageHistoryHandler
from handler.sign_in_handler import SignInHandler
from handler.sign_out_handler import SignOutHandler
from handler.sign_up_handler import SignUpHandler
from handler.reset_password_mail_handler import ResetPasswordMailHandler
from handler.reset_password_handler import ResetPasswordHandler
from handler.check_user_handler import CheckUserHandler
from handler.activation_mail_handler import ActivationMailHandler
from handler.user_activation_handler import UserActivationHandler
from handler.users_handler import UsersHandler
from handler.bots_handler import BotsHandler
from handler.bot_auth_handler import BotAuthHandler
from handler.statistic_handler import StatisticHandler
from handler.sentiment_analysis_handler import SentimentAnalysisHandler
from handler.intents_handler import IntentsHandler
from handler.utterances_handler import UtterancesHandler
from handler.endpoint_handler import EndpointHandler
from handler.find_endpoint_handler import FindEndpointHandler
from handler.entities_handler import EntitiesHandler
from handler.dialogs_handler import DialogsHandler
from handler.deployment_handler import DeploymentHandler
from handler.train_handler import TrainHandler
from handler.chat_history_handler import ChatHistoryHandler
from handler.flow_handler import FlowHandler
from handler.export_bot_handler import ExportBotHandler
from handler.import_bot_handler import ImportBotHandler
from handler.import_intents_handler import ImportIntentsHandler
from handler.import_entities_handler import ImportEntitiesHandler
from handler.import_utterances_handler import ImportUtterancesHandler
from handler.import_dialogs_handler import ImportDialogsHandler
from handler.export_dialogs_handler import ExportDialogsHandler
from handler.analyze_utterance_handler import AnalyzeUtteranceHandler
from handler.nlu.nlu_models_handler import NluModelsHandler
from handler.nlu.nlu_intents_handler import NluIntentsHandler
from handler.nlu.nlu_entities_handler import NluEntitiesHandler
from handler.nlu.nlu_utterances_handler import NluUtterancesHandler
from handler.config_test_handler import BotConfigTestHandler
from handler.config_gold_test_case_handler import ConfigGoldTestCaseHandler
from handler.train_log_handler import TrainLogHandler
from handler.intent_usage_handler import IntentUsageHandler
from handler.customer_service_auth_handler import CustomerServiceAuthHandler
from handler.notification_history_handler import NotificationHistoryHandler
from handler.auto_labeling_handler import AutoLabelingHandler
from handler.nlu_info_handler import NluInfoHandler
from handler.fb.long_lived_access_token_handler import LongLivedAccessTokenHandler

# Your OpenTracing-compatible tracer here.
from jaeger_client import Config
from tornado_opentracing.scope_managers import TornadoScopeManager
from sr_toolkit.health_manager import HealthManager, HealthHandler
from sr_toolkit.prometheus_health_manager import MetricsHandler
import os


def init_jaeger_tracer(service_name, jeager_host):
    print(jeager_host)
    config = Config(
        config={  # usually read from some yaml config
            'sampler': {
                'type': 'probabilistic',
                'param': 1,
            },
            'local_agent': {
                'reporting_host': jeager_host,
            },
        },
        service_name=f"{service_name}:{os.getenv('tenant','dev')}",
        validate=True,
        scope_manager=TornadoScopeManager()
    )
    # Create your opentracing tracer using TornadoScopeManager for active Span handling.
    return config.initialize_tracer()


def init_health_manager():
    HealthManager.get_instance()\
        .add_component(name='always_true', ping_method=lambda: True)


class Application(tornado.web.Application):
    VERSION = '1'

    def __init__(self):
        routes = [
            (r"/server_config", config_server.ServerConfigHandler),
            (r"/silkbot_config", config_server.SilkbotConfigHandler),
            (f"/health", HealthHandler),
            (f"/metrics", MetricsHandler.name(name='ChatbotplatformHealth')),
            (f"/editor/groups", GroupsHandler),
            (rf"/editor/groups/([\w=]*)", GroupsHandler),
            (f"/message/reply", ReceiveAgentMessageHistoryHandler),
            (f"/message/receive", ReceiveGatewayMessageHistoryHandler),
            (rf"/gateway/integrations/([\w=]*)?", GatewayIntegrationsHandler),
            (f"/gateway/passage/(?P<passage>[\w=]*)/send_message",
             GatewayMessageHandler),
            (f"/agent/query_nlu", QueryNluHandler),
            (f"/editor/signin", SignInHandler),
            (f"/editor/signout", SignOutHandler),
            (f"/editor/signup", SignUpHandler),
            (f"/editor/reset_password_mail", ResetPasswordMailHandler),
            (f"/editor/reset_password", ResetPasswordHandler),
            (f"/editor/check_user", CheckUserHandler),
            (f"/editor/activation_mail", ActivationMailHandler),
            (f"/editor/user_activation", UserActivationHandler),
            (f"/editor/users", UsersHandler),
            (rf"/editor/users/(.*)", UsersHandler),
            (f"/editor/bots", BotsHandler),
            (rf"/editor/bots/([\w=]*)", BotsHandler),
            (rf"/editor/bots/([\w=]*)/statistic", StatisticHandler),
            (rf"/editor/bots/([\w=]*)/sentiment_analysis",
             SentimentAnalysisHandler),
            (rf"/editor/bots/([\w=]*)/auth", BotAuthHandler),
            (rf"/editor/bots/([\w=]*)/auth/([\w=-]*)", BotAuthHandler),
            (f"/editor/intents", IntentsHandler),
            (rf"/editor/intents/([\w=]*)", IntentsHandler),
            (f"/editor/utterances", UtterancesHandler),
            (rf"/editor/utterances/([\w=]*)", UtterancesHandler),
            (rf"/editor/auto_labeling", AutoLabelingHandler),
            (f"/editor/entities", EntitiesHandler),
            (rf"/editor/entities/([\w=]*)", EntitiesHandler),
            (f"/editor/endpoint", EndpointHandler),
            (rf"/editor/endpoint/([\w=]*)", EndpointHandler),
            (f"/editor/find_endpoint", FindEndpointHandler),
            (rf"/editor/find_endpoint/([\w=]*)", FindEndpointHandler),
            (f"/editor/dialogs", DialogsHandler),
            (rf"/editor/dialogs/([\w=]*)", DialogsHandler),
            (rf"/editor/train", TrainHandler),
            (rf"/editor/deployment", DeploymentHandler),
            (rf"/editor/nlu_models/([\w=]*)", NluModelsHandler),
            (rf"/editor/nlu_models/([\w=]*)/intents", NluIntentsHandler),
            (rf"/editor/nlu_models/([\w=]*)/entities", NluEntitiesHandler),
            (rf"/editor/nlu_models/([\w=]*)/utterances", NluUtterancesHandler),
            (rf"/editor/nlu_services/analyze_utterance", AnalyzeUtteranceHandler),
            (f"/editor/integrations", IntegrationsHandler),
            (rf"/editor/integrations/([\w=]*)", IntegrationsHandler),
            (f"/editor/chathistory", ChatHistoryHandler),
            (f"/editor/flow", FlowHandler),
            (rf"/editor/flow/([\w=]*)", FlowHandler),
            (rf"/editor/bots/([\w=]*)/export", ExportBotHandler),
            (rf"/editor/bots/([\w=]*)/import", ImportBotHandler),
            (rf"/editor/bots/([\w=]*)/import_intents", ImportIntentsHandler),
            (rf"/editor/bots/([\w=]*)/import_entities", ImportEntitiesHandler),
            (rf"/editor/bots/([\w=]*)/import_utterances",
             ImportUtterancesHandler),
            (rf"/editor/bots/([\w=]*)/import_dialog", ImportDialogsHandler),
            (rf"/editor/bots/([\w=]*)/export_dialog", ExportDialogsHandler),
            (rf"/editor/config_test/([\w=]*)", BotConfigTestHandler),
            (rf"/editor/config_test_corpus/([\w=]*)",
             ConfigGoldTestCaseHandler),
            (rf"/editor/bots/([\w=]*)/train_log", TrainLogHandler),
            (rf"/editor/bots/([\w=]*)/intent_usage", IntentUsageHandler),
            (rf"/editor/bots/([\w=]*)/cs_auth", CustomerServiceAuthHandler),
            (f"/editor/notification_history", NotificationHistoryHandler),
            (rf"/editor/notification_history/([\w=]*)",
             NotificationHistoryHandler),
            ("/editor/nlu_info", NluInfoHandler),
            ("/editor/fb/long_lived_access_token", LongLivedAccessTokenHandler),

        ]
        if config.ENABLE_MOCK_NLU_SERVICE:
            import handler.nlu.nlu_rule_base_handler
            from handler.mock.mock_nlu_handler import MockNluHandler
            routes.extend([
                ("/nlu_services", MockNluHandler),
                (r"/nlu_services/([\w=]*)", MockNluHandler)])
            routes.extend([
                (f"/rule/nlu", handler.nlu.nlu_rule_base_handler.RuleBaseNlu)
            ])
        # Give prefix of /api/v1 if run on DEV mode.
        if config.IS_DEV:
            for idx, (path, path_handler) in enumerate(routes):
                assert isinstance(path, str), 'invalid path.'
                if not path.startswith('/'):
                    path = f'/{path}'
                path = f'/api/v{self.VERSION}{path}'
                routes[idx] = (path, path_handler)

        for idx, (path, path_handler) in enumerate(routes):
            routes[idx] = tornado.web.url(path, path_handler)

        settings = {}
        # Initialize tracing before creating the Application object
        tracer = init_jaeger_tracer(
            "ChatbotPlatform", jeager_host=config.JEAGER_REPORTING_HOST)
        tornado_opentracing.init_tracing()
        init_health_manager()
        tornado.web.Application.__init__(
            self,
            routes,
            opentracing_tracing=tornado_opentracing.TornadoTracing(
                tracer=tracer),
            **settings)


if __name__ == "__main__":
    print(f"listen:{config.LISTEN_PORT}")
    app = Application()
    app.listen(config.LISTEN_PORT)
    if config.IS_LOG_TO_FILE:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
            filename=config.LOG_FILE_PATH)
    tornado.ioloop.IOLoop.current().start()
