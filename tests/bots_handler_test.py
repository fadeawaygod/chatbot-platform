from unittest.mock import MagicMock, Mock, patch

from handler.bots_handler import BotsHandler
from handler.service.db import ORM
from service_caller import DbFunction
from tornado.testing import AsyncTestCase, gen_test

from tests.tools import get_fake_future, get_fake_handler


class BotsHandlerTest(AsyncTestCase):
    @patch('handler.bots_handler.ServiceCaller')
    @gen_test
    def test_get_editor_should_call_db_service_with_filter(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_bots_handler = get_fake_handler(BotsHandler)
        test_user_id = 'test_user_id'
        fake_bots_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_bots_handler.request.arguments = {}
        yield fake_bots_handler.get()

        dic_should_be_called = {
            'collection': fake_bots_handler.COLLECTION_BOT,
            'filter': {'owner': test_user_id},
            'projection': {
                '_id': False,
                'id': True,
                'owner': True,
                'accessors': True,
                'name': True,
                'config': True,
                'deploy_status': True,
                'train_status': True,
                'last_failed_log': True,
                'is_nlu_data_modified': True,
                'train_detail': True,
                'intent_threshold': True,
                'nlu_endpoint': True,
                'nlu_id': True,
                'nlu_token': True,
                'description': True,
                'use_fixed_nlu': True,
                'created': True,
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.bots_handler.ServiceCaller')
    @gen_test
    def test_get_editor_arg_contain_skip_and_limit_should_call_db_service_with_skip_and_limit(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_bots_handler = get_fake_handler(BotsHandler)
        test_user_id = 'test_user_id'
        fake_bots_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {
            'skip': '10',
            'limit': '20',
        }
        fake_bots_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_bots_handler.get()

        dic_should_be_called = {
            'collection': fake_bots_handler.COLLECTION_BOT,
            'filter': {'owner': test_user_id},
            'projection': {
                '_id': False,
                'id': True,
                'owner': True,
                'accessors': True,
                'name': True,
                'config': True,
                'deploy_status': True,
                'train_status': True,
                'last_failed_log': True,
                'is_nlu_data_modified': True,
                'train_detail': True,
                'intent_threshold': True,
                'nlu_endpoint': True,
                'nlu_id': True,
                'nlu_token': True,
                'description': True,
                'use_fixed_nlu': True,
                'created': True,
            },
            'sort': {'updated': -1},
            'skip': 10,
            'limit': 20,
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.bots_handler.ServiceCaller')
    @gen_test
    def test_get_bots_with_is_owner_false_should_call_db_service_with_filter_accessors(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_bots_handler = get_fake_handler(BotsHandler)
        test_user_id = 'test_user_id'
        fake_bots_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }

        test_request_arguments = {'is_owner': 'false'}
        fake_bots_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_bots_handler.get()

        dic_should_be_called = {
            'collection': fake_bots_handler.COLLECTION_BOT,
            'filter': {'accessors.test_user_id': {'$exists': True}},
            'projection': {
                '_id': False,
                'id': True,
                'owner': True,
                'accessors': True,
                'name': True,
                'config': True,
                'deploy_status': True,
                'train_status': True,
                'last_failed_log': True,
                'is_nlu_data_modified': True,
                'train_detail': True,
                'intent_threshold': True,
                'nlu_endpoint': True,
                'nlu_id': True,
                'nlu_token': True, 'nlu_id': True, 'nlu_token': True,
                'description': True,
                'use_fixed_nlu': True,
                'created': True,
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.bots_handler.ServiceCaller')
    @gen_test
    def test_get_single_bot_should_call_db_service_with_filter_bot_id(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test'}]}))
        fake_bots_handler = get_fake_handler(BotsHandler)
        test_user_id = 'test_user_id'
        fake_bots_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_bot_id = 'test_bot_id'
        fake_bots_handler.request.arguments = {}
        yield fake_bots_handler.get(test_bot_id)

        dic_should_be_called = {
            'collection': fake_bots_handler.COLLECTION_BOT,
            'filter': {'id': 'test_bot_id'},
            'projection': {
                '_id': False,
                'id': True,
                'owner': True,
                'accessors': True,
                'name': True,
                'config': True,
                'deploy_status': True,
                'train_status': True,
                'last_failed_log': True,
                'is_nlu_data_modified': True,
                'train_detail': True,
                'intent_threshold': True,
                'nlu_endpoint': True,
                'nlu_id': True,
                'nlu_token': True,
                'description': True,
                'use_fixed_nlu': True,
                'created': True,
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.bots_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_delete_editor_should_call_db_service(self, mock_service_caller):
        test_bot_id = 'test'
        test_user_id = 'test_user_id'
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future(
                {'data': [{'bot_id': test_bot_id, 'owner': test_user_id}]}))
        fake_bots_handler = get_fake_handler(BotsHandler)

        fake_bots_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }

        yield fake_bots_handler.delete(test_bot_id)

        dic_should_be_called = {
            'collection': fake_bots_handler.COLLECTION_BOT,
            'filter': {
                'id': test_bot_id,
                'owner': test_user_id
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.DELETE, dic_should_be_called)

    @patch('handler.bots_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_patch_editor_should_call_db_service(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_bots_handler = get_fake_handler(BotsHandler)
        test_user_id = 'test_user_id'
        fake_bots_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_bots_handler.json_args = {
            'name': 'test_name2'
        }
        test_bot_id = 'test_bot_id'
        yield fake_bots_handler.patch(test_bot_id)

        dic_should_be_called = {
            'collection': fake_bots_handler.COLLECTION_BOT,
            'filter': {
                'id': test_bot_id,
                'owner': test_user_id
            },
            'data': {
                'name': 'test_name2',
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.UPDATE, dic_should_be_called)
