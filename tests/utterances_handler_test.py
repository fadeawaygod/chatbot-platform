from unittest.mock import Mock, patch, MagicMock

from tornado.testing import AsyncTestCase, gen_test

from handler.service.db import ORM
from handler.utterances_handler import UtterancesHandler
from tests.tools import get_fake_future, get_fake_handler
from service_caller import DbFunction, NotificationFunction
from exception import NoPermissionError


class UtterancesHandlerTest(AsyncTestCase):
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_get_editor_should_query_db_service_intent_with_utterances(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'utterances': {}}]}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        test_intent_id = 'test_intent_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {'intent_id': test_intent_id}
        fake_utterances_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_utterances_handler.get()
        dic_should_be_called = {
            'collection': 'utterance',
            'filter': {'intent_ids': {'$in': ['test_intent_id']}},
            'projection': {
                '_id': False,
                'id': True,
                'text': True,
                'entities': True,
                'intent_ids': True,
            },
            'sort': {'updated': -1},
            'skip': 0
        }

        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_get_editor_arg_contain_skip_and_limit_should_call_db_service_with_skip_and_limit(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'utterances': {}}], 'count': 1}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        test_intent_id = 'test_intent_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {
            'intent_id': test_intent_id,
            'skip': '10',
            'limit': '20'
        }
        fake_utterances_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_utterances_handler.get()
        dic_should_be_called = {
            'collection': 'utterance',
            'filter': {'intent_ids': {'$in': ['test_intent_id']}},
            'projection': {
                '_id': False,
                'id': True,
                'text': True,
                'entities': True,
                'intent_ids': True,
            },
            'sort': {'updated': -1},
            'skip': 10,
            'limit': 20,
        }

        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}], 'count': 1})))
    @gen_test
    def test_post_editor_should_call_db_service(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {'id': 'test_id'}}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_utterances_handler.json_args = {
            'intent_ids': ['test_intent_id'],
            'text': 'test_text',
            'entities': [],
        }
        yield fake_utterances_handler.post()

        dic_should_be_called = {
            'collection': 'utterance',
            'data': {
                'intent_ids': ['test_intent_id'],
                'text': 'test_text',
                'entities': []
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.INSERT, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id', 'is_nlu_data_modified': False}], 'count': 1})))
    @gen_test
    def test_post_is_nlu_data_modified_false_should_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {'id': 'test_id'}}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_utterances_handler.json_args = {
            'intent_ids': ['test_intent_id'],
            'text': 'test_text',
            'entities': [],
        }
        yield fake_utterances_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_NLU_DATA,
            'test_user_id',
            'test',
            content={'is_modified': True},
        )

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}], 'count': 1})))
    @gen_test
    def test_delete_editor_should_call_db_service(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'intent_ids': ['test_id']}]}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_utterance_id = 'test_utterance_id'
        yield fake_utterances_handler.delete(test_utterance_id)

        dic_should_be_called = {
            'collection': 'utterance',
            'filter': {
                'id': test_utterance_id
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.DELETE, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id', 'is_nlu_data_modified': False}], 'count': 1})))
    @gen_test
    def test_delete_is_nlu_data_modified_false_should_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'intent_ids': ['test_id']}]}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_utterance_id = 'test_utterance_id'
        yield fake_utterances_handler.delete(test_utterance_id)

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_NLU_DATA,
            'test_user_id',
            'test',
            content={'is_modified': True},
        )

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_patch_editor_should_call_db_service(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'intent_ids': ['test_id']}]}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_utterances_handler.json_args = {
            'text': 'test_text2'
        }
        test_utterance_id = 'test_utterance_id'
        yield fake_utterances_handler.patch(test_utterance_id)

        dic_should_be_called = {
            'collection': 'utterance',
            'filter': {
                'id': test_utterance_id
            },
            'data': {
                'text': 'test_text2',
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.UPDATE, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.utterances_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id', 'is_nlu_data_modified': False}]})))
    @gen_test
    def test_patch_is_nlu_data_modified_false_should_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'intent_ids': ['test_id']}]}))
        fake_utterances_handler = get_fake_handler(UtterancesHandler)
        test_user_id = 'test_user_id'
        fake_utterances_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_utterances_handler.json_args = {
            'text': 'test_text2'
        }
        test_utterance_id = 'test_utterance_id'
        yield fake_utterances_handler.patch(test_utterance_id)

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_NLU_DATA,
            'test_user_id',
            'test',
            content={'is_modified': True},
        )
