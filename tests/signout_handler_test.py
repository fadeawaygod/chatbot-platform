
from unittest.mock import MagicMock, patch

from handler.sign_out_handler import SignOutHandler
from service_caller import AuthFunction
from tornado.testing import AsyncTestCase, gen_test

from tests.tools import get_fake_future, get_fake_handler


class SignOutHandlerHandlerTest(AsyncTestCase):
    @patch('handler.sign_out_handler.ServiceCaller')
    @gen_test
    def test_post_token_valid_should_call_auth_api_expectly(self, fake_service_caller):
        self._init_fake_service_caller(fake_service_caller)
        fake_user = {'uid': 'test_user'}

        handler = get_fake_handler(SignOutHandler)
        handler.json_args = self._get_valid_request_body()
        handler._user = fake_user
        handler._access_token = "test_access_token"
        yield handler.post()
        fake_service_caller.call_auth_service.assert_called_once_with(
            AuthFunction.SIGN_OUT,
            {
                'access_token': "test_access_token"
            }
        )

    def _get_valid_request_body(self) -> dict:
        return {
            "uid": "test-editor",
            "password": "xxx",
            "name": "test-editor-name",
            "group": "test-editor",
            "role": ["editor"],
        }

    def _init_fake_service_caller(self, fake_service_caller):
        fake_service_caller.call_auth_service = MagicMock(
            return_value=get_fake_future({"user": {"access_token": "test_access_token"}}))


class FakeUUID:
    hex = 'test_uuid'
