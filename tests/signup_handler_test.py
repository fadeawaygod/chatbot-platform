
from unittest.mock import patch, Mock, MagicMock

from exception import (GroupNotExistError, MailDuplicatedError,
                       MissRequiredParameterError, RoleInvalidError,
                       UIDDuplicatedError)
from handler.sign_up_handler import SignUpHandler
from tornado.testing import AsyncTestCase, gen_test

from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager)


class SignUpHandlerHandlerTest(AsyncTestCase):
    @gen_test
    def test_post_missing_required_parameter_should_raise(self):
        handler = get_fake_handler(SignUpHandler)
        handler.json_args = {
            "uid": "test-editor"
        }
        with self.assertRaises(MissRequiredParameterError) as _:
            yield handler.post()

    @gen_test
    def test_post_role_contains_invalid_role_should_raise(self):
        handler = get_fake_handler(SignUpHandler)
        handler.json_args = {
            "uid": "test-editor",
            "password": "Ss123456!",
            "name": "test-editor-name",
            "group": "test-editor",
            "roles": ["invalid-role"],
            "mail_address": "test@test.com"
        }
        with self.assertRaises(RoleInvalidError) as _:
            yield handler.post()

    @patch('handler.sign_up_handler.SessionManager')
    @gen_test
    def test_post_uid_duplicated_should_raise(self, fake_session_manager):
        user_fake_return_obj = {
            'data': [
                {
                    'uid': 'test-editor',
                    'mail_address': 'test2@test.com',
                }
            ],
        }
        db_fake_return_objs = [
            user_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(SignUpHandler)
        handler.json_args = self._get_valid_request_body()
        with self.assertRaises(UIDDuplicatedError) as _:
            yield handler.post()

    @patch('handler.sign_up_handler.SessionManager')
    @gen_test
    def test_post_mail_duplicated_should_raise(self, fake_session_manager):
        user_fake_return_obj = {
            'data': [
                {
                    'uid': 'test-editor2',
                    'mail_address': 'test@test.com',
                }
            ],
        }
        db_fake_return_objs = [
            user_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(SignUpHandler)
        handler.json_args = self._get_valid_request_body()
        with self.assertRaises(MailDuplicatedError) as _:
            yield handler.post()

    @patch('handler.sign_up_handler.ServiceCaller')
    @patch('handler.sign_up_handler.SessionManager')
    @gen_test
    def test_post_group_not_exist_should_raise(self, fake_session_manager, fake_service_caller):
        self.init_fake_service_caller(fake_service_caller)
        user_fake_return_obj = {
            'data': [],
        }
        group_fake_return_obj = {
            'data': [],
        }
        db_fake_return_objs = [
            user_fake_return_obj,
            group_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(SignUpHandler)
        handler.json_args = self._get_valid_request_body()
        with self.assertRaises(GroupNotExistError) as _:
            yield handler.post()

    @patch('handler.sign_up_handler.ServiceCaller')
    @patch('handler.sign_up_handler.SessionManager')
    @gen_test
    def test_post_group_empty_should_create_an_group_expectly(self, fake_session_manager, fake_service_caller):
        self.init_fake_service_caller(fake_service_caller)
        user_fake_return_obj = {'data': []}
        insert_group_fake_return_obj = {
            'status': "ok", "data": {"id": "test_group"}}
        insert_user_fake_return_obj = {'status': "ok"}
        insert_nlu_resources_fake_return_obj = {'status': "ok"}
        db_fake_return_objs = [
            user_fake_return_obj,
            insert_group_fake_return_obj,
            insert_user_fake_return_obj,
        ]
        for _ in range(SignUpHandler.RESOURCE_COUNT):
            db_fake_return_objs.append(insert_nlu_resources_fake_return_obj)

        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(SignUpHandler)
        handler.send_activation_mail = Mock(
            return_value=get_fake_future({'data': {}}))
        request_body = self._get_valid_request_body()
        del request_body['group']
        handler.json_args = request_body
        yield handler.post()
        querys = fake_session_manager.get_session().group_querys()
        self.assertEqual(len(querys), 3+SignUpHandler.RESOURCE_COUNT)
        insert_group_query = querys[1]

        self.assertDictEqual(
            insert_group_query['insert']['kargs'],
            {'collection': 'group'}
        )
        self.assertDictEqual(
            insert_group_query['data'][0]['kargs'],
            {'column': 'name', 'value': SignUpHandler.END_USER_GROUP_NAME_TEMPLATE.format(
                uid='test-editor')}
        )
        self.assertDictEqual(
            insert_group_query['data'][1]['kargs'],
            {'column': 'is_enduser', 'value': True}
        )

    @patch('handler.sign_up_handler.ServiceCaller')
    @patch('handler.sign_up_handler.SessionManager')
    @gen_test
    def test_post_parameter_correct_should_call_db_expectly(self, fake_session_manager, fake_service_caller):
        self.init_fake_service_caller(fake_service_caller)
        user_fake_return_obj = {'data': []}
        group_fake_return_obj = {'data': [{"id": "test"}]}
        insert_user_fake_return_obj = {'status': "ok"}
        insert_nlu_resources_fake_return_obj = {'status': "ok"}
        db_fake_return_objs = [
            user_fake_return_obj,
            group_fake_return_obj,
            insert_user_fake_return_obj,
        ]
        for _ in range(SignUpHandler.RESOURCE_COUNT):
            db_fake_return_objs.append(insert_nlu_resources_fake_return_obj)

        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(SignUpHandler)
        handler.send_activation_mail = Mock(
            return_value=get_fake_future({'data': {}}))
        handler.json_args = self._get_valid_request_body()
        yield handler.post()
        querys = fake_session_manager.get_session().group_querys()
        self.assertEqual(len(querys), 3+SignUpHandler.RESOURCE_COUNT)
        insert_user_query = querys[2]

        # last query is insert query
        self.assertDictEqual(
            insert_user_query['insert']['kargs'],
            {'collection': 'user'}
        )
        self.assertDictEqual(
            insert_user_query['data'][0]['kargs'],
            {'column': 'uid', 'value': 'test-editor'}
        )
        self.assertDictEqual(
            insert_user_query['data'][1]['kargs'],
            {'column': 'name', 'value': 'test-editor'}
        )
        self.assertDictEqual(
            insert_user_query['data'][2]['kargs'],
            {'column': 'group', 'value': 'test-editor'}
        )
        self.assertDictEqual(
            insert_user_query['data'][3]['kargs'],
            {'column': 'role', 'value': ['editor']}
        )
        self.assertDictEqual(
            insert_user_query['data'][4]['kargs'],
            {'column': 'mail_address', 'value': 'test@test.com'}
        )
        self.assertDictEqual(
            insert_user_query['data'][5]['kargs'],
            {'column': 'phone', 'value': ''}
        )

    def init_fake_service_caller(self, fake_service_caller):
        fake_service_caller.call_auth_service = MagicMock(
            return_value=get_fake_future({}))

    def _get_valid_request_body(self) -> dict:
        return {
            "uid": "test-editor",
            "password": "Ss111222!",
            "name": "test-editor-name",
            "group": "test-editor",
            "role": ["editor"],
            "mail_address": "test@test.com"
        }
