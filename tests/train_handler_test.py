import unittest
from unittest.mock import MagicMock, call, patch

from tornado.testing import AsyncTestCase, gen_test

from bot_event import NLU_DATA_UNCHANGED
from exception import (LabeledEntityNotEnough, TrainingFailedError, NotPermittedToUseNluError,
                       UtteranceNotEnough, NoNLUResourceAreAvailableError)
from handler.service.db import ORM
from handler.train_handler import TrainHandler
from service_caller import NluFunction, NotificationFunction
from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager,
                         mock_check_user_permision)


class TrainHandlerTest(AsyncTestCase):
    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_bot_id_invalid_should_raise(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify):

        self._set_up_patch_mock_objs(fake_service_caller)

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler, False)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        with self.assertRaises(Exception) as _:
            yield train_handler.post()

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_no_resource_should_raise(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        with self.assertRaises(NotPermittedToUseNluError) as _:
            yield train_handler.post()

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_all_resource_training_should_raise(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [{
                "id": "test-resource_id1",
                "uid": "test_editor",
                "is_training": True,
                "training_nlu_id": "",
                "last_trained_time": 0,
                "last_infered_time": 0,
                "bot_id": ""
            }],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        with self.assertRaises(NoNLUResourceAreAvailableError) as _:
            yield train_handler.post()

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_resource_contain_matched_bot_should_select_correct_resource(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [{
                "id": "test-resource_id1",
                "uid": "test_editor",
                "is_training": False,
                "training_nlu_id": "",
                "last_trained_time": 0,
                "last_infered_time": 0,
                "bot_id": ""
            },
                {
                    "id": "test-resource_id2",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": "test_bot_id"
            }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()
        fake_service_caller.call_nlu_service.assert_has_calls([
            call(NluFunction.CREATE, resource_id='test-resource_id2'),
            call(NluFunction.START_FETCH, nlu_id='test_nlu_id'),
        ])

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_contain_empty_resource_should_select_correct_resource(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [{
                "id": "test-resource_id1",
                "uid": "test_editor",
                "is_training": False,
                "training_nlu_id": "",
                "last_trained_time": 0,
                "last_infered_time": 0,
                "bot_id": "test_bot_idXXX"
            },
                {
                    "id": "test-resource_id2",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": ""
            }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()
        fake_service_caller.call_nlu_service.assert_has_calls([
            call(NluFunction.CREATE, resource_id='test-resource_id2'),
            call(NluFunction.START_FETCH, nlu_id='test_nlu_id'),
        ])

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_no_empty_resource_should_select_oldest_resource(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [{
                "id": "test-resource_id1",
                "uid": "test_editor",
                "is_training": False,
                "training_nlu_id": "",
                "last_trained_time": 0,
                "last_infered_time": 1,
                "bot_id": "test_bot_idXXX"
            },
                {
                    "id": "test-resource_id2",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": "test_bot_idXXX2"
            }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()
        fake_service_caller.call_nlu_service.assert_has_calls([
            call(NluFunction.CREATE, resource_id='test-resource_id2'),
            call(NluFunction.START_FETCH, nlu_id='test_nlu_id'),
        ])

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_nlu_data_unmodified_should_not_call_nlu_service(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler, result_bot={
            'id': 'test_bot_id',
            'name': 'test_bot',
            'owner': 'test_user_id',
            'is_nlu_data_modified': False,
            'train_status': 'trained',
        })
        bot_fake_return_obj = {
            'data': [{'id': 'test_bot_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            bot_fake_return_obj,
            insert_start_log_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_TRAIN_STATUS,
            'test_user_id',
            'test_bot_id',
            content={'id': NLU_DATA_UNCHANGED['id'],
                     'message': NLU_DATA_UNCHANGED['message']},
        )

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_nlu_data_unmodified_should_call_notification_service(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler, result_bot={
            'id': 'test_bot_id',
            'name': 'test_bot',
            'owner': 'test_user_id',
            'is_nlu_data_modified': False,
            'train_status': 'trained'
        })
        bot_fake_return_obj = {
            'data': [{'id': 'test_bot_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            bot_fake_return_obj,
            insert_start_log_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_service_caller.call_nlu_service.assert_not_called()

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_call_nlu_service_failed_should_notify(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            side_effect=Exception())

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [{
                "id": "test-resource_id1",
                "uid": "test_editor",
                "is_training": False,
                "training_nlu_id": "",
                "last_trained_time": 0,
                "last_infered_time": 1,
                "bot_id": ""
            }]
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_TRAIN_STATUS,
            'test_user_id',
            'test_bot_id',
            content={
                'status': 'failed',
                'bot_name': 'test_bot',
                'failed_log': 'Training failed:',
                'error_code': TrainingFailedError.code,
                'parameters': {}
            },
        )

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_utterance_not_enough_should_notify(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            side_effect=Exception())

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [
                {
                    "id": "test-resource_id1",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": ""
                }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [{'id': 'test_intent_id', 'name': 'test_intent'}],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_TRAIN_STATUS,
            'test_user_id',
            'test_bot_id',
            content={
                'status': 'failed',
                'bot_name': 'test_bot',
                'failed_log': TrainingFailedError.message.format(err=UtteranceNotEnough.message.format(intent_name='test_intent')),
                'error_code': UtteranceNotEnough.code,
                'parameters': {'intent_name': 'test_intent'}
            },
        )

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_labeled_entities_not_enough_should_raise(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            side_effect=Exception())

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [{
                "id": "test-resource_id1",
                "uid": "test_editor",
                "is_training": False,
                "training_nlu_id": "",
                "last_trained_time": 0,
                "last_infered_time": 1,
                "bot_id": ""
            }]
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [{'id': 'test_entity_id', 'name': 'test_entity'}],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_TRAIN_STATUS,
            'test_user_id',
            'test_bot_id',
            content={
                'status': 'failed',
                'bot_name': 'test_bot',
                'failed_log': TrainingFailedError.message.format(err=LabeledEntityNotEnough.message.format(entity_name='test_entity')),
                'error_code': LabeledEntityNotEnough.code,
                'parameters': {'entity_name': 'test_entity'}
            },
        )

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_should_call_nlu_service_expectly(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [
                {
                    "id": "test-resource_id1",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": ""
                }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_service_caller.call_nlu_service.assert_has_calls([
            call(NluFunction.CREATE, resource_id='test-resource_id1'),
            call(NluFunction.START_FETCH, nlu_id='test_nlu_id'),
        ])

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_should_call_notify_service_expectly(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [
                {
                    "id": "test-resource_id1",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": ""
                }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_TRAIN_STATUS,
            'test_user_id',
            'test_bot_id',
            content={'status': 'training', 'bot_name': 'test_bot'},
        )

    @patch('handler.train_handler.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.train_handler.ServiceCaller')
    @patch('handler.train_handler.SessionManager')
    @gen_test
    def test_post_should_call_db_service_expectly(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        train_handler = get_fake_handler(TrainHandler)
        train_handler = mock_check_user_permision(train_handler)
        get_nlu_resources_return_obj = {
            'data': [
                {
                    "id": "test-resource_id1",
                    "uid": "test_editor",
                    "is_training": False,
                    "training_nlu_id": "",
                    "last_trained_time": 0,
                    "last_infered_time": 0,
                    "bot_id": ""
                }
            ],
        }
        get_intents_fake_return_obj = {
            'data': [],
        }
        get_entities_fake_return_obj = {
            'data': [],
        }
        get_utterances_fake_return_obj = {
            'data': [],
        }
        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            get_nlu_resources_return_obj,
            get_intents_fake_return_obj,
            get_entities_fake_return_obj,
            get_utterances_fake_return_obj,
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        train_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        train_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield train_handler.post()

        db_querys = fake_session_manager.get_session().group_querys()

        self.assertEqual(len(db_querys), 7)
        get_nlu_resources_query, get_intent_query, get_entities_query, get_utterance_query, create_train_log_query, update_user_query, update_bot_query = db_querys
        self.assertDictEqual(
            get_nlu_resources_query['query']['kargs'],
            {'collection': 'nlu_resource'}
        )
        self.assertDictEqual(
            get_nlu_resources_query['filter_by']['kargs'],
            {'column': 'uid', 'value': 'test_user_id'}
        )
        self.assertDictEqual(
            get_intent_query['query']['kargs'],
            {'collection': 'intent'}
        )
        self.assertDictEqual(
            get_intent_query['filter_by']['kargs'],
            {'column': 'bot_id', 'value': 'test_bot_id'}
        )
        self.assertDictEqual(
            create_train_log_query['insert']['kargs'],
            {'collection': 'train_log'}
        )
        self.assertEqual(len(create_train_log_query['data']), 4)
        self.assertDictEqual(
            create_train_log_query['data'][0]['kargs'],
            {'column': 'bot_id', 'value': 'test_bot_id'}
        )
        self.assertDictEqual(
            create_train_log_query['data'][1]['kargs'],
            {'column': 'status', 'value': 'start'}
        )
        self.assertDictEqual(
            create_train_log_query['data'][2]['kargs'],
            {'column': 'intent_count', 'value': 0}
        )
        self.assertDictEqual(
            create_train_log_query['data'][3]['kargs'],
            {'column': 'utterance_count', 'value': 0}
        )

        self.assertDictEqual(
            create_train_log_query['insert']['kargs'],
            {'collection': 'train_log'}
        )

        self.assertEqual(len(update_bot_query['data']), 3)

        self.assertDictEqual(
            update_bot_query['data'][0]['kargs'],
            {'column': 'train_status', 'value': 'training'}
        )
        self.assertDictEqual(
            update_bot_query['data'][1]['kargs'],
            {'column': 'tmp_nlu_id', 'value': 'test_nlu_id'}
        )
        self.assertDictEqual(
            update_bot_query['data'][2]['kargs'],
            {'column': 'tmp_nlu_token', 'value': 'test_nlu_token_id'}
        )

    def _set_up_patch_mock_objs(self, fake_service_caller):
        fake_service_caller.call_nlu_service = MagicMock(
            return_value=get_fake_future({}))
