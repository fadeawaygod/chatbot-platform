
from unittest.mock import patch, MagicMock

from tornado.testing import AsyncTestCase, gen_test

from exception import (MissRequiredParameterError, UserNotFoundError)
from handler.sign_in_handler import SignInHandler
from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager)


class SignInHandlerHandlerTest(AsyncTestCase):
    @gen_test
    def test_post_missing_required_parameter_uid_should_raise(self):
        handler = get_fake_handler(SignInHandler)
        handler.json_args = {
        }

        with self.assertRaises(MissRequiredParameterError) as _:
            yield handler.post()

    @gen_test
    def test_post_missing_required_parameter_password_should_raise(self):
        handler = get_fake_handler(SignInHandler)
        handler.json_args = {
            "uid": "test-editor"
        }

        with self.assertRaises(MissRequiredParameterError) as _:
            yield handler.post()

    @gen_test
    def test_post_missing_required_parameter_client_id_should_raise(self):
        handler = get_fake_handler(SignInHandler)
        handler.json_args = {
            "uid": "test-editor",
            "password": "test"
        }

        with self.assertRaises(MissRequiredParameterError) as _:
            yield handler.post()

    @patch('handler.sign_in_handler.ServiceCaller')
    @patch('handler.sign_in_handler.SessionManager')
    @gen_test
    def test_post_user_not_found_should_raise(self, fake_session_manager, fake_service_caller):
        self._init_fake_service_caller(fake_service_caller)
        user_fake_return_obj = {
            'data': [],
        }
        db_fake_return_objs = [
            user_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(SignInHandler)
        handler.json_args = self._get_valid_request_body()

        with self.assertRaises(UserNotFoundError) as _:
            yield handler.post()

    def _init_fake_service_caller(self, fake_service_caller):
        fake_service_caller.call_auth_service = MagicMock(
            return_value=get_fake_future({"user": {"access_token": "test_access_token"}}))

    def _get_valid_request_body(self) -> dict:
        return {
            "uid": "test-editor",
            "password": "xxx",
            "client_id": "test_device_1",
        }


class FakeUUID:
    hex = 'test_uuid'
