import unittest
from unittest.mock import MagicMock, Mock, call, patch

from tornado.testing import AsyncTestCase, gen_test

import handler.nlu.nlu_models_handler as nlu_models_handler
from config.config import NLU_QUERY_URL
from handler.nlu.nlu_models_handler import NluModelsHandler
from handler.service.db import ORM
from service_caller import DbFunction, NluFunction, NotificationFunction
from tests.tools import get_fake_future, get_fake_handler


class NluModelsHandlerTest(AsyncTestCase):
    @patch('handler.nlu.nlu_models_handler.notify', MagicMock(return_value=get_fake_future({})))
    @patch('handler.nlu.nlu_models_handler.ServiceCaller')
    @patch('handler.nlu.nlu_models_handler.SessionManager')
    @gen_test
    def test_patch_data_fetched_should_call_nlu_service_with_train(self, mock_session_manager, mock_service_caller):
        self._set_up_service_caller_and_session_manager(
            mock_session_manager, mock_service_caller)

        fake_deployment_handler = get_fake_handler(NluModelsHandler)
        fake_deployment_handler.json_args = {
            'status': 'data_fetched',
        }
        fake_nlu_id = 'fake_nlu_id'
        yield fake_deployment_handler.patch(fake_nlu_id)
        mock_service_caller.call_nlu_service.assert_called_once_with(
            NluFunction.TRAIN,
            nlu_id='fake_nlu_id',
        )

    @patch('handler.nlu.nlu_models_handler.notify', MagicMock(return_value=get_fake_future({})))
    @patch('handler.nlu.nlu_models_handler.ServiceCaller')
    @patch('handler.nlu.nlu_models_handler.SessionManager')
    @gen_test
    def test_patch_trained_should_call_orm_excute_eight_times(self, mock_session_manager, mock_service_caller):

        fake_orm = self._set_up_service_caller_and_session_manager(
            mock_session_manager, mock_service_caller)
        fake_deployment_handler = get_fake_handler(NluModelsHandler)
        fake_deployment_handler.json_args = {
            'status': 'trained',
        }
        fake_nlu_id = 'fake_nlu_id'
        yield fake_deployment_handler.patch(fake_nlu_id)

        self.assertEqual(fake_orm.execute.call_count, 8)

    @patch('handler.nlu.nlu_models_handler.notify')
    @patch('handler.nlu.nlu_models_handler.ServiceCaller')
    @patch('handler.nlu.nlu_models_handler.SessionManager')
    @gen_test
    def test_patch_trained_should_call_notification_service_with_trained(self, mock_session_manager, mock_service_caller, mock_notify):

        self._set_up_service_caller_and_session_manager(
            mock_session_manager, mock_service_caller)
        mock_notify.return_value = get_fake_future({})

        fake_deployment_handler = get_fake_handler(NluModelsHandler)
        fake_deployment_handler.json_args = {
            'status': 'trained',
        }
        fake_nlu_id = 'fake_nlu_id'
        yield fake_deployment_handler.patch(fake_nlu_id)
        calls = [
            call(NotificationFunction.UPDATE_TRAIN_STATUS, 'test_owner', 'test_id', content={
                 'status': 'trained', 'bot_name': 'test_bot_name'}),
            call(NotificationFunction.UPDATE_TRAIN_STATUS, 'test_owner', 'test_id', content={
                 'status': 'untrained', 'bot_name': 'test_bot_name'}),
            call(NotificationFunction.UPDATE_DEPLOY_STATUS, 'test_owner', 'test_id', content={
                 'status': 'offline', 'bot_name': 'test_bot_name'}),
        ]

        mock_notify.assert_has_calls(calls)

    @patch('handler.nlu.nlu_models_handler.notify')
    @patch('handler.nlu.nlu_models_handler.ServiceCaller')
    @patch('handler.nlu.nlu_models_handler.SessionManager')
    @gen_test
    def test_patch_train_failed_should_call_notification_service_with_failed(self, mock_session_manager, mock_service_caller, mock_notify):
        self._set_up_service_caller_and_session_manager(
            mock_session_manager, mock_service_caller)
        mock_notify.return_value = get_fake_future({})

        fake_deployment_handler = get_fake_handler(NluModelsHandler)
        fake_deployment_handler.json_args = {
            'status': 'failed',
        }
        fake_nlu_id = 'fake_nlu_id'
        yield fake_deployment_handler.patch(fake_nlu_id)
        mock_notify.assert_called_once_with(
            NotificationFunction.UPDATE_TRAIN_STATUS, 'test_owner', 'test_id', content={'status': 'failed', 'bot_name': 'test_bot_name', 'failed_log': 'unknown error'})

    def _set_up_service_caller_and_session_manager(self, mock_session_manager, mock_service_caller):
        mock_service_caller.call_nlu_service = Mock(
            return_value=get_fake_future({}))
        mock_service_caller.call_notification_service = Mock(
            return_value=get_fake_future({}))
        mock_service_caller.call_bot_setup_service = Mock(
            return_value=get_fake_future({}))
        fake_orm = ORM(None)
        fake_orm.execute = Mock(return_value=get_fake_future(return_value={'data': [
            {
                'id': 'test_id',
                'owner': 'test_owner',
                'name': 'test_bot_name',
                'tmp_nlu_token': 'test_token',
                'tmp_nlu_id': 'nlu_id',
                'config': {'test': 'test'},
                'bot_id': 'test_bot',

            }]})
        )
        mock_session_manager.get_session = MagicMock(return_value=fake_orm)
        return fake_orm
