import unittest
from unittest.mock import MagicMock, call, patch

from tornado.testing import AsyncTestCase, gen_test

from handler.deployment_handler import DeploymentHandler
from tests.tools import (
    get_fake_handler, get_fake_future, mock_check_user_permision)


class DeploymentHandlerTest(AsyncTestCase):
    @patch('handler.deployment_handler.SessionManager')
    @gen_test
    def test_post_bot_id_invalid_should_raise(self, fake_session_manager):
        deployment_handler = get_fake_handler(DeploymentHandler)
        deployment_handler = mock_check_user_permision(
            deployment_handler, False)
        deployment_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        deployment_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        with self.assertRaises(Exception) as _:
            yield deployment_handler.post()

    @patch('handler.deployment_handler.set_up_agent')
    @patch('handler.deployment_handler.SessionManager')
    @gen_test
    def test_post_should_call_set_up_agent_expectly(self, fake_session_manager, fake_set_up_agent):
        fake_set_up_agent.return_value = get_fake_future({})
        deployment_handler = get_fake_handler(DeploymentHandler)
        deployment_handler = mock_check_user_permision(deployment_handler)
        deployment_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor']
        }
        deployment_handler.json_args = {
            'bot_id': 'test_bot_id',
        }
        yield deployment_handler.post()
        fake_set_up_agent.assert_called_with(
            {'id': 'test_bot_id', 'name': 'test_bot', 'owner': 'test_user_id', 'config': {}, 'train_status': 'untrained'}, {})
