import unittest

from handler.service.db import ORM


class ORMTest(unittest.TestCase):

    def setUp(self) -> None:
        super(ORMTest, self).setUp()
        self.orm = ORM(auth_manager=None)

    def test_query(self):
        self.orm.query(collection='test')
        self.assertDictEqual(self.orm.query_data, {
            'collection': 'test',
            'filter': {},
            'path': '{url}/data/query'
        })
        # clean
        self.orm.query_data = {}

    def test_insert(self):
        self.orm.insert(collection='test')
        self.assertDictEqual(self.orm.query_data, {
            'collection': 'test',
            'path': '{url}/data/insert'
        })
        # clean
        self.orm.query_data = {}

    def test_update(self):
        self.orm.update(collection='test')
        self.assertEqual(self.orm.query_data['collection'],  'test')
        self.assertEqual(self.orm.query_data['path'],  '{url}/data/update')
        # clean
        self.orm.query_data = {}

    def test_delete(self):
        self.orm.delete(collection='test')
        self.assertDictEqual(self.orm.query_data, {
            'collection': 'test',
            'path': '{url}/data/delete'
        })
        # clean
        self.orm.query_data = {}

    def test_filter_by(self):
        self.orm.filter_by(column='id', value='test')
        self.assertDictEqual(self.orm.query_data, {
            'filter': {
                'id': 'test'
            }
        })
        # clean
        self.orm.query_data = {}

    def test_projection_by(self):
        self.orm.projection_by(column='id', show=False)
        self.assertDictEqual(self.orm.query_data, {
            'projection': {
                'id': False
            }
        })
        # clean
        self.orm.query_data = {}

    def test_sort_by_asc(self):
        self.orm.sort_by(column='id', asc=True)
        self.assertDictEqual(self.orm.query_data, {
            'sort': {
                'id': 1
            }
        })
        # clean
        self.orm.query_data = {}

    def test_sort_by_desc(self):
        self.orm.sort_by(column='id', asc=False)
        self.assertDictEqual(self.orm.query_data, {
            'sort': {
                'id': -1
            }
        })
        # clean
        self.orm.query_data = {}

    def test_limit(self):
        self.orm.limit(limit=10)
        self.assertDictEqual(self.orm.query_data, {
            'limit': 10
        })
        # clean
        self.orm.query_data = {}

    def test_filter_gt(self):
        self.orm.filter_by(column='updated', value=1568625807123, operator='>')
        self.assertDictEqual(self.orm.query_data, {
            'filter': {
                'updated': {"$gt": 1568625807123}
            }
        })
        self.orm.query_data = {}

    def test_filter_lt(self):
        self.orm.filter_by(column='updated', value=1568625807123, operator='<')
        self.assertDictEqual(self.orm.query_data, {
            'filter': {
                'updated': {"$lt": 1568625807123}
            }
        })
        self.orm.query_data = {}

    def test_filter_gte(self):
        self.orm.filter_by(
            column='updated', value=1568625807000, operator='>=')
        self.assertDictEqual(self.orm.query_data, {
            'filter': {
                'updated': {"$gte": 1568625807000}
            }
        })
        self.orm.query_data = {}

    def test_filter_lte(self):
        self.orm.filter_by(
            column='updated', value=1568625807123, operator='<=')
        self.assertDictEqual(self.orm.query_data, {
            'filter': {
                'updated': {"$lte": 1568625807123}
            }
        })
        self.orm.query_data = {}

    def test_filter_gte_and_lte(self):
        self.orm.filter_by(column='updated', value=1568625807123, operator='>=')\
            .filter_by(column='updated', value=123456, operator='<=')
        self.assertDictEqual(self.orm.query_data, {
            'filter': {
                'updated': {"$lte": 123456, "$gte": 1568625807123}
            }
        })
        self.orm.query_data = {}
