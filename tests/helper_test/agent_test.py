import unittest
from unittest.mock import MagicMock, call, patch

from tornado.testing import AsyncTestCase, gen_test

from exception import AgentServiceError
from handler.helper.agent import set_up_agent
from handler.service.db import ORM
from service_caller import NotificationFunction
from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager,
                         mock_check_user_permision)


class AgentTest(AsyncTestCase):
    @patch('handler.helper.agent.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.helper.agent.ServiceCaller')
    @patch('handler.helper.agent.SessionManager')
    @gen_test
    def test_set_up_agent_call_agent_service_failed_should_notify_and_raise(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_bot_setup_service = MagicMock(
            side_effect=Exception())

        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [update_bot_return_obj]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        test_bot = {
            'id': 'test_bot_id',
            'name': 'test_bot',
            'config': {},
            'owner': 'test_user_id'
        }
        with self.assertRaises(AgentServiceError) as _:
            yield set_up_agent(test_bot, {})

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_DEPLOY_STATUS,
            'test_user_id',
            'test_bot_id',
            content={'status': 'failed', 'bot_name': 'test_bot',
                     'failed_log': 'Agent service error:'},
        )

    @patch('handler.helper.agent.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.helper.agent.ServiceCaller')
    @patch('handler.helper.agent.SessionManager')
    @gen_test
    def test_set_up_agent_should_call_bot_setup_service_expectly(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_bot_setup_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)

        test_bot = {
            'id': 'test_bot_id',
            'name': 'test_bot',
            'config': {},
            'owner': 'test_user_id'
        }
        yield set_up_agent(test_bot, {})

        fake_service_caller.call_bot_setup_service.assert_has_calls([
            call('test_bot_id', '{}'),
        ])

    @patch('handler.helper.agent.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.helper.agent.ServiceCaller')
    @patch('handler.helper.agent.SessionManager')
    @gen_test
    def test_set_up_agent_should_call_notify_service_expectly(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_bot_setup_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        intent_fake_return_obj = {
            'data': [{'id': 'test_intent_id'}],
        }
        insert_start_log_return_obj = {
            'status': 'ok'
        }
        insert_train_log_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            intent_fake_return_obj,
            insert_start_log_return_obj,
            insert_train_log_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)

        test_bot = {
            'id': 'test_bot_id',
            'name': 'test_bot',
            'config': {},
            'owner': 'test_user_id'
        }
        yield set_up_agent(test_bot, {})

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_DEPLOY_STATUS,
            'test_user_id',
            'test_bot_id',
            content={'status': 'ready', 'bot_name': 'test_bot'},
        )

    @patch('handler.helper.agent.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.helper.agent.ServiceCaller')
    @patch('handler.helper.agent.SessionManager')
    @gen_test
    def test_set_up_agent_without_config_history_data_should_call_db_service_expectly(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_bot_setup_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)

        test_bot = {
            'id': 'test_bot_id',
            'name': 'test_bot',
            'config': {},
            'owner': 'test_user_id'
        }
        yield set_up_agent(test_bot, {})

        db_querys = fake_session_manager.get_session().group_querys()

        self.assertEqual(len(db_querys), 1)
        update_bot_query = db_querys[0]

        self.assertDictEqual(
            update_bot_query['data']['kargs'],
            {'column': 'deploy_status', 'value': 'ready'}
        )

    @patch('handler.helper.agent.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.helper.agent.ServiceCaller')
    @patch('handler.helper.agent.SessionManager')
    @gen_test
    def test_set_up_agent_with_config_history_data_should_call_insert_a_row_to_config_history(
            self,
            fake_session_manager,
            fake_service_caller,
            fake_notify,
    ):

        self._set_up_patch_mock_objs(fake_service_caller)

        fake_service_caller.call_bot_setup_service = MagicMock(
            return_value=get_fake_future(return_value={'nlu_id': 'test_nlu_id', 'nlu_token_id': 'test_nlu_token_id', }))

        insert_config_history_return_obj = {
            'status': 'ok'
        }
        update_bot_return_obj = {
            'status': 'ok'
        }
        db_fake_return_objs = [
            insert_config_history_return_obj,
            update_bot_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)

        test_bot = {
            'id': 'test_bot_id',
            'name': 'test_bot',
            'config': {},
            'owner': 'test_user_id'
        }
        yield set_up_agent(test_bot, {'flow': {}, 'config': {}})

        db_querys = fake_session_manager.get_session().group_querys()

        self.assertEqual(len(db_querys), 2)
        insert_config_history = db_querys[0]
        self.assertEqual(
            insert_config_history['insert']['kargs'],
            {'collection': 'config_history'}
        )
        self.assertEqual(len(insert_config_history['data']), 3)

        self.assertDictEqual(
            insert_config_history['data'][0]['kargs'],
            {'column': 'bot_id', 'value': 'test_bot_id'}
        )
        self.assertDictEqual(
            insert_config_history['data'][1]['kargs'],
            {'column': 'flow', 'value': {}}
        )
        self.assertDictEqual(
            insert_config_history['data'][2]['kargs'],
            {'column': 'config', 'value': {}}
        )

    def _set_up_patch_mock_objs(self, fake_service_caller):
        fake_service_caller.call_bot_setup_service = MagicMock(
            return_value=get_fake_future({}))
