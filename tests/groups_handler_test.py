from unittest.mock import Mock, patch

from tornado.testing import AsyncTestCase, gen_test

from handler.groups_handler import GroupsHandler
from tests.tools import get_fake_future, get_fake_handler
from service_caller import DbFunction


class AgentHandlerTest(AsyncTestCase):
    @patch('handler.groups_handler.ServiceCaller')
    @gen_test
    def test_get_argument_didnt_contain_skip_and_limit_should_call_call_db_service_with_default(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_groups_handler = get_fake_handler(GroupsHandler)
        fake_groups_handler._user = {'role': ['admin']}
        fake_groups_handler.request.arguments = {}
        yield fake_groups_handler.get()

        dic_should_be_called = {
            'collection': 'group',
            'filter': {},
            'projection': {
                '_id': False,
                'id': True,
                'name': True,
                'updated': True
            },
            'skip': fake_groups_handler.DEFAULT_SKIP,
            'sort': {'updated': -1}
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.groups_handler.ServiceCaller')
    @gen_test
    def test_get_argument_didnt_contain_skip_should_call_call_db_service_with_sklp(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_groups_handler = get_fake_handler(GroupsHandler)
        fake_groups_handler._user = {'role': ['admin']}
        fake_groups_handler.request.arguments = {'skip': '1'}
        yield fake_groups_handler.get()

        dic_should_be_called = {
            'collection': 'group',
            'filter': {},
            'projection': {
                '_id': False,
                'id': True,
                'name': True,
                'updated': True
            },
            'skip': 1,
            'sort': {'updated': -1}
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.groups_handler.ServiceCaller')
    @gen_test
    def test_get_argument_didnt_contain_limit_shouldnt_call_call_db_service_with_limit(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_groups_handler = get_fake_handler(GroupsHandler)
        fake_groups_handler._user = {'role': ['admin']}
        fake_groups_handler.request.arguments = {'limit': '1'}
        yield fake_groups_handler.get()

        dic_should_be_called = {
            'collection': 'group',
            'filter': {},
            'limit': 1,
            'projection': {
                '_id': False,
                'id': True,
                'name': True,
                'updated': True
            },
            'skip': fake_groups_handler.DEFAULT_SKIP,
            'sort': {'updated': -1}
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.groups_handler.ServiceCaller')
    @gen_test
    def test_get_role_didnt_contain_admin_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_groups_handler = get_fake_handler(GroupsHandler)
        fake_groups_handler._user = {'role': ['editor']}
        fake_groups_handler.request.arguments = {'limit': '1'}
        with self.assertRaises(Exception) as context:
            yield fake_groups_handler.get()

        self.assertIsInstance(
            context.exception,
            Exception)
