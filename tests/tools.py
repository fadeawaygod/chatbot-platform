import asyncio
from typing import Dict, List
from unittest.mock import Mock


def get_fake_future(return_value: any) -> asyncio.Future:
    fake_future = asyncio.Future()
    fake_future.set_result(return_value)
    return fake_future


def get_fake_handler(class_type: 'handler.handler_base.HandlerBase') -> object:
    """fake class factory method of handler.handler_base.HandlerBase and its sub class

    Args:
        class_type ([type]): class of handler.handler_base.HandlerBase

    Returns:
        object: handler.handler_base.HandlerBase
    """
    fake_app = Mock()
    fake_app.ui_methods = {}
    fake_request = Mock()
    fake_request.headers = {}
    fake_request.body = b''
    fake_handler_base = class_type(fake_app, fake_request)
    fake_handler_base.finish = Mock()
    fake_handler_base.write = Mock()
    return fake_handler_base


def initialize_fake_session_manager(fake_session_manager: Mock, return_dicts: List[Dict]):
    def wrap_return_fake_session(function: str):
        def return_fake_session(*args, **kargs):
            if hasattr(fake_session, 'action_calls'):
                fake_session.action_calls.append(
                    {'function': function, 'args': [*args], 'kargs': {**kargs}})
            else:
                fake_session.action_calls = [
                    ({'function': function, 'args': [*args], 'kargs': {**kargs}})]

            return fake_session
        return return_fake_session

    def group_querys() -> list:
        grouped_querys = []
        current_query = {}
        for call in fake_session.method_calls:
            query_function = call[0]
            if query_function == 'execute':
                grouped_querys.append(current_query)
                current_query = {}
            else:
                query_args = call[1][0]
                if query_args['function'] in current_query:
                    if type(current_query[query_args['function']]) != list:
                        current_query[query_args['function']] = [
                            current_query[query_args['function']]]
                    current_query[query_args['function']].append(query_args)
                else:
                    current_query[query_args['function']] = query_args
        return grouped_querys

    fake_session = Mock()
    fake_session.group_querys = group_querys
    fake_session.query = wrap_return_fake_session('query')
    fake_session.insert = wrap_return_fake_session('insert')
    fake_session.update = wrap_return_fake_session('update')
    fake_session.delete = wrap_return_fake_session('delete')
    fake_session.count = wrap_return_fake_session('count')
    fake_session.data = wrap_return_fake_session('data')
    fake_session.distinct = wrap_return_fake_session('distinct')
    fake_session.filter_by = wrap_return_fake_session('filter_by')
    fake_session.projection_by = wrap_return_fake_session('projection_by')
    fake_session.sort_by = wrap_return_fake_session('sort_by')
    fake_session.skip = wrap_return_fake_session('skip')
    fake_session.limit = wrap_return_fake_session('limit')
    fake_session.execute = Mock(
        side_effect=[get_fake_future(return_dict)
                     for return_dict in return_dicts]
    )
    fake_session_manager.get_session = lambda: fake_session


def mock_check_user_permision(fake_handler: "HandlerBase", has_result=True, result_bot={}, exception=None) -> "HandlerBase":
    if has_result:
        if not result_bot:
            result_bot = {
                'id': 'test_bot_id',
                'name': 'test_bot',
                'owner': 'test_user_id',
                'config': {},
                'train_status': 'untrained',

            }
        fake_handler._check_user_permision_of_the_bot = Mock(
            return_value=get_fake_future(
                return_value=result_bot
            )
        )
    else:
        fake_handler.side_effect = Exception()
    return fake_handler
