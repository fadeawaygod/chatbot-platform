from unittest.mock import MagicMock, Mock, call, patch

from tornado.testing import AsyncTestCase, gen_test

from exception import NoPermissionError
from handler.entities_handler import EntitiesHandler
from handler.service.db import ORM
from service_caller import DbFunction, NotificationFunction
from tests.tools import get_fake_future, get_fake_handler


class EntitiesHandlerTest(AsyncTestCase):

    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_get_editor_arg_contain_bot_id_should_call_db_service_with_bot_id(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test_id'}]}))
        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        test_bot_id = 'test_bot_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {'bot_id': test_bot_id}
        fake_entities_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_entities_handler.get()
        dic_should_be_called = {
            'collection': fake_entities_handler.COLLECTION_ENTITY,
            'filter': {'bot_id': test_bot_id},
            'projection': {
                '_id': False,
                'id': True,
                'name': True,
                'entries': True,
                'prompts': True,
                'is_enabled': True,
                'is_sys': True,
            },
            'sort': {'updated': -1},
            'skip': 0
        }

        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_get_editor_arg_contain_skip_and_limit_should_call_db_service_with_skip_and_limit(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test_id'}]}))
        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        test_bot_id = 'test_bot_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {
            'bot_id': test_bot_id,
            'skip': '10',
            'limit': '20'
        }
        fake_entities_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_entities_handler.get()

        dic_should_be_called = {
            'collection': fake_entities_handler.COLLECTION_ENTITY,
            'filter': {'bot_id': test_bot_id},
            'projection': {
                '_id': False,
                'id': True,
                'name': True,
                'entries': True,
                'prompts': True,
                'is_enabled': True,
                'is_sys': True,
            },
            'sort': {'updated': -1},
            'skip': 10,
            'limit': 20
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_post_editor_should_call_db_service(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {'id': 'test_id'}}))
        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_entities_handler.json_args = {
            'name': 'test_name',
            'bot_id': 'test_bot_id'
        }
        yield fake_entities_handler.post()

        dic_should_be_called = {
            'collection': fake_entities_handler.COLLECTION_ENTITY,
            'data': {
                'name': 'test_name',
                'bot_id': 'test_bot_id',
                'entries': [],
                'prompts': [],
                'is_enabled': True,
                'is_sys': False,
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.INSERT, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id', 'is_nlu_data_modified': False}]})))
    @gen_test
    def test_post_is_nlu_data_modified_false_should_call_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {'id': 'test_id'}}))
        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_entities_handler.json_args = {
            'name': 'test_name',
            'bot_id': 'test_bot_id'
        }
        yield fake_entities_handler.post()

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_NLU_DATA,
            'test_user_id',
            'test_bot_id',
            content={'is_modified': True},
        )

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': [{'id': 'test_id', 'bot_id': 'test', 'owner': 'test_user_id', 'entities': [], 'utterances':[]}]})))
    @gen_test
    def test_delete_editor_should_call_db_service(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test_id', 'bot_id': 'test_bot_id', 'entities': []}]}))
        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_entity_id = 'test_entity_id'
        yield fake_entities_handler.delete(test_entity_id)

        calls = [
            call(DbFunction.QUERY, {'collection': 'entity', 'filter': {'id': 'test_entity_id'}, 'projection': {
                '_id': False, 'bot_id': True, 'id': True}}),

            call(DbFunction.DELETE, {
                'collection': fake_entities_handler.COLLECTION_ENTITY,
                'filter': {
                    'id': test_entity_id
                }
            }),
        ]

        mock_service_caller.call_db_service.assert_has_calls(calls)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': [{'id': 'test_id', 'bot_id': 'test', 'owner': 'test_user_id', 'entities': [], 'utterances':[], 'is_nlu_data_modified': False}]})))
    @gen_test
    def test_delete_is_nlu_data_modified_false_should_call_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test_id', 'bot_id': 'test_bot_id', 'entities': []}]}))
        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_entity_id = 'test_entity_id'
        yield fake_entities_handler.delete(test_entity_id)

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_NLU_DATA,
            'test_user_id',
            'test_bot_id',
            content={'is_modified': True},
        )

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_patch_editor_should_call_db_service(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'bot_id': 'test_id'}]}))

        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_entities_handler.json_args = {
            'name': 'test_name2'
        }
        test_entity_id = 'test_entity_id'
        yield fake_entities_handler.patch(test_entity_id)

        dic_should_be_called = {
            'collection': fake_entities_handler.COLLECTION_ENTITY,
            'filter': {
                'id': test_entity_id
            },
            'data': {
                'name': 'test_name2',
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.UPDATE, dic_should_be_called)

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id', 'is_nlu_data_modified': False}]})))
    @gen_test
    def test_patch_is_nlu_data_modified_false_should_call_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'bot_id': 'test_id'}]}))

        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_entities_handler.json_args = {
            'name': 'test_name2'
        }
        test_entity_id = 'test_entity_id'
        yield fake_entities_handler.patch(test_entity_id)

        fake_notify.assert_called_once_with(
            NotificationFunction.UPDATE_NLU_DATA,
            'test_user_id',
            'test_id',
            content={'is_modified': True},
        )

    @patch('handler.bot_handler_base.notify', return_value=get_fake_future(return_value={}))
    @patch('handler.entities_handler.ServiceCaller')
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute',
           MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id', 'is_nlu_data_modified': False}]})))
    @gen_test
    def test_patch_data_contain_only_prompts_should_not_call_notify(self, mock_service_caller, fake_notify):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'bot_id': 'test_id'}]}))

        fake_entities_handler = get_fake_handler(EntitiesHandler)
        test_user_id = 'test_user_id'
        fake_entities_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_entities_handler.json_args = {
            'prompts': []
        }
        test_entity_id = 'test_entity_id'
        yield fake_entities_handler.patch(test_entity_id)

        fake_notify.assert_not_called()
