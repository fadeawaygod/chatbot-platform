import unittest

from exception import PasswordFormatError
import handler.helper.password as p


class PasswordTest(unittest.TestCase):
    def test_validate_password_with_length_too_short_should_raise(self):
        input_password = 'x' * (p.MIN_PASSWORD_LENGTH - 1)
        with self.assertRaises(PasswordFormatError) as e:
            p.validate(input_password)
        self.assertEqual(
            e.exception.message,
            PasswordFormatError.message.format(err=p.PASSWORD_LENGTH_ERROR)
        )

    def test_validate_password_with_length_too_long_should_raise(self):
        input_password = 'x' * (p.MAX_PASSWORD_LENGTH + 1)
        with self.assertRaises(PasswordFormatError) as e:
            p.validate(input_password)
        self.assertEqual(
            e.exception.message,
            PasswordFormatError.message.format(err=p.PASSWORD_LENGTH_ERROR)
        )

    def test_validate_password_without_lowercase_letter_should_raise(self):
        input_password = 'AAAAA1'
        with self.assertRaises(PasswordFormatError) as e:
            p.validate(input_password)
        self.assertEqual(
            e.exception.message,
            PasswordFormatError.message.format(
                err=p.NOT_CONTAIN_LOWERCASE_LETTER_ERROR)
        )

    def test_validate_password_without_uppercase_letter_should_raise(self):
        input_password = 'aaaaa1'
        with self.assertRaises(PasswordFormatError) as e:
            p.validate(input_password)
        self.assertEqual(
            e.exception.message,
            PasswordFormatError.message.format(
                err=p.NOT_CONTAIN_UPPERCASE_LETTER_ERROR)
        )

    def test_validate_password_without_number_should_raise(self):
        input_password = 'aaaaaA'
        with self.assertRaises(PasswordFormatError) as e:
            p.validate(input_password)
        self.assertEqual(
            e.exception.message,
            PasswordFormatError.message.format(
                err=p.NOT_CONTAIN_NUMBER_ERROR)
        )

    def test_validate_password_contains_illegal_characters_should_raise(self):
        input_password = 'a<2a1A'
        with self.assertRaises(PasswordFormatError) as e:
            p.validate(input_password)
        self.assertEqual(
            e.exception.message,
            PasswordFormatError.message.format(
                err=p.NOT_VALID_CHAR_ERROR.format(sign='<'))
        )
