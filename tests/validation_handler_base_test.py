from unittest.mock import MagicMock, patch

from tornado.httpclient import HTTPClientError
from tornado.testing import AsyncTestCase, gen_test

from exception import TokenValidationError
from handler.validation_handler_base import ValidationHandlerBase
from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager)


class ValidationHandlerBaseTest(AsyncTestCase):
    @patch('handler.validation_handler_base.SessionManager')
    @gen_test
    def test_prepare_request_method_options_shouldnt_validate(self, fake_session_manager):
        user_fake_return_obj = {
            'data': [{'uid': 'test_user'}]}
        db_fake_return_objs = [user_fake_return_obj]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        validation_handler_base = get_fake_handler(ValidationHandlerBase)
        validation_handler_base.request.method = 'OPTIONS'
        yield validation_handler_base.prepare()
        self.assertFalse(hasattr(validation_handler_base, '_user_id'))
        self.assertFalse(hasattr(validation_handler_base, '_user_role'))

    @patch('handler.validation_handler_base.ServiceCaller')
    @patch('handler.validation_handler_base.SessionManager')
    @gen_test
    def test_prepare_request_method_get_header_didnt_conatain_Authorization_should_raise_TokenValidationError(self, fake_session_manager, fake_service_caller):
        self._init_fake_service_caller(fake_service_caller)
        user_fake_return_obj = {
            'data': [{'uid': 'test_user'}]}
        db_fake_return_objs = [user_fake_return_obj]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        validation_handler_base = get_fake_handler(ValidationHandlerBase)
        validation_handler_base.request.method = 'GET'
        with self.assertRaises(TokenValidationError) as context:
            yield validation_handler_base.prepare()

    @patch('handler.validation_handler_base.SessionManager')
    @gen_test
    def test_prepare_request_method_get_header_Authorization_value_not_start_with_bearer_should_raise_TokenValidationError(self, fake_session_manager):
        user_fake_return_obj = {
            'data': [{'uid': 'test_user'}]}
        db_fake_return_objs = [user_fake_return_obj]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        validation_handler_base = get_fake_handler(ValidationHandlerBase)
        validation_handler_base.request.method = 'GET'
        validation_handler_base.request.headers = {'Authorization': ''}
        with self.assertRaises(TokenValidationError) as context:
            yield validation_handler_base.prepare()

    @patch('handler.validation_handler_base.SessionManager')
    @gen_test
    def test_prepare_get_no_user_should_raise_TokenValidationError(self, fake_session_manager):
        user_fake_return_obj = {
            'data': []}
        db_fake_return_objs = [user_fake_return_obj]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        validation_handler_base = get_fake_handler(ValidationHandlerBase)
        validation_handler_base.request.method = 'GET'
        validation_handler_base.request.headers = {
            'Authorization': 'Bearer test_token'}

        with self.assertRaises(TokenValidationError) as _:
            yield validation_handler_base.prepare()

    @patch('handler.validation_handler_base.ServiceCaller')
    @patch('handler.validation_handler_base.SessionManager')
    @gen_test
    def test_prepare_request_method_get_valid_token_success_should_extract_info(self, fake_session_manager, fake_service_caller):
        self._init_fake_service_caller(fake_service_caller)
        test_user = {'uid': 'test_user'}
        user_fake_return_obj = {
            'data': [test_user]
        }
        db_fake_return_objs = [
            user_fake_return_obj]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        validation_handler_base = get_fake_handler(ValidationHandlerBase)
        validation_handler_base.request.method = 'GET'
        validation_handler_base.request.headers = {
            'Authorization': 'Bearer test_token'}
        yield validation_handler_base.prepare()
        self.assertDictEqual(validation_handler_base._user, test_user)

    def _init_fake_service_caller(self, fake_service_caller):
        fake_service_caller.call_auth_service = MagicMock(
            return_value=get_fake_future({"user": {"uid": "test_user_id"}}))
