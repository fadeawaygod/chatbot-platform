from unittest.mock import Mock, patch, call

from tornado.testing import AsyncTestCase, gen_test

from handler.nlu.nlu_utterances_handler import NluUtterancesHandler
from tests.tools import get_fake_future, get_fake_handler
from service_caller import DbFunction


class NluUtterancesHandlerTest(AsyncTestCase):
    @patch('handler.nlu.nlu_utterances_handler.NluHandlerBase._get_bot_id_by_nlu_id')
    @patch('handler.nlu.nlu_utterances_handler.ServiceCaller')
    @gen_test
    def test_get_should_call_db_query_service(self, mock_service_caller, mock_get_bot_id_by_nlu_id):
        """It should call db service for update bot status and nlu_id.
        """
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'tset_id', 'config': {}}]}))
        mock_get_bot_id_by_nlu_id.return_value = get_fake_future('test_bot_id')
        fake_handler = get_fake_handler(NluUtterancesHandler)
        test_request_arguments = {'intent_id': 'test_intent_id'}
        fake_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        fake_nlu_model_id = 'fake_nlu_model_id'
        yield fake_handler.get(fake_nlu_model_id)

        query_dic_should_be_called = {
            'collection': 'utterance',
            'filter': {'intent_ids': {'$in': ['test_intent_id']}},
            'projection': {
                '_id': False,
                'id': True,
                'text': True,
                'entities': True
            },
            'sort': {
                'updated': 1
            },
            'skip': 0
        }

        mock_service_caller.call_db_service.assert_called_once_with(
            DbFunction.QUERY, query_dic_should_be_called)

    @patch('handler.nlu.nlu_utterances_handler.NluHandlerBase._get_bot_id_by_nlu_id')
    @patch('handler.nlu.nlu_utterances_handler.ServiceCaller')
    @gen_test
    def test_get_show_len_true_should_call_db_query_service_twice(self, mock_service_caller, mock_get_bot_id_by_nlu_id):
        """It should call db service for update bot status and nlu_id.
        """
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({
                'data': [{'id': 'tset_id', 'config': {}}],
                'count': 10})
        )
        mock_get_bot_id_by_nlu_id.return_value = get_fake_future('test_bot_id')
        fake_handler = get_fake_handler(NluUtterancesHandler)
        test_request_arguments = {
            'intent_id': 'test_intent_id',
            'show_len': 'true'
        }
        fake_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        fake_nlu_model_id = 'fake_nlu_model_id'
        yield fake_handler.get(fake_nlu_model_id)

        query_dic_should_be_called = {
            'collection': 'utterance',
            'filter': {'intent_ids': {'$in': ['test_intent_id']}},
            'projection': {
                '_id': False,
                'id': True,
                'text': True,
                'entities': True
            },
            'sort': {
                'updated': 1
            },
            'skip': 0
        }

        count_dic_should_be_called = {
            'collection': 'utterance',
            'filter': {'intent_ids': {'$in': ['test_intent_id']}},
        }
        calls = [
            call(DbFunction.QUERY, query_dic_should_be_called),
            call(DbFunction.COUNT, count_dic_should_be_called)
        ]
        mock_service_caller.call_db_service.assert_has_calls(calls)
