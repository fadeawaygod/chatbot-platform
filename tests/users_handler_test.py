from unittest.mock import Mock, patch

from exception import NoPermissionError
from handler.users_handler import UsersHandler
from service_caller import DbFunction, AuthFunction
from tornado.testing import AsyncTestCase, gen_test

from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager)


class UsersHandlerTest(AsyncTestCase):
    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_get_admin_should_call_db_service_without_filter(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {'role': ['admin']}
        fake_users_handler.request.arguments = {}
        yield fake_users_handler.get()

        dic_should_be_called = {
            'collection': 'user',
            'filter': {},
            'projection': {
                '_id': False,
                'uid': True,
                'name': True,
                'group': True,
                'phone': True,
                'avatar_url': True,
                'mail_address': True,
                'role': True
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_get_group_admin_should_call_db_service_with_filter_group_id(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id',
            'uid': 'test_uid'
        }
        fake_users_handler.request.arguments = {}
        yield fake_users_handler.get()

        dic_should_be_called = {
            'collection': 'user',
            'filter': {'group': 'test_group_id'},
            'projection': {
                '_id': False,
                'uid': True,
                'name': True,
                'group': True,
                'phone': True,
                'avatar_url': True,
                'mail_address': True,
                'role': True
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_get_single_user_with_uid_should_call_db_service_with_filter_uid(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test_uid'}]}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {
            'role': ['editor'],
            'uid': 'test_uid',
            'group': 'test_group'
        }
        test_request_arguments = {}
        fake_users_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_users_handler.get('test_uid')
        dic_should_be_called = {
            'collection': 'user',
            'filter': {'uid': 'test_uid', 'group': 'test_group'},
            'projection': {
                '_id': False,
                'uid': True,
                'name': True,
                'group': True,
                'phone': True,
                'avatar_url': True,
                'mail_address': True,
                'role': True
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_get_editor_without_uid_should_call_db_service(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'test_uid'}]}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {
            'role': ['editor'],
            'uid': 'test_uid',
            'group': 'test_group'
        }
        test_request_arguments = {}
        fake_users_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        yield fake_users_handler.get('')
        dic_should_be_called = {
            'collection': 'user',
            'filter': {'group': 'test_group'},
            'projection': {
                '_id': False,
                'uid': True,
                'name': True,
                'group': True,
                'phone': True,
                'avatar_url': True,
                'mail_address': True,
                'role': True
            },
            'sort': {'updated': -1},
            'skip': 0
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.QUERY, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_post_group_admin_group_id_not_identical_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {
            'group': 'test',
            'role': ['test'],
            'uid': 'test_uid',
            'password': 'test_password',
            'mail_address': 'test@test.com'
        }
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        with self.assertRaises(Exception) as context:
            yield fake_users_handler.post()

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_post_group_admin_user_role_contain_admin_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {
            'group': 'test_group_id',
            'role': ['admin'],
            'uid': 'test_uid',
            'password': 'test_password',
            'mail_address': 'test@test.com'
        }
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        with self.assertRaises(Exception) as context:
            yield fake_users_handler.post()

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_post_group_admin_uid_collision_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'uid': 'test_uid'}]}))

        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {
            'group': 'test_group_id',
            'role': ['editor'],
            'uid': 'test_uid',
            'password': 'test_password'
        }
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id',
        }
        fake_users_handler.request.arguments = {}
        with self.assertRaises(Exception) as context:
            yield fake_users_handler.post()

        self.assertIsInstance(
            context.exception,
            Exception)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_post_editor_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {
            'group': 'test_group_id',
            'role': ['admin'],
            'uid': 'test_uid',
            'password': 'test_password',
            'mail_address': 'test@test.com'
        }
        fake_users_handler._user = {
            'role': ['editor'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        with self.assertRaises(Exception) as context:
            yield fake_users_handler.post()

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @patch('handler.users_handler.SessionManager')
    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_delete_admin_should_call_db_service_without_filter_group(self, mock_service_caller, fake_session_manager):
        insert_nlu_resources_fake_return_obj = {'status': "ok"}
        db_fake_return_objs = []
        for _ in range(UsersHandler.RESOURCE_COUNT):
            db_fake_return_objs.append(insert_nlu_resources_fake_return_obj)
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        mock_service_caller.call_auth_service = Mock(
            return_value=get_fake_future({'status': "ok"}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {'role': ['admin']}
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.delete(test_user_id)

        dic_should_be_called = {
            'collection': 'user',
            'filter': {
                'uid': 'test_user_id'
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.DELETE, dic_should_be_called)

    @patch('handler.users_handler.SessionManager')
    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_delete_group_admin_should_call_db_service_with_filter_group(self, mock_service_caller, fake_session_manager):
        insert_nlu_resources_fake_return_obj = {'status': "ok"}
        db_fake_return_objs = []
        for _ in range(UsersHandler.RESOURCE_COUNT):
            db_fake_return_objs.append(insert_nlu_resources_fake_return_obj)
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        mock_service_caller.call_auth_service = Mock(
            return_value=get_fake_future({'status': "ok"}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.delete(test_user_id)

        dic_should_be_called = {
            'collection': 'user',
            'filter': {
                'uid': 'test_user_id',
                'group': 'test_group_id'
            }
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.DELETE, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_delete_editor_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler._user = {
            'role': ['editor'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'

        with self.assertRaises(Exception) as context:
            yield fake_users_handler.delete(test_user_id)

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @patch('handler.users_handler.ServiceCaller')
    @patch('handler.users_handler.SessionManager')
    @gen_test
    def test_delete_editor_should_call_auth_service(self, fake_session_manager, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        mock_service_caller.call_auth_service = Mock(
            return_value=get_fake_future({'data': {}}))
        delete_nlu_resource_fake_return_obj = {'status': "ok"}
        db_fake_return_objs = [
            delete_nlu_resource_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {"enabled": True}
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.delete(test_user_id)

        mock_service_caller.call_auth_service.assert_called_once_with(
            AuthFunction.USERS,
            method='DELETE',
            rest_path='/test_user_id'
        )

    @patch('handler.users_handler.ServiceCaller')
    @patch('handler.users_handler.SessionManager')
    @gen_test
    def test_delete_editor_should_delete_nlu_resource(self, fake_session_manager, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        mock_service_caller.call_auth_service = Mock(
            return_value=get_fake_future({'data': {}}))
        delete_nlu_resource_fake_return_obj = {'status': "ok"}
        db_fake_return_objs = [
            delete_nlu_resource_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {"enabled": True}
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.delete(test_user_id)

        querys = fake_session_manager.get_session().group_querys()
        delete_user_query = querys[0]

        # last query is insert query
        self.assertDictEqual(
            delete_user_query['delete']['kargs'],
            {'collection': 'nlu_resource'}
        )
        self.assertDictEqual(
            delete_user_query['filter_by']['kargs'],
            {'column': 'uid', 'value': 'test_user_id'}
        )

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_patch_admin_should_call_db_service_without_filter_group(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {'name': 'test'}
        fake_users_handler._user = {'role': ['admin']}
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.patch(test_user_id)

        dic_should_be_called = {
            'collection': 'user',
            'filter': {'uid': test_user_id},
            'data': {'name': 'test'}
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.UPDATE, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_patch_group_admin_should_call_db_service_with_filter_group(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {'name': 'test'}
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.patch(test_user_id)

        dic_should_be_called = {
            'collection': 'user',
            'filter': {
                'uid': test_user_id,
                'group': 'test_group_id'
            },
            'data': {'name': 'test'}
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.UPDATE, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_patch_editor_should_call_db_service_with_filter_group(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {'name': 'test'}
        fake_users_handler._user = {
            'role': ['group_admin'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.patch(test_user_id)

        dic_should_be_called = {
            'collection': 'user',
            'filter': {
                'uid': test_user_id,
                'group': 'test_group_id'
            },
            'data': {'name': 'test'}
        }
        mock_service_caller.call_db_service.assert_called_with(
            DbFunction.UPDATE, dic_should_be_called)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_patch_editor_id_not_identical_should_raise(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {"phone": "0912123456"}
        fake_users_handler._user = {
            'uid': 'test_user_id_2',
            'role': ['editor'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        with self.assertRaises(Exception) as context:
            yield fake_users_handler.patch(test_user_id)

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_patch_editor_body_contains_password_should_call_auth_service(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        mock_service_caller.call_auth_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {"password": "test"}
        fake_users_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.patch(test_user_id)

        mock_service_caller.call_auth_service.assert_called_once_with(
            AuthFunction.USERS,
            body={'password': 'test'},
            method='PATCH',
            rest_path='/test_user_id'
        )

    @patch('handler.users_handler.ServiceCaller')
    @gen_test
    def test_patch_editor_body_contains_enabled_should_call_auth_service(self, mock_service_caller):
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': {}}))
        mock_service_caller.call_auth_service = Mock(
            return_value=get_fake_future({'data': {}}))
        fake_users_handler = get_fake_handler(UsersHandler)
        fake_users_handler.json_args = {"enabled": True}
        fake_users_handler._user = {
            'uid': 'test_user_id',
            'role': ['editor'],
            'group': 'test_group_id'
        }
        fake_users_handler.request.arguments = {}
        test_user_id = 'test_user_id'
        yield fake_users_handler.patch(test_user_id)

        mock_service_caller.call_auth_service.assert_called_once_with(
            AuthFunction.USERS,
            body={'enabled': True},
            method='PATCH',
            rest_path='/test_user_id'
        )
