
from unittest.mock import patch, MagicMock

from exception import (MissRequiredParameterError, MailNotExistError)
from handler.reset_password_mail_handler import ResetPasswordMailHandler
from tornado.testing import AsyncTestCase, gen_test

from tests.tools import (get_fake_future, get_fake_handler,
                         initialize_fake_session_manager)


class ResetPasswordMailHandlerTest(AsyncTestCase):
    @gen_test
    def test_post_missing_required_parameter_should_raise(self):
        handler = get_fake_handler(ResetPasswordMailHandler)
        handler.json_args = {
        }
        with self.assertRaises(MissRequiredParameterError) as _:
            yield handler.post()

    @patch('handler.reset_password_mail_handler.SessionManager')
    @gen_test
    def test_post_mail_not_exist_should_raise(self, fake_session_manager):
        user_fake_return_obj = {
            'data': [
            ],
        }
        db_fake_return_objs = [
            user_fake_return_obj,
        ]
        initialize_fake_session_manager(
            fake_session_manager, db_fake_return_objs)
        handler = get_fake_handler(ResetPasswordMailHandler)
        handler.json_args = self._get_valid_request_body()
        with self.assertRaises(MailNotExistError) as _:
            yield handler.post()

    def _get_valid_request_body(self) -> dict:
        return {
            "mail_address": "test@test.com"
        }
