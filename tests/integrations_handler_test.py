from unittest.mock import Mock, patch, call, MagicMock

from tornado.testing import AsyncTestCase, gen_test

from handler.integrations_handler import IntegrationsHandler
from handler.service.db import ORM
from tests.tools import get_fake_future, get_fake_handler
from exception import NoPermissionError, MissRequiredParameterError


class IntegrationsHandlerTest(AsyncTestCase):

    @gen_test
    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': None})))
    def test_get_not_editor_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['user']
        }
        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.get()

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @gen_test
    def test_get_lack_necessary_parameter_should_raise(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.get()

        self.assertIsInstance(
            context.exception,
            Exception)
        self.assertEqual(
            context.exception.args[0],
            fake_integrations_handler.ERROR_MISSING_REQUIRED_PARARMETER.format(
                parameter='bot_id')
        )

    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': None})))
    @gen_test
    def test_get_user_not_own_the_bot_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        test_request_arguments = {'bot_id': 'fake_bot_id'}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.get()

    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': None})))
    @gen_test
    def test_post_not_editor_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['user']
        }
        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.post()

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @gen_test
    def test_post_lack_necessary_parameter_should_raise(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_integrations_handler.json_args = {}
        test_request_arguments = {}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.post()

        self.assertIsInstance(
            context.exception,
            MissRequiredParameterError)

    @patch('handler.service.session_manager.SessionManager.get_session', MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': None})))
    @gen_test
    def test_post_user_not_own_the_bot_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_integrations_handler.json_args = {
            'bot_id': 'test_bot_id',
            'passage': 'web',
            'credential': {},
        }
        test_request_arguments = {'bot_id': 'fake_bot_id'}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.post()

    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch.object(ORM, 'execute', MagicMock(return_value=get_fake_future(return_value={'data': [{'bot_id': 'test', 'owner': 'test_user_id'}]})))
    @gen_test
    def test_post_integration_already_exist_should_raise(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_integrations_handler.json_args = {
            'bot_id': 'test_bot_id',
            'passage': 'web',
            'credential': {},
        }
        test_request_arguments = {'bot_id': 'fake_bot_id'}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.post()

        self.assertIsInstance(
            context.exception,
            Exception)

        self.assertEqual(
            context.exception.args[0],
            fake_integrations_handler.ERROR_INTEGRATION_EXIST
        )

    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': None})))
    @gen_test
    def test_delete_user_not_own_the_integration_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_integrations_handler.json_args = {
            'bot_id': 'test_bot_id',
            'passage': 'web',
            'credential': {},
        }
        test_integration_id = 'test_integration_id'
        test_request_arguments = {'bot_id': 'fake_bot_id'}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.delete(test_integration_id)

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @gen_test
    def test_delete_not_editor_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['user']
        }
        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.delete('')

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @gen_test
    def test_patch_not_editor_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['user']
        }
        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.patch('')

        self.assertIsInstance(
            context.exception,
            NoPermissionError)

    @patch('handler.service.session_manager.SessionManager.get_session',
           MagicMock(return_value=ORM(None)))
    @patch('handler.service.db.ORM.execute', MagicMock(return_value=get_fake_future(return_value={'data': None})))
    @gen_test
    def test_patch_user_not_own_the_integration_should_raise_NoPermissionError(self):
        fake_integrations_handler = get_fake_handler(IntegrationsHandler)
        test_user_id = 'test_user_id'
        fake_integrations_handler._user = {
            'uid': test_user_id,
            'role': ['editor']
        }
        fake_integrations_handler.json_args = {
            'bot_id': 'test_bot_id',
            'passage': 'web',
            'credential': {},
        }
        test_integration_id = 'test_integration_id'
        test_request_arguments = {'bot_id': 'fake_bot_id'}
        fake_integrations_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)

        with self.assertRaises(Exception) as context:
            yield fake_integrations_handler.patch(test_integration_id)

        self.assertIsInstance(
            context.exception,
            NoPermissionError)
