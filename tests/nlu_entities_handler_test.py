from unittest.mock import Mock, patch

from tornado.testing import AsyncTestCase, gen_test

from handler.nlu.nlu_entities_handler import NluEntitiesHandler
from tests.tools import get_fake_future, get_fake_handler
from service_caller import DbFunction


class NluEntitiesHandlerTest(AsyncTestCase):
    @patch('handler.nlu.nlu_entities_handler.NluHandlerBase._get_bot_id_by_nlu_id')
    @patch('handler.nlu.nlu_entities_handler.ServiceCaller')
    @gen_test
    def test_get_should_call_db_query_service(self, mock_service_caller, mock_get_bot_id_by_nlu_id):
        """It should call db service for update bot status and nlu_id.
        """
        mock_service_caller.call_db_service = Mock(
            return_value=get_fake_future({'data': [{'id': 'tset_id', 'config': {}}]}))
        mock_get_bot_id_by_nlu_id.return_value = get_fake_future('test_bot_id')
        fake_handler = get_fake_handler(NluEntitiesHandler)
        test_request_arguments = {}
        fake_handler.get_argument = lambda s, default: test_request_arguments.get(
            s, default)
        fake_nlu_model_id = 'fake_nlu_model_id'
        yield fake_handler.get(fake_nlu_model_id)

        query_dic_should_be_called = {
            'collection': 'entity',
            'filter': {'bot_id': 'test_bot_id'},
            'projection': {
                '_id': False,
                'id': True,
                'name': True,
                'entries': True
            },
            'sort': {
                'updated': 1
            },
            'skip': 0
        }

        mock_service_caller.call_db_service.assert_called_once_with(
            DbFunction.QUERY, query_dic_should_be_called)
