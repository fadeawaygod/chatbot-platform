from exception import MissRequiredParameterError
from handler.reset_password_handler import ResetPasswordHandler
from tornado.testing import AsyncTestCase, gen_test

from tests.tools import get_fake_handler


class ResetPasswordHandlerTest(AsyncTestCase):
    @gen_test
    def test_post_missing_required_parameter_should_raise(self):
        handler = get_fake_handler(ResetPasswordHandler)
        handler.json_args = {
        }
        with self.assertRaises(MissRequiredParameterError) as _:
            yield handler.post()
