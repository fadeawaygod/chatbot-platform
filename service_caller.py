import json
import time
import urllib.request
import uuid
from enum import Enum
from typing import Union

import tornado.httpclient
from tornado.httputil import url_concat

import handler.service.session_manager
import http_status_code
from config.config import (APP_ID, APP_SECRET, AUTH_SERVICE_URL, CHATBOT_AGENT_HOST,
                           COLLECTION_PREFIX, DB_SERVICE_DOMAIN,
                           DB_SERVICE_HTTP_PROTOCOL, FB_APP_ID, FB_APP_SECRET,
                           FB_GRAPH_API_URL, MAIL_SERVICE_URL, NLU_NER_TOKEN,
                           NLU_NER_URL, NLU_QUERY_URL, NLU_SERVICE_MODEL_TYPE,
                           NLU_SERVICE_URL, NLU_SERVICE_USER,
                           NOTIFICATION_SERVICE_URL)
from exception import FBAccessTokenError, UnknownFBError
from handler.helper.signature import generate_signature
from sr_toolkit import fetch_helper


async def fetch(url: str, method: str, body: Union[str, bytes, None] = None, headers: dict = None):
    try:
        code, body = await fetch_helper.fetch_async(method=method, url=url, body=body, headers=headers)
        return code, body
    except tornado.httpclient.HTTPClientError as e:
        return e.code, e.message
    except Exception as e:
        raise e


def fetch_sync(url: str, method: str, body: str = '', headers: dict = None):
    try:
        code, body = fetch_helper.fetch_sync(
            url=url, method=method, body=body, headers=headers)
        return code, body
    except tornado.httpclient.HTTPClientError as e:
        return e.code, e.message
    except Exception as e:
        raise e


def raise_e_with_function_name_async(func):
    async def wrapper(*args, **kwargs):
        try:
            result = await func(*args, **kwargs)
        except Exception as e:
            e.function = func.__name__
            raise e
        return result
    return wrapper


class DbFunction(Enum):
    INSERT = 'insert'
    UPDATE = 'update'
    UPDATE_MANY = 'updateMany'
    QUERY = 'query'
    DELETE = 'delete'
    DELETE_MANY = 'deleteMany'
    COUNT = 'count'


class NluFunction(Enum):
    CREATE = 'create'
    START_FETCH = 'start_fetch'
    TRAIN = 'train'


class NotificationFunction(Enum):
    UPDATE_DEPLOY_STATUS = 'bot_deploy_status'
    UPDATE_TRAIN_STATUS = 'bot_train_status'
    UPDATE_NLU_DATA = 'update_nlu_data'
    BOT_AUTH = 'bot_auth'
    BOT_EVENT = 'bot_event'


class AuthFunction(Enum):
    SIGN_IN = 'signin'
    SIGN_OUT = 'signout'
    SIGN_UP = 'signup'
    TOKEN_VALIDATION = 'token_validation'
    ACTIVATION_MAIL = 'activation_mail'
    USER_ACTIVATION = 'user_activation'
    RESET_PASSWORD_MAIL = 'reset_password_mail'
    RESET_PASSWORD = 'reset_password'
    USERS = 'users'


AUTH_ERROR_CODE_EX_MAPPING = {
    80000: 40000,
    80001: 50003,
    80002: 50009,
    80003: 50010,
    80004: 50012,
    80005: 50005,
    80006: 50013,
    80007: 50014,
    80008: 50015,
    80009: 50016,
    80010: 50017,
    80011: 50018,
    80012: 50019,
    80013: 50020,
    80014: 50021,
}


class ServiceCaller:
    ERROR_SYSTEM_SIGN_IN_FAIL = 'system login failed:{err}'
    ERROR_SERVICE_RESULT_HTTP_STATUS_IS_NOT_200_OK = 'query {service_name} service fail:{err}'
    SYSTEM_LOGIN_URL = '{protocol}://{domain}/system/auth/signIn'.format(
        protocol=DB_SERVICE_HTTP_PROTOCOL,
        domain=DB_SERVICE_DOMAIN)
    SYSTEM_USER_VALIDATION_URL = '{protocol}://{domain}/system/user/validateUser'.format(
        protocol=DB_SERVICE_HTTP_PROTOCOL,
        domain=DB_SERVICE_DOMAIN)
    DB_SERVICE_URL = '{protocol}://{domain}/data/{{function}}'.format(
        protocol=DB_SERVICE_HTTP_PROTOCOL,
        domain=DB_SERVICE_DOMAIN)
    BOT_SERVICE_URL_TEMPLATE = '{domain}/agent/{{bot_id}}'.format(
        domain=CHATBOT_AGENT_HOST)
    NOTIFICATION_SERVICE_URL = NOTIFICATION_SERVICE_URL

    _db_access_token = ''

    @classmethod
    @raise_e_with_function_name_async
    async def call_system_user_validation_service(cls, system_user_token: str) -> dict:
        async def get_request_obj(system_user_token):
            header = {
                'content-type': 'application/json',
                'Authorization': cls._get_db_access_token()
            }
            body = {"system_user_token": system_user_token}

            return tornado.httpclient.HTTPRequest(
                cls.SYSTEM_USER_VALIDATION_URL,
                "POST",
                header,
                json.dumps(body, ensure_ascii=False)
            )

        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(await get_request_obj(system_user_token))
        body_json = cls._extract_service_result('db_user_validation', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_db_service(cls, function: DbFunction, body: dict) -> dict:
        async def get_request_obj(function, body):
            header = {
                'content-type': 'application/json',
                'Authorization': cls._get_db_access_token()
            }

            return tornado.httpclient.HTTPRequest(
                cls.DB_SERVICE_URL.format(function=function.name),
                "POST",
                header,
                json.dumps(body, ensure_ascii=False)
            )

        def add_collection_prefix(body: dict) -> dict:
            body['collection'] = f'{COLLECTION_PREFIX}{body["collection"]}'
            return body

        body = add_collection_prefix(body)
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(await get_request_obj(function, body), raise_error=False)
        body_json = cls._extract_service_result('db', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_nlu_service(cls, function: NluFunction, resource_id='', nlu_id='') -> dict:
        def get_request_obj(url, body, method):
            header = {
                'content-type': 'application/json'
            }
            return tornado.httpclient.HTTPRequest(
                url,
                method,
                header,
                json.dumps(body, ensure_ascii=False)
            )

        url = NLU_SERVICE_URL
        if function == NluFunction.CREATE:
            body = {
                "resource_id": resource_id,
                "nlu_service_type": NLU_SERVICE_MODEL_TYPE
            }
            method = 'POST'
        elif function == NluFunction.START_FETCH:
            body = {
                'method_status': 'fetch_data',
                'nlu_id': nlu_id,
                "nlu_service_type": NLU_SERVICE_MODEL_TYPE
            }
            method = 'PATCH'
        elif function == NluFunction.TRAIN:
            body = {
                'method_status': 'train_model',
                'nlu_id': nlu_id,
                "nlu_service_type": NLU_SERVICE_MODEL_TYPE
            }
            method = 'PATCH'

        body['user_id'] = NLU_SERVICE_USER

        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(get_request_obj(url, body, method), raise_error=False)
        body_json = cls._extract_service_result('nlu', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_nlu_query_service(
        cls,
        nlu_token: str,
        query_string: str,
        threshold=-1,
        user_id='',
        bot_id='',
        msg_id='',
        pinyin=0,

    ) -> dict:
        def get_request_obj(url: str, method: str, nlu_token: str):
            header = {
                'content-type': 'application/json',
                'token': nlu_token
            }
            return tornado.httpclient.HTTPRequest(
                url,
                method,
                header
            )

        parameters = {
            'q': query_string,
            'pinyin': pinyin
        }
        if threshold >= 0:
            parameters['threshold'] = threshold
        if user_id:
            parameters['user_id'] = user_id
        if bot_id:
            parameters['bot_id'] = bot_id
        if msg_id:
            parameters['msgid'] = msg_id
        url = url_concat(NLU_QUERY_URL, parameters)
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(get_request_obj(url, 'GET', nlu_token), raise_error=False)
        body_json = cls._extract_service_result('nlu_query', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_nlu_ner_service(cls, docs: list) -> dict:
        def get_request_obj(url, body, token):
            header = {
                'content-type': 'application/json',
                'token': token
            }
            return tornado.httpclient.HTTPRequest(
                url,
                'POST',
                header,
                json.dumps(body, ensure_ascii=False)
            )

        body = {
            "docs": docs,
            "debug": "0"
        }
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(get_request_obj(NLU_NER_URL, body, NLU_NER_TOKEN), raise_error=False)
        body_json = cls._extract_service_result('nlu_ner', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_nlu_entry_list_service(cls, entity_name: str) -> dict:
        def get_request_obj(url, token):
            header = {
                'token': token
            }
            return tornado.httpclient.HTTPRequest(
                url,
                'GET',
                header,
            )

        parameters = {'entity_name': entity_name}
        url = url_concat(NLU_NER_URL, parameters)
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(get_request_obj(url, NLU_NER_TOKEN), raise_error=False)
        body_json = cls._extract_service_result('nlu_ner', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_bot_setup_service(cls, bot_id: str, config: str) -> dict:
        def get_request_obj(bot_id, config):
            header = {'Content-Type': 'application/json'}

            return tornado.httpclient.HTTPRequest(
                cls.BOT_SERVICE_URL_TEMPLATE.format(bot_id=bot_id),
                "POST",
                header,
                config
            )

        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(get_request_obj(bot_id, config), raise_error=False)

        if response.code != 200:
            try:
                error_message = json.loads(response.body.decode(
                    'utf-8')).get('message', 'unknown error')
            except Exception as _:
                error_message = f'unknown error:{ response.body.decode("utf-8")}'

            raise Exception(
                cls.ERROR_SERVICE_RESULT_HTTP_STATUS_IS_NOT_200_OK.format(
                    service_name='bot_setup_service',
                    err=error_message))

    @classmethod
    @raise_e_with_function_name_async
    async def call_cahtbot_nlu_get_api_service(cls, nlu_id: str, item: str, url_parameters={}) -> dict:
        """this function call chatbotPlatform API for mock nlu services use.
        Returns:
            dict: [description]
        """
        async def get_request_obj(nlu_id, item, url_parameters):
            header = {
                'content-type': 'application/json',
                'Authorization': cls._get_db_access_token()
            }
            CHATBOT_SERVICE_NLU_URL_TEMPLATE = 'http://127.0.0.1:5000/api/v1/editor/nlu_models/{nlu_id}/{item}'
            url = CHATBOT_SERVICE_NLU_URL_TEMPLATE.format(
                nlu_id=nlu_id,
                item=item
            )
            if url_parameters:
                url += '?'
                url += urllib.parse.urlencode(url_parameters)

            return tornado.httpclient.HTTPRequest(
                url,
                "GET",
                header
            )

        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(await get_request_obj(nlu_id, item, url_parameters), raise_error=False)
        body_json = cls._extract_service_result('chatbot api nlu', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_cahtbot_nlu_patch_api_service(cls, nlu_id: str, status: str) -> dict:
        """this function call chatbotPlatform API for mock nlu services use.
        status should be one of the following:data_fetched, trained or failed
        Returns:
            dict: [description]
        """
        async def get_request_obj(nlu_id, status):
            header = {
                'content-type': 'application/json',
                'Authorization': cls._get_db_access_token()
            }
            CHATBOT_SERVICE_NLU_URL_TEMPLATE = 'http://127.0.0.1:5000/api/v1/editor/nlu_models/{nlu_id}'
            url = CHATBOT_SERVICE_NLU_URL_TEMPLATE.format(
                nlu_id=nlu_id)

            return tornado.httpclient.HTTPRequest(
                url,
                "PATCH",
                header,
                json.dumps({'status': status}, ensure_ascii=False)
            )

        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(await get_request_obj(nlu_id, status), raise_error=False)
        body_json = cls._extract_service_result('chatbot api nlu', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_notification_service(cls, function: NotificationFunction, user_id: str, bot_id: str, content: dict) -> dict:
        http_client = tornado.httpclient.AsyncHTTPClient()
        headers = {
            'content-type': 'application/json',
            'X-Silkrode-Application-Id': 'silkrode-chatbot',
            'X-Silkrode-API-Key': 'Silkrode-API-Key',
        }
        body = {
            'event': function.value,
            'user_id': user_id,
            'agent_id': bot_id,
            'content': content
        }
        response = await http_client.fetch(
            request=cls.NOTIFICATION_SERVICE_URL,
            method='POST', headers=headers, body=json.dumps(body, ensure_ascii=False), raise_error=False)
        body_json = cls._extract_service_result(
            'notification service', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_auth_service(cls, function: AuthFunction, body=None, rest_path="", method='POST') -> dict:
        timestamp = int(time.time())
        nonce = uuid.uuid4().hex
        signature = generate_signature(
            body,
            timestamp,
            nonce,
            APP_SECRET
        )
        headers = {
            'content-type': 'application/json',
            'App-ID': APP_ID,
            'Timestamp': str(timestamp),
            'Nonce': nonce,
            'Signature': signature,
        }
        url = f"{AUTH_SERVICE_URL}/{function.value}"
        if rest_path:
            url += rest_path
        if body is not None:
            body = json.dumps(body, ensure_ascii=False)
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(
            request=url,
            method=method,
            headers=headers,
            body=body,
            raise_error=False)
        try:
            response_body_json = cls._extract_service_result(
                'auth service', response, True)
        except Exception as e:
            if e.code in AUTH_ERROR_CODE_EX_MAPPING:
                e.code = AUTH_ERROR_CODE_EX_MAPPING[e.code]
            if e.code == 50009:
                e.status_code = http_status_code.UNAUTHORIZED
            raise e

        return response_body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_mail_service(cls, mail_address: str, name: str, subject: str, content: dict) -> dict:
        http_client = tornado.httpclient.AsyncHTTPClient()
        headers = {
            'content-type': 'application/json',
            'Authorization': 'Bearer null',
        }
        body = {
            "receivers": [
                {
                    "email": mail_address,
                    "name": name
                }
            ],
            "subject": subject,
            "content": content
        }
        response = await http_client.fetch(
            request=MAIL_SERVICE_URL,
            method='POST', headers=headers, body=json.dumps(body, ensure_ascii=False), raise_error=False)
        body_json = cls._extract_service_result(
            'mail service', response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_fb_exchange_user_access_token(cls, user_access_token: str) -> dict:
        url = "{}/oauth/access_token".format(FB_GRAPH_API_URL)
        parameters = {
            "grant_type": "fb_exchange_token",
            "client_id": FB_APP_ID,
            "client_secret": FB_APP_SECRET,
            "fb_exchange_token": user_access_token
        }
        url = url_concat(url, parameters)
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(
            tornado.httpclient.HTTPRequest(
                url,
                "GET",
                {}
            ),
            raise_error=False)
        body_json = cls._extract_fb_service_result(response)
        return body_json

    @classmethod
    @raise_e_with_function_name_async
    async def call_fb_get_page_access_token(cls, user_access_token: str, page_id: str) -> dict:
        url = "{doamin}/{page_id}".format(
            doamin=FB_GRAPH_API_URL,
            page_id=page_id)
        parameters = {
            "fields": "access_token",
            "access_token": user_access_token
        }
        url = url_concat(url, parameters)
        http_client = tornado.httpclient.AsyncHTTPClient()
        response = await http_client.fetch(
            tornado.httpclient.HTTPRequest(
                url,
                "GET",
                {}
            ),
            raise_error=False)
        body_json = cls._extract_fb_service_result(response)
        return body_json

    @classmethod
    def _get_db_access_token(cls):
        if not cls._db_access_token:
            cls._db_access_token = f'Bearer {ServiceCaller._get_system_user_token()}'
        return cls._db_access_token

    @classmethod
    def _get_system_user_token(cls) -> str:
        try:
            session = handler.service.session_manager.SessionManager.get_session()
            access_token = session.auth_manager.access_token
        except Exception as e:
            raise Exception(cls.ERROR_SYSTEM_SIGN_IN_FAIL.format(err=e))
        return access_token

    @classmethod
    def _extract_service_result(cls, service_name: str, response: tornado.httpclient.HTTPResponse, bypass_status_code=False) -> dict:
        body = response.body.decode('utf-8')
        if response.code != 200:
            err_message = body
            error_code = 0
            try:
                body_json = json.loads(body)
                err_message = body_json['error']
                error_code = body_json['error_code']
            except:
                pass
            ex = Exception(
                cls.ERROR_SERVICE_RESULT_HTTP_STATUS_IS_NOT_200_OK.format(
                    service_name=service_name,
                    err=err_message))
            ex.code = error_code
            ex.status_code = response.code if bypass_status_code else http_status_code.INTERNAL_ERROR

            raise ex
        body_json = json.loads(body)
        if service_name[:3] != 'nlu' and body_json['status'].lower() != 'ok':
            raise Exception(
                cls.ERROR_SERVICE_RESULT_HTTP_STATUS_IS_NOT_200_OK.format(
                    service_name=service_name,
                    err=body_json))

        return body_json

    @classmethod
    def _extract_fb_service_result(cls, response: tornado.httpclient.HTTPResponse) -> dict:
        body = response.body.decode('utf-8')
        body_json = json.loads(body)
        if response.code != 200:
            error = body_json['error']
            if error['code'] == 190:
                raise FBAccessTokenError()
            raise UnknownFBError(error=error["message"])

        return body_json
