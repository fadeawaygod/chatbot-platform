import asyncio
import json
from typing import Optional

import aioredis
import aioredis.errors

LOCK = asyncio.Lock()


class ConnectionInterrupted(Exception):
    def __init__(self, connection, parent=None):
        self.connection = connection
        self.parent = parent

    def __str__(self):
        error_type = "ConnectionInterrupted"
        error_msg = "An error occurred while connecting to redis"

        if self.parent:
            error_type = self.parent.__class__.__name__
            error_msg = str(self.parent)

        return "Redis %s: %s" % (error_type, error_msg)


class RedisManager:

    def __init__(self, host: str, min_pool_size: int = 3, max_pool_size: int = 100):
        data = host.split(':')
        self.host = data[0]
        self.port = int(data[1]) if len(data) > 1 else 6379
        self._pool = None
        self.count = 0
        self._min_pool_size = min_pool_size
        self._max_pool_size = max_pool_size

    async def init_connection(self):
        self._pool = await aioredis.create_redis_pool(address=f'redis://{self.host}:{self.port}',
                                                      encoding='utf-8',
                                                      minsize=self._min_pool_size,
                                                      maxsize=self._max_pool_size)

    async def get_instant(self) -> aioredis.Redis:
        self.count += 1
        async with LOCK:
            if not self._pool:
                await self.init_connection()
        return await self._pool

    async def ping(self):
        try:
            with await self.get_instant() as redis:
                ping = await redis.ping()
                return True if ping == 'PONG' else False
        except Exception as e:
            return False

    async def set(self, key: str, value, ttl: int = 600, retry: int = 0):
        with await self.get_instant() as redis:
            try:
                if value is None:
                    await redis.delete(key)
                elif ttl and ttl > 0:
                    await redis.setex(key=key, seconds=ttl, value=value)
                else:
                    await redis.set(key=key, value=value)
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise e
                self._pool = None
                return await self.set(key, value, ttl, retry=retry+1)

    async def get(self, key: str, retry: int = 0):
        with await self.get_instant() as redis:
            try:
                return await redis.get(key)
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise e
                self._pool = None
                return await self.get(key, retry=retry+1)

    async def delete(self, key: str):
        with await self.get_instant() as redis:
            await redis.delete(key)

    async def push(self, key: str, value, retry: int = 0):
        with await self.get_instant() as redis:
            try:
                await redis.rpush(key, value)
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise e
                self._pool = None
                return await self.push(key, value, retry=retry+1)

    async def get_list(self, key: str, retry: int = 0):
        with await self.get_instant() as redis:
            try:
                return await redis.lrange(key, 0, -1)
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise e
                self._pool = None
                return await self.get_list(key, retry=retry+1)

    async def get_length(self, key: str) -> int:
        with await self.get_instant() as redis:
            return await redis.llen(key=key)

    async def pop(self, key):
        with await self.get_instant() as redis:
            return await redis.lpop(key=key)

    async def lget(self, key, index: int = 0):
        with await self.get_instant() as redis:
            return await redis.lindex(key=key, index=index)

    async def reset_list(self, key: str, retry: int = 0):
        with await self.get_instant() as redis:
            try:
                await redis.delete(key)
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise e
                self._pool = None
                return await self.reset_list(key, retry=retry+1)

    async def flush_all(self, retry: int = 0):
        with await self.get_instant() as redis:
            try:
                await redis.flushall()
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise e
                self._pool = None
                return await self.flush_all(retry=retry+1)

    async def flush_by_prefix(self, prefix: str, count: int = None, retry: int = 0):
        with await self.get_instant() as redis:
            kwargs = {'match': prefix, }
            if count:
                kwargs['count'] = count
                count = 0
            try:
                count = 0
                async for key in redis.iscan(**kwargs):
                    await redis.delete(key)
                    count += 1
                return count
            except aioredis.errors.RedisError as e:
                if retry > 3:
                    raise ConnectionInterrupted(connection=redis, parent=e)
                self._pool = None
                return await self.flush_by_prefix(prefix, count, retry=retry+1)


class RedisPersistence(RedisManager):

    STATUS_OPEN = 'open'
    STATUS_BUSY = 'busy'

    def __init__(self, host: str, ttl: int = 600):
        super(RedisPersistence, self).__init__(host)
        self.ttl = ttl

    @staticmethod
    def _get_key_for_queue(session_id: str) -> str:
        return f'{session_id}_message_queue'

    @staticmethod
    def _get_available_key(session_id: str) -> str:
        return f'{session_id}_is_available'

    async def put_message_into_queue(self, session_id: str, data: str):
        key = self._get_key_for_queue(session_id=session_id)
        await self.push(key=key, value=data)

    async def is_available(self, session_id: str) -> bool:
        key = self._get_available_key(session_id=session_id)
        result = await self.get(key=key)
        if result is None:
            return True
        if result == self.STATUS_OPEN:
            return True
        elif result == self.STATUS_BUSY:
            return False
        raise ValueError(f'Undefined status of availability. session_id: {session_id}, status: {result}')

    async def set_session_status(self, session_id: str, is_available: bool = True):
        key = self._get_available_key(session_id=session_id)
        value = self.STATUS_OPEN if is_available is True else self.STATUS_BUSY
        await self.set(key=key, value=value, ttl=self.ttl)

    async def get_queue_length(self, session_id: str) -> int:
        key = self._get_key_for_queue(session_id=session_id)
        return await self.get_length(key=key)

    async def pop_message_from_queue(self, session_id) -> Optional[dict]:
        key = self._get_key_for_queue(session_id=session_id)
        data = await self.pop(key=key)
        if data:
            return json.loads(data, encoding='utf-8')

    async def get_message_from_queue(self, session_id) -> Optional[dict]:
        key = self._get_key_for_queue(session_id=session_id)
        data = await self.lget(key=key, index=0)
        if data:
            return json.loads(data, encoding='utf-8')
