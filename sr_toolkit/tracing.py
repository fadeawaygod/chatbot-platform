import traceback
import urllib.parse
import urllib.request

import tornado.httpclient

try:
    import opentracing
    from opentracing.propagation import Format
    from opentracing.ext import tags
    is_opentracing_installed = True
except ImportError:
    print(traceback.format_exc())
    is_opentracing_installed = False


from .singleton import Singleton


class TornadoTracer(Singleton):

    tracer = None

    @classmethod
    def is_opentracing_installed(cls) -> bool:
        return is_opentracing_installed

    def init_tornado_tracing(self, name: str, reporting_host: str, probabilistic: int = 1):
        import tornado_opentracing
        tracer = self.init_jaeger_tracer(name=name, reporting_host=reporting_host, probabilistic=probabilistic)
        tornado_opentracing.init_tracing()
        return tornado_opentracing.TornadoTracing(tracer=tracer)

    def init_jaeger_tracer(self, name: str, reporting_host: str, probabilistic: int = 1):
        """
        Initial this before tornado application created.
        """
        assert self.is_opentracing_installed(), 'Opentracing is not found.'
        try:
            from tornado_opentracing.scope_managers import TornadoScopeManager
            from jaeger_client import Config
        except ImportError:
            raise
        probabilistic = 1 if probabilistic >= 1 else probabilistic
        config = Config(
            config={  # usually read from some yaml config
                'sampler': {
                    'type': 'probabilistic',
                    'param': probabilistic,
                },
                'local_agent': {
                    'reporting_host': reporting_host,
                },
            },
            service_name=name,
            validate=True,
            scope_manager=TornadoScopeManager()
        )
        # Create your opentracing tracer using TornadoScopeManager for active Span handling.
        self.tracer = config.initialize_tracer()
        return self.tracer

    def new_scope(self, name: str, child_of=None):
        tracer = self.tracer
        assert tracer, 'Undefined tracer.'
        parent_span = tracer.active_span if child_of is None else child_of
        return tracer.start_active_span(name, child_of=parent_span)

    @classmethod
    def _set_up_trace_tags(cls, span, method: str, url: str):
        span.set_tag(tags.SPAN_KIND, tags.SPAN_KIND_RPC_CLIENT)
        span.set_tag(tags.COMPONENT, 'tornado.httpclient')
        span.set_tag(tags.HTTP_METHOD, method.lower())
        span.set_tag(tags.HTTP_URL, url)
        return span

    def _set_up_trace_scope(self, method: str, url: str, headers: dict):
        tracer = self.tracer
        assert tracer, 'Undefined tracer.'
        parsed_url = urllib.parse.urlparse(url=url)
        scope = self.new_scope(name='request_to_{}'.format(parsed_url.netloc), child_of=tracer.active_span)
        self._set_up_trace_tags(span=scope.span, method=method, url=url)
        try:
            tracer.inject(scope.span.context, Format.HTTP_HEADERS, headers)
        except opentracing.UnsupportedFormatException:
            pass
        return scope

    @classmethod
    async def _fetch_async(cls, method: str, url: str, **kwargs):
        try:
            client = tornado.httpclient.AsyncHTTPClient()
            response = await client.fetch(request=url, method=method, **kwargs)
            return response
        except tornado.httpclient.HTTPClientError as e:
            return e.response
        except Exception:
            raise

    def fetch_sync(self, url: str, method: str, body: str = '', headers: dict = None) -> (int, str):
        scope = None
        if self.tracer:
            headers = headers or {}
            scope = self._set_up_trace_scope(method=method, url=url, headers=headers)
        try:
            req = urllib.request.Request(
                url, headers=headers, data=body.encode(), method=method)
            with urllib.request.urlopen(req) as response:
                contents = response.read()
            contents = contents.decode('utf-8')
            if scope:
                scope.span.set_tag(tags.HTTP_STATUS_CODE, response.code)
            if response.code != 200:
                return response.code, response.body.decode('utf-8')
            return response.code, contents
        except Exception:
            raise
        finally:
            if scope:
                scope.span.finish()

    async def fetch_async(self, method: str, url: str, **kwargs):
        response = None
        scope, span = None, None
        if self.tracer:
            headers = kwargs.setdefault('headers', {})
            scope = self._set_up_trace_scope(method=method, url=url, headers=headers)
            span = scope.span
        try:
            response = await self._fetch_async(method=method, url=url, **kwargs)
            return response
        except Exception:
            if span:
                span.set_tag(tags.ERROR, True)
                span.set_tag('error.object', traceback.format_exc())
            raise
        finally:
            if scope:
                if response:
                    span.set_tag(tags.HTTP_STATUS_CODE, response.code)
                scope.span.finish()
