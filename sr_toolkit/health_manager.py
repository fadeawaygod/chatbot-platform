import enum
import inspect
import traceback

import tornado.web

from .singleton import Singleton


class Status(enum.Enum):

    HEALTHY = 'OK'
    ILL = 'COMPONENT_FAILED'
    FAILED = 'FAILED'


class HealthManager(Singleton):

    _components = {}

    def _get_components(self):
        return self._components

    def _set_components(self, components: dict):
        self._components = components

    def _get_component(self, name: str):
        return self._get_components().get(name)

    def add_component(self, name: str, ping_method):
        """
        Builder pattern
        """
        components = self._get_components()
        assert name not in components, f'Duplicate service_name({name}).'
        components[name] = ping_method
        self._set_components(components=components)
        return self

    def remove_component(self, name: str):
        """
        Builder pattern
        """
        components = self._get_components()
        assert name in components, f'undefined service_name({name})'
        components.pop(name)
        self._set_components(components=components)
        return self

    def _get_ping_method(self, name: str):
        ping_method = self._get_component(name=name)
        if hasattr(ping_method, 'ping'):
            cls = ping_method
            return getattr(cls, 'ping')
        return ping_method

    async def _execute_ping_method(self, name: str) -> (bool, str):
        """
        :name: component name
        :return is_success, reason
        """
        ping_method = self._get_ping_method(name=name)
        if not ping_method:
            return False, f'undefined component({name})'
        try:
            if inspect.iscoroutinefunction(ping_method):
                ping_result = await ping_method()
            elif inspect.isfunction(ping_method):
                ping_result = ping_method()
            else:
                return False, f'ping method is undefined in service({name}).'
            is_success = True if ping_result is True else False
            if is_success is True:
                return True, ''
            return False, f'{name} is not ready!'
        except Exception as e:
            return False, f'error: {type(e)}, {str(e)},{traceback.format_exc()}'

    async def gen_report(self):
        report = {'status': Status.HEALTHY.value, 'component': {}}
        try:
            components = self._get_components()
            for component_name in components.keys():
                is_success, reason = await self._execute_ping_method(name=component_name)
                report['component'][component_name] = str(is_success)
                if is_success is False:
                    reasons = report.get('reason', '')
                    report['reason'] = reasons + f'{reason}\n'
            for component_name, is_success in report.get('component').items():
                if is_success == 'False':
                    report['status'] = Status.ILL.value
                    break
        except Exception as e:
            report['status'] = Status.FAILED.value
            reasons = report.get('reason', '')
            report['reason'] = reasons + f'error: {type(e)}, {str(e)},{traceback.format_exc()}.'
        return report


class HealthHandler(tornado.web.RequestHandler):

    def set_default_headers(self) -> None:
        def set_header_for_CORS():
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Access-Control-Allow-Headers",
                            "x-requested-with, content-type, Authorization")
            self.set_header('Access-Control-Allow-Methods',
                            'OPTIONS, POST, GET, PUT, DELETE, PATCH')

        set_header_for_CORS()

    def options(self):
        self.set_status(204)
        self.finish()

    async def get(self):
        result = await HealthManager.get_instance().gen_report()
        self.set_status(200)
        self.write(result)
