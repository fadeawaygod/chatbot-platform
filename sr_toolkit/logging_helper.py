import os
import json
import logging
import logging.handlers
from typing import Union


default_logger_name = 'agent-service'


logging.basicConfig(
    format='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
)


class LoggingPayload:

    def __init__(self, reason: str = None, exception: Union[str, Exception] = None):
        self.data = dict()
        self.data['exceptions'] = []
        self.give_reason(reason=reason)
        self.add_exception(exception=exception)

    def give_reason(self, reason: str):
        self.data['reason'] = reason
        return self

    def add_exception(self, exception: Union[str, Exception] = None):
        if exception:
            e = str(exception) if isinstance(exception, Exception) else exception
            assert isinstance(self.data.get('exceptions'), list), 'the field exceptions has been rewritten.'
            self.data['exceptions'].append(e)
        return self

    def add_field(self, name: str, data):
        """
        :param name: field name
        :param data: field data
        :return: self
        """
        self.data[name] = data
        return self

    def get(self) -> str:
        """
        :return: json string of log payload
        """
        return json.dumps(self.data, ensure_ascii=False)


def get_logger(level: str = 'DEBUG', name: str = None, file_path: str = '.', file_name: str = 'agent-server', enable_log_to_file: bool = True):
    logger = logging.getLogger(f'{default_logger_name}-{name}' or default_logger_name)
    if enable_log_to_file:
        file_name = os.path.join(file_path, f'{file_name}.log')
        filehandler = logging.handlers.TimedRotatingFileHandler(file_name,
                                                                when="midnight",
                                                                interval=1,
                                                                backupCount=10,
                                                                encoding="utf-8")
        formatter = logging.Formatter(fmt='%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
        filehandler.setFormatter(fmt=formatter)
        filehandler.suffix = "%Y-%m-%d.log"
        logger.addHandler(filehandler)
    logger.setLevel(level=level)
    return logger
