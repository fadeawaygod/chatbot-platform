import os
from ..client import ConfigClient
import dotenv

env_file_versions = {
    "local": ".env",
    "dev": ".env",
    "staging": ".env.stag",
    "stag": ".env.stag",
    "prod": ".env.prod",
    "production": ".env.prod",
    "example": ".env.example",
}


class ConfigPatchEnv(ConfigClient):
    def __init__(self, tenant=None, env_file=None):
        super().__init__()

        # loading config from .env file
        if env_file is None:
            env_file = ".env" if tenant is None else env_file_versions.get(
                tenant, ".env")
        dotenv.load_dotenv(dotenv.find_dotenv(filename=env_file))

        # update config to obj container
        self.common_config.update(dict(os.environ))

    def __getattr__(self, key: str) -> str:
        return os.getenv(key, '')
