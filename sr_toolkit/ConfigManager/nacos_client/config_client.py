import json
import requests


from ..client import ConfigClient

nacos_url = "{server}?tenant={tenant}&dataId={data_id}&group={group}"
nacos_versions = {
    "local": "local",
    "dev": "dev",
    "develop": "dev",
    "staging": "staging",
    "stag": "staging",
    "prod": "production",
    "production": "production",
}


class ConfigPatchNacos(ConfigClient):
    def __init__(self, tenant: str, data_id: str, group: str, server: str):
        super().__init__()

        # confirm var is valid.
        if not all(map(lambda i: True if isinstance(i, str) else False, [tenant, data_id, group, server])):
            raise TypeError(f"[tenant, data_id, group, server] need `str` type"
                            f" `None`: [{tenant}, {data_id}, {group}, {server}] ")

        # connect from nacos
        tenant = nacos_versions.get(tenant, tenant)
        config_server_url = nacos_url.format(server=server, tenant=tenant, data_id=data_id, group=group)
        responds = requests.get(url=config_server_url)

        # updated config
        try:
            self.common_config = responds.json()
            assert isinstance(self.common_config, dict), "Can't get Server config"
        except AssertionError as e:
            raise e
        except (AssertionError, json.decoder.JSONDecodeError) as e:
            raise AssertionError("{},Error Config: {},{}".format(type(e),responds,responds.text))
        except Exception as e:
            raise e

        # add default var
        self.common_config.update({
            "ConfigServerUrl": config_server_url
        })


