import unittest
from unittest import mock
from . import load_config, SOURCE_ENV, SOURCE_CLOUD


class MyTestCase(unittest.TestCase):
    Mock_server = "http://nacos-server:port/nacos/v1/cs/configs"
    Mock_tenant = "production"
    Mock_data_id = "file_id.json"
    Mock_group = "group-id"
    Mock_json = {"SAMPLE": Mock_server}

    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    @mock.patch('requests.get', return_value=MockResponse(Mock_json, 200))
    def test_ConfigPatch(self, _):

        a = load_config(source=SOURCE_CLOUD,server=self.Mock_server,tenant=self.Mock_tenant, data_id=self.Mock_data_id, group=self.Mock_data_id)
        self.assertEqual(f"{self.Mock_server}?tenant={self.Mock_tenant}&dataId={self.Mock_data_id}&group={self.Mock_data_id}",a.key("ConfigServerUrl"))
        self.assertEqual(f"{self.Mock_server}?tenant={self.Mock_tenant}&dataId={self.Mock_data_id}&group={self.Mock_data_id}",a.ConfigServerUrl)

        #
        self.assertEqual(a.json(),self.Mock_json)


        #
        for k,v in self.Mock_json.items():
            self.assertIn(k, a.json())
        self.assertEqual(self.Mock_server, a.select(key="SAMPLE"))

        #
        try:
            a.select(key="non-exist")
        except Exception as e:
            self.assertIsInstance(e,KeyError)

    @mock.patch('requests.get', return_value=MockResponse(Mock_json, 200))
    def test_GetAutoAttr(self, _):
        a = load_config(source=SOURCE_CLOUD, server=self.Mock_server, tenant=self.Mock_tenant,
                        data_id=self.Mock_data_id, group=self.Mock_data_id)
        self.assertEqual(self.Mock_server, a.SAMPLE)
        self.assertEqual(self.Mock_server, a.key("SAMPLE"))
        self.assertEqual(self.Mock_server, a.select("SAMPLE"))

    def test_GetENV(self):
        import os
        os.environ["testArgs"] = "testArgs"

        a = load_config(source=SOURCE_ENV, tenant="example")
        self.assertEqual(a.testArgs, "testArgs")
        self.assertEqual(a.key("testArgs"), "testArgs")
        self.assertEqual(a.select("testArgs"), "testArgs")
        v = a.select(key='MISSING_ARGS', raise_exception=False)
        assert v is None, 'MISSING_ARGS should be None.'

    def test_GetEnv_OnlyEnv(self):
        import os
        os.environ["testArgs"] = "testArgs"

        a = load_config(source='only_env', tenant="example")
        self.assertEqual(a.testArgs, "testArgs")
        self.assertEqual(a.key("testArgs"), "testArgs")
        self.assertEqual(a.select("testArgs"), "testArgs")
        v = a.select(key='MISSING_ARGS', raise_exception=False)
        assert v is None, 'MISSING_ARGS should be None.'

    def test_GetENV_FromFile(self):
        a = load_config(source=SOURCE_ENV, env_file=".env.example")
        self.assertEqual(a.testArgs, "testArgs")


if __name__ == '__main__':
    unittest.main()
