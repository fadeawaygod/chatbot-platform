## import
```
from sr_toolkit.ConfigManager import load_config,SOURCE_ENV,SOURCE_CLOUD
```


## from Nacos
```
config_source = os.getenv("CONFIG_SOURCE", SOURCE_CLOUD)
tenant = os.getenv("tenant", "local")
nacos_server_url = os.getenv("nacos_server", "http://10.205.48.80:8848/nacos/v1/cs/configs")
nacos_server_data_id = os.getenv("nacos_server_data_id", "chatbot-platform.json")
nacos_server_group = os.getenv("nacos_server_group", "chatbot")

CONFIG = load_config(
    source=config_source,
    server=nacos_server_url,
    tenant=tenant,
    data_id=nacos_server_data_id,
    group=nacos_server_group)

# GET ARGS: LISTEN_PORT
LISTEN_PORT = CONFIG.PORT
```

## from Env file
```
CONFIG = load_config(source=SOURCE_ENV, env_file='.env')
# or
CONFIG = load_config(source=SOURCE_ENV, tenant='dev')

# GET ARGS: LISTEN_PORT
LISTEN_PORT = CONFIG.PORT
```


