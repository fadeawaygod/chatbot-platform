from typing import Union

from .client import ConfigClient
from .env_client import ConfigPatchEnv
from .nacos_client.config_client import ConfigPatchNacos

SOURCE_CLOUD = "nacos"
SOURCE_ENV = "local"
SOURCE_LOCAL = "local"

ENV_DEVELOP = "dev"
ENV_PRODUCTION = "production"
ENV_STAGING = "staging"


def load_config(source: str, tenant: Union[str, None] = None, data_id: Union[str, None] = None,
                group: Union[str, None] = None, server: Union[str, None] = None,
                env_file: Union[str, None] = None) -> ConfigClient:
    """
    :param source: `local` or `nacos`
    :param tenant: version: `dev`,`staging`,`production`
    :param data_id: ex. chatbot.json
    :param group: ex. chatbot
    :param server: nacos server url
    :param env_file: default None, if exist and source is `local`, load from the file.
    :return:
    """
    if source in ('local', 'only_env'):
        if env_file is not None:
            return ConfigPatchEnv(env_file=env_file)
        return ConfigPatchEnv(tenant=tenant)
    elif source == 'nacos':
        return ConfigPatchNacos(tenant, data_id, group, server)
    else:
        raise NotImplementedError(f"{source}")
