import os
import json


class ConfigClient:
    def __init__(self):
        self.common_config = dict()

        # add multi name function
        self.key = self.select

    def __getattr__(self, key):
        try:
            # if local var or function exist
            return object.__getattribute__(self, key)
        except Exception as e:

            # if key in env var
            val = os.getenv(key, None)
            if val is not None:
                self.common_config[key] = val
                return val

            # if key in config var
            val = self.select(key=key,default=None)
            if val is not None:
                return val

            # not find var
            raise e

    def __str__(self):
        return json.dumps(self.common_config)

    def json(self):
        return dict(self.common_config)

    def text(self):
        return self.__str__()

    def select(self, key: str, default=None, raise_exception: bool = False):
        """
        find the key.
        :param key: key name
        :param raise_exception:if the key isn't exist,throw a exception.
        :param default:if the key isn't exist,return the var.
        """
        if raise_exception:
            if key not in self.common_config:
                raise KeyError(f"Config Missing: {key}")
            return self.common_config.get(key)
        else:
            return self.common_config.get(key, default)

