import typing

import tornado.web
import prometheus_client

from .health_manager import HealthManager, Status


class PrometheusHealthManager:

    __MEMO = {}

    def __init__(self):
        self._prometheus_client = None

    @classmethod
    def get_instance(cls, name: str):
        if name not in cls.__MEMO:
            self = cls()
            self.init_prometheus_client(name=name)
            cls.__MEMO[name] = self
        return cls.__MEMO[name]

    def init_prometheus_client(self, name: str):
        info = prometheus_client.Info(name, 'health-manager')
        self._prometheus_client = info

    def gen_report(self, report: dict):
        self._prometheus_client.info(val={'status': report.get('status'), **report.get('component')})
        return prometheus_client.generate_latest(registry=self._prometheus_client)


class MetricsHandler(tornado.web.RequestHandler):

    @classmethod
    def name(cls, name: str):
        cls.__client_name = name
        return cls

    def data_received(self, chunk: bytes) -> typing.Optional[typing.Awaitable[None]]:
        pass

    async def get(self):
        report = await HealthManager.get_instance().gen_report()
        response = PrometheusHealthManager.get_instance(name=self.__client_name).gen_report(report=report)
        status_code = 200 if report.get('status') == Status.HEALTHY.value else 400
        self.set_status(status_code)
        self.write(response)
