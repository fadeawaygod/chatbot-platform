from .tracing import TornadoTracer


async def fetch_async(method: str, url: str, **kwargs) -> (int, str):
    response = await TornadoTracer.get_instance().fetch_async(method, url, **kwargs)
    return response.code, response.body.decode(encoding='utf-8')


def fetch_sync(url: str, method: str, body: str = '', headers: dict = None):
    return TornadoTracer.get_instance().fetch_sync(url=url, method=method, body=body, headers=headers)
