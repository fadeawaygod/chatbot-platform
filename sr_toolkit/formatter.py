class FormatWrapper:
    def __init__(self, v):
        self.v = v

    def __format__(self, spec):
        return '{{{}{}}}'.format(self.v, (':' + spec) if spec else '')

    def __getitem__(self, key):
        return FormatWrapper('{}[{}]'.format(self.v, key))

    def __getattr__(self, attr):
        return FormatWrapper('{}.{}'.format(self.v, attr))


class MissingDict(dict):
    def __missing__(self, key):
        return FormatWrapper(key)


def safe_format(target, data: dict):
    if isinstance(target, dict):
        target = format_dict(target=target, data=data)
    elif isinstance(target, list):
        target = format_list(target=target, data=data)
    elif isinstance(target, str):
        target = format_str(text=target, data=data)
    return target


def flatten_dict(d, sep="_"):
    import collections

    obj = collections.OrderedDict()

    def recurse(t, parent_key=""):
        if isinstance(t, list):
            obj[parent_key] = t
            for i in range(len(t)):
                recurse(t[i], parent_key + sep + str(i) if parent_key else str(i))
        elif isinstance(t, dict):

            if parent_key is not "":
                obj[parent_key] = t

            for k, v in t.items():
                recurse(v, parent_key + sep + k if parent_key else k)
        else:
            obj[parent_key] = t

    recurse(d)

    return dict(obj)


def convert_template(dict_format):
    import re
    targets = re.findall(r'(\{[^\}]*\})', dict_format)
    for target in targets:
        dict_format = dict_format.replace(target, target.replace('[', '_').replace(']', ''))

    return dict_format


def format_str(text: str, data: dict) -> str:

    from collections import defaultdict

    try:
        if not text:
            return ''
        d = MissingDict(data or {})
        return convert_template(text).format_map(defaultdict(str, flatten_dict(d, "_")))
    except Exception as e:
        print(e)
        print("format_str# error:{}".format(e))
        return text


def format_list(target: list, data: dict) -> list:
    for k, v in enumerate(target):
        target[k] = safe_format(target=v, data=data)
    return target


def format_dict(target: dict, data: dict) -> dict:
    for k, v in target.items():
        target[k] = safe_format(target=v, data=data)
    return target


def test_format_dict():
    print("[test] format dict >>>>>>>>>")
    mydata1 = {"data": {"intent": "intent_hello", "entities":{"model": "AX12", "數量": 123}}}
    mydata2 = {"data": {"intent": "intent_hello", "entities":{"數量": 123}}}
    result = safe_format('intent: {data} entities[0]: {data[entities[0]]}', mydata1)
    print(result)
    print("[test] format dict <<<<<<<<")


def test_format_list():
    print("[test] format list >>>>>>>>>")
    data1 = {"response": ["item-1", "item-2", "item-3"]}
    data1["response"].append("item-4")
    print(data1["response"][3])
    data2 = {"response": [{"item":"item-1"}, {"item":"item-2"}, {"item": "item-3"}]}
    data3 = {"response": {"intent": "intent_hello", 'entities': {'產品型號': ['RAx700', 'AX12'], "model": "RX123"}, "response": [{"item":"item-1"}, {"item":"item-2"}, {"item": "item-3"}]}}

    print(flatten_dict(data3))
    template = "what: {response[entities][產品型號]}"
    print(convert_template(template))
    result = safe_format(template, data3)
    print(result)
    print("[test] format list <<<<<<<<")

def test():
    # test_format_dict()
    test_format_list()


# test()
