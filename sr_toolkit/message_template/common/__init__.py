import copy
from typing import Optional, Union

from .base import Message, Button
from .buttons import URLButton, PostbackButton, TextButton
from .text import TextMessage
from .template import ConfirmMessage, MenuMessage, CarouselMessage, ImageMapMessage


MESSAGE_POOL = [TextMessage, ConfirmMessage, MenuMessage, CarouselMessage, ImageMapMessage]
BUTTON_POOL = [URLButton, PostbackButton, TextButton]


def get_button(button: dict) -> Optional[Button]:
    button_type = button.pop('type')
    for button_template in BUTTON_POOL:
        if button_type != button_template.type:
            continue
        try:
            return button_template(**button)
        except Exception as e:
            break
    return None


def get_message(message: Union[dict, str]) -> Optional[Message]:
    if isinstance(message, str):
        return TextMessage(message=message)
    message_type = message.get('type')
    buttons = []
    if not message_type or message_type is None:
        return None
    message = copy.deepcopy(message)
    if 'buttons' in message:
        # for button_obj in message.get('buttons'):
        #     if not isinstance(button_obj, dict):
        #         continue
        #     b = get_button(button=button_obj)
        #     if b and isinstance(b, Button):
        #         buttons.append(b)
        #     if buttons and len(buttons) > 0:
        #         message['buttons'] = buttons
        message['buttons'] = factor_buttons(message.get('buttons'))
    for message_template in MESSAGE_POOL:
        if message_type != message_template.type:
            continue
        try:
            return message_template(**message)
        except Exception as e:
            break
    return None


def factor_buttons(button_objs):
    btns = []
    for button_obj in button_objs:
        if not isinstance(button_obj, dict):
            continue
        b = get_button(button=button_obj)
        if b and isinstance(b, Button):
            btns.append(b)
    if btns and len(btns) > 0:
        return btns
    else:
        return button_objs