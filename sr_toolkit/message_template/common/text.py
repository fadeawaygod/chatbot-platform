from .base import Message


class TextMessage(Message):

    type = 'text'

    def __init__(self, message, **kwargs):
        super().__init__(type=self.type)
        self.message = message
