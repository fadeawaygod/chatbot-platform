class Message:

    type = ''

    def __init__(self, type: str):
        self.type = type

    def get_type(self):
        return self.type

    def to_dict(self):
        def todict(obj, classkey=None):
            if isinstance(obj, dict):
                data = {}
                for (k, v) in obj.items():
                    data[k] = todict(v, classkey)
                return data
            elif hasattr(obj, "_ast"):
                return todict(obj._ast())
            elif hasattr(obj, "__iter__") and not isinstance(obj, str):
                return [todict(v, classkey) for v in obj]
            elif hasattr(obj, "__dict__"):
                data = dict([(key, todict(value, classkey))
                             for key, value in obj.__dict__.items()
                             if not callable(value) and not key.startswith('_')])
                if classkey is not None and hasattr(obj, "__class__"):
                    data[classkey] = obj.__class__.__name__
                return data
            else:
                return obj
        return todict(self)


class Button:

    type = ''

    def __init__(self, type: str, title: str, payload: str, **kwargs):
        self.type = type
        self.title = title
        self.payload = payload

        #
        for key, value in kwargs.items():
            self.__setattr__(key, value)
