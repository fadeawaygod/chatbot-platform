from .base import Button


class URLButton(Button):

    type = 'url'

    def __init__(self, title, payload, **kwargs):
        super(URLButton, self).__init__(type=self.type, title=title, payload=payload, **kwargs)


class PostbackButton(Button):

    type = 'postback'

    def __init__(self, title, payload, **kwargs):
        super().__init__(type=self.type, title=title, payload=payload, **kwargs)


class TextButton(Button):

    type = 'text'

    def __init__(self, title, payload, **kwargs):
        super().__init__(type=self.type, title=title, payload=payload,**kwargs)

