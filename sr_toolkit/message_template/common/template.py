from .base import Message, Button


class ConfirmMessage(Message):

    type = 'confirm'

    def __init__(self, title, buttons: [Button], **kwargs):
        super(ConfirmMessage, self).__init__(type=self.type)
        self.title = title
        self.buttons = buttons


class MenuMessage(Message):

    type = 'menu'

    def __init__(self, title: str, sub_title: str, image_url: str = None, buttons: [Button] = None, **kwargs):
        super(MenuMessage, self).__init__(type=self.type)
        self.title = title
        self.sub_title = sub_title
        self.image_url = image_url
        self.buttons = buttons


class CarouselUnit:
    def __init__(self, title, sub_title, image_url, buttons: [Button] = None):
        self.title = title
        self.sub_title = sub_title
        self.image_url = image_url
        self.buttons = buttons


class CarouselMessage(Message):

    type = 'carousel'

    def __init__(self, title: str = None, column: [CarouselUnit] = None, **kwargs):
        super(CarouselMessage, self).__init__(type=self.type)
        self.title = title
        self.column = column


class ImageMapMessage(Message):

    type = 'imagemap'

    def __init__(self, image_url, title: str = None, buttons: [Button] = None, **kwargs):
        super(ImageMapMessage, self).__init__(type=self.type)
        self.title = title
        self.image_url = image_url
        self.buttons = buttons
        self.video = None if "video" not in kwargs else kwargs["video"]
        self.baseSize = None if "baseSize" not in kwargs else kwargs["baseSize"]


