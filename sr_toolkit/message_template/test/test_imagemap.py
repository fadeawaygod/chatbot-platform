import json

from sr_toolkit.message_template import common, factory

message = {
    "type": "imagemap",
    "title": "This is an imagemap",
    "image_url": "https://i.imgur.com/ouLpvsW.jpeg",
    "baseSize": {
        "width": 1040,
        "height": 1040
    },
    "video": {
        "originalContentUrl": "https://www.facebook.com/432d955c-0c2f-42ea-b0d6-189490bc44e0",
        "previewImageUrl": "https://i.imgur.com/ouLpvsW.jpeg",
        "area": {
            "x": 0,
            "y": 0,
            "width": 1040,
            "height": 585
        },
        "externalLink": {
            "linkUri": "https://example.com/see_more.html",
            "label": "See More"
        },
    },
    "buttons": [
        {
            "type": "url",
            "title": "url",
            "payload": "https://example.com/",
            "area": {
                "x": 0,
                "y": 586,
                "width": 520,
                "height": 454
            }
        },
        {
            "type": "text",
            "title": "text",
            "payload": "Hello",
            "area": {
                "x": 520,
                "y": 586,
                "width": 520,
                "height": 454
            }
        }
    ]

}

ans = {"type": "imagemap", "baseUrl": "https://i.imgur.com/ouLpvsW.jpeg", "altText": "This is an imagemap", "baseSize": {"width": 1040, "height": 1040}, "video": {"originalContentUrl": "https://www.facebook.com/432d955c-0c2f-42ea-b0d6-189490bc44e0", "previewImageUrl": "https://i.imgur.com/ouLpvsW.jpeg", "area": {"x": 0, "y": 0, "width": 1040, "height": 585}, "externalLink": {"linkUri": "https://example.com/see_more.html", "label": "See More"}}, "actions": [{"type": "uri", "linkUri": "https://example.com/", "area": {"x": 0, "y": 586, "width": 520, "height": 454}}, {"type": "message", "text": "Hello", "area": {"x": 520, "y": 586, "width": 520, "height": 454}}]}

#
common_message = common.get_message(message=message)
passage = "line"
assert  ans == factory.to_message(type=passage, msg=common_message)
