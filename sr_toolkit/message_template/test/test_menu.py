from sr_toolkit.message_template import common, factory

message = {
            "type": "menu",
            "title": "titleTEXT",
            "sub_title": "sub_titleTEXT",
            "image_url": "http://www.messenger-rocks.com/image.jpg",
            "buttons": [
                    {
                        "type": "postback",
                        "title": "Buy",
                        "payload": "action=buy&itemid=123"
                    },
                    {
                        "type": "text",
                        "title": "Add to cart",
                        "payload": "Add to cart's Text'"
                    },
                    {
                        "type": "url",
                        "title": "View detail",
                        "payload": "http://example.com/page/123"
                    }
                ]
        }


#ans
ans = {'type': 'template', 'altText': 'titleTEXT', 'template': {'type': 'buttons', 'title': 'titleTEXT', 'text': 'sub_titleTEXT', 'actions': [{'type': 'postback', 'label': 'Buy', 'data': 'action=buy&itemid=123'}, {'type': 'message', 'label': 'Add to cart', 'text': "Add to cart's Text'"}, {'type': 'uri', 'label': 'View detail', 'uri': 'http://example.com/page/123'}], 'thumbnailImageUrl': 'http://www.messenger-rocks.com/image.jpg', 'imageAspectRatio': 'rectangle', 'imageSize': 'cover', 'imageBackgroundColor': '#FFFFFF'}}

#
common_message = common.get_message(message=message)
passage = "line"
re_dict = factory.to_message(type=passage, msg=common_message)
assert re_dict == ans