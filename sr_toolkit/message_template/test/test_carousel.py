import json

from sr_toolkit.message_template import common, factory

message = {
    "type": "carousel",
    "title": "titleTEXT",
    "column": [
        {
            "title": "titleTEXT",
            "sub_title": "sub_titleTEXT",
            "image_url": "https://example.com/bot/images/item1.jpg",
            "buttons": [
                {
                    "type": "postback",
                    "title": "Buy",
                    "payload": "action=buy&itemid=123"
                },
                {
                    "type": "text",
                    "title": "Add to cart",
                    "payload": "Add to cart's Text'"
                },
                {
                    "type": "url",
                    "title": "View detail",
                    "payload": "http://example.com/page/123"
                }
            ]
        }
    ]

}

# ans
ans = {"type": "template", "altText": "titleTEXT", "template": {"type": "carousel", "columns": [
    {"thumbnailImageUrl": "https://example.com/bot/images/item1.jpg", "imageBackgroundColor": "#000000",
     "title": "titleTEXT", "text": "sub_titleTEXT",
     "actions": [{"type": "postback", "label": "Buy", "data": "action=buy&itemid=123"},
                 {"type": "message", "label": "Add to cart", "text": "Add to cart's Text'"},
                 {"type": "uri", "label": "View detail", "uri": "http://example.com/page/123"}]}],
                                                                "imageAspectRatio": "rectangle", "imageSize": "cover"}}

#
common_message = common.get_message(message=message)
passage = "line"
assert ans == factory.to_message(type=passage, msg=common_message)
