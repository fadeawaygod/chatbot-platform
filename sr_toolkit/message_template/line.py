from .base import Converter
from .common import TextMessage, MenuMessage, ConfirmMessage, CarouselMessage,ImageMapMessage, factor_buttons
from .common import Button, URLButton, TextButton, PostbackButton
from .common.template import CarouselUnit


class TemplateMessageConverter(Converter):

    @classmethod
    def to_url_action(cls, button: URLButton) -> dict:
        return {
            'type': 'uri',
            'label': button.title,
            'uri': button.payload
        }

    @classmethod
    def to_text_action(cls, button: TextButton) -> dict:
        return {
            'type': 'message',
            'label': button.title,
            'text': button.payload
        }

    @classmethod
    def to_postback_action(cls, button: PostbackButton) -> dict:
        return {
            'type': 'postback',
            'label': button.title,
            'data': button.payload
        }

    @classmethod
    def to_action(cls, button: Button) -> dict:
        if isinstance(button, URLButton):
            return cls.to_url_action(button)
        elif isinstance(button, TextButton):
            return cls.to_text_action(button)
        elif isinstance(button, PostbackButton):
            return cls.to_postback_action(button)
        raise NotImplementedError(f'undefined button type {button.__class__.__name__}.')


class TextMessageConverter(Converter):

    @classmethod
    def convert(cls, message: TextMessage):
        return {
            'type': 'text',
            'text': message.message
        }


class ConfirmMessageConvertor(TemplateMessageConverter):

    @classmethod
    def convert(cls, message: ConfirmMessage):
        buttons = [cls.to_action(b) for b in message.buttons]
        return {
            'type': 'template',
            'altText': message.title,
            'template': {
                'type': 'confirm',
                'text': message.title,
                'actions': buttons[:2]  # max size: 2
            }
        }


class ButtonsMessageConvertor(TemplateMessageConverter):

    @classmethod
    def convert(cls, message: MenuMessage):
        buttons = [cls.to_action(b) for b in message.buttons]
        template = {
            'type': 'buttons',
            'title': message.title,
            'text': message.sub_title,
            'actions': buttons[:4]  # max size: 4
        }
        if message.image_url:
            template = {
                **template,
                'thumbnailImageUrl': message.image_url,  # optional
                'imageAspectRatio': 'rectangle',  # default
                'imageSize': 'cover',  # default
                'imageBackgroundColor': '#FFFFFF',  # default
            }
        return {
            'type': 'template',
            'altText': message.title,
            'template': template
        }


class CarouselMessageConvertor(TemplateMessageConverter):

    @classmethod
    def __gen_carousel_unit(cls, carousel: CarouselUnit):
        if carousel.buttons:
            carousel.buttons = factor_buttons(carousel.buttons)

        return {
            "thumbnailImageUrl": carousel.image_url,
            "imageBackgroundColor": "#000000",
            "title": carousel.title,
            "text": carousel.sub_title,
            "actions": [cls.to_action(b) for b in carousel.buttons]
        }


    @classmethod
    def convert(cls, message: CarouselMessage):
        # buttons = [cls.to_action(b) for b in message.buttons]
        tmp_carousels = [cls.__gen_carousel_unit(CarouselUnit(**c)) for c in message.column]

        ## buttons 數量限制一樣
        carousels = []
        max = 4
        for carousel in tmp_carousels:
            max = len(carousel["actions"]) if len(carousel["actions"]) < max else max
        for carousel in tmp_carousels:
            carousel["actions"] = carousel["actions"][:max]
            carousels.append(carousel)

        altText = message.title if message.title != "" and message.title != None else "Line系統提示："
        return {
            "type": "template",
            "altText": altText,
            "template": {
                "type": "carousel",
                "columns": carousels,
                "imageAspectRatio": "rectangle",
                "imageSize": "cover"
            }
        }


class ImageMapMessageConvertor(TemplateMessageConverter):

    @staticmethod
    def to_image_map_action(button: Button):
        def __check_int__(var):
            return True if isinstance(var, int) or isinstance(var, float) else False
        area: dict = button.__getattribute__("area")

        #
        payload = {}

        #
        for k in area.values():
            if not __check_int__(k):
                print(k)
                return None

        #
        if button.type == "url":
            payload.__setitem__("type", "uri")
            payload.__setitem__("linkUri", button.payload)
            payload.__setitem__("area", area)
        elif button.type == "text":
            payload.__setitem__("type", "message")
            payload.__setitem__("text", button.payload)
            payload.__setitem__("area", area)

        return payload

    @classmethod
    def convert(cls, message: ImageMapMessage):
        buttons = [cls.to_image_map_action(b) for b in message.buttons if cls.to_image_map_action(b) is not None]
        return {
            'type': 'imagemap',
            'baseUrl': message.image_url,
            'altText': message.title if message.title != "" and message.title is not None else "Line系統提示：",
            'baseSize': message.baseSize,
            'video': message.video,
            'actions': buttons
        }