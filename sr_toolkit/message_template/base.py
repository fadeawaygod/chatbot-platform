from .common import Message


class Converter:

    @classmethod
    def convert(cls, message: Message):
        raise NotImplemented
