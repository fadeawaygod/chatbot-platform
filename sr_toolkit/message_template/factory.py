import typing
from .base import Converter
from .common import Message, TextMessage, ConfirmMessage, MenuMessage, CarouselMessage, ImageMapMessage
from .line import TextMessageConverter, ConfirmMessageConvertor, ButtonsMessageConvertor, CarouselMessageConvertor, \
    ImageMapMessageConvertor
from .facebook import FbTextMessageConverter, FbConfirmMessageConvertor, FbButtonsMessageConvertor

LINE_MSG_CONVERT_MAP: typing.Dict[Message.__class__, Converter.__class__] = {
    TextMessage: TextMessageConverter,
    ConfirmMessage: ConfirmMessageConvertor,
    MenuMessage: ButtonsMessageConvertor,
    CarouselMessage: CarouselMessageConvertor,
    ImageMapMessage: ImageMapMessageConvertor
}

FB_MSG_CONVERT_MAP: typing.Dict[Message.__class__, Converter.__class__] = {
    TextMessage: FbTextMessageConverter,
    ConfirmMessage: FbConfirmMessageConvertor,
    MenuMessage: FbButtonsMessageConvertor
}


def to_message(type: str, msg: Message) -> dict:
    if type == 'line':
        if msg.__class__ in LINE_MSG_CONVERT_MAP:
            convertor: Converter = LINE_MSG_CONVERT_MAP[msg.__class__]
            return convertor.convert(message=msg)
    if type == 'fb':
        if msg.__class__ in FB_MSG_CONVERT_MAP:
            convertor: Converter = FB_MSG_CONVERT_MAP[msg.__class__]
            return convertor.convert(message=msg)


if __name__ == '__main__':
    from .common.buttons import TextButton, PostbackButton, URLButton

    # Example of use cases.
    message1 = TextMessage(message='hello')
    print(to_message(type='line', msg=message1))
    message2 = ConfirmMessage(title='hello world',
                              buttons=[TextButton('hello', 'world'), PostbackButton('No', 'world')])
    print(to_message(type='line', msg=message2))
    message3 = MenuMessage('hello', 'world', None,
                           buttons=[TextButton('hello', 'world'),
                                    PostbackButton('No', 'world'),
                                    URLButton('No', 'http://google.com/')])
    print(to_message(type='line', msg=message3))
