from .base import Converter
from .common import TextMessage, MenuMessage, ConfirmMessage
from .common import Button, URLButton, TextButton, PostbackButton


class TemplateMessageConverter(Converter):

    @classmethod
    def to_url_action(cls, button: URLButton) -> dict:
        return {
            'type': 'web_url',
            'title': button.title,
            'url': button.payload
        }

    @classmethod
    def to_text_action(cls, button: TextButton) -> dict:
        return {
            "type": "postback",
            "title": button.title,
            "payload": button.payload
        }

    @classmethod
    def to_postback_action(cls, button: PostbackButton) -> dict:
        return {
            "type": "postback",
            "title": button.title,
            "payload": button.payload
        }

    @classmethod
    def to_action(cls, button: Button) -> dict:
        if isinstance(button, URLButton):
            return cls.to_url_action(button)
        elif isinstance(button, TextButton):
            return cls.to_text_action(button)
        elif isinstance(button, PostbackButton):
            return cls.to_postback_action(button)
        raise NotImplementedError(f'undefined button type {button.__class__.__name__}.')


class FbTextMessageConverter(Converter):

    @classmethod
    def convert(cls, message: TextMessage):
        return {
            'text': message.message
        }


class FbConfirmMessageConvertor(TemplateMessageConverter):

    @classmethod
    def convert(cls, message: ConfirmMessage):
        buttons = [cls.to_action(b) for b in message.buttons]
        return {
              "attachment": {
                  "type": "template",
                  "payload": {
                    "template_type": "button",
                    "text": message.title,
                    "buttons": buttons
                  }
                }
          }


class FbButtonsMessageConvertor(TemplateMessageConverter):
    """
    menu msg
    """

    @classmethod
    def convert(cls, message: MenuMessage):
        buttons = [cls.to_action(b) for b in message.buttons]
        template = {
            "title": message.title,
            "subtitle": message.sub_title,
            "buttons": buttons
        }
        if message.image_url:
            template = {
                **template,
                "image_url": message.image_url,
            }
        return {
              "attachment": {
                  "type": "template",
                  "payload": {
                    "template_type": "generic",
                    "elements": [
                      template
                    ]
                  }
                }
          }
