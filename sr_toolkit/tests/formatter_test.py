import unittest

import formatter


class SafeFormatTest(unittest.TestCase):

    def test_with_slot_filling(self):
        data = {
            'hello': 'world'
        }
        format_str = 'Hello {hello}'
        result = formatter.safe_format(target=format_str, data=data)
        self.assertEqual(result, 'Hello world')

    def test_with_unknown_key(self):
        data = {
            'hello': 'world'
        }
        format_str = 'Hello {world}'
        result = formatter.safe_format(target=format_str, data=data)
        self.assertEqual(result, 'Hello {world}')

    def test_with_recursive_unknown_key(self):
        data = {
            'hello': 'world',
        }
        format_str = 'Hello {earth[world][to]}'
        result = formatter.safe_format(target=format_str, data=data)
        self.assertEqual(result, 'Hello {earth[world][to]}')

    def test_with_deep_recursive_unknown_key(self):
        data = {}
        points = ''.join(f'[{i}]'for i in range(100))
        format_str = 'Hello '+'{earth'+points+'}'
        result = formatter.safe_format(target=format_str, data=data)
        self.assertEqual(result, 'Hello {earth[0][1][2][3][4][5][6][7][8][9][10][11][12][13][14][15][16][17][18][19][20][21][22][23][24][25][26][27][28][29][30][31][32][33][34][35][36][37][38][39][40][41][42][43][44][45][46][47][48][49][50][51][52][53][54][55][56][57][58][59][60][61][62][63][64][65][66][67][68][69][70][71][72][73][74][75][76][77][78][79][80][81][82][83][84][85][86][87][88][89][90][91][92][93][94][95][96][97][98][99]}')

    def test_format_with_empty_string(self):
        data = {}
        format_str = ''
        result = formatter.safe_format(target=format_str, data=data)
        self.assertEqual(result, '')

    def test_format_with_None_value(self):
        data = {}
        format_str = None
        result = formatter.safe_format(target=format_str, data=data)
        self.assertEqual(result, format_str)
