import json


class NotificationBuilder:

    def __init__(self):
        self._data = {
            'event': None,
            'agent_id': None,
            'user_id': None,
            'group_id': None,
            'content': {}
        }

    def set_event(self, event: str):
        self._data['event'] = event
        return self

    def set_agent_id(self, agent_id: str):
        self._data['agent_id'] = agent_id
        return self

    def set_user_id(self, user_id: str):
        self._data['user_id'] = user_id
        return self

    def set_group_id(self, group_id: str):
        self._data['group_id'] = group_id
        return self

    def set_content(self, content: dict):
        self._data['content'] = content
        return self

    def to_str(self) -> str:
        return json.dumps(self._data, ensure_ascii=False)

