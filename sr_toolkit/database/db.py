import datetime
import json
import time

import tornado.httpclient

from .result import SessionResult, CountResult, InsertResult, QueryResult, UpdateResult
from .. import fetch_helper


class AuthManager:

    SIGNIN_URL_TEMPLATE = '{url}/system/auth/signIn'

    def __init__(self, url: str = None):
        self.access_token = None
        self.refresh_token = None
        self.expire_time = None
        self.url = url

    @staticmethod
    def create(account: str, password: str, url: str):
        self = AuthManager(url=url)
        self.login(account, password)
        return self

    def login(self, account, password):
        login_data = self._login(account, password)
        self.access_token = login_data['access_token']
        self.refresh_token = login_data['refresh_token']
        self.expire_time = login_data['expires_in']

    def _login(self, account, password) -> dict:
        url = self.SIGNIN_URL_TEMPLATE.format(url=self.url)
        payload = {"uid": account, "password": password}
        code, body = fetch_helper.fetch_sync(url=url, method='POST', body=json.dumps(
            payload), headers={'content-type': 'application/json'})
        if code != 200:
            raise tornado.httpclient.HTTPClientError(
                code=code, message=f'fail to login. response body: {body}')
        return json.loads(body, encoding='utf-8')

    @property
    def expire_time(self):
        return self._expire_time

    @expire_time.setter
    def expire_time(self, seconds: int):
        expire_time = None
        if seconds:
            now = datetime.datetime.now()
            expire_time = now + datetime.timedelta(seconds=seconds)
        self._expire_time = expire_time

    def is_token_expired(self) -> bool:
        expire_time = self.expire_time
        if not expire_time:
            raise ValueError('Expire time is not exist.')
        now = datetime.datetime.now()
        return now < expire_time

    def refresh(self):
        NotImplemented

class ORM:
    _return_result_class_type = None
    COUNT_URL_TEMPLATE = '{url}/data/count'
    QUERY_URL_TEMPLATE = '{url}/data/query'
    INSERT_URL_TEMPLATE = '{url}/data/insert'
    INSERT_MANY_URL_TEMPLATE = '{url}/data/insertMany'
    UPDATE_URL_TEMPLATE = '{url}/data/update'
    DELETE_URL_TEMPLATE = '{url}/data/delete'
    DELETE_MANY_URL_TEMPLATE = '{url}/data/deleteMany'

    def __init__(self, auth_manager: AuthManager):
        self.query_data = {}
        self.auth_manager = auth_manager

    def query(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.QUERY_URL_TEMPLATE
        self.query_data['collection'] = collection
        self.query_data['filter'] = {}
        self._return_result_class_type = QueryResult
        return self

    def count(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.COUNT_URL_TEMPLATE
        self.query_data['collection'] = collection
        self.query_data['filter'] = {}
        self._return_result_class_type = CountResult
        return self

    def insert(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.INSERT_URL_TEMPLATE
        self.query_data['collection'] = collection
        self._return_result_class_type = InsertResult
        return self

    def insert_many(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.INSERT_MANY_URL_TEMPLATE
        self.query_data['collection'] = collection
        self.query_data['data'] = []
        return self

    def update(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.UPDATE_URL_TEMPLATE
        self.query_data['collection'] = collection
        self._return_result_class_type = UpdateResult
        return self

    def delete(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.DELETE_URL_TEMPLATE
        self.query_data['collection'] = collection
        return self

    def delete_many(self, collection: str):
        self.query_data.clear()
        self.query_data['path'] = self.DELETE_MANY_URL_TEMPLATE
        self.query_data['collection'] = collection
        return self

    def data(self, column: str, value):
        if 'data' not in self.query_data:
            self.query_data['data'] = {}
        if isinstance(self.query_data['data'], list):
            self.query_data['data'] = value
        else:
            self.query_data['data'][column] = value
        return self

    def multi_data(self, data: dict):
        if data and isinstance(data, dict):
            for column, value in data.items():
                self.data(column=column, value=value)
        return self

    def export_filter_data(self):
        return self.query_data.get('filter') or {}

    def import_filter_data(self, filters: dict):
        if 'filter' not in self.query_data:
            self.query_data['filter'] = {}
        self.query_data['filter'] = filters
        return self

    def filter_by(self, column: str, value, operator: str = '='):
        operators = {
            '>': '$gt',
            '<': '$lt',
            '>=': '$gte',
            '<=': '$lte',
            'in': '$in',
            're': '$regex',
            'exists': '$exists',
            'ne': '$ne',
            '!=': '$ne',
            'nin': '$nin'}
        if 'filter' not in self.query_data:
            self.query_data['filter'] = {}
        if operator == '=':
            self.query_data['filter'][column] = value
        else:
            if not isinstance(self.query_data['filter'].get(column), dict):
                self.query_data['filter'][column] = {}
            op = operators.get(operator, None)
            if not op:
                raise ValueError(f'undefined operator {operator}')
            self.query_data['filter'][column][op] = value
        return self

    def multi_filter(self, filters: dict = None):
        if filters and isinstance(filters, dict):
            for column, value in filters.items():
                self.filter_by(column=column, value=value)
        return self

    def filter_in(self, column: str, array: list):
        return self.filter_by(column=column, value=array, operator='in')

    def filter_not_in(self, column: str, array: list):
        return self.filter_by(column=column, value=array, operator='nin')

    def filter_or(self, conditions: dict):
        """conditions example:[{"uid":"tj"}, {"email": "tj"}]
        Args:
            conditions (list): [description]
        """
        if 'filter' not in self.query_data:
            self.query_data['filter'] = {}
        if '$or' not in self.query_data['filter']:
            self.query_data['filter']['$or'] = []
        self.query_data['filter']['$or'].append(conditions)
        return self

    def projection_by(self, column: str, show: bool):
        if 'projection' not in self.query_data:
            self.query_data['projection'] = {}
        self.query_data['projection'][column] = show
        return self

    def sort_by(self, column: str, asc: bool = True):
        if 'sort' not in self.query_data:
            self.query_data['sort'] = {}
        self.query_data['sort'][column] = 1 if asc is True else -1
        return self

    def limit(self, limit: int):
        self.query_data['limit'] = limit
        return self

    def skip(self, skip: int):
        self.query_data['skip'] = skip
        return self

    async def execute(self) -> dict:
        if isinstance(self._return_result_class_type, UpdateResult):
            self.data(column='updated', value=int(time.time()*1000))
        query_data = self.query_data.copy()
        self.query_data.clear()
        try:
            url = query_data['path'].format(url=self.auth_manager.url)
            del query_data['path']
            if 'collection' not in query_data:
                raise ValueError('collection is not assigned.')
            headers = {'content-type': 'application/json',
                       'Authorization': f'Bearer {self.auth_manager.access_token}'}
            code, body = await fetch_helper.fetch_async(url=url,
                                                        method='POST',
                                                        body=json.dumps(query_data, ensure_ascii=False),
                                                        headers=headers)
            if code != 200:
                raise tornado.httpclient.HTTPClientError(
                    code=code, message=f'get fail. response body: {body}')
            response_data = json.loads(body, encoding='utf-8')
            return response_data
        except Exception:
            raise

    async def execute_with_result(self) -> SessionResult:
        data = await self.execute()
        if self._return_result_class_type:
            return self._return_result_class_type(payload=data)


def create_session(account: str, password: str, url: str) -> ORM:
    from tornado.ioloop import IOLoop
    auth_manager = IOLoop.current().run_sync(
        lambda: AuthManager.create(account=account, password=password, url=url))
    return ORM(auth_manager)
