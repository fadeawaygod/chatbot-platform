import typing


class SessionResult:

    def __init__(self, payload: dict):
        self.status = payload.get('status', 'failed')


class QueryResult(SessionResult):

    def __init__(self, payload: dict):
        self._data = []
        super(QueryResult, self).__init__(payload=payload)
        self.data = payload.get('data', [])

    @property
    def first(self) -> typing.Optional[dict]:
        if self.data and isinstance(self.data, list) and len(self.data) > 0:
            return self.data[0]


class InsertResult(SessionResult):

    def __init__(self, payload: dict):
        super(InsertResult, self).__init__(payload=payload)
        self.data = payload.get('data', {})
        self.message = payload.get('message', '')


class UpdateResult(SessionResult):

    def __init__(self, payload: dict):
        super(UpdateResult, self).__init__(payload=payload)
        self.count = payload.get('count', 0)


class CountResult(SessionResult):

    def __init__(self, payload: dict):
        super(CountResult, self).__init__(payload=payload)
        self.count = payload.get('count', 0)
