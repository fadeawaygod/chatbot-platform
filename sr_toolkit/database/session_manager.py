from typing import Union

from .. import singleton
from .db import ORM, AuthManager


class SessionManager(singleton.Singleton):

    __auth_manager = None
    __session = None

    @classmethod
    def config(cls, auth_manager: Union[AuthManager, str], **kwargs):
        self = cls.get_instance()
        if isinstance(auth_manager, AuthManager):
            self.__auth_manager = auth_manager
        else:
            assert 'ac' in kwargs and 'pwd' in kwargs, 'Invalid config.'
            self.__auth_manager = AuthManager.create(account=kwargs['ac'], password=kwargs['pwd'], url=auth_manager)
        return self

    @classmethod
    def get_session(cls):
        self = cls.get_instance()
        return ORM(auth_manager=self.__auth_manager)
