import unittest
import json
import urllib.request

try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])
    from integration_tests.test_base import TestBase
except:
    pass


class BotTest(TestBase):
    def setUp(self):
        self._login()

    def test_create_bot_successfully(self):
        bot_id = self._create_bot(self.TEST_BOT_NAME)
        self._assert_bot_exist(bot_id)
        self._delete_bot(bot_id)

    def test_delete_bot_successfully(self):
        bot_id = self._create_bot(self.TEST_BOT_NAME)

        self._delete_bot(bot_id)
        self._assert_bot_not_exist(bot_id)

    def test_patch_bot_successfully(self):
        bot_id = self._create_bot(self.TEST_BOT_NAME)

        new_bot_name = 'test_new_bot_name'
        self._patch_bot(bot_id, new_bot_name)
        self._assert_bot_exist(bot_id, new_bot_name)
        self._delete_bot(bot_id)

    def _assert_bot_exist(self, bot_id: str, bot_name=''):
        bots = self._fetch_bot()
        bot_ids = [bot.get('id', '') for bot in bots]
        self.assertIn(bot_id, bot_ids)

    def _assert_bot_not_exist(self, bot_id: str):
        bots = self._fetch_bot()
        bot_ids = [bot.get('uid', '') for bot in bots]
        self.assertNotIn(bot_id, bot_ids)


if __name__ == '__main__':
    unittest.main()
