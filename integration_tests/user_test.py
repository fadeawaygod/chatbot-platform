import unittest
import json
import urllib.request

try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])
    from integration_tests.test_base import TestBase
except:
    pass


class UserTest(TestBase):
    def setUp(self):
        self._login()

    def test_create_user_successfully(self):
        self._create_user(
            self.TEST_EDITOR_USER_ID_FOR_USER_TEST, ['editor'])
        try:
            self._assert_user_exist(self.TEST_EDITOR_USER_ID_FOR_USER_TEST)
        finally:
            self._delete_user(self.TEST_EDITOR_USER_ID_FOR_USER_TEST)

    def test_delete_user_successfully(self):
        self._create_user(self.TEST_EDITOR_USER_ID_FOR_USER_TEST, ['editor'])

        self._delete_user(self.TEST_EDITOR_USER_ID_FOR_USER_TEST)
        self._assert_user_not_exist(self.TEST_EDITOR_USER_ID_FOR_USER_TEST)

    def test_patch_user_successfully(self):
        self._create_user(
            self.TEST_EDITOR_USER_ID_FOR_USER_TEST, ['editor'])

        new_user_name = 'test_new_user_name'
        self._patch_user(self.TEST_EDITOR_USER_ID_FOR_USER_TEST, new_user_name)
        try:
            self._assert_user_exist(
                self.TEST_EDITOR_USER_ID_FOR_USER_TEST, new_user_name)
        finally:
            self._delete_user(self.TEST_EDITOR_USER_ID_FOR_USER_TEST)

    def _assert_user_exist(self, user_id: str, user_name=''):
        users = self._fetch_user()
        if user_name:
            user_tuples = [(user.get('uid', ''), user.get('name', ''))
                           for user in users]
            self.assertIn((user_id, user_name), user_tuples)
        else:
            user_ids = [user.get('uid', '') for user in users]
            self.assertIn(user_id, user_ids)

    def _assert_user_not_exist(self, user_id: str):
        users = self._fetch_user()
        user_ids = [user.get('uid', '') for user in users]
        self.assertNotIn(user_id, user_ids)


if __name__ == '__main__':
    unittest.main()
