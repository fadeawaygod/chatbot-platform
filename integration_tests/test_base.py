import unittest
import json
import urllib.request

try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])

    from config.integration_tests_config import (
        SERVICE_IP,
        SERVICE_API_VERSION,
        DB_SERVICE_IP,
        TEST_USER_ID,
        TEST_USER_PASSWORD)
except:
    pass


class TestBase(unittest.TestCase):
    LOGIN_URI = f'http://{DB_SERVICE_IP}/auth/signIn'
    SERVICE_URL_TEMPLATE = f'http://{SERVICE_IP}/api/v{SERVICE_API_VERSION}/{{app}}/{{resource}}'
    APP_EDITOR = 'editor'
    RESOURCE_GROUPS = 'groups'
    RESOURCE_USERS = 'users'
    RESOURCE_BOTS = 'bots'
    RESOURCE_ENTITYS = 'entities'
    RESOURCE_INTENTS = 'intents'
    RESOURCE_UTTERANCES = 'utterances'
    TEST_GROUP_NAME = 'test_group_name_for_integration_test'
    TEST_USER_NAME = 'test_user_name_for_integration_test'

    TEST_EDITOR_USER_ID = 'test_editor_id_for_integration_test'
    TEST_EDITOR_USER_ID_FOR_USER_TEST = 'test_editor_id_for_integration_user_test'
    TEST_BOT_NAME = 'test_bot_name_for_integration_test'
    TEST_BOT_NAME_FOR_OTHER_TEST = 'test_bot_name_for_other_integration_test'
    TEST_INTENT_NAME = 'test_intent_name_for_integration_test'
    TEST_ENTITY_NAME = 'test_entity_name_for_integration_test'

    def _login(self):
        body = {
            'uid': TEST_USER_ID,
            'password': TEST_USER_PASSWORD}
        login_result = self._query_external_api_json_with_details(
            self.LOGIN_URI,
            {},
            body,
            'POST',
            False
        )
        self._access_token = login_result['accessToken']

    def _query_external_api_json_with_details(self, query_uri: str, headers: dict, payload: dict, method: str, is_append_access_token=True) -> dict:
        if method != 'GET':
            headers.update({'Content-Type': 'application/json'})
        if is_append_access_token:
            headers.update({'Authorization': f'Bearer {self._access_token}'})
        payload_bytes = json.dumps(payload, ensure_ascii=False).encode()
        req = urllib.request.Request(
            query_uri, headers=headers, data=payload_bytes, method=method)
        with urllib.request.urlopen(req) as response:
            contents = response.read()
        contents = contents.decode('utf-8')
        contents = json.loads(contents)
        return contents

    def _assert_group_not_exist(self, group_id: str, group_name: str):
        groups = self._fetch_group()
        self.assertNotIn(
            {'id': group_id, 'name': group_name}, groups)

    def _assert_group_exist(self, group_id: str, group_name: str):
        groups = self._fetch_group()
        self.assertIn(
            {'id': group_id, 'name': group_name}, groups)

    def _create_group(self, group_name: str) -> str:
        """
        Returns:
            str: new group id
        """
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_GROUPS)
        body = {
            "name": group_name
        }
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'POST',
        )

        return result['group_id']

    def _patch_group(self, group_id: str, group_name: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_GROUPS)
        query_uri += f'/{group_id}'
        body = {
            "name": group_name
        }
        self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'PATCH',
        )

    def _delete_group(self, group_id: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_GROUPS)
        query_uri += f'/{group_id}'
        self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'DELETE',
        )

    def _fetch_group(self) -> dict:
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_GROUPS)
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'GET',
        )
        return result['groups']

    def _create_user(self, uid: str, role: list, group_id=''):
        def create_group_if_not_exist(group_name):
            groups = self._fetch_group()
            for group in groups:
                if group['name'] == group_name:
                    return group['id']
            return self._create_group(group_name)

        if not group_id:
            group_id = create_group_if_not_exist(
                self.TEST_GROUP_NAME)

        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_USERS)
        body = {
            'name': self.TEST_USER_NAME,
            'group': group_id,
            'uid': uid,
            'password': 'test_password',
            'role': role,
        }
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'POST',
        )

        return result

    def _fetch_user(self) -> dict:
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_USERS)
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'GET',
        )
        return result['users']

    def _delete_user(self, user_id: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_USERS)
        query_uri += f'/{user_id}'
        self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'DELETE',
        )

    def _patch_user(self, user_id: str, user_name: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_USERS)
        query_uri += f'/{user_id}'
        body = {
            "name": user_name
        }
        self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'PATCH',
        )

    def _create_bot(self, bot_name: str):
        def create_user_if_not_exist(uid):
            users = self._fetch_user()
            for user in users:
                if user.get('uid', '') == uid:
                    return
            self._create_user(uid, ['editor'])

        user_id = self.TEST_EDITOR_USER_ID
        create_user_if_not_exist(user_id)

        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_BOTS)
        body = {
            'name': bot_name,
            'owner': user_id,
            'config': 'test_nick_name',
            'description': 'test_password'
        }
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'POST',
        )

        return result['bot_id']

    def _fetch_bot(self) -> dict:
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_BOTS)
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'GET',
        )
        return result['bots']

    def _delete_bot(self, bot_id: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_BOTS)
        query_uri += f'/{bot_id}'
        self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'DELETE',
        )

    def _patch_bot(self, bot_id: str, bot_name: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_USERS)
        query_uri += f'/{bot_id}'
        body = {
            "name": bot_name
        }
        self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'PATCH',
        )

    def _fetch_intent(self, bot_id: str) -> dict:
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_INTENTS)
        query_uri += f'?bot_id={bot_id}'
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'GET',
        )
        return result['intents']

    def _create_intent(self, intent_name: str) -> tuple:
        def create_bot_if_not_exist(bot_name):
            bots = self._fetch_bot()
            for bot in bots:
                if bot.get('name', '') == bot_name:
                    return bot['id']
            return self._create_bot(bot_name)

        bot_id = create_bot_if_not_exist(self.TEST_BOT_NAME_FOR_OTHER_TEST)

        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_INTENTS)
        body = {
            'name': intent_name,
            'bot_id': bot_id,
            'priority': 10
        }
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'POST',
        )

        return bot_id, result['intent_id']
