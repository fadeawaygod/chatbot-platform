import unittest
import json
import urllib.request

try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])
    from integration_tests.test_base import TestBase
except:
    pass


class IntentTest(TestBase):
    def setUp(self):
        self._login()

    def test_create_intent_successfully(self):
        bot_id, intent_id = self._create_intent(self.TEST_INTENT_NAME)

        try:
            self._assert_intent_exist(intent_id, bot_id)
        finally:
            self._delete_intent(intent_id)

    def test_delete_intent_successfully(self):
        bot_id, intent_id = self._create_intent(self.TEST_INTENT_NAME)
        self._delete_intent(intent_id)
        self._assert_intent_not_exist(intent_id, bot_id)

    def test_patch_intent_successfully(self):
        bot_id, intent_id = self._create_intent(self.TEST_INTENT_NAME)
        new_intent_name = 'test_new_intent_name'
        self._patch_intent(intent_id, new_intent_name)
        try:
            self._assert_intent_exist(intent_id, bot_id, new_intent_name)
        finally:
            self._delete_intent(intent_id)

    def _assert_intent_exist(self, intent_id: str, bot_id: str, new_intent_name=''):
        intents = self._fetch_intent(bot_id)
        if new_intent_name:
            intent_tuples = [(intent.get('id', ''), intent.get('name', ''))
                             for intent in intents]
            self.assertIn((intent_id, new_intent_name), intent_tuples)
        else:
            intent_ids = [intent.get('id', '') for intent in intents]
            self.assertIn(intent_id, intent_ids)

    def _assert_intent_not_exist(self, intent_id: str, bot_id: str):
        intents = self._fetch_intent(bot_id)
        intent_ids = [intent.get('id', '') for intent in intents]
        self.assertNotIn(intent_id, intent_ids)

    def _delete_intent(self, intent_id: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_INTENTS)
        query_uri += f'/{intent_id}'
        self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'DELETE',
        )

    def _patch_intent(self, intent_id: str, intent_name: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_INTENTS)
        query_uri += f'/{intent_id}'
        body = {
            "name": intent_name
        }
        self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'PATCH',
        )


if __name__ == '__main__':
    unittest.main()
