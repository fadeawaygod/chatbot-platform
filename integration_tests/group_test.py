import json
import unittest


try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])
    from integration_tests.test_base import TestBase
except:
    pass


class GroupTest(TestBase):
    def setUp(self):
        self._login()

    def test_create_group_successfully(self):
        group_id = self._create_group(self.TEST_GROUP_NAME)
        try:
            self._assert_group_exist(group_id, self.TEST_GROUP_NAME)
        finally:
            self._delete_group(group_id)

    def test_delete_group_successfully(self):
        group_id = self._create_group(self.TEST_GROUP_NAME)
        self._delete_group(group_id)
        self._assert_group_not_exist(
            group_id, self.TEST_GROUP_NAME)

    def test_patch_group_successfully(self):
        group_id = self._create_group(self.TEST_GROUP_NAME)
        new_group_name = 'test_group2'
        self._patch_group(group_id, new_group_name)
        try:
            self._assert_group_exist(group_id, new_group_name)
        finally:
            self._delete_group(group_id)


if __name__ == '__main__':
    unittest.main()
