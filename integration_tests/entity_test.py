import unittest
import json
import urllib.request

try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])
    from integration_tests.test_base import TestBase
except:
    pass


class EntityTest(TestBase):
    def setUp(self):
        self._login()

    def test_create_entity_successfully(self):
        bot_id, entity_id = self._create_entity(self.TEST_ENTITY_NAME)

        try:
            self._assert_entity_exist(entity_id, bot_id)
        finally:
            self._delete_entity(entity_id)

    def test_delete_entity_successfully(self):
        bot_id, entity_id = self._create_entity(self.TEST_ENTITY_NAME)
        self._delete_entity(entity_id)
        self._assert_entity_not_exist(entity_id, bot_id)

    def test_patch_entity_successfully(self):
        bot_id, entity_id = self._create_entity(self.TEST_ENTITY_NAME)
        new_entity_name = 'test_new_entity_name'
        self._patch_entity(entity_id, new_entity_name)
        try:
            self._assert_entity_exist(entity_id, bot_id, new_entity_name)
        finally:
            self._delete_entity(entity_id)

    def _assert_entity_exist(self, entity_id: str, bot_id: str, new_entity_name=''):
        entities = self._fetch_entity(bot_id)
        if new_entity_name:
            entity_tuples = [(entity.get('id', ''), entity.get('name', ''))
                             for entity in entities]
            self.assertIn((entity_id, new_entity_name), entity_tuples)
        else:
            entity_ids = [entity.get('id', '') for entity in entities]
            self.assertIn(entity_id, entity_ids)

    def _assert_entity_not_exist(self, entity_id: str, bot_id: str):
        entities = self._fetch_entity(bot_id)
        entity_ids = [entity.get('id', '') for entity in entities]
        self.assertNotIn(entity_id, entity_ids)

    def _create_entity(self, entity_name: str) -> str:
        def create_bot_if_not_exist(bot_name):
            bots = self._fetch_bot()
            for bot in bots:
                if bot.get('name', '') == bot_name:
                    return bot['id']
            return self._create_bot(bot_name, ['editor'])

        bot_id = create_bot_if_not_exist(self.TEST_BOT_NAME_FOR_OTHER_TEST)

        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_ENTITYS)
        body = {
            'name': entity_name,
            'bot_id': bot_id
        }
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'POST',
        )

        return bot_id, result['entity_id']

    def _fetch_entity(self, bot_id: str) -> dict:
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_ENTITYS)
        query_uri += f'?bot_id={bot_id}'
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'GET',
        )
        return result['entities']

    def _delete_entity(self, entity_id: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_ENTITYS)
        query_uri += f'/{entity_id}'
        self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'DELETE',
        )

    def _patch_entity(self, entity_id: str, entity_name: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_ENTITYS)
        query_uri += f'/{entity_id}'
        body = {
            "name": entity_name
        }
        self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'PATCH',
        )


if __name__ == '__main__':
    unittest.main()
