import unittest
import json
import urllib.request

try:
    # for debug
    import sys
    sys.path.extend(['..', '.'])
    from integration_tests.test_base import TestBase
except:
    pass


class UtteranceTest(TestBase):
    TEST_UTTERANCE_TEXT = 'text'

    def setUp(self):
        self._login()

    def test_create_utterance_successfully(self):
        intent_id, utterance_id = self._create_utterance(
            self.TEST_UTTERANCE_TEXT)

        try:
            self._assert_utterance_exist(utterance_id, intent_id)
        finally:
            self._delete_utterance(utterance_id)

    def test_delete_utterance_successfully(self):
        intent_id, utterance_id = self._create_utterance(
            self.TEST_UTTERANCE_TEXT)
        self._delete_utterance(utterance_id)
        self._assert_utterance_not_exist(utterance_id, intent_id)

    def test_patch_utterance_successfully(self):
        intent_id, utterance_id = self._create_utterance(
            self.TEST_UTTERANCE_TEXT)
        new_utterance_text = 'test_new_utterance_text'
        self._patch_utterance(utterance_id, new_utterance_text)
        try:
            self._assert_utterance_exist(
                utterance_id, intent_id, new_utterance_text)
        finally:
            self._delete_utterance(utterance_id)

    def _assert_utterance_exist(self, utterance_id: str, intent_id: str, new_utterance_text=''):
        utterances = self._fetch_utterance(intent_id)
        if new_utterance_text:
            utterance_tuples = [(utterance.get('id', ''), utterance.get('text', ''))
                                for utterance in utterances]
            self.assertIn((utterance_id, new_utterance_text), utterance_tuples)
        else:
            utterance_ids = [utterance.get('id', '')
                             for utterance in utterances]
            self.assertIn(utterance_id, utterance_ids)

    def _assert_utterance_not_exist(self, utterance_id: str, bot_id: str):
        utterances = self._fetch_utterance(bot_id)
        utterance_ids = [utterance.get('id', '') for utterance in utterances]
        self.assertNotIn(utterance_id, utterance_ids)

    def _create_utterance(self, utterance_text: str) -> str:
        def create_bot_if_not_exist(bot_name):
            bots = self._fetch_bot()
            for bot in bots:
                if bot.get('name', '') == bot_name:
                    return bot['id']
            return self._create_bot(bot_name, ['editor'])

        def create_intent_if_not_exist(intent_name, bot_id):
            intents = self._fetch_intent(bot_id)
            for intent in intents:
                if intent.get('name', '') == intent_name:
                    return intent['id']
            _, intent_id = self._create_intent(intent_name)
            return intent_id

        bot_id = create_bot_if_not_exist(self.TEST_BOT_NAME_FOR_OTHER_TEST)
        intent_id = create_intent_if_not_exist(
            self.TEST_INTENT_NAME, bot_id)

        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_UTTERANCES)
        body = {
            'text': utterance_text,
            'intent_id': intent_id
        }
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'POST',
        )

        return intent_id, result['utterance_id']

    def _fetch_utterance(self, intent_id: str) -> dict:
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_UTTERANCES)
        query_uri += f'?intent_id={intent_id}'
        result = self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'GET',
        )
        return result['utterances']

    def _delete_utterance(self, utterance_id: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_UTTERANCES)
        query_uri += f'/{utterance_id}'
        self._query_external_api_json_with_details(
            query_uri,
            {},
            {},
            'DELETE',
        )

    def _patch_utterance(self, utterance_id: str, utterance_text: str):
        query_uri = self.SERVICE_URL_TEMPLATE.format(
            app=self.APP_EDITOR,
            resource=self.RESOURCE_UTTERANCES)
        query_uri += f'/{utterance_id}'
        body = {
            "text": utterance_text
        }
        self._query_external_api_json_with_details(
            query_uri,
            {},
            body,
            'PATCH',
        )


if __name__ == '__main__':
    unittest.main()
