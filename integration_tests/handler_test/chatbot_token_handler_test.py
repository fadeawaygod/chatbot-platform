import logging
import json

import tornado.testing
import tornado.web
import tornado.httpclient
import tornado.ioloop

from handler.chatbot_token_handler import ChatbotTokenHandler, token_manager

application = tornado.web.Application([
    (r'/token', ChatbotTokenHandler),
])


class ChatbotHandlerTest(tornado.testing.AsyncHTTPTestCase):

    def setUp(self) -> None:
        super(ChatbotHandlerTest, self).setUp()
        # disable logger output while testing.
        logging.disable(logging.CRITICAL)

    def get_app(self) -> tornado.web.Application:
        return application

    @staticmethod
    def delete_data(token: str = None):
        if not token:
            return
        tornado.ioloop.IOLoop.current().run_sync(lambda: token_manager.delete(token))

    def delete_datas(self, tokens: [str]):
        for token in tokens:
            self.delete_data(token)

    def get_test_data(self) -> dict:
        # Set up
        fake_user_id = 'chatbot_token_handler_test'
        fake_passage = 'FAKE_WEB'
        fake_agent_id = '213'
        fake_credential = {
            'ACCESS_TOKEN': '1234567890_chatbot_token_handler_test',
            'APP_SECRET': '0987654321_chatbot_token_handler_test'
        }
        return {
            'user_id': fake_user_id,
            'agent_id': fake_agent_id,
            'passage': fake_passage,
            'credential': fake_credential
        }

    def test_get_credential_by_token(self):
        token = None
        try:
            # Set up
            payload = self.get_test_data()
            token = tornado.ioloop.IOLoop.current().run_sync(lambda: token_manager.issue(**payload))
            self.assertIsNotNone(token)
            response = self.fetch(path=f'/token?token={token}')
            code, body = response.code, response.body
            actual_credential = json.loads(body, encoding='utf-8')
            expected_credential = payload['credential']
            self.assertEqual(code, 200)
            self.assertDictEqual(
                actual_credential['data']['credential'],
                expected_credential)
        except Exception:
            raise
        finally:
            self.delete_data(token=token)

    def test_get_credential_by_unknown_token(self):
        token = 'abca456'
        response = self.fetch(path=f'/token?token={token}')
        code, body = response.code, response.body
        self.assertEqual(code, 500)

    def test_insert_credential_data_and_expect_token_return(self):
        token = None
        try:
            # Set up
            payload = self.get_test_data()
            response = self.fetch(path='/token',
                                  method='POST',
                                  body=json.dumps(payload),
                                  headers={'content-type': 'application/json'})
            code, body = response.code, response.body
            token = json.loads(body, encoding='utf-8').get('data').get('token')
            self.assertIsNotNone(token)
            self.assertEqual(code, 200)
        except Exception:
            raise
        finally:
            self.delete_data(token=token)

    def test_insert_fail_with_out_header_content_type_json(self):
        # Set up
        payload = self.get_test_data()
        response = self.fetch(
            path='/token',
            body=json.dumps(payload),
            method='POST')
        self.assertEqual(response.code, 400)

    def test_update_credential(self):
        token = None
        try:
            # Set up
            payload = self.get_test_data()
            token = tornado.ioloop.IOLoop.current().run_sync(
                lambda: token_manager.issue(**payload))
            new_credential = {
                'ACCESS_TOKEN': '0987654321',
                'APP_SECRET': '1234567890'
            }
            response = self.fetch(
                f'/token?token={token}',
                headers={'content-type': 'application/json'},
                body=json.dumps(new_credential),
                method='PUT')
            code, data = response.code, json.loads(
                response.body, encoding='utf-8')
            self.assertEqual(code, 200)
            self.assertEqual(data['count'], 1)
            actual_credential_data = tornado.ioloop.IOLoop.current().run_sync(
                lambda: token_manager.get(token=token))
            self.assertDictEqual(
                actual_credential_data['credential'],
                new_credential)
        except Exception:
            raise
        finally:
            self.delete_data(token=token)

    def test_update_credential_with_no_token(self):
        token = None
        new_credential = {
            'ACCESS_TOKEN': '0987654321',
            'APP_SECRET': '1234567890'
        }
        response = self.fetch(f'/token?token={token}',
                              headers={'content-type': 'application/json'},
                              body=json.dumps(new_credential), method='PUT')
        self.assertEqual(response.code, 500)

    def test_delete_credential(self):
        token = None
        try:
            # Set up
            payload = self.get_test_data()
            token = tornado.ioloop.IOLoop.current().run_sync(lambda: token_manager.issue(**payload))
            response = self.fetch(f'/token?token={token}', method="DELETE")
            code, data = response.code, json.loads(
                response.body, encoding='utf-8')
            self.assertEqual(code, 200)
            self.assertEqual(data['status'], 'ok')
            self.assertEqual(data['count'], 1)
        except Exception:
            raise

        finally:
            self.delete_data(token)

    def test_delete_credential_with_unknown_token(self):
        token = None
        response = self.fetch(f'/token?token={token}', method="DELETE")
        code = response.code
        self.assertEqual(code, 500)
