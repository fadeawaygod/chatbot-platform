import asyncio
import json
import unittest.mock

import tornado.web
import tornado.testing
import tornado.httpclient
from tornado.web import Application

from handler.message_history_handler import ReceiveAgentMessageHistoryHandler, ReceiveGatewayMessageHistoryHandler

application = tornado.web.Application([
        (r'/receive/agent', ReceiveAgentMessageHistoryHandler),
        (r'/receive/gateway', ReceiveGatewayMessageHistoryHandler)
])


class MessageHistoryHandlerTest(tornado.testing.AsyncHTTPTestCase):

    def setUp(self) -> None:
        super(MessageHistoryHandlerTest, self).setUp()

    def get_app(self) -> Application:
        return application

    @unittest.mock.patch('handler.message_history_handler.save_message_history')
    def test_receive_message_from_agent(self, mocked_save_message_history):
        future = asyncio.Future()
        future.set_result(None)
        mocked_save_message_history.return_value = future
        test_data = {
            'token': 'eyJhZ2VudF9pZCI6IDIxfQ==',
            'agent_id': 21,
            'source': {
                'from': 'web',
                'data': '{"credential": {"agent_id": 21}}'},
            'user_id': 'U4af4980629...',
            'messages': [{'type': 'text', 'message': 'Hi!'}]}
        with unittest.mock.patch('handler.service.message_history_service.Proxy.to_gateway') as mocked_proxy_to_gateway:
            future = asyncio.Future()
            future.set_result(None)
            mocked_proxy_to_gateway.return_value = future
            response = self.fetch(path='/receive/agent', method="POST", body=json.dumps(test_data))
            mocked_save_message_history.assert_called_with(source_from='web', agent_id=21, user_id='U4af4980629...', message='Hi!')
            mocked_proxy_to_gateway.assert_called_with(json.dumps(test_data).encode('utf-8'))
            self.assertEqual(response.code, 200)

    @unittest.mock.patch('handler.message_history_handler.save_message_history')
    def test_receive_message_from_gateway(self, mocked_save_message_history):
        future = asyncio.Future()
        future.set_result(None)
        mocked_save_message_history.return_value = future
        test_data = {
            'token': 'eyJhZ2VudF9pZCI6IDIxfQ==',
            'agent_id': 21,
            'source': {
                'from': 'web',
                'data': ''},
            'user_id': 'U4af4980629...',
            'message': 'Hello'}
        with unittest.mock.patch('handler.service.message_history_service.Proxy.to_agent') as mocked_proxy_to_agent:
            future = asyncio.Future()
            future.set_result(None)
            mocked_proxy_to_agent.return_value = future
            response = self.fetch(path='/receive/gateway', method="POST", body=json.dumps(test_data))
            mocked_save_message_history.assert_called_with(source_from='web', agent_id=21, user_id='U4af4980629...',
                                                           message='Hello')
            mocked_proxy_to_agent.assert_called_with(agent_id=21, body=json.dumps(test_data).encode('utf-8'))
            self.assertEqual(response.code, 200)
