import asyncio
import json
import unittest.mock

import tornado.ioloop
import tornado.testing
import tornado.httpclient
from tornado.web import Application

from handler.service.message_history_service import Proxy
from config import config


class ProxyToAgentTest(tornado.testing.AsyncHTTPTestCase):

    def setUp(self) -> None:
        super(ProxyToAgentTest, self).setUp()
        self.agent_id = 21
        self.test_data = {
            'token': 'eyJhZ2VudF9pZCI6IDIxfQ==',
            'agent_id': self.agent_id,
            'source': {
                'from': 'web',
                'data': ''},
            'user_id': 'U4af4980629...',
            'message': 'Hello'}

    def get_app(self) -> Application:
        return tornado.web.Application()

    def get_new_ioloop(self):
        return tornado.ioloop.IOLoop.instance()

    @unittest.mock.patch('handler.service.message_history_service.Proxy.fetch')
    @tornado.testing.gen_test
    async def test_proxy_with_response_200(self, mocked_fetch):
        agent_id = self.agent_id
        test_data = self.test_data.copy()
        future = asyncio.Future()
        future.set_result((200, ''))
        mocked_fetch.return_value = future
        status, body = await Proxy.to_agent(agent_id=agent_id, body=json.dumps(test_data))
        mocked_fetch.assert_called_with(f'{config.CHATBOT_AGENT_HOST}/agent/{agent_id}/callback', 'POST', json.dumps(test_data))
        self.assertEqual(status, 200)

    @unittest.mock.patch('tornado.httpclient.AsyncHTTPClient.fetch')
    @tornado.testing.gen_test
    async def test_proxy_fail_with_response_400(self, mocked_fetch):
        # Set up Client fetch patch.
        fake_request = tornado.httpclient.HTTPRequest(url='http://example.com', method='POST', body='{}')
        fake_response = tornado.httpclient.HTTPResponse(fake_request, code=400)
        future = asyncio.Future()
        future.set_result(fake_response)
        mocked_fetch.return_value = future
        with unittest.mock.patch('logging.Logger.error') as mocked_logging_error:
            agent_id = self.agent_id
            test_data = self.test_data.copy()
            status, body = await Proxy.to_agent(agent_id=agent_id, body=json.dumps(test_data))
            mocked_fetch.assert_called_once()
            mocked_logging_error.assert_called_once()
            self.assertEqual(status, 400)
