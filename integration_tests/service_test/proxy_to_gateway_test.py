import asyncio
import json
import unittest.mock

import tornado.ioloop
import tornado.testing
import tornado.httpclient
from tornado.web import Application

from handler.service.message_history_service import Proxy
from config import config


class ProxyToGatewayTest(tornado.testing.AsyncHTTPTestCase):

    def setUp(self) -> None:
        super(ProxyToGatewayTest, self).setUp()
        self.test_data = {
            'token': 'eyJhZ2VudF9pZCI6IDIxfQ==',
            'agent_id': 21,
            'source': {
                'from': 'web',
                'data': ''},
            'user_id': 'U4af4980629...',
            'messages': [{'type': 'text', 'message': 'Hi!'}]}

    def get_app(self) -> Application:
        return tornado.web.Application()

    def get_new_ioloop(self):
        return tornado.ioloop.IOLoop.instance()

    @unittest.mock.patch('handler.service.message_history_service.Proxy.fetch')
    @tornado.testing.gen_test
    async def test_proxy_to_gateway_response_200(self, mocked_fetch):
        test_data = self.test_data.copy()
        future = asyncio.Future()
        future.set_result((200, ''))
        mocked_fetch.return_value = future
        status, body = await Proxy.to_gateway(body=json.dumps(test_data))
        mocked_fetch.assert_called_with(f'{config.CHATBOT_GATEWAY_HOST}/gateway/reply', 'POST', json.dumps(test_data))
        self.assertEqual(status, 200)

    @unittest.mock.patch('tornado.httpclient.AsyncHTTPClient.fetch')
    @tornado.testing.gen_test
    async def test_proxy_fail_with_response_400(self, mocked_fetch):
        # Set up Client fetch patch.
        fake_request = tornado.httpclient.HTTPRequest(url='http://example.com', method='POST', body='{}')
        fake_response = tornado.httpclient.HTTPResponse(fake_request, code=400)
        future = asyncio.Future()
        future.set_result(fake_response)
        mocked_fetch.return_value = future
        with unittest.mock.patch('logging.Logger.error') as mocked_logging_error:
            test_data = self.test_data.copy()
            status, body = await Proxy.to_gateway(json.dumps(test_data))
            mocked_fetch.assert_called_once()
            mocked_logging_error.assert_called_once()
            self.assertEqual(status, 400)
