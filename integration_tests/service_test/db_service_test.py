import tornado.testing
from tornado.web import Application

import handler.service.db


class DBServiceTest(tornado.testing.AsyncHTTPTestCase):

    def setUp(self) -> None:
        super(DBServiceTest, self).setUp()
        self.session = handler.service.db.create_session(
                account='gateway',
                password='Silkrode@13@gateway',
                url='http://35.201.225.106')

    def tearDown(self) -> None:
        super().tearDown()

    def get_app(self) -> Application:
        pass

    @tornado.testing.gen_test
    async def test_insert_and_delete_data(self):
        target_collection = 'gateway_token'
        expected_title = '123'
        expected_name = 456
        data = await self.session.insert(collection=target_collection)\
            .data(column='title', value=expected_title)\
            .data(column='name', value=expected_name)\
            .execute()
        self.assertIsNotNone(data['data']['_id'])
        self.assertEqual(data['data']['title'], expected_title)
        self.assertEqual(data['data']['name'], expected_name)
        # clean data
        response = await self.session.delete(collection=target_collection) \
            .filter_by('_id', data['data']['_id'])\
            .execute()
        self.assertEqual(response['status'], 'ok')
        self.assertEqual(response['count'], 1)
