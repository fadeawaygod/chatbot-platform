# ChatbotPlatform

# Deploy Environment
- Ubuntu 18.04 LTS
- python 3.7.3
- pip 1.19

# Deploy Steps(manual)
1. pip install requirements.txt
2. submodule update --progress --init --recursive
3. copy templent.env to .env
4. configurate .env
5. python tornado_server.py

# Insert NLU data
- insert nlu data to bot
  1. Given bot id in insert_nlu_data.py
- 2. python3 insert_nlu_data.py
- transfer NLU data to export file Format
  1. Given OUTPUT_FILENAME in transfer_nlu_data_to_export_format.py
  2. python3 transfer_nlu_data_to_export_format.py