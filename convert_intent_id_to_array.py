import asyncio
from handler.service.session_manager import SessionManager


async def process():
    session = SessionManager.get_session()
    query_result = await session.query(collection='intent')\
        .projection_by(column='_id', show=False)\
        .projection_by(column='id', show=True)\
        .sort_by(column='updated', asc=True)\
        .execute()
    intent_ids = [intent['id']
                  for intent in query_result['data'] if 'id' in intent]

    for i, intent_id in enumerate(intent_ids):
        print(f'processing:{i}/{len(intent_ids)}')
        query_result = await session.query(collection='utterance')\
            .filter_by(column='intent_id', value=intent_id)\
            .projection_by(column='_id', show=False)\
            .projection_by(column='id', show=True)\
            .projection_by(column='intent_id', show=True)\
            .projection_by(column='intent_ids', show=True)\
            .sort_by(column='updated', asc=True)\
            .execute()
        for utterance in query_result['data']:
            if 'intent_ids' not in utterance:
                await session.update(collection='utterance')\
                    .filter_by(column='id', value=utterance['id'])\
                    .data(column='intent_ids', value=[utterance['intent_id']])\
                    .execute()

asyncio.run(process())
