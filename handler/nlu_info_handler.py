from handler.validation_handler_base import ValidationHandlerBase
from config.config import NLU_QUERY_URL_FOR_AGENT


class NluInfoHandler(ValidationHandlerBase):
    async def get(self):
        response_body = {
            'nlu_info': {
                'query_url': NLU_QUERY_URL_FOR_AGENT,
            }
        }
        self.write(self._get_success_result(response_body))
