from exception import IdInvalidError, NoPermissionError
from service_caller import DbFunction, ServiceCaller

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len


class UtterancesHandler(BotHandlerBase):
    ERROR_UTTERANCE_ID_INVALID = 'this utterance id is invalid'
    ERR_ENTITY_UNDER_UTTERANCE_FORMAT_MSG = 'error entity format'
    DEFAULT_SKIP = 0
    VALID_PATCH_UTTERANCE_KEY = ['text', 'entities', 'intent_ids']
    UTTERANCE_ENTITY_KEY = ['entity_id', 'text', 'start_position', 'len']

    async def get(self):
        def get_query_utterance_dic(intent_id):
            query_utterance_dic = {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {'intent_ids': {"$in": [intent_id]}},
                'projection': {
                    '_id': False,
                    'id': True,
                    'text': True,
                    'entities': True,
                    'intent_ids': True,
                },
                'sort': {
                    'updated': -1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_utterance_dic['limit'] = limit
            keyword = self.get_argument('keyword', '')
            if keyword:
                query_utterance_dic['filter']['text'] = {
                    "$regex": replace_reserved_char(keyword)}
            return query_utterance_dic

        def extract_utterances(query_result):
            return query_result['data']

        self._check_user_role_contains_editor()
        intent_id = self._try_get_url_parameter_value('intent_id')
        await self._check_user_permision_of_the_intent(intent_id, PermissionType.VIEW)

        query_utterance_dic = get_query_utterance_dic(intent_id)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_utterance_dic)
        utterances = extract_utterances(query_result)
        response_body = {
            'utterances': utterances
        }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body['len'] = await get_collection_len(self.COLLECTION_UTTERANCE, query_utterance_dic['filter'])
        self.write(self._get_success_result(response_body))

    async def post(self):
        def get_insert_utterance_dic(intent_ids):
            entities = self.json_args.get('entities', [])
            self._check_utterance_post_entity_valid(entities)
            return {
                'collection': self.COLLECTION_UTTERANCE,
                'data': {
                    'intent_ids': intent_ids,
                    'text': self._try_get_json_value('text'),
                    'entities': entities
                }
            }

        self._check_user_role_contains_editor()
        intent_ids = self._try_get_json_value('intent_ids')
        if intent_ids:
            bot_id = await self._check_user_permision_of_the_intent(intent_ids[0], PermissionType.EDIT)
            await self._set_nlu_data_is_modified(bot_id)

        insert_utterance_dic = get_insert_utterance_dic(intent_ids)
        result = await ServiceCaller.call_db_service(DbFunction.INSERT, insert_utterance_dic)
        await self.update_intent_utterance_count(intent_ids)
        new_utterance_id = result['data']['id']
        self.write(self._get_success_result(
            {'utterance_id': new_utterance_id}))

    async def delete(self, utterance_id):
        def get_delete_utterance_dic(utterance_id):
            delete_dic = {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {'id': utterance_id}
            }
            return delete_dic

        async def get_utterance(utterance_id: str) -> dict:
            query_utterance_dic = {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {'id': utterance_id},
                'projection': {
                    '_id': False,
                },
                'sort': {
                    'updated': -1
                }
            }
            query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_utterance_dic)
            if len(query_result['data']) == 0:
                raise IdInvalidError(id=utterance_id)
            return query_result['data'][0]

        self._check_user_role_contains_editor()
        bot_id = await self._check_user_permision_of_the_utterance(utterance_id, PermissionType.EDIT)
        utterance = await get_utterance(utterance_id)
        delete_utterance_dic = get_delete_utterance_dic(utterance_id)

        await ServiceCaller.call_db_service(DbFunction.DELETE, delete_utterance_dic)
        await self.update_intent_utterance_count(utterance['intent_ids'])

        await self._set_nlu_data_is_modified(bot_id)
        self.write(self._get_success_result())

    async def patch(self, utterance_id):
        def get_update_utterance_dic(utterance_id: str):
            data = self._extract_body(self.VALID_PATCH_UTTERANCE_KEY)
            self._check_utterance_patch_entity_valid(data)
            update_dic = {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {
                    'id': utterance_id
                },
                'data': data
            }
            return update_dic

        self._check_user_role_contains_editor()
        bot_id = await self._check_user_permision_of_the_utterance(utterance_id, PermissionType.EDIT)
        update_utterance_dic = get_update_utterance_dic(utterance_id)

        await ServiceCaller.call_db_service(DbFunction.UPDATE, update_utterance_dic)
        # bot_id could be empty if this utterance was imported by Import Dialog API
        if bot_id:
            await self._set_nlu_data_is_modified(bot_id)
        self.write(self._get_success_result())

    async def _check_user_permision_of_the_utterance(self, utterance_id: str, permission_type: PermissionType) -> str:
        def get_query_utterance_dic(utterance_id):
            return {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {'id': utterance_id},
                'projection': {
                    '_id': False,
                    'intent_ids': True
                }
            }

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, get_query_utterance_dic(utterance_id))
        if not query_result['data']:
            self._finish_with_failed_result(
                self.ERROR_UTTERANCE_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

        intent_ids = query_result['data'][0]['intent_ids']
        bot_id = ''
        for intent_id in intent_ids:
            bot_id = await self._check_user_permision_of_the_intent(intent_id, permission_type)

        return bot_id

    def _check_utterance_post_entity_valid(self, entities):
        for entity in entities:
            if len(set(self.UTTERANCE_ENTITY_KEY) - set(list(entity.keys()))) != 0:
                self._finish_with_failed_result(
                    self.ERR_ENTITY_UNDER_UTTERANCE_FORMAT_MSG,
                    self.HTTP_BAD_REQUEST_ERROR_CODE,
                    exception_class=NoPermissionError)

    def _check_utterance_patch_entity_valid(self, data):
        if 'entities' in data:
            for entity in data['entities']:
                if len(set(self.UTTERANCE_ENTITY_KEY) - set(list(entity.keys()))) != 0:
                    self._finish_with_failed_result(
                        self.ERR_ENTITY_UNDER_UTTERANCE_FORMAT_MSG,
                        self.HTTP_BAD_REQUEST_ERROR_CODE,
                        exception_class=NoPermissionError)
