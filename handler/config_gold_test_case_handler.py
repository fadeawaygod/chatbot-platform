from typing import Optional, Awaitable

from .bot_handler_base import BotHandlerBase, PermissionType
from .service.session_manager import SessionManager
from .repository.config_gold_test_case import ConfigGoldTestCaseRepository


class ConfigGoldTestCaseHandler(BotHandlerBase):

    collection = 'config_gold_test_case'

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def ensure_request_payload(self, fields: [str]):
        try:
            for field in fields:
                assert field in self.json_args, f'{field} must in request payload.'
        except AssertionError as e:
            self._finish_with_failed_result(err_msg=str(e), status_code=400)

    async def get(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        data = await ConfigGoldTestCaseRepository.get_all_test_case(bot_id=bot_id)
        self._finish_with_success_result(return_dict={
            'count': len(data),
            'result': data
        })

    async def post(self, bot_id):
        try:
            await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
            self.ensure_request_payload(fields=['corpus'])
            result = await ConfigGoldTestCaseRepository.insert_a_test_case(bot_id=bot_id,
                                                                           corpus=self.json_args['corpus'])
            self._finish_with_success_result(return_dict=result)
        except AssertionError as e:
            self._finish_with_failed_result(err_msg=str(e), status_code=400)

    async def patch(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        self.ensure_request_payload(fields=['id', 'corpus'])
        session = SessionManager.get_session()
        result = await session.update(collection=self.collection)\
            .data(column='corpus', value=self.json_args['corpus'])\
            .filter_by(column='bot_id', value=bot_id)\
            .filter_by(column='id', value=self.json_args['id'])\
            .execute()
        self._finish_with_success_result(return_dict=result)

    async def delete(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        self.ensure_request_payload(fields=['id'])
        session = SessionManager.get_session()
        result = await session.delete(collection=self.collection)\
            .filter_by(column='bot_id', value=bot_id)\
            .filter_by(column='id', value=self.json_args['id'])\
            .execute()
        self._finish_with_success_result(return_dict=result)
