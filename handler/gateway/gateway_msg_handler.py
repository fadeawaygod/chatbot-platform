import json
import logging
from typing import Optional, Awaitable

from config.config import CHATBOT_GATEWAY_HOST
from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager
from service_caller import fetch


class GatewayMessageHandler(HandlerBase):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    async def post(self, passage: str):

        token = self.get_argument(name='token', default=None)
        payload = self.json_args
        url = f"{CHATBOT_GATEWAY_HOST}/gateway/passage/{passage}/send_message?token={token}"
        code, response_body = await fetch(method="POST", url=url,headers={}, body=json.dumps(payload, ensure_ascii=False))
        self.write(self._get_success_result(json.loads(response_body)))


