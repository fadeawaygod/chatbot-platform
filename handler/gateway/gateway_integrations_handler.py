from typing import Optional, Awaitable

from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager


class GatewayIntegrationsHandler(HandlerBase):
    """
    TODO: validation.
    """
    COLLECTION = 'integration'
    ERROR_INTEGRATION_IS_NOT_FOUND = 'this integration id:{integration_id} is invalid or disabled.'

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    async def get(self, integration_id):
        if not integration_id:
            integration_id = self.get_argument(name='id', default=None)
        bot_id = self.get_argument(name='bot_id', default=None)
        passage = self.get_argument(name='passage', default=None)

        session = SessionManager.get_session()
        session.query(collection=self.COLLECTION)
        if bot_id and passage:
            session.filter_by(column='bot_id', value=bot_id)\
                .filter_by(column='passage', value=passage)
        else:
            assert integration_id, 'integration can not be None.'
            session.filter_by(column='id', value=integration_id)
        query_result = await session.projection_by(column='_id', show=False) \
            .projection_by(column='id', show=True) \
            .projection_by(column='bot_id', show=True) \
            .projection_by(column='passage', show=True) \
            .projection_by(column='credential', show=True) \
            .projection_by(column='is_enable', show=True) \
            .sort_by(column='updated', asc=True)\
            .limit(limit=1)\
            .execute()

        data = query_result.get('data', [])
        if not data:
            self._finish_with_failed_result(
                self.ERROR_INTEGRATION_IS_NOT_FOUND.format(
                    integration_id=integration_id),
                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)

        response_body = {
            'integration': data[0]
        }
        self._finish_with_success_result(return_dict=response_body)
