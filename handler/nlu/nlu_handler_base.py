from handler.system_user_validation_handler_base import SystemUserValidationHandlerBase
from service_caller import ServiceCaller, DbFunction


class NluHandlerBase(SystemUserValidationHandlerBase):
    COLLECTION_BOT = 'bot'
    COLLECTION_INTENT = 'intent'
    COLLECTION_ENTITY = 'entity'
    COLLECTION_UTTERANCE = 'utterance'
    COLLECTION_TRAIN_LOG = 'train_log'
    COLLECTION_USER = 'user'
    COLLECTION_NLU_RESOURCE = 'nlu_resource'
    DEFAULT_SKIP = 0
    ERROR_NLU_MODEL_ID_IS_INVALID = 'this nlu_model_id is invalid'

    async def _get_bot_id_by_nlu_id(self, nlu_model_id: str) -> str:
        def get_query_bot_dic(nlu_model_id):
            query_intent_dic = {
                'collection': self.COLLECTION_BOT,
                'filter': {'tmp_nlu_id': nlu_model_id},
                'projection': {
                    '_id': False,
                    'id': True
                },
                'sort': {
                    'updated': 1
                },
                'limit': 1
            }
            return query_intent_dic

        query_bot_dic = get_query_bot_dic(nlu_model_id)
        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_bot_dic)
        if not query_result['data']:
            self._finish_with_failed_result(self.ERROR_NLU_MODEL_ID_IS_INVALID)
        return query_result['data'][0]['id']
