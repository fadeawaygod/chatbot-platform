from handler.nlu.nlu_handler_base import NluHandlerBase
from service_caller import ServiceCaller, DbFunction


class NluUtterancesHandler(NluHandlerBase):
    async def get(self, nlu_model_id):
        def get_query_utterance_dic(intent_id):
            query_utterance_dic = {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {'intent_ids': {"$in": [intent_id]}},
                'projection': {
                    '_id': False,
                    'id': True,
                    'text': True,
                    'entities': True
                },
                'sort': {
                    'updated': 1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_utterance_dic['limit'] = limit
            return query_utterance_dic

        async def append_utterance_len(intent_id, response_body):
            count_utterance_dic = get_count_utterance_dic(intent_id)
            query_result = await ServiceCaller.call_db_service(DbFunction.COUNT, count_utterance_dic)
            response_body['len'] = query_result['count']
            return response_body

        def get_count_utterance_dic(intent_id):
            query_utterance_dic = {
                'collection': self.COLLECTION_UTTERANCE,
                'filter': {'intent_ids': {"$in": [intent_id]}}
            }
            return query_utterance_dic

        intent_id = self._try_get_url_parameter_value('intent_id')

        query_utterance_dic = get_query_utterance_dic(intent_id)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_utterance_dic)
        utterances = query_result['data']
        response_body = {
            'utterances': utterances
        }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body = await append_utterance_len(intent_id, response_body)
        self.write(self._get_success_result(response_body))
