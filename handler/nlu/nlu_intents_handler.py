from handler.nlu.nlu_handler_base import NluHandlerBase
from service_caller import ServiceCaller, DbFunction


class NluIntentsHandler(NluHandlerBase):
    async def get(self, nlu_model_id):
        def get_query_intent_dic(bot_id):
            query_intent_dic = {
                'collection': self.COLLECTION_INTENT,
                'filter': {'bot_id': bot_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'name': True,
                    'priority': True,
                    'entities': True
                },
                'sort': {
                    'updated': 1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_intent_dic['limit'] = limit
            return query_intent_dic

        bot_id = await self._get_bot_id_by_nlu_id(nlu_model_id)

        query_intent_dic = get_query_intent_dic(bot_id)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_intent_dic)
        intents = query_result['data']
        response_body = {
            'intents': intents
        }
        self.write(self._get_success_result(response_body))
