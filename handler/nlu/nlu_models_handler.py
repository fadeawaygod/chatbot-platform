import time
from typing import List

from exception import TrainingFailedError, IdInvalidError, NLUResourceNotFoundByNLUIDError
from handler.helper.notification import notify
from handler.nlu.nlu_handler_base import NluHandlerBase
from handler.service.session_manager import SessionManager
from service_caller import NluFunction, NotificationFunction, ServiceCaller


class NluModelsHandler(NluHandlerBase):
    ERROR_NO_SUCH_STATUS = 'your status is invalid'
    BOT_STATUS_FAILED = 'failed'
    BOT_STATUS_UNTRAINED = 'untrained'
    DEPLOY_STATUS_OFFLINE = 'offline'

    async def patch(self, nlu_id):
        """This api is for nlu service to update nlu model status.
        When status is data_fetched, it should call nlu train service.
        When status is trained, it should call agent create service and update status service.
        When status is failed, it should call update status service.

        Args:
            nlu_id ([type]): [description]
        """
        async def get_bot(nlu_id: str) -> dict:
            session = SessionManager.get_session()
            query_result = await session.query(collection=self.COLLECTION_BOT)\
                .filter_by(column='tmp_nlu_id', value=nlu_id)\
                .projection_by(column='_id', show=False)\
                .execute()
            if not query_result['data']:
                raise IdInvalidError(id=nlu_id)
            return query_result['data'][0]

        async def get_bot_by_id(id: str) -> dict:
            session = SessionManager.get_session()
            query_result = await session.query(collection=self.COLLECTION_BOT)\
                .filter_by(column='id', value=id)\
                .projection_by(column='_id', show=False)\
                .execute()

            return query_result['data'][0] if len(query_result['data']) > 0 else {}

        async def insert_train_log(bot_id: str, bot_status: str, train_detail={}, error='', error_code=0):
            session = SessionManager.get_session()
            session = session.insert(collection=self.COLLECTION_TRAIN_LOG)\
                .data(column='bot_id', value=bot_id)\
                .data(column='status', value=bot_status)

            if error:
                session.data(column='error', value=error)
                session.data(column='error_code', value=error_code)

            if train_detail:
                session.data(column='train_detail', value=train_detail)

            await session.execute()

        async def update_bot_and_notify(nlu_id: str, update_obj: dict):
            await update_bot(nlu_id, update_obj)
            bot = await get_bot(nlu_id)
            if(status == self.BOT_STATUS_FAILED):
                await insert_train_log(
                    bot['id'],
                    update_obj['train_status'],
                    {},
                    update_obj['last_failed_log'],
                    TrainingFailedError.code)
            await call_notification_service(
                bot,
                update_obj['train_status'],
                update_obj.get('last_failed_log', ''))

        async def call_notification_service(bot, status, failed_log=""):
            content = {'status': status, 'bot_name': bot['name']}
            if failed_log:
                content['failed_log'] = failed_log
            await notify(
                NotificationFunction.UPDATE_TRAIN_STATUS,
                bot['owner'],
                bot['id'],
                content=content)

        async def update_bot(nlu_id: str, update_obj: dict):
            session = SessionManager.get_session()
            session = session.update(collection=self.COLLECTION_BOT)\
                .filter_by(column='tmp_nlu_id', value=nlu_id)

            for key, value in update_obj.items():
                session = session.data(column=key, value=value)
            await session.execute()

        async def update_bot_by_id(id: str, update_obj: dict):
            session = SessionManager.get_session()
            session = session.update(collection=self.COLLECTION_BOT)\
                .filter_by(column='id', value=id)

            for key, value in update_obj.items():
                session = session.data(column=key, value=value)
            await session.execute()

        async def update_nlu_resource_and_notify(bot: dict, is_success: bool):
            user_id = bot['owner']
            nlu_id = bot['tmp_nlu_id']
            bot_id = bot['id']
            last_trained_time = 0
            current_bot_id = ''
            nlu_token = ''
            nlu_resource = await get_nlu_resource(nlu_id)

            if is_success:
                # unix time in second
                last_trained_time = int(time.time())
                nlu_token = bot['tmp_nlu_token']
                if nlu_resource['bot_id'] != bot_id:
                    old_bot = await get_bot_by_id(nlu_resource['bot_id'])
                    if old_bot:
                        await update_bot_by_id(
                            old_bot['id'],
                            {'train_status': self.BOT_STATUS_UNTRAINED}
                        )
                        await notify(
                            NotificationFunction.UPDATE_TRAIN_STATUS,
                            user_id,
                            old_bot['id'],
                            content={
                                'status': self.BOT_STATUS_UNTRAINED,
                                'bot_name': old_bot['name']
                            }
                        )
                    current_bot_id = bot_id

            await update_nlu_resource(nlu_id, nlu_token, last_trained_time, current_bot_id)

        async def get_nlu_resource(nlu_id: str) -> List[dict]:
            session = SessionManager.get_session()
            session = session.query(collection=self.COLLECTION_NLU_RESOURCE)\
                .filter_by(column='training_nlu_id', value=nlu_id)\
                .projection_by(column='_id', show=False)
            result = await session.execute()
            if len(result['data']) == 0:
                raise NLUResourceNotFoundByNLUIDError(nlu_id=nlu_id)
            return result['data'][0]

        async def update_nlu_resource(
                nlu_id: str,
                nlu_token: str,
                last_trained_time: int,
                current_bot_id: str,
        ):
            session = SessionManager.get_session()
            session = session.update(collection=self.COLLECTION_NLU_RESOURCE)\
                .filter_by(column='training_nlu_id', value=nlu_id) \
                .data(column='is_training', value=False) \
                .data(column='training_nlu_id', value='')

            if nlu_token:
                session = session .data(
                    column='nlu_token', value=nlu_token)
            if last_trained_time:
                session = session .data(
                    column='last_trained_time', value=last_trained_time)
            if current_bot_id:
                session = session .data(column='bot_id', value=current_bot_id)

            await session.execute()

        async def notify_deploy_status_reset(bot):
            content = {'status': self.DEPLOY_STATUS_OFFLINE,
                       'bot_name': bot['name']}
            await notify(
                NotificationFunction.UPDATE_DEPLOY_STATUS,
                bot['owner'],
                bot['id'],
                content=content)

        try:
            status = self._try_get_json_value('status')
            update_obj = {
                'train_status': status,
            }
            bot = await get_bot(nlu_id)
            bot_id = bot['id']
            if status == 'data_fetched':
                await ServiceCaller.call_nlu_service(NluFunction.TRAIN, nlu_id=nlu_id)
            elif status == 'trained':
                train_detail = self.json_args.get('train_detail', {})

                update_obj['is_nlu_data_modified'] = False
                update_obj['nlu_id'] = bot['tmp_nlu_id']
                update_obj['nlu_token'] = bot['tmp_nlu_token']
                update_obj['train_detail'] = train_detail
                await update_bot_and_notify(nlu_id, update_obj)
                await insert_train_log(bot['id'], 'success', train_detail)
                await update_nlu_resource_and_notify(bot, True)
                await notify_deploy_status_reset(bot)
            elif status == 'failed':
                update_obj['last_failed_log'] = self.json_args.get(
                    'note', 'unknown error')
                await update_bot_and_notify(nlu_id, update_obj)
                await update_nlu_resource_and_notify(bot, False)
            else:
                self._finish_with_failed_result(
                    self.ERROR_NO_SUCH_STATUS, self.HTTP_BAD_REQUEST_ERROR_CODE)
        except Exception as e:
            update_obj['train_status'] = 'failed'
            update_obj[
                'last_failed_log'] = f'Train failed after {status}, log:{str(e)}'
            await update_bot_and_notify(nlu_id, update_obj)
            await update_nlu_resource_and_notify(bot, False)

        self.write(self._get_success_result())
