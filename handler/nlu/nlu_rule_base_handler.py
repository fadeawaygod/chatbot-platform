from typing import Optional, Awaitable

from ..handler_base import HandlerBase
from ..service import rule_base_nlu_service, nlu_train_data_service


class RuleBaseNlu(HandlerBase):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    async def get(self):
        bot_id = self.get_argument(name='bot_id')
        msg_id = self.get_argument(name='msg_id', default=None)
        query_msg = self.get_argument(name='q')
        training_data = await nlu_train_data_service.get_train_data(bot_id=bot_id)
        model = rule_base_nlu_service.StringMatcher(**training_data)
        result = model.predict(query_msg=query_msg)
        self.write({
            'msgid': msg_id,
            '_text': query_msg,
            'retStr': {
                'intents': result.intent,
                'entities': result.entities
            }
        })
