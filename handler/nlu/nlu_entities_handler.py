from handler.nlu.nlu_handler_base import NluHandlerBase
from service_caller import ServiceCaller, DbFunction


class NluEntitiesHandler(NluHandlerBase):
    async def get(self, nlu_model_id):
        def get_query_entity_dic(bot_id):
            query_entity_dic = {
                'collection': self.COLLECTION_ENTITY,
                'filter': {'bot_id': bot_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'name': True,
                    'entries': True
                },
                'sort': {
                    'updated': 1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_entity_dic['limit'] = limit
            return query_entity_dic

        bot_id = await self._get_bot_id_by_nlu_id(nlu_model_id)

        query_entity_dic = get_query_entity_dic(bot_id)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_entity_dic)
        entities = query_result['data']
        response_body = {
            'entities': entities
        }
        self.write(self._get_success_result(response_body))
