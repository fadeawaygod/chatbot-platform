from typing import List, Tuple

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager
from exception import IntentNotExistError


class ImportIntentsHandler(BotHandlerBase):
    DEFAULT_INTENT_PRIORITY = 10

    async def post(self, bot_id):
        def extract_input_intent_names(tsv_body: List[dict]) -> List[str]:
            intent_names = set()
            for line in tsv_body:
                intent_names.add(line['intent'])
            return list(intent_names)

        async def filter_exist_intents(bot_id: str, input_intent_names: List[str], exist_intents: List[dict]):
            exist_intent_names = set([intent['name']
                                      for intent in exist_intents])
            return [input_intent_name for input_intent_name in input_intent_names if input_intent_name not in exist_intent_names]

        async def get_intents(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_INTENT)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            return result['data']

        async def import_utterances(tsv_body: List[dict], exist_intents: List[dict]):
            utterance_to_be_inserted, related_intent_ids = self._convert_to_utterance_insert_many_format(
                tsv_body, exist_intents)
            await self._insert_many_utterance(utterance_to_be_inserted)
            return related_intent_ids

        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        input_intent_names = extract_input_intent_names(self.tsv_body)
        exist_intents = await get_intents(bot_id)
        input_intent_names = await filter_exist_intents(bot_id, input_intent_names, exist_intents)

        if input_intent_names:
            intents_data = self._convert_to_intent_insert_many_format(
                input_intent_names, bot_id)
            intent_responses = await self._insert_many_intent(intents_data)
            exist_intents.extend(intent_responses)

        related_intent_ids = await import_utterances(self.tsv_body, exist_intents)
        await self.update_intent_utterance_count(related_intent_ids)

        self.write(self._get_success_result())

    async def _insert_many_intent(self, intent_data) -> List[dict]:
        """ insert intents to db, return new intents
        """
        session = SessionManager.get_session()
        intent_response = await session.insert_many(collection=self.COLLECTION_INTENT)\
            .data(column='ignore', value=intent_data)\
            .execute()
        return intent_response['data']

    def _convert_to_intent_insert_many_format(self, input_intent_names: List[str], bot_id: str) -> List[dict]:
        return [
            {
                'name': input_intent_name,
                'bot_id': bot_id,
                'priority':  self.DEFAULT_INTENT_PRIORITY,
                'entities': [],
                'utterance_count': 0
            } for input_intent_name in input_intent_names
        ]

    def _convert_to_utterance_insert_many_format(self, tsv_body: List[dict], exist_intents: List[dict]) -> Tuple[List[dict], List[str]]:
        result = []
        related_intent_ids = set()
        for line in tsv_body:
            intent_name = line['intent']
            utterance_text = line['utterance']
            intent_id = next(
                intent['id'] for intent in exist_intents if intent['name'] == intent_name)
            if not intent_id:
                raise IntentNotExistError(name=intent_name)
            result.append({
                'intent_ids': [intent_id],
                'text': utterance_text,
                'entities': [],
                'utterance_count': 0
            })
            related_intent_ids.add(intent_id)
        return result, list(related_intent_ids)

    async def _insert_many_utterance(self, utterance_data: List[dict]):
        """ insert utterances to db
        """
        session = SessionManager.get_session()
        await session.insert_many(collection=self.COLLECTION_UTTERANCE)\
            .data(column='ignore', value=utterance_data)\
            .execute()
