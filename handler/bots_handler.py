
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len
# import ServiceCaller must be after get_collection_len to prevent unittest import error
from service_caller import DbFunction, ServiceCaller


class BotsHandler(BotHandlerBase):
    ERROR_ONLY_EDITOR_CAN_DO_THIS_OPERATION = 'only editor can do this operation.'
    ERROR_BOT_IS_NOT_FOUND = 'bot:{bot_id} is not found'
    DEFAULT_SKIP = 0
    DEFAULT_BOT_DEPLOY_STATUS = 'offline'
    DEFAULT_BOT_TRAIN_STATUS = 'untrained'
    VALID_PATCH_BOT_KEY = ['name', 'config', 'description',
                           'use_fixed_nlu', 'nlu_id', 'nlu_token', 'intent_threshold', 'nlu_endpoint']
    DEFAULT_INTENT_THRESHOLD = 0.9
    DEFAULT_NLU_ENDPOINT = {
        "nlu_name": "default nlu",
        "http_type": "GET",
        "url": "",
        "header": {
            "token": ""
        },
        "url_parameter": {},
        "body": "",
        "endpoint_type": "AI"
    }

    async def get(self, bot_id=''):
        def get_query_bot_dic(user_id, bot_id, is_owner):
            query_bot_dic = {
                'collection': self.COLLECTION_BOT,
                'filter': {},
                'projection': {
                    '_id': False,
                    'id': True,
                    'owner': True,
                    'accessors': True,
                    'name': True,
                    'config': True,
                    'deploy_status': True,
                    'train_status': True,
                    'last_failed_log': True,
                    'is_nlu_data_modified': True,
                    'train_detail': True,
                    'intent_threshold': True,
                    'nlu_endpoint': True,
                    'nlu_id': True,
                    'nlu_token': True,
                    'description': True,
                    'use_fixed_nlu': True,
                    'created': True,
                },
                'sort': {
                    'updated': -1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))

            if limit:
                query_bot_dic['limit'] = limit
            if bot_id:
                query_bot_dic['filter']['id'] = bot_id
            else:
                if is_owner:
                    query_bot_dic['filter']['owner'] = user_id
                else:
                    query_bot_dic['filter'][f'accessors.{user_id}'] = {
                        '$exists': True}

            keyword = self.get_argument('keyword', '')
            if keyword:
                query_bot_dic['filter']['name'] = {
                    "$regex": replace_reserved_char(keyword)}
            return query_bot_dic

        self._check_user_role_contains_editor()
        user_id = self._user['uid']
        is_owner_str = self.get_argument('is_owner', '').lower()
        is_owner = False if is_owner_str == 'false' else True

        query_bot_dic = get_query_bot_dic(user_id, bot_id, is_owner)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_bot_dic)
        bots = query_result['data']
        if bot_id:
            if not bots:
                self._finish_with_failed_result(
                    self.ERROR_BOT_IS_NOT_FOUND.format(bot_id=bot_id),
                    status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
            response_body = {
                'bot': bots[0]
            }
        else:
            response_body = {
                'bots': bots
            }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body['len'] = await get_collection_len(self.COLLECTION_BOT, query_bot_dic['filter'])

        self.write(self._get_success_result(response_body))

    async def post(self):
        def get_insert_bot_dic():
            return {
                'collection': self.COLLECTION_BOT,
                'data': {
                    'owner': self._user['uid'],
                    'accessors': {},
                    'name': self._try_get_json_value('name'),
                    'config': self._try_get_json_value('config'),
                    'deploy_status': self.DEFAULT_BOT_DEPLOY_STATUS,
                    'train_status': self.DEFAULT_BOT_TRAIN_STATUS,
                    'description': self._try_get_json_value('description'),
                    'nlu_id': '',
                    'is_nlu_data_modified': True,
                    'last_failed_log': '',
                    'intent_threshold': self.DEFAULT_INTENT_THRESHOLD,
                    'nlu_endpoint': self.DEFAULT_NLU_ENDPOINT,
                }
            }

        async def insert_default_credentials(bot_id):
            for query_dic in get_insert_integrations_dics(bot_id):
                await ServiceCaller.call_db_service(DbFunction.INSERT, query_dic)

        def get_insert_integrations_dics(bot_id):
            return [
                {
                    'collection': self.COLLECTION_INTEGRATION,
                    'data': {
                        'bot_id': bot_id,
                        'passage': 'demo',
                        'credential': {},
                        'is_enable': True,
                    }
                }
            ]

        self._check_user_role_contains_editor()

        insert_bot_dic = get_insert_bot_dic()
        result = await ServiceCaller.call_db_service(DbFunction.INSERT, insert_bot_dic)
        new_bot_id = result['data']['id']
        await insert_default_credentials(new_bot_id)
        self.write(self._get_success_result({'bot_id': new_bot_id}))

    async def delete(self, bot_id):
        def get_delete_bot_dic(bot_id):
            delete_dic = {
                'collection': self.COLLECTION_BOT,
                'filter': {
                    'id': bot_id,
                    'owner': self._user['uid']}
            }
            return delete_dic

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.OWNER)
        delete_bot_dic = get_delete_bot_dic(bot_id)

        await ServiceCaller.call_db_service(DbFunction.DELETE, delete_bot_dic)
        self.write(self._get_success_result())

    async def patch(self, bot_id):
        def get_update_bot_dic(bot_id):
            data = self._extract_body(self.VALID_PATCH_BOT_KEY)
            update_dic = {
                'collection': self.COLLECTION_BOT,
                'filter': {
                    'id': bot_id,
                    'owner': self._user['uid']},
                'data': data
            }
            return update_dic

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.OWNER)

        update_bot_dic = get_update_bot_dic(bot_id)

        await ServiceCaller.call_db_service(DbFunction.UPDATE, update_bot_dic)
        self.write(self._get_success_result())
