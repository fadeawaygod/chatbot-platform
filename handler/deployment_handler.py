
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.agent import set_up_agent
from handler.service.session_manager import SessionManager


class DeploymentHandler(BotHandlerBase):
    async def post(self):
        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        config_history_data = self.json_args.get('config_history_data', {})
        bot = await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        await set_up_agent(bot, config_history_data)
        self.write(self._get_success_result())
