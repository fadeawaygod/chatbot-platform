import json

import tornado.web

from error_code import NO_VALID_KEY_ERR
from exception import MissRequiredParameterError, ParseTsvFailedError


class HandlerBase(tornado.web.RequestHandler):
    HTTP_SUCCESS_CODE = 200
    HTTP_INTERNAL_ERROR_CODE = 500
    HTTP_BAD_REQUEST_ERROR_CODE = 400
    HTTP_UNAUTHORIZED_ERROR_CODE = 401
    HTTP_FORBIDDEN_ERROR_CODE = 403
    HTTP_NOT_FOUND_ERROR_CODE = 404

    ERROR_KEY_NOT_FOUND = 'key not found:{key_name}'
    ERROR_MISSING_REQUIRED_PARARMETER = 'missing required pararmeter:{parameter}'

    def prepare(self) -> None:
        '''override the RequestHandler's prepare'''
        def parse_request_body():
            if not self.request.body:
                return
            content_type = self.request.headers.get("Content-Type", "").lower()

            if content_type.startswith("application/json"):
                self.json_args = parse_json_body()
            if content_type.startswith("text/tab-separated-values"):
                self.tsv_body = parse_tsv_body()

        def parse_json_body() -> dict:
            return json.loads(self.request.body.decode('utf-8'))

        def parse_tsv_body() -> list:
            """
            input :
            header1\theader2
            data1-1\tdata1-2
            data2-1\tdata2-2

            Returns: e.g.
               [
                   {
                       "header1": "data1-1",
                       "theader2": "data1-2"
                   },
                   {
                       "header1": "data2-1",
                       "theader2": "data2-2"
                   },
               ]
            """
            body = self.request.body.decode('utf-8')
            if not body:
                return []
            result = []
            lines = body.split('\n')
            fields = lines[0].strip().split('\t')
            for index, line in enumerate(lines[1:]):
                items = line.strip().split('\t')
                if len(items) != len(fields):
                    raise ParseTsvFailedError(line=index)
                result.append(
                    {key: value for key, value in zip(fields, items)}
                )

            return result

        parse_request_body()

    def set_default_headers(self):
        def set_header_for_CORS():
            self.set_header("Access-Control-Allow-Origin", "*")
            self.set_header("Access-Control-Allow-Headers",
                            "x-requested-with, content-type, Authorization")
            self.set_header('Access-Control-Allow-Methods',
                            'OPTIONS, POST, GET, PUT, DELETE, PATCH')

        set_header_for_CORS()

    def write_error(self, *args, **kwargs):
        exception = kwargs['exc_info'][1]

        self._finish_with_failed_result(
            getattr(exception, 'message', str(exception)),
            status_code=getattr(
                exception,
                'status_code',
                self.HTTP_INTERNAL_ERROR_CODE),
            is_raise=False,
            error_code=getattr(exception, 'code', 0)
        )

    def options(self, *args, **kargs):
        """for CORS request
        """
        self.set_status(204)
        self.finish()

    @staticmethod
    def _get_error_result(msg: str = ''):
        return {'status': 'error', 'error': msg}

    @staticmethod
    def _get_success_result(return_dic: dict = None) -> dict:
        success_result = {'status': 'ok'}
        if return_dic and isinstance(return_dic, dict):
            success_result.update(return_dic)
        return success_result

    def _finish_with_success_result(self, code: int = 200, return_dict: dict = None):
        self.set_status(code)
        self.finish(self._get_success_result(return_dic=return_dict))

    def _finish_with_failed_result(self, err_msg: str,  status_code=500, is_raise=True, exception_class=Exception, error_code=0):
        self.set_status(status_code)
        faild_result = {'status': 'error',
                        'error': err_msg,
                        'error_code': error_code}
        self.finish(faild_result)
        if is_raise:
            raise exception_class(err_msg)

    def _try_get_json_value(self, key: str) -> str:
        if key not in self.json_args:
            raise MissRequiredParameterError(parameter=key)

        return self.json_args[key]

    def _try_get_url_parameter_value(self, key: str) -> str:
        value = self.get_argument(key, '')
        if not value:
            err_msg = self.ERROR_MISSING_REQUIRED_PARARMETER.format(
                parameter=key)
            self._finish_with_failed_result(
                err_msg, self.HTTP_BAD_REQUEST_ERROR_CODE)
        return value

    def _extract_body(self, valid_keys: list) -> dict:
        result = {key: value for key, value in self.json_args.items()
                  if key in valid_keys}
        if not result:
            self._finish_with_failed_result(
                NO_VALID_KEY_ERR['message'],
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=NO_VALID_KEY_ERR['code'])
        return result
