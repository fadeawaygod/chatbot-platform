from typing import List

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager
from exception import IntentNotExistError


class ImportUtterancesHandler(BotHandlerBase):
    DEFAULT_INTENT_PRIORITY = 10

    async def post(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        intent_id = self._try_get_json_value('intent_id')
        utterance_texts = self._try_get_json_value('utterances')
        utterances_to_be_inserted = self._convert_to_utterance_insert_many_format(
            utterance_texts, intent_id)
        await self._insert_many_utterance(utterances_to_be_inserted)
        await self.update_intent_utterance_count([intent_id])

        self.write(self._get_success_result())

    def _convert_to_utterance_insert_many_format(self, utterance_texts: List[str], intent_id: str) -> List[dict]:
        return [
            {
                'intent_ids': [intent_id],
                'text': utterance_text,
                'entities': [],
                'utterance_count': 0
            } for utterance_text in utterance_texts
        ]

    async def _insert_many_utterance(self, utterance_data: List[dict]):
        """ insert utterances to db
        """
        session = SessionManager.get_session()
        await session.insert_many(collection=self.COLLECTION_UTTERANCE) \
            .data(column='ignore', value=utterance_data) \
            .execute()
