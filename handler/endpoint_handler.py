from exception import NoPermissionError
from handler.bot_handler_base import BotHandlerBase
from service_caller import DbFunction, ServiceCaller
from .service.session_manager import SessionManager
from handler.bot_handler_base import BotHandlerBase


class EndpointHandler(BotHandlerBase):

    async def get(self):
        self._check_user_role_contains_editor()
        user_id = self._user['uid']
        session = SessionManager.get_session()
        result = await session.query(collection=self.COLLECTION_ENDPOINT)\
            .filter_by('user_id', user_id)\
            .execute()
        result_data = result.get('data', [])
        response_body = {
            'endpoint': result_data
        }
        self.write(self._get_success_result(response_body))
        self.finish()

    async def post(self):
        self._check_user_role_contains_editor()
        session = SessionManager.get_session()
        user_id = self._user['uid']
        http_type = self._try_get_json_value('http_type')
        nlu_name = self._try_get_json_value('nlu_name')
        url = self._try_get_json_value('url')
        header = self._try_get_json_value('header')
        body = self._try_get_json_value('body')
        endpoint_type = self._try_get_json_value('endpoint_type')
        try:
            parameter = self._try_get_json_value('parameter')
        except:
            parameter = {}
        result = await session.insert(collection=self.COLLECTION_ENDPOINT)\
            .data(column='user_id', value=user_id)\
            .data(column='http_type', value=http_type)\
            .data(column='url', value=url)\
            .data(column='nlu_name', value=nlu_name)\
            .data(column='header', value=header)\
            .data(column='body', value=body)\
            .data(column='parameter', value=parameter)\
            .data(column='endpoint_type', value=endpoint_type)\
            .execute()
        new_endpoint_id = result['data']['id']
        self.write(self._get_success_result({'endpoint_id': new_endpoint_id}))
        self.finish()

    async def delete(self):
        self._check_user_role_contains_editor()
        session = SessionManager.get_session()
        id = self._try_get_json_value('id')
        self._check_user_has_nlu_id(id)
        await session.delete(collection=self.COLLECTION_ENDPOINT)\
            .filter_by(column='id', value=id) \
            .execute()
        self.write(self._get_success_result())
        self.finish()

    async def patch(self):
        self._check_user_role_contains_editor()
        session = SessionManager.get_session()
        id = self._try_get_json_value('id')
        user_id = self._user['uid']
        http_type = self._try_get_json_value('http_type')
        url = self._try_get_json_value('url')
        nlu_name = self._try_get_json_value('nlu_name')
        header = self._try_get_json_value('header')
        body = self._try_get_json_value('body')
        endpoint_type = self._try_get_json_value('endpoint_type')
        try:
            parameter = self._try_get_json_value('parameter')
        except:
            parameter = {}
        await session.update(collection=self.COLLECTION_ENDPOINT)\
            .filter_by(column='id', value=id) \
            .data(column='user_id', value=user_id)\
            .data(column='http_type', value=http_type)\
            .data(column='url', value=url)\
            .data(column='nlu_name', value=nlu_name)\
            .data(column='header', value=header)\
            .data(column='body', value=body)\
            .data(column='parameter', value=parameter)\
            .data(column='endpoint_type', value=endpoint_type)\
            .execute()
        self.write(self._get_success_result())
        self.finish()
