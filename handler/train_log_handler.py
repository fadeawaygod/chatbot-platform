from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helpers import get_collection_len
from handler.service.session_manager import SessionManager


class TrainLogHandler(BotHandlerBase):
    COLLECTION = 'train_log'

    async def get(self, bot_id):
        async def get_train_log(
                bot_id,
                skip,
                limit,
                start_time,
                end_time):
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False) \
                .projection_by(column='status', show=True) \
                .projection_by(column='intent_count', show=True) \
                .projection_by(column='utterance_count', show=True) \
                .projection_by(column='train_detail', show=True) \
                .projection_by(column='error', show=True) \
                .projection_by(column='error_code', show=True) \
                .projection_by(column='updated', show=True) \
                .sort_by(column='updated', asc=False)

            if(start_time):
                query = query.filter_by(
                    column='updated', value=start_time, operator='>=')
            if(end_time):
                query = query.filter_by(
                    column='updated', value=end_time, operator='<')

            if(skip):
                query = query.skip(skip)
            if(limit):
                query = query.limit(limit)
            return await query.execute()

        async def get_train_log_len(bot_id, start_time, end_time):
            train_log_filter = {'bot_id': bot_id}
            if(start_time):
                if 'updated' not in train_log_filter:
                    train_log_filter['updated'] = {}
                train_log_filter['updated']['$gte'] = start_time
            if(end_time):
                if 'updated' not in train_log_filter:
                    train_log_filter['updated'] = {}
                train_log_filter['updated']['$lte'] = end_time

            return await get_collection_len(self.COLLECTION, train_log_filter)

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)
        skip = int(self.get_argument('skip', 0))
        limit = int(self.get_argument('limit', 0))
        start_time = int(self.get_argument('start_time', 0))
        end_time = int(self.get_argument('end_time', 0))
        query_result = await get_train_log(
            bot_id,
            skip,
            limit,
            start_time,
            end_time
        )

        response_body = {
            'train_log': query_result['data']
        }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body['len'] = await get_train_log_len(bot_id, start_time, end_time)

        self.write(self._get_success_result(response_body))
