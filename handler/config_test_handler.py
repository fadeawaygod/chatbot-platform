import asyncio
import os
import time
from typing import Awaitable, Optional

import config.config

from handler.helpers import get_collection_len

from .bot_handler_base import BotHandlerBase, PermissionType
from .repository.config_gold_test_case import ConfigGoldTestCaseRepository
from .service.config_judger import Judge
from .service.session_manager import SessionManager


class BotConfigTestHandler(BotHandlerBase):
    async def get(self, bot_id):
        async def get_bot_config(bot_id: str, skip: int, limit: int) -> list:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_CONFIG_HISTORY)\
                .filter_by('bot_id', bot_id)\
                .sort_by(column='updated', asc=False)
            if skip:
                query = query.skip(skip)
            if limit:
                query = query.limit(limit)
            result = await query.execute()

            return result.get('data', [])

        async def get_configs_len(bot_id: str) -> int:
            query_filter = {'bot_id': bot_id}
            return await get_collection_len(self.COLLECTION_CONFIG_HISTORY, query_filter)

        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)
        limit = int(self.get_argument('limit', 0))
        skip = int(self.get_argument('skip', 0))
        config_len, configs = await asyncio.gather(get_configs_len(bot_id), get_bot_config(bot_id, skip, limit))

        self._finish_with_success_result(return_dict={
            'count': config_len,
            'result': configs
        })

    async def post(self, bot_id):
        session = SessionManager.get_session()
        data = self.json_args
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        assert 'flow' in data, 'flow is required.'
        assert 'config' in data, 'config is required.'
        test_cases = await ConfigGoldTestCaseRepository.get_all_test_case(bot_id=bot_id)
        corpus = [test_case['corpus'] for test_case in test_cases]
        test_result = []
        if len(corpus) > 0:
            judge = Judge(corpus=corpus, thresh=0.7)
            url = os.path.join(f"{config.config.CHATBOT_AGENT_HOST}/agent/",
                               f"{bot_id}",
                               f"simulator?timeout={20}")
            test_result = await judge.testing(url=url)
        result = await session.insert(collection=self.COLLECTION_CONFIG_HISTORY)\
            .data(column='bot_id', value=bot_id)\
            .data(column='create_time', value=int(time.time()))\
            .data(column='flow', value=data['flow'])\
            .data(column='config', value=data['config'])\
            .data(column='test_result', value=test_result)\
            .execute()
        self._finish_with_success_result(return_dict={
            **result,
            'test_result': test_result
        })

    async def delete(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        data = self.json_args
        assert 'id' in data, 'id is required.'
        session = SessionManager.get_session()
        result = await session.delete(collection=self.COLLECTION_CONFIG_HISTORY)\
            .filter_by(column='bot_id', value=bot_id)\
            .filter_by(column='id', value=data['id'])\
            .execute()
        self._finish_with_success_result(return_dict=result)
