import uuid
from typing import List

from error_code import DIALOG_NAME_DUPLICATED_ERR, NAME_DUPLICATED_IN_QUERY_ERR
from handler.bot_handler_base import (BotHandlerBase, DialogStatus,
                                      PermissionType)
from handler.service.session_manager import SessionManager


class ImportEntitiesHandler(BotHandlerBase):
    DEFAULT_INTENT_PRIORITY = 10

    async def post(self, bot_id: str):
        async def get_entities(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            query_result = await session.query(collection=self.COLLECTION_ENTITY)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False) \
                .projection_by(column='id', show=True) \
                .projection_by(column='name', show=True) \
                .projection_by(column='entries', show=True) \
                .sort_by(column='updated', asc=False) \
                .execute()
            return query_result['data']

        async def filter_exist_entities(bot_id: str, input_entities: List[dict]):
            exist_entities = await get_entities(bot_id)

            exist_entity_names = set([entity['name']
                                      for entity in exist_entities])
            return [input_entity for input_entity in input_entities if input_entity['name'] not in exist_entity_names]

        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        input_entities = self._try_get_json_value('data')
        filtered_entities = await filter_exist_entities(bot_id, input_entities)

        if filtered_entities:
            entities_data = self._convert_to_entities_insert_many_format(
                filtered_entities, bot_id)
            await self._insert_many_entities(entities_data)

        self.write(self._get_success_result())

    def _convert_to_entities_insert_many_format(self, filtered_entities: List[dict], bot_id: str) -> List[dict]:
        return [
            {
                'name': entity['name'],
                'bot_id': bot_id,
                'entries': entity['entries'],
                'is_sys': False,
                'is_enabled': True,
                'prompts': entity['prompts']
            } for entity in filtered_entities
        ]

    async def _insert_many_entities(self, entities_data: List[dict]):
        """ insert utterances to db
        """
        session = SessionManager.get_session()
        await session.insert_many(collection=self.COLLECTION_ENTITY)\
            .data(column='ignore', value=entities_data)\
            .execute()
