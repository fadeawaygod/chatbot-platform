from typing import List

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class ExportBotHandler(BotHandlerBase):
    async def get(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        return_dic = {}

        content_entities = await self._get_entity(bot_id)
        return_dic['entities'] = content_entities.get('data', [])

        content_intents = await self._get_intent(bot_id)
        return_dic['intents'] = content_intents.get('data', [])
        return_dic['dialogs'] = await self._get_dialog(bot_id)
        return_dic['utterances'] = await self._get_utterance(return_dic['intents'], return_dic['dialogs'])

        return_dic['flow'] = await self._get_flow(bot_id)
        return_dic['endpoints'] = await self._get_endpoints(return_dic['flow'])
        response_body = {
            'data': return_dic
        }
        self.write(self._get_success_result(response_body))
        self.finish()

    async def _get_utterance(self, intents: List[dict], dialogs: List[dict]):
        result = []
        for intent in intents:
            content_utterances = await self._get_utterances_by_intent_id(intent['id'])
            result.extend(content_utterances['data'])

        for dialog in dialogs:
            utterance_ids = [utterance['utterance_id']
                             for utterance in dialog['utterances']]
            utterances = await self._get_utterances_by_ids(utterance_ids)
            result.extend(utterances['data'])

        result = self._remove_duplicated_items_by_id(result)
        return result

    def _remove_duplicated_items_by_id(self, items: list) -> list:
        result = []
        id_set = set()
        for item in items:
            if item['id'] in id_set:
                continue
            result.append(item)
            id_set.add(item['id'])
        return result

    async def _get_flow(self, bot_id):
        session = SessionManager.get_session()
        response = await session.query(collection=self.COLLECTION_CHATBOT_FLOW)\
            .filter_by(column='bot_id', value=bot_id)\
            .projection_by(column='_id', show=False) \
            .projection_by(column='content', show=True) \
            .execute()
        response = response.get('data', [])
        if not response or 'content' not in response[0]:
            return {}
        return response[0]['content']

    async def _get_entity(self, bot_id):
        session = SessionManager.get_session()
        return await session.query(collection=self.COLLECTION_ENTITY)\
            .filter_by(column='bot_id', value=bot_id)\
            .projection_by(column='_id', show=False) \
            .projection_by(column='name', show=True) \
            .projection_by(column='entries', show=True) \
            .projection_by(column='prompts', show=True) \
            .projection_by(column='is_enabled', show=True) \
            .projection_by(column='is_sys', show=True) \
            .projection_by(column='id', show=True) \
            .execute()

    async def _get_intent(self, bot_id):
        session = SessionManager.get_session()
        return await session.query(collection=self.COLLECTION_INTENT)\
            .filter_by(column='bot_id', value=bot_id)\
            .projection_by(column='_id', show=False) \
            .projection_by(column='name', show=True) \
            .projection_by(column='priority', show=True) \
            .projection_by(column='entities', show=True) \
            .projection_by(column='id', show=True) \
            .execute()

    async def _get_utterances_by_intent_id(self, intent_id: str):
        session = SessionManager.get_session()
        return await session.query(collection=self.COLLECTION_UTTERANCE)\
            .filter_in(column='intent_ids', array=[intent_id]) \
            .projection_by(column='_id', show=False) \
            .projection_by(column='id', show=True) \
            .projection_by(column='intent_ids', show=True) \
            .projection_by(column='text', show=True) \
            .projection_by(column='entities', show=True) \
            .projection_by(column='sent_time', show=True) \
            .projection_by(column='commented', show=True) \
            .projection_by(column='from_user', show=True) \
            .execute()

    async def _get_utterances_by_ids(self, utterance_ids: List[str]):
        session = SessionManager.get_session()
        return await session.query(collection=self.COLLECTION_UTTERANCE)\
            .filter_in(column='id', array=utterance_ids) \
            .projection_by(column='_id', show=False) \
            .projection_by(column='id', show=True) \
            .projection_by(column='intent_ids', show=True) \
            .projection_by(column='text', show=True) \
            .projection_by(column='entities', show=True) \
            .projection_by(column='sent_time', show=True) \
            .projection_by(column='commented', show=True) \
            .projection_by(column='from_user', show=True) \
            .execute()

    async def _get_dialog(self, bot_id: str) -> List[dict]:
        session = SessionManager.get_session()
        result = await session.query(collection=self.COLLECTION_DIALOG)\
            .filter_by(column='bot_id', value=bot_id)\
            .projection_by(column='_id', show=False) \
            .projection_by(column='name', show=True) \
            .projection_by(column='status', show=True) \
            .projection_by(column='labeler', show=True) \
            .projection_by(column='utterances', show=True) \
            .projection_by(column='comment_count', show=True) \
            .projection_by(column='start_labeling_time', show=True) \
            .projection_by(column='labeling_total_time', show=True) \
            .execute()
        return result.get('data', [])

    async def _get_NLU_endpoints(self, nlu_id: str) -> List[dict]:
        session = SessionManager.get_session()
        result = await session.query(collection=self.COLLECTION_ENDPOINT)\
            .filter_by(column='id', value=nlu_id)\
            .projection_by(column='_id', show=False) \
            .projection_by(column='http_type', show=True) \
            .projection_by(column='url', show=True) \
            .projection_by(column='nlu_name', show=True) \
            .projection_by(column='header', show=True) \
            .projection_by(column='body', show=True) \
            .projection_by(column='endpoint_type', show=True) \
            .execute()
        return result.get('data', [])

    async def _get_endpoints(self, flow):
        endpoints = []
        nlu_ids = []
        if self._check_flow(flow):
            for state_id in flow['states']:
                for node in flow['states'][state_id]['nodes']:
                    if node['info']['widget']['type'] == 'nlu' and 'nlu_id' in node["info"] and node["info"]['nlu_id'] != "":
                        if node["info"]['nlu_id'] not in nlu_ids:
                            nlu_ids.append(node["info"]['nlu_id'])
                            endpoint = await self._get_NLU_endpoints(
                                node['info']['nlu_id'])
                            if len(endpoint) == 0:
                                pass
                            else:
                                endpoint[0]['nlu_id'] = node['info']['nlu_id']
                                endpoints.append(endpoint[0])
        return endpoints

    def _check_flow(self, flow):
        if 'states' in flow:
            return True
        return False
