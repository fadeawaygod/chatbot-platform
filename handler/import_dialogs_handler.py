from typing import List

from error_code import NAME_DUPLICATED_IN_QUERY_ERR
from handler.bot_handler_base import (BotHandlerBase, PermissionType)
from handler.service.redis_manager import RedisManager


class ImportDialogsHandler(BotHandlerBase):
    DEFAULT_INTENT_PRIORITY = 10

    async def post(self, bot_id):
        def check_name_not_duplicated(dialogs: List[dict]) -> bool:
            return len(dialogs) == len(set([dialog['name'] for dialog in dialogs]))

        self._check_user_role_contains_editor()
        bot = await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        dialogs = self.json_args['data']
        if not check_name_not_duplicated(dialogs):
            self._finish_with_failed_result(
                NAME_DUPLICATED_IN_QUERY_ERR['message'],
                status_code=400,
                error_code=NAME_DUPLICATED_IN_QUERY_ERR['code'],
            )
        await RedisManager.produce(
            {
                'func_name': "import_dialogs_and_utterances",
                "func_args": {
                    "bot_id": bot_id,
                    "bot_name": bot['name'],
                    "user_id": self._user['uid'],
                    "dialogs": dialogs
                }
            }
        )
        self.write(self._get_success_result())
