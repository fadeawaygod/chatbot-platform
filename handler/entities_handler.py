from typing import List

from exception import NoPermissionError
from service_caller import DbFunction, ServiceCaller

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len


class EntitiesHandler(BotHandlerBase):
    ERROR_ENTITY_ID_INVALID = 'this entity id is invalid'
    DEFAULT_SKIP = 0
    VALID_PATCH_ENTITY_KEY = ['name', 'entries', 'prompts', 'is_enabled']

    async def get(self):
        def get_query_entity_dic(bot_id):
            query_entity_dic = {
                'collection': self.COLLECTION_ENTITY,
                'filter': {'bot_id': bot_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'name': True,
                    'entries': True,
                    'prompts': True,
                    'is_enabled': True,
                    'is_sys': True,
                },
                'sort': {
                    'updated': -1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_entity_dic['limit'] = limit
            keyword = self.get_argument('keyword', '')
            if keyword:
                query_entity_dic['filter']['name'] = {
                    "$regex": replace_reserved_char(keyword)}

            is_sys = self.get_argument('is_sys', None)
            if is_sys != None:
                query_entity_dic['filter']['is_sys'] = is_sys.lower() == 'true'
            return query_entity_dic

        self._check_user_role_contains_editor()
        bot_id = self._try_get_url_parameter_value('bot_id')
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        query_entity_dic = get_query_entity_dic(bot_id)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_entity_dic)
        entities = query_result['data']
        response_body = {
            'entities': entities
        }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body['len'] = await get_collection_len(self.COLLECTION_ENTITY, query_entity_dic['filter'])
        self.write(self._get_success_result(response_body))

    async def post(self):
        def get_insert_entity_dic(bot_id):
            return {
                'collection': self.COLLECTION_ENTITY,
                'data': {
                    'name': self._try_get_json_value('name'),
                    'bot_id': bot_id,
                    'entries': [],
                    'prompts': [],
                    'is_enabled': True,
                    'is_sys': False,
                }
            }

        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        insert_entity_dic = get_insert_entity_dic(bot_id)
        result = await ServiceCaller.call_db_service(DbFunction.INSERT, insert_entity_dic)
        await self._set_nlu_data_is_modified(bot_id)
        new_entity_id = result['data']['id']
        self.write(self._get_success_result({'entity_id': new_entity_id}))

    async def delete(self, entity_id):
        def get_delete_entity_dic(entity_id):
            delete_dic = {
                'collection': self.COLLECTION_ENTITY,
                'filter': {'id': entity_id}
            }
            return delete_dic

        async def get_intents(bot_id: str) -> List[dict]:
            query_intent_dic = get_query_intent_dic(bot_id)
            result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_intent_dic)
            return result['data']

        def get_query_intent_dic(bot_id) -> dict:
            query_intent_dic = {
                'collection': 'intent',
                'filter': {'bot_id': bot_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'entities': True
                },
                'sort': {
                    'updated': -1
                },
            }
            return query_intent_dic

        self._check_user_role_contains_editor()
        entity = await self._check_user_permision_of_the_entity(entity_id, PermissionType.EDIT)
        delete_entity_dic = get_delete_entity_dic(entity_id)
        bot_id = entity['bot_id']

        await ServiceCaller.call_db_service(DbFunction.DELETE, delete_entity_dic)
        await self.delete_entity_and_related_rows(entity)
        await self._set_nlu_data_is_modified(bot_id)
        self.write(self._get_success_result())

    async def patch(self, entity_id: str):
        def get_update_entity_dic(entity_id: str, data: dict) -> dict:
            update_dic = {
                'collection': self.COLLECTION_ENTITY,
                'filter': {
                    'id': entity_id
                },
                'data': data
            }
            return update_dic

        def is_need_update_nlu(data: dict) -> bool:
            if 'prompts' in data and len(data) == 1:
                return False
            return True

        self._check_user_role_contains_editor()
        entity = await self._check_user_permision_of_the_entity(entity_id, PermissionType.EDIT)
        data = self._extract_body(self.VALID_PATCH_ENTITY_KEY)
        update_entity_dic = get_update_entity_dic(entity_id, data)
        await ServiceCaller.call_db_service(DbFunction.UPDATE, update_entity_dic)
        if is_need_update_nlu(data):
            await self._set_nlu_data_is_modified(entity['bot_id'])
        self.write(self._get_success_result())

    async def _check_user_permision_of_the_entity(self, entity_id: str, permission_type: PermissionType) -> dict:
        def get_query_entity_dic(entity_id):
            return {
                'collection': 'entity',
                'filter': {'id': entity_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'bot_id': True
                }
            }

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, get_query_entity_dic(entity_id))
        if not query_result['data']:
            self._finish_with_failed_result(
                self.ERROR_ENTITY_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)
        entity = query_result['data'][0]
        bot_id = entity['bot_id']
        await self._check_user_permision_of_the_bot(bot_id, permission_type)

        return entity
