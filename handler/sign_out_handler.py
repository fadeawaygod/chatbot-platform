from handler.validation_handler_base import ValidationHandlerBase
from service_caller import ServiceCaller, AuthFunction


class SignOutHandler(ValidationHandlerBase):
    async def post(self):
        await ServiceCaller.call_auth_service(
            AuthFunction.SIGN_OUT,
            {
                'access_token': self._access_token
            }
        )
        self.write(self._get_success_result())
