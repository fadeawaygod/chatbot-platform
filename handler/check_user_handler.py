from exception import (MailDuplicatedError, UIDDuplicatedError)
from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager


class CheckUserHandler(HandlerBase):
    COLLECTION_USER = 'user'
    COLLECTION_GROUP = 'group'
    COLLECTION_NLU_RESOURCE = 'nlu_resource'
    VALID_ROLE = ['group_admin', 'editor']
    RESOURCE_COUNT = 5

    async def get(self):
        async def check_user_unique(uid: str, mail_address: str):
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_USER)\
                .filter_by(column='', value=[{'uid': uid}, {'mail_address': mail_address}], operator="or") \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if result['data']:
                first_duplicated_data = result['data'][0]
                if first_duplicated_data['uid'] == uid:
                    raise UIDDuplicatedError(uid=uid)
                if first_duplicated_data['mail_address'] == mail_address:
                    raise MailDuplicatedError(mail=mail_address)

        uid = self._try_get_url_parameter_value('uid')
        mail_address = self._try_get_url_parameter_value('mail_address')

        await check_user_unique(uid, mail_address)

        self.write(self._get_success_result())
