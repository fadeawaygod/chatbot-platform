import asyncio
from typing import List

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class ExportDialogsHandler(BotHandlerBase):
    async def get(self, bot_id):
        async def get_intents(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            session = session.query(collection=self.COLLECTION_INTENT)\
                .filter_by(column='bot_id', value=bot_id)\
                .projection_by(column='_id', show=False)
            result = await session.execute()
            return result['data']

        async def get_entities(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            result = await session.query(collection=self.COLLECTION_ENTITY)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False) \
                .execute()
            return result.get('data', [])

        async def get_dialogs(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            result = await session.query(collection=self.COLLECTION_DIALOG)\
                .filter_by(column='bot_id', value=bot_id)\
                .projection_by(column='_id', show=False) \
                .execute()
            return result.get('data', [])

        async def get_utterances_by_ids(dialogs: List[dict]) -> List[dict]:
            utterance_ids = [utterance['utterance_id']
                             for dialog in dialogs for utterance in dialog['utterances']]
            session = SessionManager.get_session()
            result = await session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='id', value=utterance_ids, operator='in')\
                .projection_by(column='_id', show=False) \
                .execute()
            return result.get('data', [])

        async def transform_dialogs(intents: List[dict], entities: List[dict], dialogs: List[dict]) -> List[dict]:
            transformed_dialogs = []
            utterances = await get_utterances_by_ids(dialogs)
            for dialog in dialogs:
                linked_utterances = link_utterances(
                    dialog['utterances'], utterances)
                transformed_utterances = transform_utterances(
                    linked_utterances,
                    intents,
                    entities)
                transformed_dialog = {
                    'name': dialog['name'],
                    'utterances': transformed_utterances
                }
                transformed_dialogs.append(transformed_dialog)
            return transformed_dialogs

        def link_utterances(raw_dialog_utterances: List[dict], utterances: List[dict]) -> List[dict]:
            """replace utterance_id of utterance with real utterance

            Args:
                raw_dialog_utterance (List[dic]): contains 3 key:
                  - utterance_id
                  - from_user
                  - sent_time

            Returns:
                List[dict]: 
                - text
                - from_user
                - sent_time
                - intent_names
                - entities
            """
            linked_utterances = []
            for raw_dialog_utterance in raw_dialog_utterances:
                target_utterance = next(
                    (utterance for utterance in utterances if utterance['id'] == raw_dialog_utterance['utterance_id']), {})
                if target_utterance:
                    linked_utterances.append(target_utterance)
            return linked_utterances

        def transform_utterances(linked_utterances: List[dict], intents: List[dict], entities: List[dict]) -> List[dict]:
            """convert following properties:
            - intent_ids to intent_names
            - convert entities
            Args:
                linked_utterances (List): [description]

            Returns:
                List[dict]: [description]
            """
            transformed_utterances = []
            for linked_utterance in linked_utterances:
                transformed_utterances.append({
                    'from_user': linked_utterance['from_user'],
                    'sent_time': linked_utterance['sent_time'],
                    'text': linked_utterance['text'],
                    'intent_names': intent_id_to_names(
                        linked_utterance['intent_ids'],
                        intents
                    ),
                    'entities': entity_id_to_names(linked_utterance['entities'], entities)
                })
            return transformed_utterances

        def intent_id_to_names(intent_ids: List[str], intents: List[dict]) -> List[str]:
            intent_names = []
            for intent_id in intent_ids:
                target_intent = next(
                    (intent for intent in intents if intent['id'] == intent_id), {})
                if target_intent:
                    intent_names.append(target_intent['name'])
            return intent_names

        def entity_id_to_names(entities_in_utterance: List[str], entities: List[dict]) -> List[str]:
            """convert entity id in utterance with entity name

            Args:
                entities_int_utterance (List[str]): [description]
                entities (List[dict]): [description]

            Returns:
                List[str]: [description]
            """
            for entity_in_utterance in entities_in_utterance:
                target_entity = next(
                    (entity for entity in entities if entity['id'] == entity_in_utterance['entity_id']), {})
                if target_entity:
                    entity_in_utterance['entity_name'] = target_entity['name']
                del entity_in_utterance['entity_id']
            return entities_in_utterance

        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)
        intents, entities, dialogs = await asyncio.gather(
            get_intents(bot_id),
            get_entities(bot_id),
            get_dialogs(bot_id)
        )

        transformed_dialogs = await transform_dialogs(intents, entities, dialogs)
        response_body = {
            'data': transformed_dialogs
        }
        self.write(self._get_success_result(response_body))
        self.finish()
