from error_code import (USER_ID_ALREADY_EXIST_IN_AUTH_ERR,
                        USER_ID_NOT_EXIST_IN_AUTH_ERR)
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.notification import notify
from service_caller import NotificationFunction

from .service.session_manager import SessionManager


class BotAuthHandler(BotHandlerBase):
    COLLECTION_BOT = 'bot'
    VALID_PATCH_INTEGRATION_KEY = ['credential', 'is_enable']

    async def get(self, bot_id):

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        response_body = await self._get_bot(bot_id)
        self.write(self._get_success_result(response_body))

    async def post(self, bot_id):
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.OWNER)

        user_id = self._try_get_json_value('user_id')
        permission = self._try_get_json_value('permission')

        bot = await self._get_bot(bot_id)
        accessors = bot.get('accessors', {})
        if user_id in accessors:
            self._finish_with_failed_result(
                err_msg=USER_ID_ALREADY_EXIST_IN_AUTH_ERR['message'].format(
                    user_id=user_id),
                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=USER_ID_ALREADY_EXIST_IN_AUTH_ERR['code']
            )
        accessors[user_id] = permission
        await self._patch_bot(bot_id, accessors)
        await notify(
            NotificationFunction.BOT_AUTH,
            user_id,
            bot_id,
            content={'type': 'create', 'bot_name': bot['name'], 'owner_name': self._user['name']})

        self.write(self._get_success_result())

    async def delete(self, bot_id, user_id):
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.OWNER)

        bot = await self._get_bot(bot_id)
        accessors = bot.get('accessors', {})
        if user_id not in accessors:
            self._finish_with_failed_result(
                err_msg=USER_ID_NOT_EXIST_IN_AUTH_ERR['message'].format(
                    user_id=user_id),
                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=USER_ID_NOT_EXIST_IN_AUTH_ERR['code']
            )

        del accessors[user_id]

        await self._patch_bot(bot_id, accessors)
        await notify(
            NotificationFunction.BOT_AUTH,
            user_id,
            bot_id,
            content={'type': 'delete', 'bot_name': bot['name'], 'owner_name': self._user['name']})
        self.write(self._get_success_result())

    async def patch(self, bot_id, user_id):
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.OWNER)

        is_viewable = self._try_get_json_value('view')
        is_editable = self._try_get_json_value('edit')
        change_owner = self._try_get_json_value('changeOwner')

        bot = await self._get_bot(bot_id)
        accessors = bot.get('accessors', {})
        owner = bot.get('owner', self._user['uid'])
        if user_id not in accessors:
            self._finish_with_failed_result(
                err_msg=USER_ID_NOT_EXIST_IN_AUTH_ERR['message'].format(
                    user_id=user_id),
                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=USER_ID_NOT_EXIST_IN_AUTH_ERR['code']
            )

        accessors[user_id] = {
            'view': is_viewable,
            'edit': is_editable
        }
        if change_owner:
            owner = user_id
            accessors[self._user['uid']] = {
                'view': True,
                'edit': True
            }
            if user_id in accessors:
                del accessors[user_id]

        await self._patch_bot(bot_id, accessors, owner)
        await notify(
            NotificationFunction.BOT_AUTH,
            user_id,
            bot_id,
            content={'type': 'update', 'bot_name': bot['name'], 'owner_name': self._user['name']})
        self.write(self._get_success_result())

    async def _patch_bot(self, bot_id, accessors, owner=''):
        session = SessionManager.get_session()
        if owner == '':
            owner = self._user['uid']
        query = session.update(collection=self.COLLECTION_BOT)\
            .filter_by(column='id', value=bot_id) \
            .data(column='accessors', value=accessors) \
            .data(column='owner', value=owner)
        await query.execute()
