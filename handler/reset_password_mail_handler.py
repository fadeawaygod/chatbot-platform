from config.config import RESET_PASSWORD_URL
from exception import MailNotExistError
from service_caller import AuthFunction, ServiceCaller

from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager


class ResetPasswordMailHandler(HandlerBase):
    COLLECTION_USER = 'user'
    RESET_PASSWORD_MAIL_SUBJECT = 'Reset your passowrd'

    async def post(self):
        async def check_user_exist(mail_address: str):
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_USER)\
                .filter_by(column='mail_address', value=mail_address) \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if not result['data']:
                raise MailNotExistError(mail=mail_address)
            return result['data'][0]

        mail_address = self._try_get_json_value('mail_address')
        user = await check_user_exist(mail_address)

        await ServiceCaller.call_auth_service(
            AuthFunction.RESET_PASSWORD_MAIL,
            {
                "mail_address": mail_address,
                "activation_mail_data": self.get_reset_password_mail_data(user['name']),
            }
        )

        self.write(self._get_success_result())

    def get_reset_password_mail_data(cls, name: str):
        reset_passrord_link = RESET_PASSWORD_URL + \
            r"?uid={{uid}}&activation_code={{activation_code}}"
        # TODO can be refactor to asnyc manner in the future
        with open('static/reset_password_mail.html', 'r', encoding='utf-8') as f:
            html_template = f.read()
        html_template = html_template.replace(
            r'{{user_name}}', name)
        html_template = html_template.replace(
            r'{{link}}', reset_passrord_link)
        return {
            "subject": cls.RESET_PASSWORD_MAIL_SUBJECT,
            "content_template": html_template
        }
