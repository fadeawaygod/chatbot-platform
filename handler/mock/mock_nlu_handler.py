import uuid
import json

import tornado.httpclient

from handler.handler_base import HandlerBase
from service_caller import ServiceCaller

nlu_models = {}


class MockNluHandler(HandlerBase):
    UTTERANCE_LIMIT_PER_EPOCH = 5

    def get(self):
        self.write(self._get_success_result({'nlu_models': nlu_models}))

    def post(self):
        name = self._try_get_json_value('name')
        new_nlu_id = uuid.uuid4().hex
        new_nlu_token_id = uuid.uuid4().hex
        nlu_models[new_nlu_id] = {
            'name': name,
            'status': 'stop'
        }
        self.write(self._get_success_result(
            {
                'nlu_id': new_nlu_id,
                'nlu_token_id': new_nlu_token_id
            }
        ))

    async def patch(self):
        async def fetch_nlu_data(nlu_id):
            intents = await get_intents(nlu_id)
            entities = await get_entities(nlu_id)
            return intents, entities

        async def get_intents(nlu_id):
            result = await ServiceCaller.call_cahtbot_nlu_get_api_service(
                nlu_id,
                'intents',
            )
            intents = result['intents']
            for intent in intents:
                intent_id = intent['id']
                utteraces = await get_utterances(intent_id)
                intent['utteraces'] = utteraces

            return intents

        async def get_entities(nlu_id):
            result = await ServiceCaller.call_cahtbot_nlu_get_api_service(
                nlu_id,
                'entities',
            )
            return result['entities']

        async def get_utterances(intent_id):
            epoch_limit = self.UTTERANCE_LIMIT_PER_EPOCH
            result = await ServiceCaller.call_cahtbot_nlu_get_api_service(
                nlu_id,
                'utterances',
                {
                    'intent_id': intent_id,
                    'skip': 0,
                    'limit': epoch_limit,
                    'show_len': True,

                }
            )
            utterances = result['utterances']
            utterance_len = result['len']
            for i in range(epoch_limit, utterance_len, epoch_limit):
                result = await ServiceCaller.call_cahtbot_nlu_get_api_service(
                    nlu_id,
                    'utterances',
                    {
                        'intent_id': intent_id,
                        'skip': i,
                        'limit': epoch_limit,
                        'show_len': False,

                    }
                )
                utterances.extend(result['utterances'])

            return utterances

        status = self._try_get_json_value('method_status')
        nlu_id = self._try_get_json_value('nlu_id')
        self.write(self._get_success_result())
        self.finish()
        if status == 'fetch_data':
            nlu_models[nlu_id]['status'] = 'data_fetching'
            intents, entities = await fetch_nlu_data(nlu_id)
            nlu_models[nlu_id]['intents'] = intents
            nlu_models[nlu_id]['entities'] = entities
            nlu_models[nlu_id]['status'] = 'data_fetched'
            await ServiceCaller.call_cahtbot_nlu_patch_api_service(
                nlu_id,
                'data_fetched',
            )
        elif status == 'train_model':
            nlu_models[nlu_id]['status'] = 'trained'
            await ServiceCaller.call_cahtbot_nlu_patch_api_service(
                nlu_id,
                'trained',
            )
