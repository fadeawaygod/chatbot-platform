from service_caller import DbFunction, ServiceCaller

from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len
from handler.validation_handler_base import UserRole, ValidationHandlerBase


class GroupsHandler(ValidationHandlerBase):
    DEFAULT_SKIP = 0
    COLLECTION = 'group'

    async def get(self):
        def get_query_group_dic():
            query_group_dic = {
                "collection": self.COLLECTION,
                "filter": {},
                "projection": {
                    "_id": False,
                    "id": True,
                    "name": True,
                    "updated": True
                },
                "sort": {
                    "updated": -1
                },
                "skip": int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_group_dic['limit'] = limit
            keyword = self.get_argument('keyword', '')
            if keyword:
                query_group_dic['filter']['name'] = {
                    "$regex": replace_reserved_char(keyword)}
            return query_group_dic

        self._validate_user_role(UserRole.ADMIN)
        query_group_dic = get_query_group_dic()
        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_group_dic)
        groups = query_result['data']
        response_body = {
            'groups': groups
        }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body['len'] = await get_collection_len(self.COLLECTION, query_group_dic['filter'])
        self.write(self._get_success_result(response_body))

    async def post(self):
        def get_insert_group_dic(group_name):
            return {
                "collection": self.COLLECTION,
                "data": {"name": group_name}
            }

        self._validate_user_role(UserRole.ADMIN)
        group_name = self._try_get_json_value('name')
        insert_group_dic = get_insert_group_dic(group_name)
        result = await ServiceCaller.call_db_service(DbFunction.INSERT, insert_group_dic)
        new_group_id = result['data']['id']
        self.write(self._get_success_result({'group_id': new_group_id}))

    async def delete(self, group_id):
        def get_delete_group_dic(group_id):
            return {
                "collection": self.COLLECTION,
                "filter": {"id": group_id}
            }

        self._validate_user_role(UserRole.ADMIN)
        delete_group_dic = get_delete_group_dic(group_id)
        await ServiceCaller.call_db_service(DbFunction.DELETE, delete_group_dic)
        # TODO: delete all users in this group
        self.write(self._get_success_result())

    async def patch(self, group_id):
        def get_update_group_dic(group_id, group_name):
            return {
                "collection": self.COLLECTION,
                "filter": {"id": group_id},
                "data": {"name": group_name}
            }

        self._validate_user_role(UserRole.ADMIN)
        group_name = self._try_get_json_value('name')
        update_group_dic = get_update_group_dic(group_id, group_name)
        await ServiceCaller.call_db_service(DbFunction.UPDATE, update_group_dic)
        self.write(self._get_success_result())
