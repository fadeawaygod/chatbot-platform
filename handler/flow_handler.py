import logging

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class FlowHandler(BotHandlerBase):
    async def get(self):
        self._check_user_role_contains_editor()
        bot_id = self._try_get_url_parameter_value('bot_id')
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        session = SessionManager.get_session()
        content = await session.query(collection=self.COLLECTION_CHATBOT_FLOW)\
            .filter_by(column='bot_id', value=bot_id)\
            .execute()
        response_body = {
            'flows': content.get('data', [])
        }
        self._finish_with_success_result(return_dict=response_body)

    async def post(self):
        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        session = SessionManager.get_session()
        await session.insert(collection=self.COLLECTION_CHATBOT_FLOW)\
            .data(column='bot_id', value=bot_id)\
            .execute()
        self.write(self._get_success_result())
        self.finish()

    async def patch(self, flow_id):
        self._check_user_role_contains_editor()
        session = SessionManager.get_session()
        content = self._extract_body_without_check_key()
        await self._check_user_permision_of_the_flow(flow_id, PermissionType.EDIT)
        await session.update(collection=self.COLLECTION_CHATBOT_FLOW)\
            .filter_by(column='id', value=flow_id)\
            .data(column='content', value=content)\
            .execute()
        self.write(self._get_success_result())
        self.finish()

    async def delete(self, flow_id):
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_flow(flow_id, PermissionType.EDIT)
        session = SessionManager.get_session()
        await session.delete(collection=self.COLLECTION_CHATBOT_FLOW)\
            .filter_by(column='id', value=flow_id)\
            .execute()
        self.write(self._get_success_result())
        self.finish()

    def _extract_body_without_check_key(self) -> dict:
        return {key: value for key, value in self.json_args.items()}
