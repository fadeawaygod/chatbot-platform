from typing import List

from exception import IdInvalidError, NameDuplicatedError, NoPermissionError
from service_caller import DbFunction, ServiceCaller

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len
from handler.service.session_manager import SessionManager


class IntentsHandler(BotHandlerBase):
    DEFAULT_SKIP = 0
    VALID_PATCH_INTENT_KEY = ['name', 'priority', 'entities']

    async def get(self, intent_id=''):
        def get_query_intent_dic(intent_id: str):
            query_intent_dic = {
                'collection': self.COLLECTION_INTENT,
                'filter': {'id': intent_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'name': True,
                    'priority': True,
                    'entities': True,
                    'utterance_count': True
                },
                'sort': {
                    'updated': -1
                },
            }
            return query_intent_dic

        def get_query_intents_dic(bot_id: str):
            query_intent_dic = {
                'collection': self.COLLECTION_INTENT,
                'filter': {'bot_id': bot_id},
                'projection': {
                    '_id': False,
                    'id': True,
                    'name': True,
                    'priority': True,
                    'entities': True,
                    'utterance_count': True
                },
                'sort': {
                    'updated': -1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP))
            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_intent_dic['limit'] = limit
            keyword = self.get_argument('keyword', '')
            if keyword:
                query_intent_dic['filter']['name'] = {
                    "$regex": replace_reserved_char(keyword)}
            min_utterance = int(self.get_argument('min_utterance', 0))
            if min_utterance:
                query_intent_dic['filter']['utterance_count'] = {
                    "$gte": min_utterance}
            max_utterance = int(self.get_argument('max_utterance', 0))
            if max_utterance:
                query_intent_dic['filter']['utterance_count'] = {
                    "$lt": max_utterance}
            return query_intent_dic

        self._check_user_role_contains_editor()
        if intent_id:
            await self._check_user_permision_of_the_intent(intent_id, PermissionType.VIEW)
            query_intent_dic = get_query_intent_dic(intent_id)
        else:
            bot_id = self._try_get_url_parameter_value('bot_id')
            await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)
            query_intent_dic = get_query_intents_dic(bot_id)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_intent_dic)
        intents = query_result['data']
        if intent_id:
            if len(intents) == 0:
                raise IdInvalidError(id=intent_id)
            response_body = {
                'intent': intents[0]
            }
        else:
            response_body = {
                'intents': intents
            }
        if self.get_argument('show_len', '').lower() == 'true':
            response_body['len'] = await get_collection_len(self.COLLECTION_INTENT,  query_intent_dic['filter'])
        self.write(self._get_success_result(response_body))

    async def post(self):
        def get_insert_intent_dic(bot_id):
            return {
                'collection': self.COLLECTION_INTENT,
                'data': {
                    'name': self._try_get_json_value('name'),
                    'bot_id': bot_id,
                    'priority': self._try_get_json_value('priority'),
                    'entities': [],
                    'utterance_count': 0
                }
            }

        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        all_intents_name = await self._get_intent_name_under_bot(bot_id)
        self._check_intent_name_not_duplicate(
            self._try_get_json_value('name'), all_intents_name)

        insert_intent_dic = get_insert_intent_dic(bot_id)
        result = await ServiceCaller.call_db_service(DbFunction.INSERT, insert_intent_dic)
        await self._set_nlu_data_is_modified(bot_id)
        new_intent_id = result['data']['id']
        self.write(self._get_success_result({'intent_id': new_intent_id}))

    async def delete(self, intent_id):
        def get_delete_intent_dic(intent_id):
            delete_dic = {
                'collection': self.COLLECTION_INTENT,
                'filter': {'id': intent_id}
            }
            return delete_dic

        self._check_user_role_contains_editor()
        bot_id = await self._check_user_permision_of_the_intent(intent_id, PermissionType.EDIT)
        delete_intent_dic = get_delete_intent_dic(intent_id)

        await ServiceCaller.call_db_service(DbFunction.DELETE, delete_intent_dic)
        await self._delete_intent_id_in_utterance(intent_id)
        await self._set_nlu_data_is_modified(bot_id)
        self.write(self._get_success_result())

    async def patch(self, intent_id):
        def get_update_intent_dic(intent_id):
            data = self._extract_body(self.VALID_PATCH_INTENT_KEY)
            update_dic = {
                'collection': self.COLLECTION_INTENT,
                'filter': {
                    'id': intent_id
                },
                'data': data
            }
            return update_dic

        self._check_user_role_contains_editor()
        bot_id = await self._check_user_permision_of_the_intent(intent_id, PermissionType.EDIT)

        update_intent_dic = get_update_intent_dic(intent_id)

        await ServiceCaller.call_db_service(DbFunction.UPDATE, update_intent_dic)
        await self._set_nlu_data_is_modified(bot_id)
        self.write(self._get_success_result())

    async def _get_intent_name_under_bot(self, bot_id):
        session = SessionManager.get_session()
        intents_name = []
        response = await session.query(collection=self.COLLECTION_INTENT)\
            .filter_by(column='bot_id', value=bot_id)\
            .execute()
        for intent in response['data']:
            if 'name' in intent:
                intents_name.append(intent['name'])
        return intents_name

    def _check_intent_name_not_duplicate(self, name, intent_names):
        if name in intent_names:
            raise NameDuplicatedError(name=name)

    async def _delete_intent_id_in_utterance(self, intent_id: str):
        async def get_utterances(intent_id: str) -> List[dict]:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='intent_ids', value=[intent_id], operator='in') \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            return result['data']

        async def update_utterance_intent_ids(utterance_id: str, intent_ids: List[str]):
            session = SessionManager.get_session()
            query = session.update(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='id', value=utterance_id) \
                .data(column='intent_ids', value=intent_ids)
            await query.execute()

        utterances = await get_utterances(intent_id)
        for utterance in utterances:
            utterance['intent_ids'].remove(intent_id)
            await update_utterance_intent_ids(
                utterance['id'], utterance['intent_ids'])
