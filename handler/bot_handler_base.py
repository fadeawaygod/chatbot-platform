from enum import Enum
from typing import List

from exception import NoPermissionError
from handler.helper.notification import notify
from handler.validation_handler_base import UserRole, ValidationHandlerBase
from service_caller import NotificationFunction

from .service.session_manager import SessionManager


class PermissionType(Enum):
    OWNER = 'owner'
    VIEW = 'view'
    EDIT = 'edit'


class DialogStatus(Enum):
    UNLABELED = 'unlabeled'
    LABELING = 'labeling'
    DONE = 'done'


class BotHandlerBase(ValidationHandlerBase):
    COLLECTION_USER = 'user'
    COLLECTION_NLU_RESOURCE = 'nlu_resource'
    COLLECTION_BOT = 'bot'
    COLLECTION_INTENT = 'intent'
    COLLECTION_ENTITY = 'entity'
    COLLECTION_UTTERANCE = 'utterance'
    COLLECTION_DIALOG = 'dialog'
    COLLECTION_INTEGRATION = 'integration'
    COLLECTION_TRAIN_LOG = 'train_log'
    COLLECTION_CHATBOT_FLOW = 'chatbot_flow'
    COLLECTION_MESSAGE_SENTIMENT = 'message_sentiment'
    COLLECTION_MESSAGE_HISTORY = 'message_history'
    COLLECTION_CONFIG_HISTORY = 'config_history'
    COLLECTION_ENDPOINT = 'endpoint'

    ERROR_ONLY_EDITOR_CAN_DO_THIS_OPERATION = 'only editor can do this operation'
    ERROR_INTENT_ID_INVALID = 'this intent id is invalid'
    ERROR_FLOW_ID_INVALID = 'this flow id is invalid'
    ERROR_BOT_ID_INVALID = 'this bot id is invalid'
    ERROR_NLU_ID_INVALID = 'nlu id is invalid'

    BOT_STATUS_DEPLOYING = 'deploying'
    BOT_STATUS_READY = 'ready'
    BOT_STATUS_FAILED = 'failed'
    TRAIN_STATUS_START = 'start'

    TRAIN_STATUS_UNTRAINED = 'untrained'
    TRAIN_STATUS_TRAINING = 'training'

    def _check_user_role_contains_editor(self):
        if UserRole.EDITOR.value not in self._user['role']:
            self._finish_with_failed_result(
                self.ERROR_ONLY_EDITOR_CAN_DO_THIS_OPERATION,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

    async def _check_user_permision_of_the_bot(self, bot_id: str, permission_type: PermissionType) -> dict:

        user_id = self._user['uid']

        bot = await self._get_bot(bot_id)

        if bot.get('owner', '') != user_id:
            if permission_type == PermissionType.OWNER:
                self._finish_with_failed_result(
                    self.ERROR_NO_PERMISSION,
                    self.HTTP_FORBIDDEN_ERROR_CODE,
                    exception_class=NoPermissionError)
            else:
                accessors = bot.get('accessors', {})
                target = accessors.get(user_id, {})
                if not target or not target.get(permission_type.value, False):
                    self._finish_with_failed_result(
                        self.ERROR_NO_PERMISSION,
                        self.HTTP_FORBIDDEN_ERROR_CODE,
                        exception_class=NoPermissionError)
        return bot

    async def _check_user_permision_of_the_intent(self, intent_id: str, permission_type: PermissionType) -> str:
        session = SessionManager.get_session()
        query_result = await session.query(collection=self.COLLECTION_INTENT)\
            .filter_by(column='id', value=intent_id)\
            .projection_by(column='_id', show=False)\
            .projection_by(column='bot_id', show=True)\
            .execute()
        if not query_result['data']:
            self._finish_with_failed_result(
                self.ERROR_INTENT_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

        bot_id = query_result['data'][0]['bot_id']
        await self._check_user_permision_of_the_bot(bot_id, permission_type)
        return bot_id

    async def _check_user_permision_of_the_flow(self, flow_id: str, permission_type: PermissionType):
        session = SessionManager.get_session()
        query_result = await session.query(collection='chatbot_flow')\
            .filter_by(column='id', value=flow_id)\
            .execute()
        if not query_result['data']:
            self._finish_with_failed_result(
                self.ERROR_FLOW_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

        bot_id = query_result['data'][0]['bot_id']
        await self._check_user_permision_of_the_bot(bot_id, permission_type)

    async def _set_nlu_data_is_modified(self, bot_id: str):
        bot = await self._get_bot(bot_id)
        if bot.get('is_nlu_data_modified', True):
            return
        session = SessionManager.get_session()
        await session.update(collection='bot')\
            .filter_by(column='id', value=bot_id)\
            .data(column='is_nlu_data_modified', value=True) \
            .execute()
        user_id = self._user.get('uid', '')
        await notify(
            NotificationFunction.UPDATE_NLU_DATA,
            user_id,
            bot_id,
            content={'is_modified': True})

    async def delete_entity_and_related_rows(self, entity: dict):
        async def get_intents(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_INTENT)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False) \
                .sort_by(column='updated', asc=False)
            result = await query.execute()
            return result['data']

        def is_entity_exist(intent: dict, entity_id: str) -> bool:
            for entity in intent['entities']:
                if entity['id'] == entity_id:
                    return True
            return False

        async def delete_entity_in_intents(intents: List[dict], target_entity_id: str):
            for intent in intents:
                entities_without_deleted_item = [
                    entity_id for entity_id in intent['entities'] if entity_id != target_entity_id]

                if len(entities_without_deleted_item) == len(intent['entities']):
                    continue

                session = SessionManager.get_session()
                query = session.update(collection=self.COLLECTION_INTENT)\
                    .filter_by(column='id', value=intent['id']) \
                    .data(column='entities', value=entities_without_deleted_item)
                await query.execute()

        async def get_utterances(intents: List[dict]) -> List[dict]:
            intent_ids = [intent['id'] for intent in intents]
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='intent_ids', value=intent_ids, operator='in') \
                .projection_by(column='_id', show=False) \
                .sort_by(column='updated', asc=False)
            result = await query.execute()
            return result['data']

        async def get_utterances_by_dialogs(bot_id: str) -> List[dict]:
            dialogs = await get_dialogs(bot_id)
            utterance_ids = [
                utterance['utterance_id'] for dialog in dialogs for utterance in dialog['utterances']]
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='id', value=utterance_ids, operator='in') \
                .projection_by(column='_id', show=False) \
                .sort_by(column='updated', asc=False)
            result = await query.execute()
            return result['data']

        async def get_dialogs(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_DIALOG)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False) \
                .sort_by(column='updated', asc=False)
            result = await query.execute()
            return result['data']

        async def delete_entities_in_utterances(utterances: List[dict], entity_id: str):
            for utterance in utterances:
                entities_without_deleted_item = [entity for entity in utterance['entities']
                                                 if entity['entity_id'] != entity_id]
                if len(entities_without_deleted_item) == len(utterance['entities']):
                    continue
                await update_utterance_entity(utterance['id'], entities_without_deleted_item)

        async def update_utterance_entity(utterance_id: str, new_entities: List[dict]):
            session = SessionManager.get_session()
            query = session.update(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='id', value=utterance_id) \
                .data(column='entities', value=new_entities)

            await query.execute()

        entity_id = entity['id']
        bot_id = entity['bot_id']
        intents = await get_intents(bot_id)
        await delete_entity_in_intents(intents, entity_id)
        utterances = await get_utterances(intents)
        await delete_entities_in_utterances(utterances, entity_id)
        dialog_utterances = await get_utterances_by_dialogs(bot_id)
        await delete_entities_in_utterances(dialog_utterances, entity_id)

    async def update_intent_utterance_count(self, intent_ids: List[str]):
        async def count_utterance(intent_id: str) -> int:
            session = SessionManager.get_session()
            session = session.count(collection=self.COLLECTION_UTTERANCE) \
                .filter_in(column='intent_ids', array=[intent_id])
            query_result = await session.execute()
            return query_result['count']

        async def update_intent_utterance_count(intent_id: str, utterance_count: int):
            session = SessionManager.get_session()
            await session.update(collection=self.COLLECTION_INTENT)\
                .filter_by(column='id', value=intent_id)\
                .data(column='utterance_count', value=utterance_count) \
                .execute()

        for intent_id in intent_ids:
            utterance_count = await count_utterance(intent_id)
            await update_intent_utterance_count(intent_id, utterance_count)

    async def _check_user_has_nlu_id(self, id: str):
        session = SessionManager.get_session()
        result = await session.query(collection=self.COLLECTION_ENDPOINT)\
            .filter_by('id', id)\
            .execute()
        if result['data'][0]['user_id'] == self._user['uid']:
            self._finish_with_failed_result(
                self.ERROR_NLU_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

    async def _get_bot(self, bot_id: str):
        session = SessionManager.get_session()
        query_result = await session.query(collection='bot')\
            .filter_by(column='id', value=bot_id)\
            .projection_by(column='_id', show=False)\
            .execute()
        if not query_result['data']:
            self._finish_with_failed_result(
                self.ERROR_BOT_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE)

        bot = query_result['data'][0]
        return bot
