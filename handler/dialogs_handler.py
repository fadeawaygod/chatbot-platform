from typing import List

from error_code import DIALOG_ID_INVALID_ERR, SORT_BY_INVALID_ERR
from exception import OthersIsEditingThisDialogError

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len
from handler.service.session_manager import SessionManager


class DialogsHandler(BotHandlerBase):
    DIALOG_STATUS_LABELING = 'labeling'
    SORT_BY_VALID_KEYS = [
        'name',
        'status',
        'labeler',
        'updated',
        'labeling_total_time'
    ]
    VALID_PATCH_KEY = [
        'name',
        'status',
        'labeler',
        'utterances',
        'start_labeling_time',
        'labeling_total_time',
        'comment_count'
    ]

    async def get(self, dialog_id=''):
        async def get_dialogs_by_bot_id(
                bot_id: str,
                skip: int,
                limit: int,
                sort_by: str,
                is_asc: bool,
                status: str,
                keyword: str,
                min_comment: int
        ) -> List[dict]:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_DIALOG)\
                .filter_by(column='bot_id', value=bot_id)\
                .projection_by(column='_id', show=False) \
                .projection_by(column='id', show=True) \
                .projection_by(column='name', show=True) \
                .projection_by(column='utterances', show=True) \
                .projection_by(column='status', show=True) \
                .projection_by(column='labeler', show=True) \
                .projection_by(column='updated', show=True) \
                .projection_by(column='start_labeling_time', show=True) \
                .projection_by(column='labeling_total_time', show=True) \
                .projection_by(column='comment_count', show=True) \
                .sort_by(column=sort_by, asc=is_asc)

            if skip:
                query = query.skip(skip)
            if limit:
                query = query.limit(limit)
            if status:
                query = query.filter_by(column='status', value=status)
            if min_comment:
                query = query.filter_by(
                    column='comment_count', value=min_comment, operator='>=')
            if keyword:
                query = query.filter_by(
                    column='name', value=replace_reserved_char(keyword), operator='regex')

            query_result = await query.execute()
            return query_result['data']

        async def get_dialogs_len(
                bot_id: str,
                status: str,
                keyword: str,
                min_comment: int) -> int:
            query_filter = {'bot_id': bot_id}
            if status:
                query_filter['status'] = status
            if min_comment:
                query_filter['comment_count'] = {"$gte": min_comment}
            if keyword:
                query_filter['name'] = {
                    "$regex": replace_reserved_char(keyword)}

            return await get_collection_len(self.COLLECTION_DIALOG, query_filter)

        async def link_utterances(utterances: List[dict]) -> List[dict]:
            utterance_ids = [utterance['utterance_id']
                             for utterance in utterances]
            raw_utterances = await get_utterances(utterance_ids)
            utterances = attach_raw_utterances(utterances, raw_utterances)
            return utterances

        def attach_raw_utterances(utterances: List[dict], raw_utterances: List[dict]):
            for utterance in utterances:
                utterance['raw_utterance'] = next(
                    (raw_utterance for raw_utterance in raw_utterances if raw_utterance['id'] == utterance['utterance_id']), {})
            return utterances

        async def get_utterances(utterance_ids: List[str]) -> List[str]:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='id', value=utterance_ids, operator='in')\
                .projection_by(column='_id', show=False) \
                .projection_by(column='id', show=True) \
                .projection_by(column='intent_ids', show=True) \
                .projection_by(column='text', show=True) \
                .projection_by(column='entities', show=True) \
                .projection_by(column='from_user', show=True) \
                .projection_by(column='sent_time', show=True)

            query_result = await query.execute()
            return query_result['data']

        def check_sort_by_valid(sort_by: str) -> bool:
            return sort_by in self.SORT_BY_VALID_KEYS

        self._check_user_role_contains_editor()
        if dialog_id:
            dialog = await self._check_user_permision_of_the_dialog(dialog_id, PermissionType.VIEW)
            dialog['utterances'] = await link_utterances(dialog['utterances'])
            response_body = {
                'dialog': dialog
            }
        else:
            bot_id = self._try_get_url_parameter_value('bot_id')
            skip = int(self.get_argument('skip', 0))
            limit = int(self.get_argument('limit', 0))
            sort_by = self.get_argument('sort_by', 'updated').lower()
            if not check_sort_by_valid(sort_by):
                self._finish_with_failed_result(
                    SORT_BY_INVALID_ERR['message'].format(key=sort_by),
                    status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                    error_code=SORT_BY_INVALID_ERR['code']
                )

            is_asc = self.get_argument('is_asc', '').lower() == 'true'
            status = self.get_argument('status', '')
            keyword = self.get_argument('keyword', '')
            min_comment = int(self.get_argument('min_comment', 0))

            await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)
            response_body = {
                'dialogs': await get_dialogs_by_bot_id(
                    bot_id,
                    skip,
                    limit,
                    sort_by,
                    is_asc,
                    status,
                    keyword,
                    min_comment
                )
            }
            if self.get_argument('show_len', '').lower() == 'true':
                response_body['len'] = await get_dialogs_len(
                    bot_id,
                    status,
                    keyword,
                    min_comment
                )

        self._finish_with_success_result(return_dict=response_body)

    async def patch(self, dialog_id):
        async def update_dialog(dialog_id: str, data: dict):
            session = SessionManager.get_session()
            update_session = session.update(collection=self.COLLECTION_DIALOG).filter_by(
                column='id', value=dialog_id)
            for key, value in data.items():
                update_session.data(column=key, value=value)
            await update_session.execute()

        def check_others_are_not_labeling(dialog: dict, user_id: str):
            if dialog['status'] == self.DIALOG_STATUS_LABELING and dialog.get('labeler', '') != user_id:
                raise OthersIsEditingThisDialogError(
                    user_id=dialog.get('labeler', ''))

        self._check_user_role_contains_editor()
        dialog = await self._check_user_permision_of_the_dialog(dialog_id, PermissionType.EDIT)
        check_others_are_not_labeling(dialog, self._user['uid'])

        data = self._extract_body(self.VALID_PATCH_KEY)

        await update_dialog(dialog_id, data)
        self._finish_with_success_result()

    async def _check_user_permision_of_the_dialog(self, dialog_id: str, permission_type: PermissionType) -> dict:
        """
        Returns:
            dict: dialog
        """
        async def get_dialog_by_id(dialog_id: str) -> dict:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_DIALOG)\
                .filter_by(column='id', value=dialog_id)\
                .projection_by(column='_id', show=False) \
                .projection_by(column='id', show=True) \
                .projection_by(column='bot_id', show=True) \
                .projection_by(column='name', show=True) \
                .projection_by(column='utterances', show=True) \
                .projection_by(column='labeler', show=True) \
                .projection_by(column='status', show=True) \
                .projection_by(column='start_labeling_time', show=True) \
                .projection_by(column='labeling_total_time', show=True) \
                .projection_by(column='comment_count', show=True) \
                .projection_by(column='updated', show=True) \
                .sort_by(column='updated', asc=False)

            query_result = await query.execute()
            return query_result['data']

        data = await get_dialog_by_id(dialog_id)
        if not data:
            self._finish_with_failed_result(
                DIALOG_ID_INVALID_ERR['message'].format(
                    dialog_id=dialog_id),
                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=DIALOG_ID_INVALID_ERR['code']
            )
        dialog = data[0]
        await self._check_user_permision_of_the_bot(dialog['bot_id'], permission_type)
        return dialog
