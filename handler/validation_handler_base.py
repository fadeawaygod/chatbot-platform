from enum import Enum

from exception import IdInvalidError, NoPermissionError, TokenValidationError
from service_caller import AuthFunction, ServiceCaller

from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager


class UserRole(Enum):
    ADMIN = 'admin'
    GROUP_ADMIN = 'group_admin'
    EDITOR = 'editor'
    USER = 'user'


class ValidationHandlerBase(HandlerBase):
    COLLECTION_USER = 'user'
    COLLECTION_AUTH_TOKEN = 'auth_token'
    ERROR_AUTHORIZATION_FORMAT_ERROR = 'Authorization format error'
    ERROR_NO_PERMISSION = 'You have no permission to do this operation'
    EDITOR_USER_ROLE = 'editor'

    async def prepare(self) -> None:
        async def validate_token(access_token) -> dict:
            """example of self._user:'
                _id':'xxx'
                'created':1564459391851
                'mail_address':'xxx'
                'group':'xxx'
                'ip':'xxx'
                'role':['admin', 'group_admin', 'editor', 'user']
                'uid':'xxx'
                'updated':1564544106474

            Args:
                self ([type]): [description]
            """
            auth_user = {}
            try:
                result = await ServiceCaller.call_auth_service(
                    AuthFunction.TOKEN_VALIDATION, {
                        'access_token': self._access_token
                    })
                auth_user = result['user']
            except Exception as e:
                raise TokenValidationError(cause=str(e))
            return auth_user

        def extract_token():
            authorization_value = self.request.headers.get('Authorization', '')
            if not authorization_value.startswith('Bearer '):
                raise TokenValidationError(
                    cause=self.ERROR_AUTHORIZATION_FORMAT_ERROR)

            return authorization_value[7:]

        async def get_user_by_uid(uid: str) -> dict:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_USER)\
                .filter_by(column='uid', value=uid) \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if not result['data']:
                raise IdInvalidError(id=uid)
            return result['data'][0]

        super().prepare()

        if self.request.method != 'OPTIONS':
            self.set_status(200)
            access_token = extract_token()
            self._access_token = access_token
            auth_user = await validate_token(access_token)
            self._user = await get_user_by_uid(auth_user['uid'])

    def _validate_user_role(self, role: UserRole):
        if role.value not in self._user['role']:
            self._finish_with_failed_result(
                self.ERROR_NO_PERMISSION,
                status_code=self.HTTP_FORBIDDEN_ERROR_CODE,
                is_raise=True,
                exception_class=NoPermissionError)
