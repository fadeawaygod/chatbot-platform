"""
Comparing the dialogues of current bot with ideal dialgues

parameters:
    input_file: input csv file path
    output_path: output directory to save output result
    save: whether to save the file
    threshold: cosine similarity threshold to judge success dialogue
    output_type: "csv" "json"
"""
import re
import json
from difflib import SequenceMatcher

from service_caller import fetch


class Judge:
    """Test the bot performance with template dialogues"""
    def __init__(self, corpus, thresh):
        self.thresh = thresh
        self._corpus = corpus
        self._names = ["bot", "user"]
        self.usr_dial, self.agent_dial, self.test_dial = self.preprocess(
            self._corpus)

    def preprocess(self, dials):
        """parse data to required json format"""
        dial = [d.split("\n") for d in dials]
        rplu = re.compile(r"{}: *".format(self._names[1]))
        rpla = re.compile(r"{}: *".format(self._names[0]))
        usr_dial = [[re.sub(rplu, "", d)
                     for d in li if self._names[1] in d] for li in dial]
        agent_dial = [[re.sub(rpla, "", d)
                       for d in li if self._names[0] in d] for li in dial]
        test_dial = []
        for _ud in usr_dial:
            inp = {"sessions": [[]]}
            for _u in _ud:
                inp["sessions"][0].append(_u)
            test_dial.append(inp)
        return usr_dial, agent_dial, test_dial

    async def testing(self, url) -> [dict]:
        """call bot API for testing"""
        _dt = []
        for _td in self.test_dial:
            code, body = await fetch(url=url, method='POST', body=json.dumps(_td, ensure_ascii=False))
            assert code == 200, f"Test Fail on dial - test_dial: {_td} code: {code} body: {body}"
            resp = json.loads(body, encoding='utf-8')["conversations"]
            _dt.append(resp[0])
        dial = [[d["sender"] + d["message"] for d in li] for li in _dt]
        result = [[d.replace("user", self._names[1] + ": ") for d in li]
                  for li in dial]
        result = ["\n".join(d.replace("bot", self._names[0] + ": ")
                            for d in li) for li in result]
        answer = [[d["message"] for d in li if d["sender"] == 'bot']
                  for li in _dt]
        score = [get_similar("".join(ans), "".join(agn))
                 for ans, agn in zip(answer, self.agent_dial)]
        confuse = ["".join(s).count("華華不懂您的意思") for s in answer]
        _pass = []
        for i, _s in enumerate(score):
            if self.thresh > _s or confuse[i] > 0:
                _pass.append("FAIL")
            else:
                _pass.append("SUCCESS")
        return [
            {
                "origin": o,
                "result": r,
                "score": s,
                "confuse": c,
                "pass": p
            } for o, r, s, c, p in zip(self._corpus, result, score, confuse, _pass)
        ]


def get_similar(str1, str2):
    """get cosine similarity of two string"""
    if not str1 or not str2:
        return 0
    sorted_str1 = "".join(sorted(str1))
    sorted_str2 = "".join(sorted(str2))
    return SequenceMatcher(None, sorted_str1, sorted_str2).ratio()
