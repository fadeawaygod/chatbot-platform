from handler.service.session_manager import SessionManager


async def get_intents_by_bot_id(bot_id: str) -> [dict]:
    session = SessionManager.get_session()
    result = await session.query(collection='intent')\
        .filter_by(column='bot_id', value=bot_id)\
        .sort_by(column='updated', asc=True)\
        .projection_by(column='_id', show=False)\
        .projection_by(column='id', show=True)\
        .projection_by(column='name', show=True)\
        .projection_by(column='priority', show=True)\
        .projection_by(column='entities', show=True)\
        .execute()
    return result['data']


async def get_utterances_by_intent_ids(intent_ids: [str]) -> [dict]:
    session = SessionManager.get_session()
    result = await session.query(collection='utterance') \
        .filter_in(column='intent_ids', array=intent_ids) \
        .projection_by(column='_id', show=False) \
        .projection_by(column='id', show=True) \
        .projection_by(column='intent_ids', show=True) \
        .projection_by(column='text', show=True) \
        .projection_by(column='entities', show=True) \
        .sort_by(column='updated', asc=True) \
        .execute()
    return result['data']


async def get_entities(bot_id: str) -> [dict]:
    session = SessionManager.get_session()
    result = await session.query(collection='entity') \
        .filter_by(column='bot_id', value=bot_id) \
        .projection_by(column='_id', show=False) \
        .projection_by(column='name', show=True) \
        .projection_by(column='entries', show=True) \
        .execute()
    print(result)
    return result['data']


async def get_train_data(bot_id: str) -> dict:
    intent_query_result = await get_intents_by_bot_id(bot_id=bot_id)
    entities = {}
    intents = []
    intent_ids = [intent['id']
                  for intent in intent_query_result if intent.get('id')]
    utterances = await get_utterances_by_intent_ids(intent_ids=intent_ids)
    for utterance in utterances:
        intent_ids = utterance['intent_ids']
        match_intents = [
            intent for intent in intent_query_result if intent['id'] in intent_ids]
        for intent in match_intents:
            if 'utterances' not in intent:
                intent['utterances'] = []
            if utterance.get('text'):
                intent['utterances'].append(utterance)

    for intent in intent_query_result:
        intent_instance = {'id': intent.get('id'), 'name': intent.get('name')}
        for utterance in intent['utterances']:
            utterance_instance = {
                'intent_id': intent['id'],
                'value': utterance.get('text'),
                'entities': []
            }
            for s in utterance.get('entities', []):
                entity_name = s.get('entity_name')
                entity = s.get('text')
                entity = {'name': entity_name, 'entity': entity}
                if entity_name in entities:
                    if entity not in entities[entity_name]:
                        entities[entity_name].append(entity)
                elif entity_name and entity:
                    entities[entity_name] = [entity]
                if entity['name'] and entity['entity']:
                    utterance_instance['entities'].append(entity)
            if utterance_instance['value'] and utterance_instance['intent_id']:
                utterances.append(utterance_instance)
        intents.append(intent_instance)
    entity_instancies = []
    for entity_name, entities in entities.items():
        for entity in entities:
            entity_instancies.append({'name': entity_name, 'entity': entity})
    if not entity_instancies:
        entities = await get_entities(bot_id=bot_id)
        for entity in entities:
            name = entity.get('name', None)
            if not name:
                continue
            for e in entity['entries']:
                if not e.get('value', None):
                    continue
                entity_instancies.append({'name': name, 'entity': e['value']})
    return {
        'intents': intents,
        'utterances': utterances,
        'entities': entity_instancies
    }
