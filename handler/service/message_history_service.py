import logging
from typing import Union

import tornado.httpclient

from config.config import CHATBOT_AGENT_HOST, CHATBOT_GATEWAY_HOST

logger = logging.getLogger()

GATEWAY_URL_TEMPLATE = '{domain_name}/gateway/reply'
AGENT_URL_TEMPLATE = '{domain_name}/agent/{agent_id}/callback'


class Proxy:

    @staticmethod
    async def fetch(url: str, method: str, body: Union[str, bytes]):
        try:
            client = tornado.httpclient.AsyncHTTPClient()
            headers = {'content-type': 'application/json'}
            response = await client.fetch(url, headers=headers, method=method, body=body)
            if response.code != 200:
                raise tornado.httpclient.HTTPClientError(code=response.code, message=response.body.decode('utf-8'), response=response)
            return response.code, response.body.decode('utf-8')
        except tornado.httpclient.HTTPClientError as e:
            logger.error(e)
            return e.code, e.message
        except Exception as e:
            logger.error('Unexpected Error occur ', e)
        return None, None

    @staticmethod
    async def to_agent(agent_id: Union[str, int], body: Union[str, bytes]):
        try:
            url = AGENT_URL_TEMPLATE.format(domain_name=CHATBOT_AGENT_HOST, agent_id=agent_id)
            return await Proxy.fetch(url, 'POST', body)
        except Exception as e:
            logger.error('Unexpected Error occur ', e)

    @staticmethod
    async def to_gateway(body: Union[str, bytes]):
        try:
            url = GATEWAY_URL_TEMPLATE.format(domain_name=CHATBOT_GATEWAY_HOST)
            return await Proxy.fetch(url, 'POST', body)
        except Exception as e:
            logger.error('Unexpected Error occur ', e)
