from config import config

from .db import ORM, AuthManager


class SessionManager:

    __auth_manager = None
    __session = None

    @staticmethod
    def get_session():

        if not SessionManager.__auth_manager:

            account, password = config.DB_SERVICE_UID, config.DB_SERVICE_PASSWORD
            url = f'{config.DB_SERVICE_HTTP_PROTOCOL}://{config.DB_SERVICE_DOMAIN}'
            SessionManager.__auth_manager = AuthManager.create(
                account=account, password=password, url=url)
            # SessionManager.__auth_manager = IOLoop.current().run_sync(
            #     lambda: AuthManager.create(account=account, password=password, url=url))
        return ORM(auth_manager=SessionManager.__auth_manager)
