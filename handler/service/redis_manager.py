import aredis
import torrelque

from config.config import REDIS_HOST, REDIS_PORT, REDIS_DB

redis = aredis.StrictRedis(
    host=REDIS_HOST,
    port=REDIS_PORT,
    db=REDIS_DB
)
queue = torrelque.Torrelque(redis)
queue.schedule_sweep(10)


class RedisManager:
    @staticmethod
    async def produce(task: dict):
        """task is a dictonary, it contains:
            func_name(str): the function to be excute.
            func_args(dict): the args when the parameter excuting, i.e. func_name(**func_args)
        Args:
            task (dict)
        """
        await queue.enqueue(task)
