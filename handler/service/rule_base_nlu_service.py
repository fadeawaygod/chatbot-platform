from difflib import SequenceMatcher


class Entity:

    def __init__(self, name: str, entity: str):
        self.name = name
        self.entity = entity


class Utterance:

    def __init__(self, value: str, entities: [Entity]):
        self.value = value
        self.entities = entities


class Intent:

    def __init__(self, name: str, utterances: [Utterance]):
        self.name = name
        self.utterances = utterances


class Ratio:

    def __init__(self, intent: str, value: float, utterance: Utterance):
        self.intent = intent
        self.value = value
        self.utterance = utterance


def similar(str1, str2) -> float:
    if not str1 or not str2:
        return 0
    sorted_str1 = "".join(sorted(str1))
    sorted_str2 = "".join(sorted(str2))
    return SequenceMatcher(None, sorted_str1, sorted_str2).ratio()*100


def get_intent_ratios(input_str: str, intents: [Intent]) -> [Ratio]:
    intent_similar_ratio_map = {}
    for intent in intents:
        highest_similar_ratio = 0
        for utterance in intent.utterances:
            similar_ratio = similar(input_str, utterance.value)
            if similar_ratio > highest_similar_ratio:
                intent_similar_ratio_map[intent.name] = Ratio(intent=intent.name,
                                                              value=similar_ratio,
                                                              utterance=utterance)
                highest_similar_ratio = similar_ratio
    return [ratio for intent, ratio in intent_similar_ratio_map.items()]


def get_ratios_by_rank_desc(ratios: [Ratio], rank: int = 3) -> [Ratio]:
    sorted_ratios = sorted(ratios, key=lambda ratio: ratio.value, reverse=True)
    return sorted_ratios[0:rank]


def entity_extraction(input_str, entities: [Entity]) -> dict:
    extract_entities = {}
    for entity in entities:
        if entity.entity in input_str:
            if entity.name in extract_entities:
                extract_entities[entity.name].append(entity.entity)
            else:
                extract_entities[entity.name] = [entity.entity]
    return extract_entities


class NluCompareResponse:

    def __init__(self, ratios: [Ratio], entities: dict):
        self.intent = []
        for ratio in ratios:
            data = {
                'value': ratio.intent,
                'confidence': ratio.value
            }
            self.intent.append(data)
        self.entities = entities or {}


def compare(user_input: str, intents: [Intent], entities: [Entity]) -> NluCompareResponse:

    def get_entity_compare_score(filled_count: int, all_keys_count: int) -> int:
        filled_count = filled_count if filled_count > 0 else 1
        all_keys_count = all_keys_count if all_keys_count > 0 else 1
        division_result = filled_count / all_keys_count
        division_result = division_result if division_result <= 1 else 1
        return int((division_result * 100) / 2)

    def to_softmax(ratios: [Ratio]) -> [Ratio]:
        if not ratios:
            return []
        index = 0
        for ratio in ratios:
            probability = ratio.value
            ratio.value = float("%.4f" % probability)
            index += 1
        return ratios

    ratios = get_intent_ratios(user_input, intents)
    rank_five_ratios = get_ratios_by_rank_desc(ratios=ratios, rank=5)
    extracted_entity = entity_extraction(
        input_str=user_input, entities=entities)
    filled_entity_keys = extracted_entity.keys()
    for ratio in rank_five_ratios:
        filled_keys_count = 0
        entity_keys = [entity.name for entity in ratio.utterance.entities]
        for key in filled_entity_keys:
            if key in entity_keys:
                filled_keys_count += 1
        entity_compare_score = get_entity_compare_score(
            filled_keys_count, len(entity_keys))
        ratio.value += entity_compare_score
    return NluCompareResponse(ratios=to_softmax(rank_five_ratios), entities=extracted_entity)


class StringMatcher:

    def __init__(self, intents: [dict], utterances, entities):
        self.intents = self.convert_to_intent_objects(intents=intents)
        for intent, intent_obj in zip(intents, self.intents):
            intent_id = intent.get('id')
            intent_utterances = [utterance for utterance in utterances if intent_id in utterance.get(
                'intent_ids', [])]
            intent_obj.utterances = self.convert_to_utterance_objects(
                utterances=intent_utterances)
        self.entities = self.convert_to_entity_objects(entities=entities)

    def convert_to_intent_objects(self, intents: [dict]) -> [Intent]:
        result = []
        for intent in intents:
            i = Intent(name=intent.get('name'), utterances=[])
            result.append(i)
        return result

    def convert_to_utterance_objects(self, utterances: [dict]) -> [Utterance]:
        result = []
        for utterance in utterances:
            entities = self.convert_to_entity_objects(
                entities=utterance.get('entities', []))
            result.append(
                Utterance(value=utterance.get('value'), entities=entities))
        return result

    def convert_to_entity_objects(self, entities: [dict]):
        return [
            Entity(name=entity.get('name'), entity=entity.get('entity'))
            for entity in entities if entity
        ]

    def predict(self, query_msg: str):
        return compare(user_input=query_msg, intents=self.intents, entities=self.entities)
