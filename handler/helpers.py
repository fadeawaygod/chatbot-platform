from handler.service.session_manager import SessionManager


async def get_collection_len(collection: str, filter: dict) -> dict:
    session = SessionManager.get_session()
    session = session.count(collection=collection)
    for column, value in filter.items():
        session = session.filter_by(column=column, value=value)

    query_result = await session.execute()
    return query_result['count']
