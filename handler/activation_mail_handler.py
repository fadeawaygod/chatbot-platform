from exception import UserNotFoundError
from service_caller import AuthFunction, ServiceCaller

from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager
from handler.sign_up_handler import SignUpHandler


class ActivationMailHandler(HandlerBase):
    COLLECTION_USER = 'user'

    async def post(self):
        async def get_user(uid: str) -> dict:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_USER)\
                .filter_by(column='uid', value=uid) \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if not result['data']:
                raise UserNotFoundError(uid=uid)
            return result['data'][0]

        uid = self._try_get_json_value('uid')
        user = await get_user(uid)
        await ServiceCaller.call_auth_service(
            AuthFunction.ACTIVATION_MAIL,
            {
                "mail_address": user['mail_address'],
                "activation_mail_data": SignUpHandler.get_activation_mail_data(user['name']),
            }
        )

        self.write(self._get_success_result())
