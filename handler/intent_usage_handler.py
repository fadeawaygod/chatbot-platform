from error_code import EXCEED_MAX_TIME_INTERVAL_ERR
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class IntentUsageHandler(BotHandlerBase):
    MAX_QUERY_TIME_INTERVAL = 5356800000
    COLLECTION_MESSAGE_MATCHED_INTENTS = 'message_matched_intents'

    async def get(self, bot_id):
        async def get_message_matched_intents(bot_id: str, start_time: int, end_time: int) -> list:
            """get data for collection:message_matched_intents

            Args:
                bot_id (str):
                start_time (int): UNIX time stamp millisecond
                end_time (int): UNIX time stamp millisecond

            Returns:
                list:
            """
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_MESSAGE_MATCHED_INTENTS)\
                .filter_by(column='bot_id', value=bot_id)\
                .filter_by(column='operation', value='INTENT')\
                .filter_by(column='updated', value=start_time, operator='>=')\
                .filter_by(column='updated', value=end_time, operator='<')\
                .projection_by(column='_id', show=False)\
                .projection_by(column='matched_values', show=True)\
                .sort_by(column='updated', asc=True)

            result = await query.execute()
            return result.get('data', [])

        def sum_up_matched_intents(matched_intents: list) -> list:
            """[summary]

            Args:
                matched_intents (list): [description]

            Returns:
                list: [description]
            """
            intent_count_mapping = {}
            for matched_intent in matched_intents:
                for intent_name in matched_intent['matched_values']:
                    if intent_name not in intent_count_mapping:
                        intent_count_mapping[intent_name] = 0
                    intent_count_mapping[intent_name] += 1
            intent_count_objs = [{'intent_name': intent_name, 'count': count}
                                 for intent_name, count in intent_count_mapping.items()]
            intent_count_objs.sort(reverse=True, key=lambda o: o['count'])
            return intent_count_objs
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        start_time = int(self._try_get_url_parameter_value('start_time'))
        end_time = int(self._try_get_url_parameter_value('end_time'))
        if end_time-start_time > IntentUsageHandler.MAX_QUERY_TIME_INTERVAL:
            self._finish_with_failed_result(
                EXCEED_MAX_TIME_INTERVAL_ERR['message'],
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=EXCEED_MAX_TIME_INTERVAL_ERR['code'],)
        result = await get_message_matched_intents(bot_id, start_time, end_time)
        intent_usage = sum_up_matched_intents(result)
        repsponse_body = {
            'intent_usage': intent_usage
        }
        self.write(self._get_success_result(repsponse_body))
