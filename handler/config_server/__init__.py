from typing import Optional, Awaitable

import requests
import tornado.web

from config import config
import os
from importlib import reload

from sr_toolkit.ConfigManager import load_config


class ServerConfigHandler(tornado.web.RequestHandler):

    @staticmethod
    def reload_config(version):
        os.environ["tenant"] = version
        reload(config)

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass
    def get(self):
        self.write({
            "status": "ok",
            "version": os.environ["tenant"],
            "config": config.CONFIG.json().copy()
        })

    def post(self):
        version = self.get_body_argument('version', "production")
        old_config = config.CONFIG.json().copy()
        old_version = os.environ["tenant"][:]
        self.reload_config(version)
        self.write({
            "status": "ok",
            "new_version": os.environ["tenant"],
            "new": config.CONFIG.json(),
            "old_version": old_version,
            "old": old_config
        })


class SilkbotConfigHandler(tornado.web.RequestHandler):

    def get(self):

        Silkbot_nacos_server = self.get_argument('nacos_server', "http://10.205.48.80:8848/nacos/v1/cs/configs")
        Silkbot_tenant = self.get_argument('version', "production")
        dataId = "silkbot.json"
        try:
            Silkbot_CONFIG = load_config(server=Silkbot_nacos_server, tenant=Silkbot_tenant, dataId=dataId, group="chatbot")

            self.write({
                "status": "ok",
                "version": Silkbot_tenant,
                "config": Silkbot_CONFIG.json()
            })
        except AssertionError as e:
            print()
            self.write({
                "status": "fail",
                "Silkbot_nacos_server":Silkbot_nacos_server,
                "Silkbot_tenant":Silkbot_tenant,
                "msg":str(e)
            })



def test_reload_function():
    ServerConfigHandler.reload_config("dev")
    CONFIG_1 = config.CONFIG.json().copy()
    ServerConfigHandler.reload_config("staging")
    assert CONFIG_1 != config.CONFIG.json()
