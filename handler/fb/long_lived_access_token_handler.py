from handler.validation_handler_base import ValidationHandlerBase
from service_caller import ServiceCaller
import asyncio


class LongLivedAccessTokenHandler(ValidationHandlerBase):
    async def get(self):
        async def get_long_lived_user_access_token(user_access_token: str) -> str:
            result = await ServiceCaller.call_fb_exchange_user_access_token(
                user_access_token
            )
            return result['access_token']

        async def get_long_lived_page_access_tokens(
            long_lived_user_access_token: str,
            page_ids: list
        ) -> dict:
            queries = []
            for page_id in page_ids:
                queries.append(
                    ServiceCaller.call_fb_get_page_access_token(
                        long_lived_user_access_token,
                        page_id
                    )
                )
            results = await asyncio.gather(*queries)
            return {
                page_id: result['access_token']
                for page_id, result in zip(page_ids, results)
            }

        user_access_token = self._try_get_url_parameter_value(
            'user_access_token')
        page_ids_string = self._try_get_url_parameter_value('page_ids')
        page_ids = page_ids_string.split(',')
        long_lived_user_access_token = await get_long_lived_user_access_token(
            user_access_token)
        long_lived_page_access_token_mapping = await get_long_lived_page_access_tokens(
            long_lived_user_access_token, page_ids)

        response = {
            'long_lived_user_access_token': long_lived_user_access_token,
            'long_lived_page_access_token_mapping': long_lived_page_access_token_mapping
        }

        self.write(self._get_success_result(response))
