from typing import Optional, Awaitable

from .handler_base import HandlerBase


class HealthHandler(HandlerBase):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    async def get(self):
        self.set_status(200)
        await self.finish()
