from handler.service.session_manager import SessionManager
from handler.validation_handler_base import ValidationHandlerBase


class NotificationHistoryHandler(ValidationHandlerBase):
    COLLECTION_NOTIFICATION = 'notification_history'
    DEFAULT_SKIP = 0
    DEFAULT_LIMIT = 0
    VALID_PATCH_KEY = ['is_checked']

    async def get(self):
        async def get_notification_history(
                user_id: str,
                skip: int,
                limit: int,
                is_checked: str,
                event: str,
                start_time: int,
                end_time: int) -> list:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_NOTIFICATION)\
                .projection_by(column='_id', show=False)\
                .projection_by(column='id', show=True)\
                .projection_by(column='bot_id', show=True)\
                .projection_by(column='user_id', show=True)\
                .projection_by(column='event', show=True)\
                .projection_by(column='content', show=True)\
                .projection_by(column='is_checked', show=True)\
                .projection_by(column='updated', show=True)\
                .sort_by(column='updated', asc=False)

            query = append_condition(
                query,
                user_id,
                is_checked,
                event,
                start_time,
                end_time,
                skip,
                limit)

            result = await query.execute()
            return result.get('data', [])

        async def get_notification_len(
                user_id: str,
                is_checked: str,
                event: str,
                start_time: int,
                end_time: int) -> int:
            session = SessionManager.get_session()
            query = session.count(collection=self.COLLECTION_NOTIFICATION)
            query = append_condition(
                query,
                user_id,
                is_checked,
                event,
                start_time,
                end_time)

            result = await query.execute()
            return result['count']

        def append_condition(
                query: "ORM",
                user_id: str,
                is_checked: str,
                event: str,
                start_time: int,
                end_time: int,
                skip=0,
                limit=0,) -> "ORM":
            query = query.filter_by(column='user_id', value=user_id)\

            if is_checked:
                is_checked_bool = is_checked.lower() == 'true'
                query = query.filter_by(
                    column='is_checked', value=is_checked_bool)
            if event:
                query = query.filter_by(column='event', value=event)

            if skip:
                query = query.skip(skip)
            if limit:
                query = query.limit(limit)

            if start_time:
                query = query.filter_by(
                    column='updated', value=start_time, operator='>=')
            if end_time:
                query = query.filter_by(
                    column='updated', value=end_time, operator='<')
            return query

        user_id = self._user['uid']
        skip = int(self.get_argument('skip', self.DEFAULT_SKIP))
        limit = int(self.get_argument('limit', self.DEFAULT_LIMIT))
        is_checked = self.get_argument('is_checked', '')
        event = self.get_argument('event', '')
        start_time = int(self.get_argument('start_time', 0))
        end_time = int(self.get_argument('end_time', 0))
        response_body = {
            'notification_history': await get_notification_history(
                user_id,
                skip,
                limit,
                is_checked,
                event,
                start_time,
                end_time
            ),
            'len': await get_notification_len(
                user_id,
                is_checked,
                event,
                start_time,
                end_time
            )
        }
        self.write(self._get_success_result(response_body))

    async def patch(self, notification_id):
        async def update_notification_history(user_id, notification_id, data):
            session = SessionManager.get_session()
            query = session.update(collection=self.COLLECTION_NOTIFICATION)\
                .filter_by(column='id', value=notification_id)\
                .filter_by(column='user_id', value=user_id)
            for key, value in data.items():
                query.data(column=key, value=value)
            await query.execute()

        user_id = self._user['uid']

        data = self._extract_body(self.VALID_PATCH_KEY)
        await update_notification_history(user_id, notification_id, data)

        self.write(self._get_success_result())
