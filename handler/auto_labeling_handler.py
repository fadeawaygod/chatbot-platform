from typing import List

from exception import IdInvalidError
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager
from service_caller import ServiceCaller


class AutoLabelingHandler(BotHandlerBase):
    SYSTEM_ENTITY_PREFIX = 'sys_'

    async def post(self):
        async def get_utterances_by_ids(utterance_ids: List[str]) -> List[dict]:
            session = SessionManager.get_session()
            result = await session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_in(column='id', array=utterance_ids) \
                .projection_by(column='_id', show=False) \
                .execute()
            return result.get('data', [])

        async def get_entities(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            result = await session.query(collection=self.COLLECTION_ENTITY)\
                .filter_by(column='bot_id', value=bot_id) \
                .filter_by(column='is_sys', value=True) \
                .projection_by(column='_id', show=False) \
                .execute()
            return result.get('data', [])

        async def get_intents(intent_ids: List[str]) -> List[dict]:
            session = SessionManager.get_session()
            session = session.query(collection=self.COLLECTION_INTENT)\
                .filter_by(column='id', value=intent_ids, operator='in')\
                .projection_by(column='_id', show=False)
            result = await session.execute()
            return result['data']

        def check_utterance_ids(utterance_ids: List[str], utterances: List[str]):
            tmp_utterance_ids = utterance_ids[:]  # shallow clone
            result_ids = [utterance['id'] for utterance in utterances]
            for utterance_id in utterance_ids:
                if utterance_id not in result_ids:
                    raise IdInvalidError(id=utterance_id)

        async def label_utterances(bot_id: str, utterances: List[dict], entities: List[dict]):
            utterances_id_mapping, docs = create_request_ner_service_docs(
                utterances)
            ner_results = await ServiceCaller.call_nlu_ner_service(docs)
            for ner_result in ner_results['docs']:
                if not ner_result['entities']:
                    continue
                entities = await create_or_update_entities(bot_id, ner_result, entities)
                target_utterance = utterances_id_mapping[ner_result['id']]
                labeled_entity_ids = await label_utterance(target_utterance, ner_result, entities)
                await extend_intent_entity(target_utterance, labeled_entity_ids)

        def create_request_ner_service_docs(utterances: List[str]):
            """Due to NER service doesn't guarantee the order of result is equal to inputs,
                so we need to create a mapping by ourselves.
            Args:
                utterances (List[str]): [description]

            Returns:
                [type]: [description]
            """
            utterances_id_mapping = {
                str(i): utterance for i, utterance in enumerate(utterances)
            }
            docs = [{"id": str(i), "text": utterance['text']}
                    for i, utterance in utterances_id_mapping.items()]
            return utterances_id_mapping, docs

        async def create_or_update_entities(bot_id: str, ner_result: dict, entities: List[dict]) -> List[dict]:
            for result_entity_name, label_objs in ner_result['entities'].items():
                result_entries = [label_obj['text']
                                  for label_obj in label_objs]
                prefixed_entity_name = self.SYSTEM_ENTITY_PREFIX + result_entity_name
                entity = next(
                    (entity for entity in entities if entity['name'] == prefixed_entity_name), {})
                if not entity:
                    nlu_entry_list_result = await ServiceCaller.call_nlu_entry_list_service(
                        prefixed_entity_name)
                    entries_to_be_inserted = nlu_entry_list_result.get(
                        'entries', [])
                    entries_to_be_inserted.extend(
                        [{'value': result_entry, 'synonyms': [result_entry]}
                            for result_entry in result_entries]
                    )
                    result = await create_system_entity(bot_id, result_entity_name, entries_to_be_inserted)
                    entities.append(result)
                else:
                    tmp_entry_text = [entry['value']
                                      for entry in entity['entries']]  # shallow clone
                    is_need_update = False
                    for result_entry in result_entries:
                        if result_entry not in tmp_entry_text:
                            entity['entries'].append(
                                {'value': result_entry, 'synonyms': []})
                            is_need_update = True
                    if is_need_update:
                        await update_entity(entity)

            return entities

        async def create_system_entity(bot_id: str, entity_name: str, entries_to_be_inserted: List[dict]) -> dict:
            session = SessionManager.get_session()
            result = await session.insert(collection=self.COLLECTION_ENTITY)\
                .data(column='bot_id', value=bot_id)\
                .data(column='name', value=self.SYSTEM_ENTITY_PREFIX + entity_name)\
                .data(column='entries', value=entries_to_be_inserted) \
                .data(column='prompts', value=[])\
                .data(column='is_enabled', value=True)\
                .data(column='is_sys', value=True)\
                .execute()

            return result['data']

        async def update_entity(entity: dict):
            session = SessionManager.get_session()
            await session.update(collection=self.COLLECTION_ENTITY)\
                .filter_by(column='id', value=entity['id']) \
                .data(column='entries', value=entity['entries']) \
                .execute()

        async def update_intent_entity(intent: dict):
            session = SessionManager.get_session()
            await session.update(collection=self.COLLECTION_INTENT)\
                .filter_by(column='id', value=intent['id']) \
                .data(column='entities', value=intent['entities']) \
                .execute()

        async def label_utterance(utterance: dict, ner_result: dict, entities: List[dict]) -> List[str]:
            """ label an utterance with ner_result

            Args:
                utterance (dict): [description]
                ner_result (dict): [description]
                entities (List[dict]): all entities of the bot

            Returns:
                List[str]: labeled entities' id
            """
            is_need_update = False
            labeled_entity_ids = set()
            for entity_name, label_objs in ner_result['entities'].items():
                target_entity = next(
                    (entity for entity in entities if entity['name'] == (self.SYSTEM_ENTITY_PREFIX + entity_name)), {})
                if not target_entity:
                    continue
                for label_obj in label_objs:
                    new_labeled_entity = {
                        'entity_id': target_entity['id'],
                        'start_position': label_obj['offset'],
                        'len': len(label_obj['text']),
                        'text': label_obj['text']
                    }
                    if check_labeled_entity_not_duplicated(utterance, new_labeled_entity):
                        utterance['entities'].append(new_labeled_entity)
                        labeled_entity_ids.add(new_labeled_entity['entity_id'])
                        is_need_update = True

            if is_need_update:
                await update_utterance(utterance)
            return list(labeled_entity_ids)

        async def extend_intent_entity(utterance: dict, labeled_entity_ids: List[dict]):
            if not utterance['intent_ids']:
                return
            intents = await get_intents(utterance['intent_ids'])
            for intent in intents:
                is_need_update = False
                for entity_id in labeled_entity_ids:
                    if entity_id not in intent['entities']:
                        intent['entities'].append(entity_id)
                        is_need_update = True
                if is_need_update:
                    await update_intent_entity(intent)

        def check_labeled_entity_not_duplicated(utterance: dict, new_labeled_entity: dict) -> bool:
            for labeled_entity in utterance['entities']:
                if labeled_entity['start_position'] == new_labeled_entity['start_position'] and \
                        labeled_entity['len'] == new_labeled_entity['len']:
                    return False
            return True

        async def update_utterance(utterance: dict):
            session = SessionManager.get_session()
            await session.update(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='id', value=utterance['id']) \
                .data(column='entities', value=utterance['entities']) \
                .execute()

        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        utterance_ids = self._try_get_json_value('utterance_ids')
        bot = await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        if utterance_ids:
            utterances = await get_utterances_by_ids(utterance_ids)
            check_utterance_ids(utterance_ids, utterances)
            entities = await get_entities(bot_id)
            await label_utterances(bot_id, utterances, entities)

        self.write(self._get_success_result())
