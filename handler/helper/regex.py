RESERVED_CHARS = [
    '.',
    '(',
    ')',
    '{',
    '}',
    '[',
    ']',
    '|',
    '*',
    '+',
    '?',
    '^',
    '$',
    '/',
    '-',
    '\\'
]


def replace_reserved_char(input_str: str):
    """the function replace the reserved characters of regex like * to \*
    """
    for reserved_char in RESERVED_CHARS:
        input_str = input_str.replace(reserved_char, rf'\{reserved_char}')
    return input_str
