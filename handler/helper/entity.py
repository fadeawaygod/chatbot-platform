from typing import List

from handler.service.session_manager import SessionManager

COLLECTION_ENTITY = "entity"


async def resolute_entities(
    bot_id: str,
    input_entities: dict
) -> dict:
    """the function resolute entities captured by nlu service to their entry group name

    Args:
        bot_id (str): the id of bot
        input_entities (dict): e.g. {
            "車站": ["高雄"]
        }
    Returns:
        e.g.
        {
            "車站": ["左營"]
        }
    """
    if not input_entities:
        return {}

    entities = await _get_entities(bot_id)
    for entity_name, words in input_entities.items():
        target_entity = _search_entity_by_name(entity_name, entities)
        if not target_entity:
            continue
        new_words = []
        for word in words:
            target_entry = _search_entry_by_word(word, target_entity)
            new_word = target_entry['value'] if target_entry else word
            new_words.append(new_word)
        input_entities[entity_name] = new_words
    return input_entities


async def _get_entities(bot_id: str) -> List[dict]:
    session = SessionManager.get_session()
    result = await session.query(collection=COLLECTION_ENTITY)\
        .filter_by(column='bot_id', value=bot_id) \
        .projection_by(column='_id', show=False) \
        .execute()
    return result.get('data', [])


def _search_entity_by_name(entity_name: str, entities: List[dict]) -> dict:
    return next((entity for entity in entities if entity['name'] == entity_name), {})


def _search_entry_by_word(word: str, entity: dict) -> dict:
    """search every entries' synonyms
    """
    for entry in entity["entries"]:
        for synonym in entry["synonyms"]:
            if synonym == word:
                return entry
    return {}
