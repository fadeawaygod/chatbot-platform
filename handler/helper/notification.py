from handler.service.session_manager import SessionManager
from service_caller import NotificationFunction, ServiceCaller

COLLECTION_NOTIFICATION = 'notification_history'


async def notify(function: NotificationFunction,
                 user_id: str,
                 bot_id: str,
                 content: dict,
                 ):
    """call notification service and save to notification_history collection

    Args:
        function (NotificationFunction): [description]
        user_id (str): [description]
        bot_id (str): [description]
        content (dict): [description]
    """
    await ServiceCaller.call_notification_service(
        function,
        user_id,
        bot_id,
        content=content)

    session = SessionManager.get_session()
    await session.insert(collection=COLLECTION_NOTIFICATION)\
        .data(column='bot_id', value=bot_id)\
        .data(column='user_id', value=user_id)\
        .data(column='bot_id', value=bot_id)\
        .data(column='event', value=function.value)\
        .data(column='content', value=content)\
        .data(column='is_checked', value=False)\
        .execute()
