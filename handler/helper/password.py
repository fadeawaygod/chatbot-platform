import re

import argon2

from exception import PasswordFormatError

_hasher = argon2.PasswordHasher()

PASSWORD_LENGTH_ERROR = 'password length should between 6-20'
NOT_CONTAIN_UPPERCASE_LETTER_ERROR = 'Password should have at least one upper case letter'
NOT_CONTAIN_LOWERCASE_LETTER_ERROR = 'Password should have at least one lower case letter'
NOT_CONTAIN_NUMBER_ERROR = 'Password should have at least one number'
NOT_VALID_CHAR_ERROR = 'Password should not contain:{sign}'
MIN_PASSWORD_LENGTH = 6
MAX_PASSWORD_LENGTH = 20
LEGAL_CHARACTERS = {'!', '@', '#', '$', '%',
                    '^', '&', '*', '(', ')', '=', '-', '_', '+'}


def hash_password(password: str) -> str:
    return _hasher.hash(password)


def verify(hashed_password: str, raw_password: str) -> bool:
    try:
        _hasher.verify(hashed_password, raw_password)
        return True
    except argon2.exceptions.VerifyMismatchError:
        return False
    return False


def validate(password: str):
    if (len(password) < MIN_PASSWORD_LENGTH or len(password) > MAX_PASSWORD_LENGTH):
        raise PasswordFormatError(err=PASSWORD_LENGTH_ERROR)
    elif not re.search("[a-z]", password):
        raise PasswordFormatError(err=NOT_CONTAIN_LOWERCASE_LETTER_ERROR)
    elif not re.search("[0-9]", password):
        raise PasswordFormatError(err=NOT_CONTAIN_NUMBER_ERROR)
    elif not re.search("[A-Z]", password):
        raise PasswordFormatError(err=NOT_CONTAIN_UPPERCASE_LETTER_ERROR)

    for c in password:
        if not c.isalnum() and c not in LEGAL_CHARACTERS:
            raise PasswordFormatError(err=NOT_VALID_CHAR_ERROR.format(sign=c))
