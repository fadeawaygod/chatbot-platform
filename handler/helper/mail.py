import re

from exception import MailFormatInvalidError
from service_caller import ServiceCaller


def validate_mail_address_format(mail_address: str):
    if not re.match(r"[^@]+@[^@]+\.[^@]+", mail_address):
        raise MailFormatInvalidError(mail=mail_address)


async def send_mail(mail_address: str, name: str, subject: str, content: str) -> None:
    await ServiceCaller.call_mail_service(mail_address, name, subject, content)
