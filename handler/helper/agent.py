import json

from exception import AgentServiceError
from handler.bot_handler_base import BotHandlerBase
from handler.helper.notification import notify
from handler.service.session_manager import SessionManager
from service_caller import NotificationFunction, ServiceCaller


async def set_up_agent(bot: dict, config_history_data={}):
    async def update_bot_and_notify(bot, status, failed_log=''):
        session = SessionManager.get_session()
        session = session.update(collection=BotHandlerBase.COLLECTION_BOT)\
            .filter_by(column='id', value=bot['id']) \
            .filter_by(column='owner', value=bot['owner']) \
            .data(column='deploy_status', value=status)

        if failed_log:
            session.data(column='last_failed_log', value=failed_log)

        await session.execute()
        await call_notification_service(bot, status, failed_log)

    async def call_notification_service(bot, status, failed_log=''):
        content = {'status': status, 'bot_name': bot['name']}
        if failed_log:
            content['failed_log'] = failed_log
        await notify(
            NotificationFunction.UPDATE_DEPLOY_STATUS,
            bot['owner'],
            bot['id'],
            content=content)

    async def save_data_to_config_history(bot_id: str, config_history_data: dict):
        """save config(for agent) and flow(for frontend) to config_history

        Args:
            bot_id (str): 
            config_history_data (dict): contains flow and config
        """
        if not config_history_data or 'flow' not in config_history_data or 'config' not in config_history_data:
            return

        session = SessionManager.get_session()
        await session.insert(collection=BotHandlerBase.COLLECTION_CONFIG_HISTORY)\
            .data(column='bot_id', value=bot_id)\
            .data(column='flow', value=config_history_data['flow'])\
            .data(column='config', value=config_history_data['config'])\
            .execute()

    bot_id = bot['id']
    try:
        config_str = json.dumps(bot['config'], ensure_ascii=False)
        await ServiceCaller.call_bot_setup_service(bot_id, config_str)
        await save_data_to_config_history(bot_id, config_history_data)
    except Exception as e:
        err_message = AgentServiceError.message.format(
            err=str(e))
        await update_bot_and_notify(
            bot,
            BotHandlerBase.BOT_STATUS_FAILED,
            failed_log=err_message
        )
        raise AgentServiceError(err=str(e))

    await update_bot_and_notify(
        bot,
        BotHandlerBase.BOT_STATUS_READY
    )
