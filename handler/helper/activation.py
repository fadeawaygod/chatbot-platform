
import uuid


def generate_activation(expired_time: int) -> dict:
    """generate an activation dic

    Args:
        expired_time (int): int(unix timestamp in second)

    Returns:
        dict: {'code': str, 'expired_time': int(unix timestamp in second)}
    """
    return {
        'code': uuid.uuid4().hex,
        'expired_time': expired_time
    }
