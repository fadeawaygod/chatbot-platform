"""
    This is a module for middleware to validate a signature.
    You can also use the scripts to generate a signature with nonce&timestamp automatically generated,
    by change the secret&body in code.

"""
import json
import hashlib
import hmac
import base64


def generate_signature(
    body: dict,
    timestamp: int,
    nonce: str,
    secret: str
) -> str:
    """ validate a signature, the steps listed following:
        1. compose a dictionary, contains: "timestamp", "nonce" and "data".
        2. stringfy this dictionary
        3. use HMAC-SHA256 to hash(with key)
        4. encode with base64
    Args:
        body (dict): request body
        timestamp (int): Unix timestamp(UTC+0) in second
        nonce (str): nonce is a random number
        secret (str): used to hash
    """
    request = {
        "timestamp": timestamp,
        "nonce": nonce,
    }
    if body is not None:
        request["data"] = body

    request_str = json.dumps(request)
    request_str = request_str.lower()
    request_bytes = bytes(request_str, 'utf-8')
    key_bytes = bytes(secret, 'utf-8')
    signature = hmac.new(key_bytes, request_bytes, hashlib.sha256).digest()
    signature_base64 = base64.b64encode(signature)
    return signature_base64.decode("utf-8")


if __name__ == "__main__":
    import time
    import uuid

    secret = "602cc880f63b33db5167d79c"
    body = {
        "uid": "test-tom",
        "password": "Sr123456",
        "app_id": "test_app",
        "client_id": "test_client2"
    }
    timestamp = int(time.time())
    nonce = uuid.uuid4().hex

    signature = generate_signature(
        body,
        timestamp,
        nonce,
        secret
    )
    print(f"timestamp: {timestamp}")
    print(f"nonce: {nonce}")
    print(f"signature: {signature}")
