from exception import UserNotFoundError
from service_caller import AuthFunction, ServiceCaller

from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager


class SignInHandler(HandlerBase):
    COLLECTION_USER = 'user'

    async def post(self):
        async def get_user(uid: str) -> dict:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_USER)\
                .filter_by(column='uid', value=uid) \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if not result['data']:
                raise UserNotFoundError(uid=uid)
            user = result['data'][0]
            return user

        def extract_user(user: dict, access_token: str) -> dict:
            return {
                "uid": user.get("uid", ""),
                "access_token":  access_token,
                "role":  user.get("role", []),
                "name":  user.get("name", ""),
                "phone":  user.get("phone", ""),
                "group":  user.get("group", ""),
                "mail_address":  user.get("mail_address", ""),
            }

        uid = self._try_get_json_value('uid')
        password = self._try_get_json_value('password')
        client_id = self._try_get_json_value('client_id')
        result = await ServiceCaller.call_auth_service(
            AuthFunction.SIGN_IN,
            {
                "uid": uid,
                "password": password,
                "client_id": client_id,
            }
        )
        access_token = result["user"]["access_token"]
        user = await get_user(uid)
        response = {
            "user": extract_user(user, access_token)
        }

        self.write(self._get_success_result(response))
