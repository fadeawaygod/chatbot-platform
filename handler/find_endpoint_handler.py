from exception import NoPermissionError
from handler.bot_handler_base import BotHandlerBase
from service_caller import DbFunction, ServiceCaller
from .service.session_manager import SessionManager
from handler.bot_handler_base import BotHandlerBase


class FindEndpointHandler(BotHandlerBase):

    async def get(self):
        token_id = self._try_get_url_parameter_value('token_id')
        accessors = self._try_get_url_parameter_value('accessors')
        if type(accessors) == str:
            accessors = eval(accessors)
        session = SessionManager.get_session()
        result = await session.query(collection=self.COLLECTION_ENDPOINT)\
            .filter_by('id', token_id)\
            .execute()
        result_data = result.get('data', [])
        response_body = {
            'endpoint': []
        }
        if result_data[0]["user_id"] in accessors:
            if accessors[result_data[0]["user_id"]]["edit"] == 'true':
                response_body = {
                    'endpoint': result_data
                }
        self.write(self._get_success_result(response_body))
        self.finish()
