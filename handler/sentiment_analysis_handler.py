import datetime
import time

from error_code import EXCEED_MAX_TIME_INTERVAL_ERR
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class SentimentAnalysisHandler(BotHandlerBase):
    MAX_QUERY_TIME_INTERVAL = 5356800000
    SENTIMENT_SCORE_MAX_ROUND = 3

    async def get(self, bot_id):
        def integrate_message_sentiment_data(message_sentiment_data: list) -> dict:
            total_dic = {}
            for daily_data in message_sentiment_data:
                for sentiment, score in daily_data['sentiment_averages'].items():
                    if sentiment not in total_dic:
                        total_dic[sentiment] = 0
                    total_dic[sentiment] += score
            result = {sentiment: round(total_score/len(message_sentiment_data), SentimentAnalysisHandler.SENTIMENT_SCORE_MAX_ROUND)
                      for sentiment, total_score in total_dic.items()}
            return result

        async def get_message_sentiment(bot_id: str, start_time: int, end_time: int) -> list:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_MESSAGE_SENTIMENT)\
                .filter_by(column='bot_id', value=bot_id)\
                .filter_by(column='date_timestamp', value=start_time, operator='>=')\
                .filter_by(column='date_timestamp', value=end_time, operator='<')\
                .projection_by(column='_id', show=False)\
                .projection_by(column='date_timestamp', show=True)\
                .projection_by(column='sentiment_averages', show=True)\
                .sort_by(column='updated', asc=True)
            result = await query.execute()

            return result.get('data', [])

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        start_date_timestamp = int(
            self._try_get_url_parameter_value('start_date_timestamp'))
        end_date_timestamp = int(
            self._try_get_url_parameter_value('end_date_timestamp'))
        if end_date_timestamp-start_date_timestamp > SentimentAnalysisHandler.MAX_QUERY_TIME_INTERVAL:
            self._finish_with_failed_result(
                EXCEED_MAX_TIME_INTERVAL_ERR['message'],
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=EXCEED_MAX_TIME_INTERVAL_ERR['code'],)

        message_sentiment_data = await get_message_sentiment(
            bot_id,
            start_date_timestamp,
            end_date_timestamp
        )
        integrated_analysis = integrate_message_sentiment_data(
            message_sentiment_data)

        response_body = {
            'analysis': integrated_analysis
        }
        self.write(self._get_success_result(response_body))
