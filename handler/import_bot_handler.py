from typing import List

import tornado.web

from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class ImportBotHandler(BotHandlerBase):
    ERR_MSG = 'Wrong json key'
    ERR_ENTITY_FORMAT_MSG = 'Wrong Entity Format'
    ERR_INTENT_FORMAT_MSG = 'Wrong Intent Format'
    ERR_UTTERANCE_FORMAT_MSG = 'Wrong Utterance Format'

    async def post(self, bot_id):
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        json_input = self.json_args

        self._check_json_format(json_input)
        old_new_entity_id_mapping = old_entity_id_name_mapping = {}
        old_new_intent_id_mapping = {}

        if json_input['entities']:
            entities_data = self._convert_to_entity_insert_many_format(
                json_input['entities'], bot_id)

            old_entity_id_name_mapping = self._get_id_name_mapping(
                json_input['entities'])
            entity_responses = await self._insert_many_entity(entities_data)
            new_entity_name_id_mapping = self._get_name_id_mapping(
                entity_responses)
            old_new_entity_id_mapping = self._get_old_new_entity_id_mapping(
                old_entity_id_name_mapping, new_entity_name_id_mapping)
            json_input['intents'] = self._get_new_entity_id(
                json_input['intents'], old_new_entity_id_mapping)

        if json_input['intents']:
            intents_data = self._convert_to_intent_insert_many_format(
                json_input['intents'], bot_id)
            intent_responses = await self._insert_many_intent(intents_data)
            old_new_intent_id_mapping = self._get_old_new_id_mapping(
                json_input['intents'],
                intent_responses)
            new_intent_name_id_mapping = self._get_name_id_mapping(
                intent_responses)
            utterance_insert_data = self._prepare_insertmany_utterance(
                json_input['utterances'], old_new_intent_id_mapping, old_new_entity_id_mapping)
            utterance_response = await self._insert_many_utterance(utterance_insert_data)
            old_new_utterance_id_mapping = self._get_old_new_id_mapping(
                json_input['utterances'],
                utterance_response)
            await self.update_intent_utterance_count([response['id'] for response in intent_responses])

        if json_input['dialogs']:
            await self._import_dialog(json_input['dialogs'], bot_id, old_new_utterance_id_mapping)

        nlu_id_dict = {}
        if 'endpoints' in json_input:
            nlu_id_dict = await self._insert_endpoints(json_input['id'], json_input['endpoints'])

        flow = self._update_flow_to_new_intent_id(
            json_input['flow'], old_new_intent_id_mapping)
        flow = self._get_flow_new_entity_id(
            flow, old_entity_id_name_mapping, old_new_entity_id_mapping)
        await self._update_flow(flow, bot_id, nlu_id_dict)

        self.write(self._get_success_result())
        self.finish()

    def _check_json_format(self, json_input):
        def _check_entity_format(json_input):
            if 'entities' not in json_input:
                self._finish_with_failed_result(
                    self.ERR_ENTITY_FORMAT_MSG,
                    status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
            entity_key = ['name', 'entries', 'prompts', 'is_enabled', 'is_sys']
            entities = json_input['entities']
            for entity in entities:
                for key in entity_key:
                    if key not in entity:
                        self._finish_with_failed_result(
                            self.ERR_ENTITY_FORMAT_MSG,
                            status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)

        def _check_intent_format(json_input):
            if 'intents' not in json_input:
                self._finish_with_failed_result(
                    self.ERR_INTENT_FORMAT_MSG,
                    status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
            intents = json_input['intents']
            intent_key = ['name', 'priority', 'entities', 'id']
            for intent in intents:
                for key in intent_key:
                    if key not in intent:
                        self._finish_with_failed_result(
                            self.ERR_INTENT_FORMAT_MSG,
                            status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
                for entity_id in intent['entities']:
                    if type(entity_id) != str:
                        self._finish_with_failed_result(
                            self.ERR_INTENT_FORMAT_MSG,
                            status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)

        def _check_utterance_format(json_input):
            if 'utterances' not in json_input:
                self._finish_with_failed_result(
                    self.ERR_ENTITY_FORMAT_MSG,
                    status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
            utterances = json_input['utterances']
            utterance_key = ['intent_ids', 'text', 'entities']
            utterance_entity_key = ['start_position', 'text', 'len']
            for utterance in utterances:
                for entity in utterance['entities']:
                    for key in utterance_entity_key:
                        if key not in entity:
                            self._finish_with_failed_result(
                                self.ERR_UTTERANCE_FORMAT_MSG,
                                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
                for key in utterance_key:
                    if key not in utterance:
                        self._finish_with_failed_result(
                            self.ERR_UTTERANCE_FORMAT_MSG,
                            status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)

        _check_entity_format(json_input)
        _check_intent_format(json_input)
        _check_utterance_format(json_input)

    async def _insert_many_entity(self, entity_data):
        session = SessionManager.get_session()
        try:
            entity_response = await session.insert_many(collection=self.COLLECTION_ENTITY)\
                .data(column='ignore', value=entity_data)\
                .execute()
        except Exception as e:
            print(e)
        return entity_response['data']

    async def _insert_many_intent(self, intent_data):
        session = SessionManager.get_session()
        try:
            intent_response = await session.insert_many(collection=self.COLLECTION_INTENT)\
                .data(column='ignore', value=intent_data)\
                .execute()
        except Exception as e:
            print(e)
        return intent_response['data']

    async def _insert_many_utterance(self, utterance_insert_data: list) -> list:
        if utterance_insert_data == []:
            return []
        session = SessionManager.get_session()
        response = await session.insert_many(collection=self.COLLECTION_UTTERANCE)\
            .data(column='ignore', value=utterance_insert_data)\
            .execute()
        return response['data']

    async def _import_dialog(self, dialogs: List[dict], bot_id: str, utterance_id_mapping: dict):
        dialogs = self._replace_utterance_id(dialogs, utterance_id_mapping)
        dialogs = self._append_bot_id(bot_id, dialogs)
        await self._insert_many_dialog(dialogs)

    def _replace_utterance_id(self, dialogs: List[str], utterance_id_mapping: dict):
        for dialog in dialogs:
            for utterance in dialog['utterances']:
                raw_utterance_id = utterance['utterance_id']
                new_utterance_id = utterance_id_mapping[raw_utterance_id]
                utterance['utterance_id'] = new_utterance_id
        return dialogs

    def _append_bot_id(self, bot_id: str, items: List[str]) -> List[str]:
        for item in items:
            item['bot_id'] = bot_id
        return items

    async def _insert_many_dialog(self, dialogs: List[dict]):
        session = SessionManager.get_session()
        response = await session.insert_many(collection=self.COLLECTION_DIALOG)\
            .data(column='ignore', value=dialogs)\
            .execute()

    def _update_flow_to_new_intent_id(self, flows, old_new_intent_id_mapping):
        if 'states' not in flows:
            return flows
        for state_id in flows['states']:
            for node in flows['states'][state_id]['nodes']:
                if 'transitions' not in node['info']:
                    continue
                for transition in node['info']['transitions']:
                    if 'conditions' not in transition:
                        continue
                    for condition in transition['conditions']:
                        if condition['operation'] == 'intent':
                            if condition['id'] in old_new_intent_id_mapping:
                                condition['id'] = old_new_intent_id_mapping[condition['id']]
        return flows

    async def _update_flow(self, flows, bot_id, nlu_id_dict):
        session = SessionManager.get_session()
        flow_id = await self._get_flow_id(bot_id)
        flows = self.fix_flow_nlu_id(flows, nlu_id_dict)
        await session.update(collection=self.COLLECTION_CHATBOT_FLOW)\
            .filter_by(column='id', value=flow_id)\
            .data(column='content', value=flows)\
            .execute()

    def fix_flow_nlu_id(self, flows, nlu_id_dict):
        if nlu_id_dict == {}:
            return flows
        if 'states' in flows:
            for state_id in flows['states']:
                for node in flows['states'][state_id]['nodes']:
                    if node['info']['widget']['type'] == 'nlu' and 'nlu_id' in node["info"]:
                        if node["info"]['nlu_id'] in nlu_id_dict:
                            node["info"]['nlu_id'] = nlu_id_dict[node["info"]['nlu_id']]
        return flows

    def _get_flow_new_entity_id(self, flows, old_entity_mapping={}, old_new_entity_id_mapping={}):
        if old_entity_mapping == {} or old_new_entity_id_mapping == {}:
            return flows
        old_entity_mapping = dict((v, k)
                                  for k, v in old_entity_mapping.items())
        if 'states' in flows:
            for state_id in flows['states']:
                for node in flows['states'][state_id]['nodes']:
                    if 'transitions' not in node['info']:
                        continue
                    for transition in node["info"]["transitions"]:
                        if 'conditions' not in transition:
                            continue
                        for condition in transition["conditions"]:
                            if "entities" not in condition:
                                continue
                            for entity in condition["entities"]:
                                if "id" in entity:
                                    entity["id"] = old_new_entity_id_mapping[entity["id"]]
                                else:
                                    if entity["entity"] in old_entity_mapping:
                                        entity["id"] = old_new_entity_id_mapping[old_entity_mapping[entity["entity"]]]
        return flows

    async def _get_flow_id(self, bot_id):
        session = SessionManager.get_session()
        response = await session.query(collection=self.COLLECTION_CHATBOT_FLOW)\
            .filter_by(column='bot_id', value=bot_id)\
            .execute()
        response = response.get('data', [])
        if not response:
            return await self._get_new_flow_id(bot_id)
        return response[0]['id']

    async def _get_new_flow_id(self, bot_id):
        session = SessionManager.get_session()
        response = await session.insert(collection=self.COLLECTION_CHATBOT_FLOW)\
            .data(column='bot_id', value=bot_id)\
            .execute()
        response = response.get('data', [])
        return response['id']

    def _convert_to_intent_insert_many_format(self, intents: list, bot_id: str) -> list:
        return [
            {
                'name': intent['name'],
                'bot_id': bot_id,
                'priority': intent['priority'],
                'entities': intent['entities'],
                'utterance_count': 0
            } for intent in intents
        ]

    def _convert_to_entity_insert_many_format(self, entities: list, bot_id: str) -> list:
        data = []
        for entity in entities:
            tmp_dic = {}
            tmp_dic['name'] = entity['name']
            tmp_dic['bot_id'] = bot_id
            tmp_dic['entries'] = entity['entries']
            tmp_dic['prompts'] = entity['prompts']
            tmp_dic['is_enabled'] = entity['is_enabled']
            tmp_dic['is_sys'] = entity['is_sys']
            data.append(tmp_dic)
        return data

    def _get_name_id_mapping(self, datas):
        mapping = {}
        for data in datas:
            mapping[data['name']] = data['id']
        return mapping

    def _get_id_name_mapping(self, datas):
        mapping = {}
        for data in datas:
            mapping[data['id']] = data['name']
        return mapping

    def _prepare_insertmany_utterance(self, utterances, intent_mapping, old_new_entity_id_mapping={}):
        utterance_insert_data = []
        for utterance in utterances:
            utterance['intent_ids'] = [intent_mapping[intent_id]
                                       for intent_id in utterance['intent_ids']]

            for entity in utterance['entities']:
                new_entity_id = old_new_entity_id_mapping[entity['entity_id']]
                entity['entity_id'] = new_entity_id

            utterance_insert_data.append(utterance)
        return utterance_insert_data

    def _get_new_entity_id(self, intents, old_new_entity_id_mapping):
        for intent in intents:
            new_entities = []
            for old_entity_id in intent['entities']:
                new_entities.append(old_new_entity_id_mapping[old_entity_id])
            intent['entities'] = new_entities
        return intents

    def _get_old_new_entity_id_mapping(self, old_entity_id_name_mapping, new_entity_name_id_mapping):
        old_new_entity_id_mapping = {}
        for i in old_entity_id_name_mapping:
            old_new_entity_id_mapping[i] = new_entity_name_id_mapping[old_entity_id_name_mapping[i]]
        return old_new_entity_id_mapping

    def _get_old_new_id_mapping(self, old_list: List[dict], new_list: List[dict]):
        return {
            old_item['id']: new_item['id']
            for old_item, new_item in zip(old_list, new_list)
        }

    async def _prepare_endpoints(self, bot_id: str, endpoints: List[dict]):
        old_nlu_id = []
        tmp_endpoints = []
        return_old_nlu_id = []
        for endpoint in endpoints:
            endpoint['nlu_name'] = endpoint['nlu_name'] + '-' + bot_id
            endpoint['user_id'] = self._user['uid']
            old_nlu_id.append(endpoint['nlu_id'])
            del endpoint['nlu_id']
        nlu_exist = await self._check_user_own_nlu_id(old_nlu_id)
        for i, element in enumerate(nlu_exist):
            if not element:
                tmp_endpoints.append(endpoints[i])
                return_old_nlu_id.append(old_nlu_id[i])
        return tmp_endpoints, return_old_nlu_id

    async def _insert_endpoints(self, bot_id: str, endpoints: List[dict]):
        session = SessionManager.get_session()
        nlu_id_dict = {}
        endpoints, old_nlu_id = await self._prepare_endpoints(bot_id, endpoints)
        response = await session.insert_many(collection=self.COLLECTION_ENDPOINT)\
            .data(column='ignore', value=endpoints)\
            .execute()
        for i, element in enumerate(old_nlu_id):
            nlu_id_dict[element] = response['data'][i]['id']
        return nlu_id_dict  # {old_nlu_id: new_nlu_id,...}

    async def _check_user_own_nlu_id(self, nlu_ids):
        session = SessionManager.get_session()
        nlu_id_list = []
        nlu_exist = []
        result = await session.query(collection=self.COLLECTION_ENDPOINT)\
            .filter_by('user_id', self._user['uid'])\
            .execute()
        result_data = result.get('data', [])
        for data in result_data:
            nlu_id_list.append(data['id'])
        for nlu_id in nlu_ids:
            if nlu_id in nlu_id_list:
                nlu_exist.append(True)
            else:
                nlu_exist.append(False)
        return nlu_exist
