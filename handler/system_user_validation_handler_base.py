from handler.handler_base import HandlerBase
from service_caller import ServiceCaller
from exception import TokenValidationError


class SystemUserValidationHandlerBase(HandlerBase):
    ERROR_INVALID_TOKEN = 'Invalid token'
    ERROR_AUTHORIZATION_FORMAT_ERROR = 'Authorization format error'
    ERROR_NO_PERMISSION = 'You have no permission to do this operation'

    async def prepare(self) -> None:
        async def validate_token():
            """example of self._user:'
                _id':'xxx'
                'created':1564459391851
                'mail_address':'xxx'
                'group':'xxx'
                'ip':'xxx'
                'role':['admin', 'group_admin', 'editor', 'user']
                'uid':'xxx'
                'updated':1564544106474

            Args:
                self ([type]): [description]
            """
            self.set_status(200)
            token = extract_token()
            result = await ServiceCaller.call_system_user_validation_service(token)
            if 'user' not in result:
                self._finish_with_failed_result(
                    self.ERROR_INVALID_TOKEN,
                    status_code=self.HTTP_UNAUTHORIZED_ERROR_CODE,
                    is_raise=True,
                    exception_class=TokenValidationError)
            self._user = result['user']

        def extract_token():
            authorization_value = self.request.headers.get('Authorization', '')
            if not authorization_value.startswith('Bearer '):
                raise TokenValidationError(
                    cause=self.ERROR_AUTHORIZATION_FORMAT_ERROR)

            return authorization_value[7:]

        super().prepare()

        await validate_token()
