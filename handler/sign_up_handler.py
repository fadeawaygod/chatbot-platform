import asyncio
from typing import List

from config.config import ACTIVATE_URL
from exception import (GroupNotExistError, MailDuplicatedError, RoleEmptyError,
                       RoleInvalidError, UIDDuplicatedError)
from service_caller import ServiceCaller, AuthFunction

from handler.handler_base import HandlerBase
from handler.helper.mail import validate_mail_address_format
from handler.helper.password import validate
from handler.service.session_manager import SessionManager


class SignUpHandler(HandlerBase):
    COLLECTION_USER = 'user'
    COLLECTION_GROUP = 'group'
    COLLECTION_NLU_RESOURCE = 'nlu_resource'
    VALID_ROLE = ['group_admin', 'editor']
    RESOURCE_COUNT = 5
    ACTIVATION_CODE_EXPIRED_TIME_OFFSET = 86400
    ACTIVATION_SUBJECT = 'Activate your Silkrode account'
    END_USER_GROUP_NAME_TEMPLATE = 'enduser_{uid}'

    async def post(self):
        def check_role_valid(roles: List[str]):
            if not roles:
                raise RoleEmptyError()
            for role in roles:
                if role not in self.VALID_ROLE:
                    raise RoleInvalidError(role=role)

        async def check_user_unique(uid: str, mail_address: str):
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_USER)\
                .filter_by(column='', value=[{'uid': uid}, {'mail_address': mail_address}], operator="or") \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if result['data']:
                first_duplicated_data = result['data'][0]
                if first_duplicated_data['uid'] == uid:
                    raise UIDDuplicatedError(uid=uid)
                if first_duplicated_data['mail_address'] == mail_address:
                    raise MailDuplicatedError(mail=mail_address)

        async def check_group_exist(group_id: str):
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_GROUP)\
                .filter_by(column='id', value=group_id) \
                .projection_by(column='_id', show=False)
            result = await query.execute()
            if not result['data']:
                raise GroupNotExistError(group_id=group_id)

        async def insert_user(
            uid: str,
            name: str,
            group: str,
            roles: List[str],
            mail_address: str,
            phone: str,
        ):
            session = SessionManager.get_session()

            await session.insert(collection=self.COLLECTION_USER)\
                .data(column='uid', value=uid)\
                .data(column='name', value=name)\
                .data(column='group', value=group)\
                .data(column='role', value=roles)\
                .data(column='mail_address', value=mail_address)\
                .data(column='phone', value=phone)\
                .execute()

        async def insert_enduser_group(uid: str) -> str:
            session = SessionManager.get_session()
            result = await session.insert(collection=self.COLLECTION_GROUP)\
                .data(column='name', value=self.END_USER_GROUP_NAME_TEMPLATE.format(uid=uid))\
                .data(column='is_enduser', value=True)\
                .execute()
            return result['data']['id']

        async def insert_nlu_resources(uid: str):
            queries = []
            for _ in range(self.RESOURCE_COUNT):
                queries.append(insert_nlu_resource(uid))
            await asyncio.gather(*queries)

        async def insert_nlu_resource(uid: str):
            session = SessionManager.get_session()

            await session.insert(collection=self.COLLECTION_NLU_RESOURCE)\
                .data(column='uid', value=uid)\
                .data(column='is_training', value=False)\
                .data(column='training_nlu_id', value='')\
                .data(column='last_trained_time', value=0)\
                .data(column='last_infered_time', value=0)\
                .data(column='nlu_token', value='')\
                .data(column='bot_id', value='')\
                .execute()

        uid = self._try_get_json_value('uid')
        password = self._try_get_json_value('password')
        name = self.json_args.get('NAME', uid)
        group = self.json_args.get('group', '')
        roles = self.json_args.get('roles', ['editor'])
        mail_address = self.json_args.get('mail_address', '')
        validate_mail_address_format(mail_address)
        phone = self.json_args.get('phone', '')

        check_role_valid(roles)
        validate(password)
        await check_user_unique(uid, mail_address)
        if group:
            await check_group_exist(group)

        await ServiceCaller.call_auth_service(
            AuthFunction.SIGN_UP,
            {
                "uid": uid,
                "password": password,
                "mail_address": mail_address,
                "activation_mail_data": self.get_activation_mail_data(name)
            }
        )
        if not group:
            group = await insert_enduser_group(uid)

        await insert_user(
            uid,
            name,
            group,
            roles,
            mail_address,
            phone,
        )
        await insert_nlu_resources(uid)

        self.write(self._get_success_result())

    @classmethod
    def get_activation_mail_data(cls, name: str):
        activation_link = ACTIVATE_URL + \
            r"?uid={{uid}}&activation_code={{activation_code}}"
        # TODO can be refactor to asnyc manner in the future
        with open('static/activation_mail.html', 'r', encoding='utf-8') as f:
            activation_html_template = f.read()
        activation_html_template = activation_html_template.replace(
            r'{{user_name}}', name)
        activation_html_template = activation_html_template.replace(
            r'{{link}}', activation_link)
        return {
            "subject": cls.ACTIVATION_SUBJECT,
            "content_template": activation_html_template
        }
