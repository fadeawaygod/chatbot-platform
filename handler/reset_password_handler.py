from service_caller import AuthFunction, ServiceCaller

from handler.handler_base import HandlerBase


class ResetPasswordHandler(HandlerBase):
    async def post(self):
        uid = self._try_get_json_value('uid')
        reset_password_token = self._try_get_json_value('token')
        password = self._try_get_json_value('password')
        await ServiceCaller.call_auth_service(
            AuthFunction.RESET_PASSWORD,
            {
                "uid": uid,
                "token": reset_password_token,
                "password": password,
            }
        )

        self.write(self._get_success_result())
