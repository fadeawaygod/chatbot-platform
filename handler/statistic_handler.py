import datetime

from error_code import EXCEED_MAX_TIME_INTERVAL_ERR
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager


class StatisticHandler(BotHandlerBase):
    DAY_MILLSECOND = 86400000
    HOUR_MILLSECOND = 3600000
    MAX_QUERY_TIME_INTERVAL = 5356800000
    SECOND_TO_MILLISECOND_TIMES = 1000

    async def get(self, bot_id):
        def extract_message_traffic(data: list, start_time: int, end_time: int, show_day_statistic: bool):
            message_traffic = initialize_traffic_list(
                start_time, end_time, show_day_statistic, False)

            for record in data:
                updated = int(record['updated'])
                for item in message_traffic:
                    if item['start_time_stamp'] <= updated < item['end_time_stamp']:
                        item['count'] += 1
                        if show_day_statistic:
                            append_day_statistic(item, updated)
                        break
            return remove_time_stamp(message_traffic)

        def extract_user_traffic_and_user_count(data: list, start_time: int, end_time: int, show_day_statistic: bool):
            user_traffic = initialize_traffic_list(
                start_time, end_time, show_day_statistic, True)
            for record in data:
                updated = int(record['updated'])
                user_id = record['user_id']
                for item in user_traffic:
                    if item['start_time_stamp'] <= updated < item['end_time_stamp']:
                        if user_id not in item['user']:
                            item['count'] += 1
                            item['user'].add(user_id)
                        if show_day_statistic:
                            append_user_day_statistic(item, user_id, updated)
                        break

            user_count = get_total_user_count(user_traffic)
            user_traffic = remove_time_stamp(user_traffic)
            user_traffic = remove_user_set(user_traffic)
            user_traffic = remove_daily_user_set(user_traffic)
            return user_traffic, user_count

        def initialize_traffic_list(start_time: int, end_time: int, show_day_statistic: bool, has_user: bool):
            result = []
            for date_timestamp in range(start_time, end_time, StatisticHandler.DAY_MILLSECOND):
                new_item = {
                    'start_time_stamp': date_timestamp,
                    'end_time_stamp': date_timestamp + StatisticHandler.DAY_MILLSECOND,
                    'date': datetime.datetime.fromtimestamp(date_timestamp//StatisticHandler.SECOND_TO_MILLISECOND_TIMES).strftime('%Y-%m-%d'),
                    'count': 0,
                }
                if show_day_statistic:
                    new_item['day_statistic'] = {}
                if has_user:
                    new_item['user'] = set()
                result.append(new_item)
            return result

        def append_day_statistic(item, data_time):
            hour = (data_time-item['start_time_stamp']
                    )//StatisticHandler.HOUR_MILLSECOND
            if hour not in item['day_statistic']:
                item['day_statistic'][hour] = 0
            item['day_statistic'][hour] += 1

        def append_user_day_statistic(item, user_id, data_time):
            hour = (data_time-item['start_time_stamp']
                    )//StatisticHandler.HOUR_MILLSECOND
            if hour not in item['day_statistic']:
                item['day_statistic'][hour] = {'count': 0, 'user': set()}
            if user_id not in item['day_statistic'][hour]['user']:
                item['day_statistic'][hour]['count'] += 1
                item['day_statistic'][hour]['user'].add(user_id)

        def get_total_user_count(user_traffic):
            total_user = set()
            for item in user_traffic:
                total_user = total_user.union(item['user'])
            return len(total_user)

        def remove_time_stamp(traffic):
            for item in traffic:
                del item['start_time_stamp']
                del item['end_time_stamp']
            return traffic

        def remove_user_set(traffic):
            for item in traffic:
                del item['user']
            return traffic

        def remove_daily_user_set(traffic):
            for item in traffic:
                for hour in item.get('day_statistic', []):
                    item['day_statistic'][hour] = item['day_statistic'][hour]['count']
            return traffic

        async def get_message_history(bot_id, start_time, end_time, passage, integration_name):
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_MESSAGE_HISTORY)\
                .filter_by(column='bot_id', value=bot_id)\
                .filter_by(column='from_user', value=True)\
                .projection_by(column='_id', show=False)\
                .projection_by(column='user_id', show=True)\
                .projection_by(column='source_from', show=True)\
                .projection_by(column='hook_name', show=True)\
                .projection_by(column='updated', show=True)\
                .sort_by(column='updated', asc=True)

            if start_time:
                query = query.filter_by(
                    column='updated', value=start_time, operator='>=')
            if end_time:
                query = query.filter_by(
                    column='updated', value=end_time, operator='<')
            if passage:
                query = query.filter_by(column='source_from', value=passage)
            if integration_name:
                query = query.filter_by(
                    column='hook_name', value=integration_name)
            result = await query.execute()

            return result
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        show_day_statistic = self.get_argument(
            'show_day_statistic', 'false').lower() == 'true'
        passage = self.get_argument('passage', '')
        integration_name = self.get_argument('integration_name', '')

        start_time = int(self._try_get_url_parameter_value('start_time'))
        end_time = int(self._try_get_url_parameter_value('end_time'))
        if end_time-start_time > StatisticHandler.MAX_QUERY_TIME_INTERVAL:
            self._finish_with_failed_result(
                EXCEED_MAX_TIME_INTERVAL_ERR['message'],
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=EXCEED_MAX_TIME_INTERVAL_ERR['code'],)
        result = await get_message_history(bot_id, start_time, end_time, passage, integration_name)

        data = result.get('data', [])
        message_traffic = extract_message_traffic(
            data, start_time, end_time, show_day_statistic)
        user_traffic, user_count = extract_user_traffic_and_user_count(
            data, start_time, end_time, show_day_statistic)
        response_body = {
            'statstic': {
                'message_traffic': message_traffic,
                'user_traffic': user_traffic,
                'user_count': user_count,
            }
        }
        self.write(self._get_success_result(response_body))
