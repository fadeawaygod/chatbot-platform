from service_caller import AuthFunction, ServiceCaller

from handler.handler_base import HandlerBase


class UserActivationHandler(HandlerBase):

    async def post(self):
        uid = self._try_get_json_value('uid')
        activation_token = self._try_get_json_value('token')
        await ServiceCaller.call_auth_service(
            AuthFunction.USER_ACTIVATION,
            {
                "uid": uid,
                "token": activation_token,
            }
        )

        self.write(self._get_success_result())
