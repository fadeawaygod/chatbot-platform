from error_code import UID_DUPLICATED_ERR
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.service.session_manager import SessionManager
from handler.helper.password import hash_password, validate
from handler.helpers import get_collection_len


class CustomerServiceAuthHandler(BotHandlerBase):
    ADMIN_ROLE = 'admin'
    COLLECTION = 'cs_hub_user'

    async def get(self, bot_id: str):
        async def get_cs_users_len(
                bot_id: str,
                role: str) -> int:
            query_filter = {'bot_id': bot_id, 'role': role}
            return await get_collection_len(self.COLLECTION, query_filter)

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)
        role = self._try_get_url_parameter_value('role')
        skip = int(self.get_argument('skip', 0))
        limit = int(self.get_argument('limit', 0))
        result = await self._get_customer_service_auth(bot_id, role, skip, limit)
        response_body = {
            'cs_auth_users': result,
            'len': await get_cs_users_len(bot_id, role)}
        self.write(self._get_success_result(response_body))

    async def patch(self, bot_id):
        async def create_cs_auth(bot_id: str, uid: str, name: str, hashed_password: str):
            session = SessionManager.get_session()
            create_session = session.insert(collection=self.COLLECTION)\
                .data(column='bot_id', value=bot_id)\
                .data(column='uid', value=uid)\
                .data(column='password', value=hashed_password)\
                .data(column='role', value=self.ADMIN_ROLE)\
                .data(column='name', value=name)\
                .data(column='enabled', value=True)

            await create_session.execute()

        async def patch_cs_auth(bot_id: str, uid: str, name: str,  hashed_password: str):
            session = SessionManager.get_session()
            update_session = session.update(collection=self.COLLECTION)\
                .filter_by(column='bot_id', value=bot_id)\
                .data(column='bot_id', value=bot_id)\
                .data(column='uid', value=uid)\
                .data(column='name', value=name)

            if hashed_password:
                update_session = update_session.data(
                    column='password', value=hashed_password)

            await update_session.execute()

        async def is_uid_duplicated(bot_id: str, uid: str) -> bool:
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION)\
                .filter_by(column='bot_id', value=bot_id, operator='!=')\
                .filter_by(column='uid', value=uid)\
                .projection_by(column='_id', show=False) \
                .projection_by(column='uid', show=True)

            query_result = await query.execute()
            return len(query_result['data']) > 0

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        uid = self._try_get_json_value('uid')
        name = self._try_get_json_value('name')
        password = self.json_args.get('password', '')
        if password:
            validate(password)
        hashed_password = hash_password(password) if password else ''
        result = await self._get_customer_service_auth(bot_id, self.ADMIN_ROLE)

        if await is_uid_duplicated(bot_id, uid):
            self._finish_with_failed_result(
                err_msg=UID_DUPLICATED_ERR['message'],
                status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                error_code=UID_DUPLICATED_ERR['code']
            )

        if result:
            await patch_cs_auth(bot_id, uid, name, hashed_password)
        else:
            await create_cs_auth(bot_id, uid, name, hashed_password)

        self.write(self._get_success_result())

    async def _get_customer_service_auth(
        self,
        bot_id: str,
        role: str,
        skip=0,
        limit=0
    ) -> dict:
        session = SessionManager.get_session()
        query = session.query(collection=self.COLLECTION)\
            .filter_by(column='bot_id', value=bot_id)\
            .filter_by(column='role', value=role)\
            .projection_by(column='_id', show=False) \
            .projection_by(column='uid', show=True)\
            .projection_by(column='name', show=True)
        if skip:
            query = query.skip(skip)
        if limit:
            query = query.limit(limit)

        query_result = await query.execute()
        return query_result['data']
