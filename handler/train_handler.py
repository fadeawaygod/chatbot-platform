import asyncio
from typing import List, Tuple

from bot_event import NLU_DATA_UNCHANGED
from exception import (LabeledEntityNotEnough, LabeledEntityNotEnough, NotPermittedToUseNluError,
                       TrainingFailedError, UtteranceNotEnough, NoNLUResourceAreAvailableError, BotAlreadyTrainingError)
from handler.bot_handler_base import BotHandlerBase, PermissionType
from handler.helper.notification import notify
from handler.service.session_manager import SessionManager
from service_caller import NluFunction, NotificationFunction, ServiceCaller


class TrainHandler(BotHandlerBase):
    MINIMUM_UTTERANCE_COUNT_OF_INTENT = 5
    MINIMUM_LABLED_ENTITY_COUNT_OF_ENTITY = 5

    async def post(self):
        async def update_bot_and_notify(bot, status, tmp_nlu_id='', tmp_nlu_token='', failed_log='', error_code=0, parameters={}):
            session = SessionManager.get_session()
            session = session.update(collection=self.COLLECTION_BOT)\
                .filter_by(column='id', value=bot['id']) \
                .filter_by(column='owner', value=bot['owner']) \
                .data(column='train_status', value=status)

            if tmp_nlu_id:
                session.data(column='tmp_nlu_id', value=tmp_nlu_id)
            if tmp_nlu_token:
                session.data(column='tmp_nlu_token', value=tmp_nlu_token)
            if failed_log:
                session.data(column='last_failed_log', value=failed_log)

            await session.execute()
            await call_notification_service(bot, status, failed_log, error_code, parameters)

        async def get_nlu_resources(user_id: str) -> List[dict]:
            session = SessionManager.get_session()
            session = session.query(collection=self.COLLECTION_NLU_RESOURCE)\
                .filter_by(column='uid', value=user_id)\
                .projection_by(column='_id', show=False)
            result = await session.execute()
            return result['data']

        async def validate_data(bot_id: str) -> Tuple[List, List]:
            intents, entities = await asyncio.gather(get_intents(bot_id),  get_entities(bot_id))
            utterances = await get_utterances(intents)
            check_intent_and_entity_countain_utterance(
                intents,
                entities,
                utterances
            )
            return intents, entities

        async def get_intents(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            session = session.query(collection=self.COLLECTION_INTENT)\
                .filter_by(column='bot_id', value=bot_id)\
                .projection_by(column='_id', show=False)
            result = await session.execute()
            return result['data']

        async def get_entities(bot_id: str) -> List[dict]:
            session = SessionManager.get_session()
            result = await session.query(collection=self.COLLECTION_ENTITY)\
                .filter_by(column='bot_id', value=bot_id) \
                .projection_by(column='_id', show=False) \
                .execute()
            return result.get('data', [])

        async def get_utterances(intents: List[dict]) -> List[dict]:
            intent_ids = [intent['id'] for intent in intents]
            session = SessionManager.get_session()
            query = session.query(collection=self.COLLECTION_UTTERANCE)\
                .filter_by(column='intent_ids', value=intent_ids, operator='in') \
                .projection_by(column='_id', show=False)

            result = await query.execute()
            return result['data']

        def check_intent_and_entity_countain_utterance(intents: List[dict], entities: List[dict], utterances: List[dict]):
            """
                check every intent has minimal utterance.
                check every entity has minimal labeled in utterance.
            """
            intent_count = {intent['id']: {
                'name': intent['name'], 'count': 0} for intent in intents}
            entity_count = {entity['id']: {
                'name': entity['name'], 'count': 0} for entity in entities}
            for utterance in utterances:
                for intent_id in utterance['intent_ids']:
                    if intent_id in intent_count:
                        intent_count[intent_id]['count'] += 1
                tmp_entity_set = set()
                for entity in utterance['entities']:
                    entity_id = entity['entity_id']
                    if entity_id in tmp_entity_set:
                        continue
                    if entity_id in entity_count:
                        entity_count[entity_id]['count'] += 1
                        tmp_entity_set.add(entity_id)

            for intent_id, countObj in intent_count.items():
                if countObj['count'] < self.MINIMUM_UTTERANCE_COUNT_OF_INTENT:
                    raise UtteranceNotEnough(intent_name=countObj['name'])
            for entity_id, countObj in entity_count.items():
                if countObj['count'] < self.MINIMUM_LABLED_ENTITY_COUNT_OF_ENTITY:
                    raise LabeledEntityNotEnough(entity_name=countObj['name'])

        async def insert_train_log(bot_id: str, bot_status: str, error='', error_code=0, intent_count=-1, utterance_count=-1):
            session = SessionManager.get_session()
            session = session.insert(collection=self.COLLECTION_TRAIN_LOG)\
                .data(column='bot_id', value=bot_id)\
                .data(column='status', value=bot_status)

            if error:
                session.data(column='error', value=error)
                session.data(column='error_code', value=error_code)

            if intent_count >= 0:
                session.data(column='intent_count', value=intent_count)
            if utterance_count >= 0:
                session.data(column='utterance_count', value=utterance_count)

            await session.execute()

        async def call_notification_service(bot: dict, status: str, failed_log='', error_code=0, parameters={}):
            content = {'status': status, 'bot_name': bot['name']}
            if failed_log:
                content['failed_log'] = failed_log
                content['error_code'] = error_code
                content['parameters'] = parameters
            await notify(
                NotificationFunction.UPDATE_TRAIN_STATUS,
                bot['owner'],
                bot['id'],
                content=content)

        def select_resource(nlu_resources: List[dict], bot_id: str) -> dict:
            """select one not training nlu resource by:
               1. bot_id matched
               2. bot_id is empty
               3. the resource whose last_infered_time is minest
            """
            enabled_nlu_resources = [
                resource for resource in nlu_resources if not resource.get('is_training', False)]

            bot_id_matched_resource = next(
                (resource for resource in enabled_nlu_resources if resource['bot_id'] == bot_id), {})
            if bot_id_matched_resource:
                return bot_id_matched_resource

            first_empty_resource = next(
                (resource for resource in enabled_nlu_resources if not resource['bot_id']), {})
            if first_empty_resource:
                return first_empty_resource

            oldest_nlu_resource = min(
                enabled_nlu_resources, key=lambda r: r.get('last_infered_time', 0)) if enabled_nlu_resources else {}
            return oldest_nlu_resource

        async def set_resource_training(resource_id: str, training_nlu_id: str):
            session = SessionManager.get_session()
            session = session.update(collection=self.COLLECTION_NLU_RESOURCE)\
                .filter_by(column='id', value=resource_id) \
                .data(column='is_training', value=True) \
                .data(column='training_nlu_id', value=training_nlu_id)
            await session.execute()

        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        bot = await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)
        if bot.get('train_status', '') == self.TRAIN_STATUS_TRAINING:
            await update_bot_and_notify(
                bot,
                self.BOT_STATUS_FAILED,
                failed_log=BotAlreadyTrainingError.message,
                error_code=BotAlreadyTrainingError.code,
            )
            raise NotPermittedToUseNluError()
        user_id = bot['owner']
        if bot['train_status'] == self. TRAIN_STATUS_UNTRAINED or bot.get('is_nlu_data_modified', True):
            nlu_resources = await get_nlu_resources(user_id)
            if not nlu_resources:
                await update_bot_and_notify(
                    bot,
                    self.BOT_STATUS_FAILED,
                    failed_log=NotPermittedToUseNluError.message,
                    error_code=NotPermittedToUseNluError.code,
                )
                raise NotPermittedToUseNluError()
            target_nlu_resource = select_resource(nlu_resources, bot_id)
            if not target_nlu_resource:
                await update_bot_and_notify(
                    bot,
                    self.BOT_STATUS_FAILED,
                    failed_log=NoNLUResourceAreAvailableError.message,
                    error_code=NoNLUResourceAreAvailableError.code,
                )
                raise NoNLUResourceAreAvailableError()

            try:
                intents, entities = await validate_data(bot_id)
                await insert_train_log(
                    bot_id,
                    self.TRAIN_STATUS_START,
                    intent_count=len(intents),
                    utterance_count=len(entities)
                )
                result = await ServiceCaller.call_nlu_service(NluFunction.CREATE, resource_id=target_nlu_resource['id'])
                tmp_nlu_id = result['nlu_id']
                tmp_nlu_token = result['nlu_token_id']
                await set_resource_training(target_nlu_resource['id'], tmp_nlu_id)
                await update_bot_and_notify(
                    bot,
                    self.TRAIN_STATUS_TRAINING,
                    tmp_nlu_id,
                    tmp_nlu_token,
                )
                await ServiceCaller.call_nlu_service(NluFunction.START_FETCH, nlu_id=tmp_nlu_id)
            except Exception as e:
                err_message = TrainingFailedError.message.format(err=str(e))
                await update_bot_and_notify(
                    bot,
                    self.BOT_STATUS_FAILED,
                    failed_log=err_message,
                    error_code=e.code if hasattr(
                        e, 'code') else TrainingFailedError.code,
                    parameters=e.parameters if hasattr(
                        e, 'parameters') else {}
                )
                await insert_train_log(
                    bot_id,
                    self.BOT_STATUS_FAILED,
                    err_message,
                    TrainingFailedError.code)
        else:
            await notify(
                NotificationFunction.UPDATE_TRAIN_STATUS,
                bot['owner'],
                bot['id'],
                content={'id': NLU_DATA_UNCHANGED['id'], 'message':  NLU_DATA_UNCHANGED['message']})

        self.write(self._get_success_result())
