import asyncio

from exception import NoPermissionError, UIDEmptyError
from service_caller import DbFunction, ServiceCaller, AuthFunction

from handler.helper.password import validate
from handler.helper.regex import replace_reserved_char
from handler.helpers import get_collection_len
from handler.service.session_manager import SessionManager
from handler.validation_handler_base import UserRole, ValidationHandlerBase


class UsersHandler(ValidationHandlerBase):
    COLLECTION = 'user'
    COLLECTION_NLU_RESOURCE = 'nlu_resource'
    ERROR_GROUP_ID_NOT_IDENTICAL = 'you can only add user to your group'
    ERROR_USER_ID_NOT_IDENTICAL = 'you can only edit your profile'
    ERROR_CANT_ADD_ADMIN_IN_ROLE = 'you can\'t add admin in role'
    ERROR_UID_ALREADY_EXIST = 'this uid is already exist'
    ERROR_USER_IS_NOT_FOUND = 'user:{uid} is not found'
    DEFAULT_SKIP = 0
    RESOURCE_COUNT = 5
    VALID_PATCH_USER_KEY = ['name',
                            'group',
                            'phone',
                            'avatar_url',
                            'mail_address',
                            'role',
                            'password',
                            'enabled']

    async def get(self, uid=''):
        def get_query_user_dic(uid, group_id=''):
            query_user_dic = {
                'collection': self.COLLECTION,
                'filter': {},
                'projection': {
                    '_id': False,
                    'uid': True,
                    'name': True,
                    'group': True,
                    'phone': True,
                    'avatar_url': True,
                    'mail_address': True,
                    'role': True,
                },
                'sort': {
                    'updated': -1
                },
                'skip': int(self.get_argument('skip', self.DEFAULT_SKIP)),

            }
            limit = int(self.get_argument('limit', 0))
            if limit:
                query_user_dic['limit'] = limit
            if uid:
                query_user_dic['filter']['uid'] = uid
            if group_id:
                query_user_dic['filter']['group'] = group_id
            keyword = self.get_argument('keyword', '')
            if keyword:
                query_user_dic['filter']['uid'] = {
                    "$regex": replace_reserved_char(keyword)}
            role = self.get_argument('role', '')
            if role:
                query_user_dic['filter']['role'] = {"$in": [role]}
            return query_user_dic
        query_user_dic = {}
        if UserRole.ADMIN.value in self._user['role']:
            group_id = self.get_argument('group', '')
            query_user_dic = get_query_user_dic(uid, group_id)
        elif UserRole.GROUP_ADMIN.value in self._user['role']:
            belonged_group_id = self._user['group']
            query_user_dic = get_query_user_dic(uid, belonged_group_id)
        else:
            if not uid or self._user['uid'] == uid:
                belonged_group_id = self._user['group']
                query_user_dic = get_query_user_dic(uid, belonged_group_id)
            else:
                self._finish_with_failed_result(
                    self.ERROR_NO_PERMISSION,
                    self.HTTP_BAD_REQUEST_ERROR_CODE,
                    exception_class=NoPermissionError)

        query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, query_user_dic)
        users = query_result['data']
        if uid:
            if not users:
                self._finish_with_failed_result(
                    self.ERROR_USER_IS_NOT_FOUND.format(uid=uid),
                    status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)
            response_body = {
                'user': users[0]
            }
        else:
            response_body = {
                'users': users
            }
            if self.get_argument('show_len', '').lower() == 'true':
                response_body['len'] = await get_collection_len(self.COLLECTION, query_user_dic['filter'])

        self.write(self._get_success_result(response_body))

    async def post(self):
        def get_insert_user_dic(
            user_id,
            user_name,
            group,
            phone,
            mail_address,
            avatar_url,
            role
        ): return {
            'collection': self.COLLECTION,
            'data': {
                'uid': user_id,
                'name': user_name,
                'group': group,
                'phone': phone,
                'mail_address': mail_address,
                'avatar_url': avatar_url,
                'role': role,
                'enabled': True,
            }
        }

        def check_role_shouldnt_contain_admin(role):
            if UserRole.ADMIN.value in role:
                self._finish_with_failed_result(
                    self.ERROR_CANT_ADD_ADMIN_IN_ROLE,
                    self.HTTP_BAD_REQUEST_ERROR_CODE,
                    exception_class=NoPermissionError)

        async def check_uid_unique_in_group(group, user_id):
            query_result = await ServiceCaller.call_db_service(DbFunction.QUERY, get_query_user_dic(group, user_id))
            if query_result['data']:
                raise Exception(self.ERROR_UID_ALREADY_EXIST)

        def get_query_user_dic(group_id, user_id):
            return {
                'collection': self.COLLECTION,
                'filter': {
                    'uid': user_id,
                    'group': group_id
                },
                'projection': {
                    '_id': False,
                    'uid': True
                },
                'sort': {
                    'updated': -1
                }
            }

        async def insert_nlu_resources(uid: str):
            queries = []
            for _ in range(self.RESOURCE_COUNT):
                queries.append(insert_nlu_resource(uid))
            await asyncio.gather(*queries)

        async def insert_nlu_resource(uid: str):
            session = SessionManager.get_session()

            await session.insert(collection=self.COLLECTION_NLU_RESOURCE)\
                .data(column='uid', value=uid)\
                .data(column='is_training', value=False)\
                .data(column='training_nlu_id', value='')\
                .data(column='last_trained_time', value=0)\
                .data(column='last_infered_time', value=0)\
                .data(column='nlu_token', value='')\
                .data(column='bot_id', value='')\
                .execute()

        group = self._try_get_json_value('group')
        role = self._try_get_json_value('role')
        user_id = self._try_get_json_value('uid')
        password = self._try_get_json_value('password')
        mail_address = self._try_get_json_value('mail_address')
        if UserRole.ADMIN.value in self._user['role']:
            pass
        elif UserRole.GROUP_ADMIN.value in self._user['role']:
            self._check_group_id_identical(group)
            check_role_shouldnt_contain_admin(role)
        else:
            self._finish_with_failed_result(
                self.ERROR_NO_PERMISSION,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

        await check_uid_unique_in_group(group, user_id)
        if not user_id:
            raise UIDEmptyError()
        validate(password)

        user_name = self.json_args.get('name', '')
        phone = self.json_args.get('phone', '')
        avatar_url = self.json_args.get('avatar_url', '')

        await ServiceCaller.call_auth_service(
            AuthFunction.USERS,
            {
                "uid": user_id,
                "password": password,
                "mail_address": mail_address,
                "enabled": True
            },
        )

        insert_user_dic = get_insert_user_dic(
            user_id,
            user_name,
            group,
            phone,
            mail_address,
            avatar_url,
            role)
        await ServiceCaller.call_db_service(DbFunction.INSERT, insert_user_dic)
        await insert_nlu_resources(user_id)

        self.write(self._get_success_result())

    async def delete(self, user_id):
        def get_delete_user_dic(user_id, group_id=''):
            delete_dic = {
                'collection': self.COLLECTION,
                'filter': {
                    'uid': user_id}
            }
            if group_id:
                delete_dic['filter']['group'] = group_id
            return delete_dic

        async def delete_nlu_resources(uid: str):
            session = SessionManager.get_session()

            await session.delete(collection=self.COLLECTION_NLU_RESOURCE)\
                .filter_by(column='uid', value=uid)\
                .execute()

        delete_user_dic = {}
        if UserRole.ADMIN.value in self._user['role']:
            delete_user_dic = get_delete_user_dic(user_id)
        elif UserRole.GROUP_ADMIN.value in self._user['role']:
            delete_user_dic = get_delete_user_dic(user_id, self._user['group'])
        else:
            self._finish_with_failed_result(
                self.ERROR_NO_PERMISSION,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

        await ServiceCaller.call_auth_service(
            AuthFunction.USERS,
            rest_path=f"/{user_id}",
            method="DELETE"
        )

        await ServiceCaller.call_db_service(DbFunction.DELETE, delete_user_dic)
        await delete_nlu_resources(user_id)

        self.write(self._get_success_result())

    async def patch(self, user_id):
        def get_update_user_dic(user_id, data, filter_group_id=''):
            update_dic = {
                'collection': self.COLLECTION,
                'filter': {'uid': user_id},
                'data': {}
            }
            if filter_group_id:
                update_dic['filter']['group'] = filter_group_id
            if 'name' in data:
                update_dic['data']['name'] = data['name']
            if 'group' in data:
                update_dic['data']['group'] = data['group']
            if 'phone' in data:
                update_dic['data']['phone'] = data['phone']
            if 'avatar_url' in data:
                update_dic['data']['avatar_url'] = data['avatar_url']
            if 'mail_address' in data:
                update_dic['data']['mail_address'] = data['mail_address']
            if 'role' in data:
                update_dic['data']['role'] = data['role']
            return update_dic

        def check_user_id_identical(user_id):
            if user_id != self._user['uid']:
                self._finish_with_failed_result(
                    self.ERROR_USER_ID_NOT_IDENTICAL,
                    self.HTTP_BAD_REQUEST_ERROR_CODE,
                    exception_class=NoPermissionError)

        data = self._extract_body(self.VALID_PATCH_USER_KEY)
        if UserRole.ADMIN.value in self._user['role']:
            update_user_dic = get_update_user_dic(user_id, data)
        elif UserRole.GROUP_ADMIN.value in self._user['role']:
            update_user_dic = get_update_user_dic(
                user_id, data, self._user['group'])
        else:
            check_user_id_identical(user_id)
            update_user_dic = get_update_user_dic(
                user_id, data, self._user['group'])

        request_auth_api_body = {}
        if "password" in data:
            request_auth_api_body["password"] = data["password"]
        if "enabled" in data:
            request_auth_api_body["enabled"] = data["enabled"]
        if request_auth_api_body:
            await ServiceCaller.call_auth_service(
                AuthFunction.USERS,
                rest_path=f"/{user_id}",
                body=request_auth_api_body,
                method="PATCH"
            )
        await ServiceCaller.call_db_service(DbFunction.UPDATE, update_user_dic)
        self.write(self._get_success_result())

    def _check_group_id_identical(self, group_id):
        if group_id != self._user['group']:
            self._finish_with_failed_result(
                self.ERROR_GROUP_ID_NOT_IDENTICAL,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)
