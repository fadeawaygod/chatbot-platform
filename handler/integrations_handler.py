import time

from error_code import INTEGRATION_NAME_DUPLICATED_ERR
from exception import NoPermissionError

from handler.bot_handler_base import BotHandlerBase, PermissionType

from .service.session_manager import SessionManager


class IntegrationsHandler(BotHandlerBase):
    ERROR_INTEGRATION_ID_INVALID = 'this integration id is invalid'
    ERROR_INTEGRATION_EXIST = 'this integration has already exist'
    VALID_PATCH_INTEGRATION_KEY = ['credential', 'is_enable']

    async def get(self):
        self._check_user_role_contains_editor()
        bot_id = self._try_get_url_parameter_value('bot_id')
        passage = self.get_argument('passage', None)
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.VIEW)

        session = SessionManager.get_session()
        query_result = session.query(collection=self.COLLECTION_INTEGRATION)\
            .filter_by(column='bot_id', value=bot_id) \
            .projection_by(column='_id', show=False) \
            .projection_by(column='id', show=True) \
            .projection_by(column='bot_id', show=True) \
            .projection_by(column='passage', show=True) \
            .projection_by(column='credential', show=True) \
            .projection_by(column='is_enable', show=True) \
            .projection_by(column='callback_secret', show=True) \
            .sort_by(column='updated', asc=True)
        if passage:
            query_result.filter_by(column='passage', value=passage)
        query_result = await query_result.execute()
        response_body = {
            'integrations': query_result['data']
        }
        self.write(self._get_success_result(response_body))

    async def post(self):
        self._check_user_role_contains_editor()
        bot_id = self._try_get_json_value('bot_id')
        await self._check_user_permision_of_the_bot(bot_id, PermissionType.EDIT)

        passage = self._try_get_json_value('passage')
        credential = self._try_get_json_value('credential')
        await self._check_the_integration_not_exist(bot_id, passage)
        session = SessionManager.get_session()
        result = await session.insert(collection=self.COLLECTION_INTEGRATION)\
            .data(column='bot_id', value=bot_id)\
            .data(column='passage', value=passage)\
            .data(column='credential', value=credential)\
            .data(column='is_enable', value=False)\
            .execute()
        new_integration_id = result['data']['id']
        return_dic = {'id': new_integration_id}
        self.write(self._get_success_result(return_dic))

    async def delete(self, integration_id):
        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_integration(integration_id, PermissionType.EDIT)

        session = SessionManager.get_session()
        await session.delete(collection=self.COLLECTION_INTEGRATION)\
            .filter_by(column='id', value=integration_id)\
            .execute()
        self.write(self._get_success_result())

    async def patch(self, integration_id):
        def check_sub_credential_name_unique(data):
            sub_credential_names = set()
            for sub_credential in data['credential'].get('credentials', []):
                sub_credential_name = sub_credential.get('name', '')
                if sub_credential_name in sub_credential_names:
                    self._finish_with_failed_result(
                        err_msg=INTEGRATION_NAME_DUPLICATED_ERR['message'],
                        status_code=self.HTTP_BAD_REQUEST_ERROR_CODE,
                        error_code=INTEGRATION_NAME_DUPLICATED_ERR['code']
                    )
                sub_credential_names.add(sub_credential_name)

        self._check_user_role_contains_editor()
        await self._check_user_permision_of_the_integration(integration_id, PermissionType.EDIT)

        data = self._extract_body(self.VALID_PATCH_INTEGRATION_KEY)
        check_sub_credential_name_unique(data)
        session = SessionManager.get_session()
        update_session = session.update(collection=self.COLLECTION_INTEGRATION).filter_by(
            column='id', value=integration_id)
        for key, value in data.items():
            update_session.data(column=key, value=value)
        await update_session.execute()
        self.write(self._get_success_result())

    async def _check_the_integration_not_exist(self, bot_id, passage):
        session = SessionManager.get_session()
        query_result = await session.query(collection=self.COLLECTION_INTEGRATION)\
            .filter_by(column='bot_id', value=bot_id)\
            .filter_by(column='passage', value=passage)\
            .projection_by(column='_id', show=False)\
            .projection_by(column='id', show=True)\
            .execute()
        if query_result['data']:
            self._finish_with_failed_result(err_msg=self.ERROR_INTEGRATION_EXIST,
                                            status_code=self.HTTP_BAD_REQUEST_ERROR_CODE)

    async def _check_user_permision_of_the_integration(self, integration_id: str, permission_type: PermissionType):
        session = SessionManager.get_session()
        query_result = await session.query(collection=self.COLLECTION_INTEGRATION)\
            .filter_by(column='id', value=integration_id)\
            .projection_by(column='_id', show=False)\
            .projection_by(column='bot_id', show=True)\
            .execute()
        if not query_result['data']:
            self._finish_with_failed_result(
                self.ERROR_INTEGRATION_ID_INVALID,
                self.HTTP_BAD_REQUEST_ERROR_CODE,
                exception_class=NoPermissionError)

        bot_id = query_result['data'][0]['bot_id']
        await self._check_user_permision_of_the_bot(bot_id, permission_type)
