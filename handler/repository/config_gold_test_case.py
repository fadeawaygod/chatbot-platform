from ..service.session_manager import SessionManager


class ConfigGoldTestCaseRepository:

    collection = 'config_gold_test_case'

    @classmethod
    async def get_all_test_case(cls, bot_id: str) -> [dict]:
        session = SessionManager.get_session()
        result = await session.query(collection=cls.collection) \
            .filter_by(column='bot_id', value=bot_id) \
            .execute()
        return result.get('data', [])

    @classmethod
    async def insert_a_test_case(cls, bot_id: str, corpus: str) -> dict:
        session = SessionManager.get_session()
        result = await session.insert(collection=cls.collection) \
            .data(column='bot_id', value=bot_id) \
            .data(column='corpus', value=corpus) \
            .execute()
        return result
