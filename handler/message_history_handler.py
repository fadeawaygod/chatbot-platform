import json
import logging
import time
from typing import Optional, Awaitable, Union

import tornado.web
import tornado.escape
import tornado.httpclient

from .service.message_history_service import Proxy
from .service.session_manager import SessionManager

logger = logging.getLogger()

COLLECTION_MESSAGE_HISTORY = 'message_history'


async def save_message_history(source_from: str,
                               bot_id: str,
                               user_id: str,
                               message: Union[str, dict],
                               from_user: bool,
                               hook_name: str = None,
                               hook_url: str = None):
    session = SessionManager.get_session()
    try:
        session.insert(collection=COLLECTION_MESSAGE_HISTORY)\
            .data(column='source_from', value=source_from)\
            .data(column='bot_id', value=str(bot_id))\
            .data(column='user_id', value=str(user_id))\
            .data(column='message', value=message)\
            .data(column='from_user', value=from_user)\
            .data(column='hook_name', value=hook_name)\
            .data(column='hook_url', value=hook_url)\
            .data(column='updated', value=int(time.time()*1000))
        logger.info(f'history saving payload: {session.query_data}')
        await session.execute()
    except Exception as e:
        logger.error('Save history ', e)


class ReceiveGatewayMessageHistoryHandler(tornado.web.RequestHandler):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    async def post(self):
        payload = {}
        try:
            payload = tornado.escape.json_decode(self.request.body)
            await save_message_history(source_from=payload['source']['from'],
                                       bot_id=payload['agent_id'],
                                       user_id=payload['user_id'],
                                       message=payload['message'],
                                       from_user=True,
                                       hook_name=payload.get('hook_name'),
                                       hook_url=payload.get('hook_url'))
            self.set_status(200)
            await self.finish()
        except Exception as e:
            self.set_status(400)
            self.write({'message': e})
        await Proxy.to_agent(agent_id=payload.get('agent_id'), body=self.request.body)


class ReceiveAgentMessageHistoryHandler(tornado.web.RequestHandler):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    @staticmethod
    async def save_message_histories(data: dict):
        try:
            for message in data.get('messages', []):
                await save_message_history(source_from=data['source']['from'],
                                           bot_id=data['agent_id'],
                                           user_id=data['user_id'],
                                           message=message,
                                           from_user=False,
                                           hook_name=data.get('hook_name'),
                                           hook_url=data.get('hook_url'))
        except Exception as e:
            logger.error('Save history ', e)

    async def post(self):
        payload = tornado.escape.json_decode(self.request.body)
        logger.info(f'Agent message payload {payload}')
        await self.save_message_histories(payload)
        await Proxy.to_gateway(self.request.body)
