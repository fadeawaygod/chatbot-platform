from handler.validation_handler_base import ValidationHandlerBase
from service_caller import ServiceCaller


class AnalyzeUtteranceHandler(ValidationHandlerBase):
    async def get(self):
        nlu_token = self._try_get_url_parameter_value('nlu_token')
        query_string = self._try_get_url_parameter_value('query_string')
        threshold = float(self._try_get_url_parameter_value('threshold'))
        result = await ServiceCaller.call_nlu_query_service(nlu_token, query_string, threshold)

        self.write(self._get_success_result(
            {'analysis': result.get('retStr')}
        ))
