import time

from handler.handler_base import HandlerBase
from handler.service.session_manager import SessionManager
from service_caller import ServiceCaller
from handler.helper.entity import resolute_entities


class QueryNluHandler(HandlerBase):
    COLLECTION_NLU_RESOURCE = 'nlu_resource'

    async def get(self):
        async def update_nlu_resource(nlu_token: str):
            session = SessionManager.get_session()
            update_session = session.update(collection=self.COLLECTION_NLU_RESOURCE)\
                .filter_by(column='nlu_token', value=nlu_token)\
                .data(column='last_infered_time', value=int(time.time()))
            await update_session.execute()

        headers = self.request.headers
        nlu_token = headers.get("token", "")

        query_string = self._try_get_url_parameter_value('q')
        user_id = self.get_argument('user_id', '')
        bot_id = self.get_argument('bot_id', '')
        msg_id = self.get_argument('msgid', '')
        pinyin = self.get_argument('pinyin', 0)

        result = await ServiceCaller.call_nlu_query_service(
            nlu_token,
            query_string,
            user_id=user_id,
            bot_id=bot_id,
            msg_id=msg_id,
            pinyin=pinyin,
        )
        result["retStr"]["entities"] = await resolute_entities(
            bot_id,
            result["retStr"]["entities"]
        )

        if nlu_token:
            await update_nlu_resource(nlu_token)
        self.finish(result)
