import logging
from typing import Optional, Awaitable

import sr_toolkit.message_template.common
import sr_toolkit.message_template.factory
from handler.validation_handler_base import ValidationHandlerBase
from .service.session_manager import SessionManager

logger = logging.getLogger(name='ChatHistoryHandler')


class ChatHistoryHandler(ValidationHandlerBase):
    COLLECTION = 'message_history'

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    async def get(self):
        limit = int(self.get_argument('limit', 0))
        skip = int(self.get_argument('skip', 0))
        user_id = self.get_argument('user_id')
        bot_id = self.get_argument('bot_id')
        source_from = self.get_argument('source_from')
        session = SessionManager.get_session()
        try:
            history = await session.query(collection=self.COLLECTION)\
                .filter_by(column='user_id', value=user_id)\
                .filter_by(column='source_from', value=source_from)\
                .filter_by(column='bot_id', value=bot_id)\
                .projection_by(column='_id', show=False)\
                .sort_by(column='updated', asc=False)\
                .limit(limit)\
                .skip(skip)\
                .execute()
            histories = history.get('data', [])
            for idx, message in enumerate(histories):
                msg_obj = message.get('message')
                if isinstance(msg_obj, str):
                    msg_obj = sr_toolkit.message_template.common.text.TextMessage(msg_obj)
                elif isinstance(msg_obj, dict):
                    msg_obj = sr_toolkit.message_template.common.get_message(message=msg_obj)
                if isinstance(msg_obj, sr_toolkit.message_template.common.Message):
                    msg_obj = sr_toolkit.message_template.factory.to_message(type='line', msg=msg_obj)
                    histories[idx]['message'] = msg_obj
            response_body = {
                'histories': history.get('data', [])
            }
            self._finish_with_success_result(return_dict=response_body)
        except Exception as e:
            logger.error(e)
            self._finish_with_failed_result(err_msg=str(e), status_code=400)
