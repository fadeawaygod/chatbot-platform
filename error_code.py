NO_VALID_KEY_ERR = {
    'message': 'There is no valiid keys to patch',
    'code': 1
}

INTEGRATION_NAME_DUPLICATED_ERR = {
    'message': 'Integration name duplicate.',
    'code': 11000
}
EXCEED_MAX_TIME_INTERVAL_ERR = {
    'message': 'Exceed max time interval',
    'code': 12000
}
DIALOG_ID_INVALID_ERR = {
    'message': 'this dialog id is invalid:{dialog_id}',
    'code': 13003
}
SORT_BY_INVALID_ERR = {
    'message': 'the sort by field is invalid:{key}',
    'code': 13004
}
NAME_DUPLICATED_IN_QUERY_ERR = {
    'message': 'these name is duplicated in query',
    'code': 13005
}
DIALOG_NAME_DUPLICATED_ERR = {
    'message': 'dialog name duplicate:{name}',
    'code': 13006
}
USER_ID_NOT_EXIST_IN_AUTH_ERR = {
    'message': 'the user id is not in auth:{user_id}',
    'code': 14000
}
USER_ID_ALREADY_EXIST_IN_AUTH_ERR = {
    'message': 'the user id is already in auth:{user_id}',
    'code': 14001
}
UID_DUPLICATED_ERR = {
    'message': 'uid duplicate.',
    'code': 14002
}

NLU_SERVER_TRAIN_ERR = {
    'message': 'NLU server train error',
    'code': 20000
}
