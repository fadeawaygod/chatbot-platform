import http_status_code


class ExceptionBase(Exception):
    def __init__(self, *args, **kwargs):
        self.message = self.message.format(**kwargs)
        self.parameters = {**kwargs}

    def __str__(self):
        return self.message

# code start from 10000~80000, 90000+ is preserved for Chatbot Platform Worker


class TrainingFailedError(ExceptionBase):
    status_code = http_status_code.INTERNAL_ERROR
    code = 10000
    message = 'Training failed:{err}'


class NotPermittedToUseNluError(ExceptionBase):
    status_code = http_status_code.FORBIDDEN
    code = 10001
    message = 'You are Not allowed to use NLU resources'


class UtteranceNotEnough(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 10002
    message = 'the utterances of this intent are not enough: {intent_name}'


class LabeledEntityNotEnough(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 10003
    message = 'the labeled entities of this entity is not enough: {entity_name}'


class NoNLUResourceAreAvailableError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 10004
    message = 'There are no available NLU resources'


class BotAlreadyTrainingError(ExceptionBase):
    status_code = http_status_code.INTERNAL_ERROR
    code = 10005
    message = 'The bot is already training'


class NLUResourceNotFoundByNLUIDError(ExceptionBase):
    status_code = http_status_code.INTERNAL_ERROR
    code = 10005
    message = 'The nlu resource is not found:nlu_id={nlu_id}'


class AgentServiceError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 30000
    message = 'Agent service error:{err}'


class DeployFailedError(ExceptionBase):
    status_code = http_status_code.INTERNAL_ERROR
    code = 30001
    message = 'deploy failed:{err}'


class OthersIsEditingThisDialogError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 40000
    message = '{user_id} is editing this dialog'


class TokenValidationError(ExceptionBase):
    status_code = http_status_code.UNAUTHORIZED
    code = 50000
    message = 'Token validation failed:{cause}'


class NoPermissionError(ExceptionBase):
    status_code = http_status_code.FORBIDDEN
    code = 50002
    message = 'you have no permission to do this operation'


class IdInvalidError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50003
    message = 'this id:{id} is invalid'


class NameDuplicatedError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50004
    message = 'this name:{name} is already exist'


class UIDDuplicatedError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50005
    message = 'this uid:{uid} is already exist'


class RoleInvalidError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50006
    message = 'this role:{role} is invalid'


class RoleEmptyError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50007
    message = 'role should not be empty'


class GroupNotExistError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50008
    message = 'this group:{group_id} is not exist'


class UidOrPasswordIncorrectError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50009
    message = 'uid or password incorrect'


class PasswordFormatError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50010
    message = 'password format error:{err}'


class IntentNotExistError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50011
    message = 'this intent is not exist:{name}'


class UIDEmptyError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50012
    message = 'this uid should not be empty'


class MailDuplicatedError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50013
    message = 'this mail:{mail} is already exist'


class MailFormatInvalidError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50014
    message = 'this mail foramt:{mail} is invalid'


class MailNotExistError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50015
    message = 'this mail:{mail} is not exist'


class AlreadyActivatedError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50016
    message = 'this user has already activated'


class ActivationTokenInvalid(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50017
    message = 'this activation token is invalid'


class ActivationTokenExpired(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50018
    message = 'this activation token has expired'


class UserNotActivatedYetError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50019
    message = 'you have not activated yet'


class ResetPasswordTokenInvalid(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50020
    message = 'this reset password token is invalid'


class ResetPasswordTokenExpired(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50021
    message = 'this reset password token has expired'


class UserNotFoundError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 50022
    message = 'this user:{uid} is not found'
# middleware


class MissRequiredParameterError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 60000
    message = 'the required parameter:{parameter} is missing'


class ParseTsvFailedError(ExceptionBase):
    status_code = http_status_code.BAD_REQUEST
    code = 60001
    message = 'parse tsv failed at line:{line}'

# 3 party service


class UnknownFBError(ExceptionBase):
    status_code = http_status_code.INTERNAL_ERROR
    code = 70000
    message = 'Unknown FB Error:{error}'


class FBAccessTokenError(ExceptionBase):
    status_code = http_status_code.INTERNAL_ERROR
    code = 70001
    message = 'access token has expired'

# auth service:80000-89999
