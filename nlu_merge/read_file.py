def load_data(file_name):
    all_intents = []
    all_entities = []
    all_utterances = []
    all_intent_entity_pairs = []

    f = open(file_name, 'r')
    while True:
        line = f.readline()
        if not line:
            break

        intents_str, utterance_str, entity_tags_str = line.split('\t')
        intents = eval(intents_str)
        all_intents = merge_intents(intents, all_intents)

        entities_dic_list, all_entities = extract_entities(
            entity_tags_str, all_entities, utterance_str)

        all_utterances = extract_utterance(
            intents, utterance_str, entities_dic_list, all_utterances)

    all_intent_entity_pairs = extract_entity_with_intent(
        all_utterances, all_intent_entity_pairs)

    entities = extract_entity_entries(all_utterances)

    all_intent_entity_pairs_remove_duplicate = []

    all_intent_entity_pairs_remove_duplicate = remove_duplicate_pair(
        all_intent_entity_pairs, all_intent_entity_pairs_remove_duplicate)

    # print(all_entities)
    return all_intents, all_entities, all_utterances, all_intent_entity_pairs_remove_duplicate, entities


def remove_duplicate_pair(all_intent_entity_pairs, all_intent_entity_pairs_remove_duplicate):
    for i in all_intent_entity_pairs:
        if i not in all_intent_entity_pairs_remove_duplicate:
            all_intent_entity_pairs_remove_duplicate.append(i)
    return all_intent_entity_pairs_remove_duplicate


def extract_entity_with_intent(all_utterances, all_intent_entity_pairs):
    for utterance in all_utterances:
        if utterance['entities']:
            for u in utterance['entities']:
                entity_intent_dic = {}
                entity_intent_dic['intent'] = utterance['intent']
                # entity_intent_dic['name'] = u['text']
                entity_intent_dic['name'] = u['entity_name']
                entity_intent_dic['entity'] = u['entity_name']
                entity_intent_dic['entity_text'] = u['text']
                all_intent_entity_pairs.append(entity_intent_dic)
    return all_intent_entity_pairs


def extract_entity_entries(all_utterances):
    entities = {}
    for utterance in all_utterances:
        if utterance['entities']:
            for u in utterance['entities']:
                if u['entity_name'] not in entities:
                    entities[u['entity_name']] = []
                entities[u['entity_name']].append(u['text'])
    return entities


def extract_utterance(intents, utterance_str, entities_dic_list, all_utterances):
    utterance_dic = {}
    for intent in intents:
        utterance_dic['intent'] = intent
        utterance_dic['text'] = utterance_str
        utterance_dic['entities'] = entities_dic_list
        all_utterances.append(utterance_dic)
        utterance_dic = {}
    return all_utterances


def convert_language(input_str):
    mapping = {}
    '''
    mapping = {"TIM": "时间",
               "DATE": "日期",
               "MON": "钱",
               "CHA": "渠道",
               "order": "订单",
               "GOLD": "金币",
               "NAME1": "称呼",
               "PER": "时间区",
               "GAME": "游戏",
               }
    '''
    if input_str in mapping:
        return mapping[input_str]
    else:
        return input_str


def extract_entities(entity_tags_str, all_entities, utterance_str):
    tags = entity_tags_str.split()
    entities = []
    entity = ''
    entity_dic = {}
    entities_dic_list = []

    for i, element in enumerate(tags):
        if element[0] == 'B':
            if convert_language(element[2:]) not in all_entities:
                all_entities.append(convert_language(element[2:]))
            if entity:
                entity_dic['text'] = entity
                entity_dic['len'] = len(entity)
                entities_dic_list.append(entity_dic)
                entities.append(entity)
                entity_dic = {}
                entity_dic['entity_name'] = convert_language(element[2:])
                entity_dic['start_position'] = i
            else:
                entity_dic['entity_name'] = convert_language(element[2:])
                entity_dic['start_position'] = i
            entity = ''
            entity += utterance_str[i]
        elif element[0] == 'I':
            entity += utterance_str[i]
            if (i + 1) == len(tags):
                entity_dic['text'] = entity
                entity_dic['len'] = len(entity)
                entities_dic_list.append(entity_dic)
                entities.append(entity)
                entity_dic = {}
        else:
            if entity:
                entity_dic['text'] = entity
                entity_dic['len'] = len(entity)
                entities_dic_list.append(entity_dic)
                entities.append(entity)
                entity_dic = {}
            entity = ''
    return entities_dic_list, all_entities


def merge_intents(intents, all_intents):
    remove_duplicate_pair(intents, all_intents)
    return all_intents


if __name__ == '__main__':
    # load_data("nlu_merge/train.tsv")
    load_data("train.tsv")
