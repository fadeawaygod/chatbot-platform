import uuid


def get_intents(intent_name, intent_entity_pairs, entity_id_mapping):
    intents = []
    intent_name_id_mapping = {}
    for name in intent_name:
        intent_dic = {}
        duplicate_entity = []
        entities = []
        intent_dic['name'] = name
        intent_dic['priority'] = 10
        intent_dic['id'] = str(uuid.uuid4())
        intent_name_id_mapping[intent_dic['name']] = intent_dic['id']
        for pair in intent_entity_pairs:
            if pair['intent'] == name and pair['entity'] not in duplicate_entity:
                entity_dic = {}
                entity_dic['name'] = pair['name']
                entity_dic['entity_id'] = entity_id_mapping[pair['name']]
                duplicate_entity.append(pair['entity'])
                entities.append(entity_dic)
        intent_dic['entities'] = entities
        intents.append(intent_dic)
    return intents, intent_name_id_mapping


def get_entities(entities):
    name_id_mapping = {}
    entity = []
    for entity_name in entities:
        entries = []
        entity_dic = {}
        entity_dic['name'] = entity_name
        entity_dic['id'] = str(uuid.uuid4())
        for value in entities[entity_name]:
            entries_dict = {}
            entries_dict['value'] = value
            entries_dict['synonyms'] = []
            entries.append(entries_dict)
        entity_dic['entries'] = entries
        name_id_mapping[entity_dic['name']] = entity_dic['id']
        entity.append(entity_dic)
    return entity, name_id_mapping


def get_utterance(intents_name, utterances_form_data, intent_name_id_mapping, intents):
    all_utterances = []
    entity_under_intent_id_mapping = get_entity_id_mapping(intents)
    for name in intents_name:
        utterances = []
        for utterance in utterances_form_data:
            if name == utterance['intent']:
                utterance_dic = {}
                utterance_dic['intent_ids'] = [intent_name_id_mapping[name]]
                utterance_dic['text'] = utterance['text']
                entities = []
                for entity in utterance['entities']:
                    entity_dic = {}
                    entity_dic['len'] = entity['len']
                    entity_dic['entity_name'] = entity['entity_name']
                    entity_dic['start_position'] = entity['start_position']
                    entity_dic['text'] = entity['text']
                    entity_dic['entity_id'] = entity_under_intent_id_mapping[utterance['intent'] +
                                                                             '_' + entity['entity_name']]
                    entities.append(entity_dic)
                utterance_dic['entities'] = entities
                utterances.append(utterance_dic)
        all_utterances.append(utterances)
    return all_utterances


def get_entity_id_mapping(intents):
    mapping = {}
    for intent in intents:
        for entity in intent['entities']:
            mapping[intent['name'] + '_' + entity['name']] = entity['id']
    return mapping
