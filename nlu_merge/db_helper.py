from handler.service.session_manager import SessionManager


async def post_intent(data: list) -> dict:
    session = SessionManager.get_session()
    return await session.insert_many(collection='intent')\
        .data(column='ignore', value=data)\
        .execute()


async def post_entity(data: list) -> dict:
    session = SessionManager.get_session()
    return await session.insert_many(collection='entity')\
        .data(column='ignore', value=data)\
        .execute()


async def post_utterance(data: list) -> dict:
    session = SessionManager.get_session()
    return await session.insert_many(collection='utterance')\
        .data(column='ignore', value=data)\
        .execute()


async def patch_intent_entities(intent_id: str, entities: list):
    session = SessionManager.get_session()
    await session.update(collection='intent')\
        .filter_by(column='id', value=intent_id) \
        .data(column='entities', value=entities) \
        .execute()


async def patch_entity(entity_id: str, values: list):
    entries = []
    for value in values:
        entries.append({'value': value, 'synonyms': []})
    session = SessionManager.get_session()
    await session.update(collection='entity')\
        .filter_by(column='id', value=entity_id) \
        .data(column='entries', value=entries) \
        .execute()


async def remove_old_entity(bot_id: str):
    session = SessionManager.get_session()
    await session.delete_many(collection='entity')\
        .filter_by(column='bot_id', value=bot_id)\
        .execute()


async def remove_old_intent(bot_id: str):
    session = SessionManager.get_session()
    await session.delete_many(collection='intent')\
        .filter_by(column='bot_id', value=bot_id)\
        .execute()


async def query_intent(bot_id: str) -> dict:
    session = SessionManager.get_session()
    return await session.query(collection='intent')\
        .filter_by(column='bot_id', value=bot_id)\
        .execute()


async def remove_old_utterance(intent_id: str):
    session = SessionManager.get_session()
    await session.delete_many(collection='utterance')\
        .filter_in(column='intent_ids', array=[intent_id]) \
        .execute()
