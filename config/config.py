import os
from sr_toolkit.ConfigManager import load_config
import dotenv
ENV = os.getenv('ENVIRONMENT', 'DEV')
IS_DEV = True if ENV == 'DEV' else False

# load .env file if the file is exist.
dotenv.load_dotenv()
config_source = os.getenv("CONFIG_SOURCE", "nacos")
if config_source != "local":
    tenant = os.getenv("tenant", "local")
    nacos_server_url = os.getenv(
        "nacos_server", "http://10.205.48.80:8848/nacos/v1/cs/configs")
    nacos_server_data_id = os.getenv(
        "nacos_server_data_id", "chatbot-platform.json")
    nacos_server_group = os.getenv("nacos_server_group", "chatbot")
    CONFIG = load_config(
        source=config_source,
        server=nacos_server_url,
        tenant=tenant,
        data_id=nacos_server_data_id,
        group=nacos_server_group)
else:
    CONFIG = load_config(source=config_source)


# logging
IS_LOG_TO_FILE = CONFIG.IS_LOG_TO_FILE == 'True'
LOG_FILE_PATH = CONFIG.LOG_FILE_PATH  # os.getenv('LOG_FILE_PATH')

# tornado
LISTEN_PORT = int(CONFIG.PORT)  # int(os.getenv('PORT'))

# agent
CHATBOT_AGENT_HOST = CONFIG.CHATBOT_AGENT_HOST

# gateway
CHATBOT_GATEWAY_HOST = CONFIG.CHATBOT_GATEWAY_HOST

# db service
DB_SERVICE_UID = CONFIG.DB_SERVICE_UID
DB_SERVICE_PASSWORD = CONFIG.DB_SERVICE_PASSWORD
DB_SERVICE_HTTP_PROTOCOL = CONFIG.DB_SERVICE_HTTP_PROTOCOL
DB_SERVICE_DOMAIN = CONFIG.DB_SERVICE_DOMAIN
COLLECTION_PREFIX = CONFIG.COLLECTION_PREFIX

# nlu service
NLU_SERVICE_URL = CONFIG.NLU_SERVICE_URL
NLU_QUERY_URL = CONFIG.NLU_QUERY_URL
NLU_QUERY_URL_FOR_AGENT = CONFIG.NLU_QUERY_URL_FOR_AGENT
NLU_NER_URL = CONFIG.NLU_NER_URL
NLU_NER_TOKEN = CONFIG.NLU_NER_TOKEN
NLU_SERVICE_USER = CONFIG.NLU_SERVICE_USER
NLU_SERVICE_MODEL_TYPE = CONFIG.NLU_SERVICE_MODEL_TYPE
ENABLE_MOCK_NLU_SERVICE = CONFIG.IS_LOG_TO_FILE == 'True'

# auth service
APP_ID = CONFIG.APP_ID
APP_SECRET = CONFIG.APP_SECRET
AUTH_SERVICE_URL = CONFIG.AUTH_SERVICE_URL
# notification service
NOTIFICATION_SERVICE_URL = CONFIG.NOTIFICATION_SERVICE_URL
# mail service
MAIL_SERVICE_URL = CONFIG.MAIL_SERVICE_URL
ACTIVATE_URL = CONFIG.ACTIVATE_URL
# reset password
RESET_PASSWORD_URL = CONFIG.RESET_PASSWORD_URL
# FB
FB_GRAPH_API_URL = CONFIG.FB_GRAPH_API_URL
FB_APP_ID = CONFIG.FB_APP_ID
FB_APP_SECRET = CONFIG.FB_APP_SECRET

# jeager
JEAGER_REPORTING_HOST = CONFIG.select("JEAGER_REPORTING_HOST", "10.205.48.66")

# redis
REDIS_HOST = CONFIG.REDIS_HOST
REDIS_PORT = CONFIG.REDIS_PORT
REDIS_DB = CONFIG.REDIS_DB
