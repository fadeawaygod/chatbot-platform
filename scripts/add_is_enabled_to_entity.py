'''
    this script convert data of bots from chatbot 2.0 to 2.5(no slot.
'''
import json
import sys
import urllib.request
from enum import Enum

SIGN_IN_SERVICE_URL = 'http://10.205.49.8:9000/system/auth/signIn'

DB_SERVICE_URL = 'http://10.205.49.8:9000/data/{function}'
DB_SERVICE_UID = 'chatbot'
DB_SERVICE_PASSWORD = ''

SOURCE_COLLECTION_PREFIX = ''

db_access_token = ''


class DbFunction(Enum):
    INSERT = 'insert'
    UPDATE = 'update'
    UPDATE_MANY = 'updateMany'
    QUERY = 'query'
    DELETE = 'delete'
    DELETE_MANY = 'deleteMany'
    COUNT = 'count'


def call_db_service(function: DbFunction, body: dict) -> dict:
    request_header = {
        'content-type': 'application/json',
        'Authorization': f'Bearer {db_access_token}'
    }

    response = fetch_sync(
        DB_SERVICE_URL.format(function=function.value),
        "POST",
        json.dumps(body, ensure_ascii=False),
        request_header
    )
    body_json = extract_service_result(response)
    return body_json


def extract_service_result(response: tuple) -> dict:
    response_code = response[0]
    response_body = response[1]
    if response_code != 200:
        raise Exception()
    body_json = json.loads(response_body)
    if body_json['status'].lower() != 'ok':
        raise Exception()
    return body_json


def get_db_access_token():
    url = SIGN_IN_SERVICE_URL
    payload = {"uid": DB_SERVICE_UID, "password": DB_SERVICE_PASSWORD}
    code, body = fetch_sync(url=url, method='POST', body=json.dumps(
        payload), headers={'content-type': 'application/json'})
    body_json = json.loads(body, encoding='utf-8')
    return body_json['access_token']


def fetch_sync(url: str, method: str, body: str, headers: dict):
    req = urllib.request.Request(
        url, headers=headers, data=body.encode(), method=method)
    with urllib.request.urlopen(req) as response:
        contents = response.read()
    contents = contents.decode('utf-8')
    return response.code, contents


def process():
    entities = get_entities()
    for entity in entities:
        if 'is_enabled' not in entity:
            append_entity_is_enabled(entity['id'])


def get_entities() -> list:
    query_dic = {
        'collection': 'entity',
        'filter': {},
        'projection': {
            '_id': False,
        },
    }
    result = call_db_service(DbFunction.QUERY, query_dic)
    entities = result.get('data', [])
    if not entities:
        raise Exception('entity is empty')
    return entities


def append_entity_is_enabled(entity_id: str) -> list:
    query_dic = {
        'collection': 'entity',
        'filter': {'id': entity_id},
        'data': {
            'is_enabled': True
        },
    }
    call_db_service(DbFunction.UPDATE, query_dic)


if __name__ == '__main__':
    db_access_token = get_db_access_token()
    process()
