import json

from nlu_merge.read_file import load_data
from nlu_merge.export_format import get_entities, get_intents, get_utterance, get_entity_id_mapping

FILENAME = "nlu_merge/multi_chinese_and_ner.csv"
OUTPUT_FILENAME = "output.json"
intents_name, entities_name, utterances, intent_entity_pairs, entities = load_data(
    FILENAME)

output_dict = {}

output_dict['entities'], entity_name_id_mapping = get_entities(entities)
output_dict['intents'], intent_name_id_mapping = get_intents(
    intents_name, intent_entity_pairs, entity_name_id_mapping)

output_dict['utterances'] = get_utterance(
    intents_name, utterances, intent_name_id_mapping, output_dict['intents'])
output_dict['flow'] = {}

with open(OUTPUT_FILENAME, 'w') as json_file:
    json.dump(output_dict, json_file,  ensure_ascii=False)
