'''
    this script convert user to auther and insert it.
'''
import json
import urllib.request
from enum import Enum

SIGN_IN_SERVICE_URL = 'https://chatbot.65lzg.com/system/auth/signIn'

DB_SERVICE_URL = 'https://chatbot.65lzg.com/data/{function}'
DB_SERVICE_URL = 'http://34.80.24.249:8000/data/{function}'
DB_SERVICE_UID = 'chatbot'
DB_SERVICE_PASSWORD = ''

APP_ID = '4DPYE8ZYRJggfUtJqpNdDWnp2RwAA2Umr3VynwmGD0Y'

db_access_token = ''


class DbFunction(Enum):
    INSERT = 'insert'
    UPDATE = 'update'
    UPDATE_MANY = 'updateMany'
    QUERY = 'query'
    DELETE = 'delete'
    DELETE_MANY = 'deleteMany'
    COUNT = 'count'


def call_db_service(function: DbFunction, body: dict) -> dict:
    request_header = {
        'content-type': 'application/json',
        'Authorization': f'Bearer {db_access_token}'
    }

    response = fetch_sync(
        DB_SERVICE_URL.format(function=function.value),
        "POST",
        json.dumps(body, ensure_ascii=False),
        request_header
    )
    body_json = extract_service_result(response)
    return body_json


def extract_service_result(response: tuple) -> dict:
    response_code = response[0]
    response_body = response[1]
    if response_code != 200:
        raise Exception()
    body_json = json.loads(response_body)
    if body_json['status'].lower() != 'ok':
        raise Exception()
    return body_json


def get_db_access_token():
    url = SIGN_IN_SERVICE_URL
    payload = {"uid": DB_SERVICE_UID, "password": DB_SERVICE_PASSWORD}
    code, body = fetch_sync(url=url, method='POST', body=json.dumps(
        payload), headers={'content-type': 'application/json'})
    body_json = json.loads(body, encoding='utf-8')
    return body_json['access_token']


def fetch_sync(url: str, method: str, body: str, headers: dict):
    req = urllib.request.Request(
        url, headers=headers, data=body.encode(), method=method)
    with urllib.request.urlopen(req) as response:
        contents = response.read()
    contents = contents.decode('utf-8')
    return response.code, contents


def migrate():
    users = get_users()
    count = 0
    for user in users:
        if "uid" in user and check_auth_user_not_exist(user):
            auth_user = {
                "uid": user["uid"],
                "app_id": APP_ID,
                "password": user.get("password", ""),
                "mail_address": user.get("mail_address", ""),
                "enabled": True,
            }
            insert_auth_user(auth_user)
            count += 1
    print(f"user inserted:{count}")


def get_users() -> list:
    query_dic = {
        'collection': 'user',
        'filter': {},
        'projection': {
            '_id': False,
        },
    }
    result = call_db_service(DbFunction.QUERY, query_dic)
    users = result.get('data', [])
    return users


def check_auth_user_not_exist(user: dict) -> bool:
    query_dic = {
        'collection': 'auth_user',
        'filter': {"uid": user["uid"], "app_id": APP_ID},
        'projection': {
            '_id': False,
        },
    }
    result = call_db_service(DbFunction.QUERY, query_dic)
    users = result.get('data', [])
    return len(users) == 0


def insert_auth_user(auth_user: dict):
    insert_dic = {
        'collection': 'auth_user',
        'data': auth_user
    }
    call_db_service(DbFunction.INSERT, insert_dic)


if __name__ == '__main__':
    db_access_token = get_db_access_token()
    migrate()
