'''
    env: python3.7 ot later
    This Scripts transform bret's data(txt and ann) into silkbot data format.
    python bret_to_silkbot.py args1 args2
    args1: input dir, contain bret's data(txt and ann).
    args2: output file path
    example:
    python bret_to_silkbot.py /input result.json
'''
import json
import os
import sys
from datetime import datetime
from typing import List, Tuple

DIALOG_SPLIT_TAG = ' '
LABELING_SPLIT_TAG = '\t'
CUSTOMER_TAG = '[客戶]'
SENT_TIME_PARSE_FORMAT = '%Y-%m-%d %H:%M:%S'
THIS_INTENT_TAG = '本句意圖'
FILTERED_LABELING_TEXT = ['客戶']
UTTERNACE_IN_LINE_OFFSET = 31


def remove_header_line(input_list: list) -> list:
    """the first line of dialogs is useless
    """
    return input_list[1:]


def datetime_str_to_timestamp(datetime_str: str) -> int:
    """convert str to timestamp(ms)
    for example: 2020-01-29 12:33:30 -> 1580272410000
    """
    datetime_obj = datetime.strptime(datetime_str, SENT_TIME_PARSE_FORMAT)
    time_stamp = datetime_obj.timestamp()
    time_stamp_ms = int(time_stamp*1000)
    return time_stamp_ms


def read_dialog(file_path: str) -> Tuple[List[dict], str]:
    with open(file_path, encoding="utf-8") as f:
        raw_text = f.read()
    input_dialogs = raw_text.splitlines()
    return input_dialogs, raw_text


def extract_uttrances(raw_utterances: List[dict]) -> List[dict]:
    """extract uttrances, each utterance contains:
        - from_user: bool
        - sent_time: int
        - text: str
        - intent_names: list
        - entities: list
    """
    raw_utterances = remove_header_line(raw_utterances)
    output_utterances = []
    current_year_str = datetime.now().year
    for dialog in raw_utterances:
        dialog_items = dialog.split(DIALOG_SPLIT_TAG)
        sent_datetime_str = f'{current_year_str}-{dialog_items[0]} {dialog_items[1]}'
        utterance_text = ' '.join(dialog_items[6:]).strip()
        output_utterances.append({
            'from_user': dialog_items[4] == CUSTOMER_TAG,
            'sent_time': datetime_str_to_timestamp(sent_datetime_str),
            'text': utterance_text,
            'intent_names': [],
            'entities': []
        })
    return output_utterances


def append_intents_and_entities(
        file_path: str,
        utterances: List[dict],
        raw_utterances: List[dict],
        raw_text: str) -> List[dict]:
    """read .ann file to append entities and intents information to utterances
       we accept para raw_text(txt file) because ann file use char's position to locate labeling.
       It will append intent_names and entities to utterance
    """
    try:
        with open(file_path, encoding="utf-8") as f:
            labelings = f.readlines()
    except Exception as e:
        print(f'warning:load ann file fail. file path:{file_path}')
        return

    for labeling in labelings:
        labeling_items = labeling.split(LABELING_SPLIT_TAG)
        if len(labeling_items) != 3:
            continue
        labeled_text = labeling_items[2].strip()
        if labeled_text in FILTERED_LABELING_TEXT:
            continue
        labeled_information = labeling_items[1].split(' ')
        name, start_position, end_position = (
            labeled_information[0],
            int(labeled_information[1]),
            int(labeled_information[2])
        )

        line_count, line_start_position = locate_label(
            raw_text, start_position)
        # firs line is not imported to utterances
        target_utterance = utterances[line_count-1]
        if labeled_text == THIS_INTENT_TAG:
            target_utterance['intent_names'].append(name)
        else:
            target_utterance['entities'].append({
                "text": labeled_text,
                "entity_name": name,
                "start_position": start_position - line_start_position - UTTERNACE_IN_LINE_OFFSET,
                "len": end_position - start_position
            })

    return utterances


def locate_label(raw_text: str, position: int) -> Tuple[int, int]:
    """ count beloned line based on the number of \n
        return a tuple:
        int: line count
        int: the start position of that line
    """
    belonged_line = raw_text[:position].count('\n')
    line_start_position = next(i for i in reversed(
        range(len(raw_text[:position]))) if raw_text[i] == '\n') + 1
    return belonged_line, line_start_position


input_dir = sys.argv[1]
output_path = sys.argv[2]
output_dialogs = []
for file_full_name in os.listdir(input_dir):
    file_name = file_full_name.split('.')[0]
    file_format = file_full_name.split('.')[1]
    if file_format != 'txt':
        continue

    txt_file_full_path = os.path.join(input_dir, file_full_name)
    ann_file_full_path = os.path.join(input_dir, f'{file_name}.ann')

    raw_utterances, raw_text = read_dialog(txt_file_full_path)
    utterances = extract_uttrances(raw_utterances)
    utterances = append_intents_and_entities(
        ann_file_full_path, utterances, raw_utterances, raw_text)

    output_dialogs.append({
        'name': file_name,
        'utterances': utterances
    })
output_dic = {
    "data": output_dialogs
}
with open(output_path, 'w', encoding='utf-8') as fp:
    json.dump(output_dic, fp, ensure_ascii=False)
