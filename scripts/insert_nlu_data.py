from service_caller import ServiceCaller, DbFunction
import uuid

import asyncio

from nlu_merge.read_file import load_data
from nlu_merge.db_helper import post_intent, post_entity, post_utterance, patch_intent_entities, patch_entity, remove_old_utterance, remove_old_intent, remove_old_entity, query_intent

FILENAME = "nlu_merge/multi_chinese_and_ner.csv"
bot_id = '4aD017T6WaugRWfbhKcIgzZq6IpglN7gfq8J4vZCRHyMTU2ODYyNzk0NTI4MA=='
intents_name, entities_name, utterances, intent_entity_pairs, entities = load_data(
    FILENAME)


def convert_to_intent_insert_many_format(intents_name: list, bot_id: str) -> list:
    data = []
    for intent in intents_name:
        tmp_dic = {}
        tmp_dic['name'] = intent
        tmp_dic['bot_id'] = bot_id
        tmp_dic['priority'] = 10
        tmp_dic['entities'] = []
        data.append(tmp_dic)
    return data


def convert_to_entity_insert_many_format(entities_name: list, bot_id: str) -> list:
    data = []
    for entity in entities_name:
        tmp_dic = {}
        tmp_dic['name'] = entity
        tmp_dic['bot_id'] = bot_id
        tmp_dic['entries'] = []
        tmp_dic['prompts'] = []
        tmp_dic['is_enabled'] = True
        data.append(tmp_dic)
    return data


def convert_to_utterance_insert_many_format(utterances: list, mapping: dict) -> list:
    data = []
    for utterance in utterances:
        tmp_dic = {}
        tmp_dic['intent_ids'] = [intent_mapping[utterance['intent']]]
        tmp_dic['text'] = utterance['text']
        for entity in utterance['entities']:
            entity[''] = mapping[utterance['intent'] +
                                 '_' + entity['entity_name']]
        tmp_dic['entities'] = utterance['entities']
        data.append(tmp_dic)
    return data


async def delete_old_utterances(intents):
    for intent in intents:
        await remove_old_utterance(intent['id'])


async def remove_old_data(bot_id: str):
    old_intents = await query_intent(bot_id)
    await remove_old_entity(bot_id)
    await remove_old_intent(bot_id)
    await delete_old_utterances(old_intents['data'])


def get_mapping(responses):
    mapping = {}
    for res in responses:
        mapping[res['name']] = res['id']
    return mapping


asyncio.run(remove_old_data(bot_id))

response_intent = asyncio.run(post_intent(
    convert_to_intent_insert_many_format(intents_name, bot_id)))
intent_mapping = get_mapping(response_intent['data'])
print('insert intent done')

response_entity = asyncio.run(post_entity(
    convert_to_entity_insert_many_format(entities_name, bot_id)))
entity_mapping = get_mapping(response_entity['data'])
print('insert entity done')

for entity in entities:
    asyncio.run(patch_entity(entity_mapping[entity], entities[entity]))

entity_id_mapping = {}

for intent in intents_name:
    patch_intent_list = []
    duplicate_entity = []
    for pair in intent_entity_pairs:
        if pair['intent'] == intent and pair['name'] not in duplicate_entity:
            entity_dic = {}
            entity_dic['name'] = pair['name']

            entity_id_mapping[pair['intent'] + '_' +
                              pair['name']] = entity_dic['id']
            patch_intent_list.append(entity_mapping[pair['entity']])
            duplicate_entity.append(pair['name'])
    asyncio.run(patch_intent_entities(
        intent_mapping[intent], patch_intent_list))
print('patch intent done')

asyncio.run(post_utterance(
    convert_to_utterance_insert_many_format(utterances, entity_id_mapping)))
print('insert utterance done')
