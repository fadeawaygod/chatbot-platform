'''
    this script convert data of bots from chatbot 2.0 to 2.5(no slot.
'''
import json
import sys
import urllib.request
from enum import Enum

SIGN_IN_SERVICE_URL = 'https://chatbot.65lzg.com/system/auth/signIn'

DB_SERVICE_URL = 'https://chatbot.65lzg.com/data/{function}'
DB_SERVICE_UID = 'chatbot'
DB_SERVICE_PASSWORD = ''

SOURCE_COLLECTION_PREFIX = ''

db_access_token = ''


class DbFunction(Enum):
    INSERT = 'insert'
    UPDATE = 'update'
    UPDATE_MANY = 'updateMany'
    QUERY = 'query'
    DELETE = 'delete'
    DELETE_MANY = 'deleteMany'
    COUNT = 'count'


def call_db_service(function: DbFunction, body: dict) -> dict:
    request_header = {
        'content-type': 'application/json',
        'Authorization': f'Bearer {db_access_token}'
    }

    response = fetch_sync(
        DB_SERVICE_URL.format(function=function.value),
        "POST",
        json.dumps(body, ensure_ascii=False),
        request_header
    )
    body_json = extract_service_result(response)
    return body_json


def extract_service_result(response: tuple) -> dict:
    response_code = response[0]
    response_body = response[1]
    if response_code != 200:
        raise Exception()
    body_json = json.loads(response_body)
    if body_json['status'].lower() != 'ok':
        raise Exception()
    return body_json


def get_db_access_token():
    url = SIGN_IN_SERVICE_URL
    payload = {"uid": DB_SERVICE_UID, "password": DB_SERVICE_PASSWORD}
    code, body = fetch_sync(url=url, method='POST', body=json.dumps(
        payload), headers={'content-type': 'application/json'})
    body_json = json.loads(body, encoding='utf-8')
    return body_json['access_token']


def fetch_sync(url: str, method: str, body: str, headers: dict):
    req = urllib.request.Request(
        url, headers=headers, data=body.encode(), method=method)
    with urllib.request.urlopen(req) as response:
        contents = response.read()
    contents = contents.decode('utf-8')
    return response.code, contents


def transfer_bots(user_id: str):
    bots = get_bots_from_source_collection(user_id)
    for bot in bots:
        bot_id = bot['id']
        intents = get_intents(bot_id)
        intent_id_utterances_mapping = get_utterances(intents)
        convert_intents(intents, intent_id_utterances_mapping)
        convert_utterances(intents, intent_id_utterances_mapping)


def get_bots_from_source_collection(user_id: str) -> list:
    query_dic = {
        'collection': f'{SOURCE_COLLECTION_PREFIX}bot',
        'filter': {'owner': user_id},
        'projection': {
            '_id': False,
        },
    }
    result = call_db_service(DbFunction.QUERY, query_dic)
    bots = result.get('data', [])
    if not bots:
        raise Exception('this user id has no bot')
    return bots


def get_intents(bot_id: str) -> list:
    query_dic = {
        'collection': f'{SOURCE_COLLECTION_PREFIX}intent',
        'filter': {'bot_id': bot_id},
        'projection': {
            '_id': False,
        },
    }
    result = call_db_service(DbFunction.QUERY, query_dic)
    return result.get('data', [])


def get_utterances(intents: list) -> dict:
    """return a dictionary that key is intent_id, value is utterances
    """
    intent_id_utterances_mapping = {}
    for intent in intents:
        intent_id = intent['id']
        query_dic = {
            'collection': f'{SOURCE_COLLECTION_PREFIX}utterance',
            'filter': {'intent_ids': {'$in': [intent_id]}},
            'projection': {
                '_id': False,
            },
        }
        result = call_db_service(DbFunction.QUERY, query_dic)
        utterances = result.get('data', [])
        intent_id_utterances_mapping[intent_id] = utterances

    return intent_id_utterances_mapping


def convert_intents(intents: list, intent_id_utterances_mapping: dict):
    for intent in intents:
        temp_intent = {}
        temp_intent['utterance_count'] = get_utterance_count(
            intent, intent_id_utterances_mapping)
        temp_intent['entities'] = convert_intent_slots_to_entity_ids(intent)
        # remove slots
        update_intent(intent['id'], temp_intent)


def update_intent(intent_id: str, intent: dict) -> list:
    query_dic = {
        'collection': f'{SOURCE_COLLECTION_PREFIX}intent',
        'filter': {'id': intent_id},
        'data': intent,
    }
    call_db_service(DbFunction.UPDATE, query_dic)


def get_utterance_count(intent: dict, intent_id_utterances_mapping: dict) -> int:
    return len(intent_id_utterances_mapping[intent['id']])


def convert_intent_slots_to_entity_ids(intent: dict) -> list:
    entity_ids = intent.get('entities', [])
    for slot in intent.get('slots', []):
        entity_id = slot.get('entity_id', '')
        if entity_id and entity_id not in entity_ids:
            entity_ids.append(entity_id)
    return entity_ids


def convert_utterances(intents: list, intent_id_utterances_mapping: dict):
    for intent_id, utterances in intent_id_utterances_mapping.items():
        for utterance in utterances:
            target_intent = next(
                intent for intent in intents if intent['id'] == intent_id)
            temp_utterance = {}
            temp_utterance['entities'] = convert_utterance_slots_to_entities(
                target_intent, utterance)
            update_utterance(utterance['id'], temp_utterance)


def convert_utterance_slots_to_entities(intent: dict, utterance: dict) -> list:
    utterance_entities = utterance.get('entities', [])
    if not utterance_entities:
        for slot in utterance.get('slots', []):
            slot_id = slot.get('slot_id', '')
            if not slot_id:
                continue
            intent_slot = next(
                (slot for slot in intent['slots'] if slot['id'] == slot_id), {})
            entity_id = intent_slot.get('entity_id', '')
            if not entity_id:
                continue

            new_utterance_entity = {
                'entity_id': entity_id,
                'start_pos': slot['start_position'],
                'text': slot['text'],
                'len': slot['len'],
            }
            utterance_entities.append(new_utterance_entity)
    return utterance_entities


def update_utterance(utterance_id: str, utterance: dict) -> list:
    query_dic = {
        'collection': f'{SOURCE_COLLECTION_PREFIX}utterance',
        'filter': {'id': utterance_id},
        'data': utterance,
    }
    result = call_db_service(DbFunction.UPDATE, query_dic)


if __name__ == '__main__':
    db_access_token = get_db_access_token()
    user_id = sys.argv[1]

    transfer_bots(user_id)
