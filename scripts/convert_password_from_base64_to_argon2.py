'''
    this script convert data of bots from chatbot 2.0 to 2.5(no slot.
'''
import base64
import json
import urllib.request
from enum import Enum

import argon2

_hasher = argon2.PasswordHasher()


SIGN_IN_SERVICE_URL = 'https://chatbot.65lzg.com/system/auth/signIn'

DB_SERVICE_URL = 'https://chatbot.65lzg.com/data/{function}'
DB_SERVICE_UID = 'chatbot'
DB_SERVICE_PASSWORD = ''

SOURCE_COLLECTION_PREFIX = ''

db_access_token = ''


class DbFunction(Enum):
    INSERT = 'insert'
    UPDATE = 'update'
    UPDATE_MANY = 'updateMany'
    QUERY = 'query'
    DELETE = 'delete'
    DELETE_MANY = 'deleteMany'
    COUNT = 'count'


def call_db_service(function: DbFunction, body: dict) -> dict:
    request_header = {
        'content-type': 'application/json',
        'Authorization': f'Bearer {db_access_token}'
    }

    response = fetch_sync(
        DB_SERVICE_URL.format(function=function.value),
        "POST",
        json.dumps(body, ensure_ascii=False),
        request_header
    )
    body_json = extract_service_result(response)
    return body_json


def extract_service_result(response: tuple) -> dict:
    response_code = response[0]
    response_body = response[1]
    if response_code != 200:
        raise Exception()
    body_json = json.loads(response_body)
    if body_json['status'].lower() != 'ok':
        raise Exception()
    return body_json


def get_db_access_token():
    url = SIGN_IN_SERVICE_URL
    payload = {"uid": DB_SERVICE_UID, "password": DB_SERVICE_PASSWORD}
    code, body = fetch_sync(url=url, method='POST', body=json.dumps(
        payload), headers={'content-type': 'application/json'})
    body_json = json.loads(body, encoding='utf-8')
    return body_json['access_token']


def fetch_sync(url: str, method: str, body: str, headers: dict):
    req = urllib.request.Request(
        url, headers=headers, data=body.encode(), method=method)
    with urllib.request.urlopen(req) as response:
        contents = response.read()
    contents = contents.decode('utf-8')
    return response.code, contents


def update_user_password(user: dict, hased_password: str):
    query_dic = {
        'collection': f'{SOURCE_COLLECTION_PREFIX}user',
        'filter': {'uid': user['uid']},
        'data': {
            'password': hased_password
        },
    }
    call_db_service(DbFunction.UPDATE, query_dic)


def convert_users_password():
    users = get_users()
    for user in users:
        base64_password = user.get('password')
        if not base64_password or base64_password[:7] == '$argon2':
            continue
        raw_password = base64.b64decode(
            bytes(base64_password, 'utf-8')).decode('utf-8')
        argon2_hased_password = _hasher.hash(raw_password)
        update_user_password(user, argon2_hased_password)


def get_users() -> list:
    query_dic = {
        'collection': f'{SOURCE_COLLECTION_PREFIX}user',
        'filter': {},
        'projection': {
            '_id': False,
            'uid': True,
            'password': True
        },
    }
    result = call_db_service(DbFunction.QUERY, query_dic)
    users = result.get('data', [])
    return users


if __name__ == '__main__':
    db_access_token = get_db_access_token()
    convert_users_password()
