**export dialogs**
----
  export dialogs

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/export_dialogs

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type    | note                                  |
          | :----- | :------------ | :------------------------------------ |
          | status | string        | ok or error                           |
          | error  | string        | show error message if status is error |
          | data   | dialogs array |                                       |

          dialog

          | Key        | Value           | Note |
          | :--------- | :-------------- | :--- |
          | name       | str             |      |
          | utterances | utterance array |      |

          utterance

          | Key          | Value        | Note                                                 |
          | :----------- | :----------- | :--------------------------------------------------- |
          | text         | string       | utterance text                                       |
          | from_user    | bool         | indicates whether this utterance was sent from user  |
          | sent_time    | int          | UNIX timestamp millsecond,  e.g.1580439515000,       |
          | commented    | bool         | commented means this utterance should be check later |
          | intent_names | string array |                                                      |
          | entities     | entity array |                                                      |

          entity

          | Key            | Value Type | note                                                    |
          | :------------- | :--------- | :------------------------------------------------------ |
          | entity_name    | string     | name of entity                                          |
          | text           | string     | entity value                                            |
          | start_position | int        | indicate the start position of this entity in utterance |
          | len            | int        | indicate the len of this entity                         |

          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/bots/bot_id/export

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "data":{
          "entities":[],
          "intents":[],
          "utterances":[],
          "flow":[],
        }
      }

* **Notes:**
