**Get Integration**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/integrations/:integration_id

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key         | Value Type  | note                                  |
          | :---------- | :---------- | :------------------------------------ |
          | status      | string      | ok or error                           |
          | error       | string      | show error message if status is error |
          | integration | integration |                                       |

          integration
          
          | Key        | Value Type | note                                                                        |
          | :--------- | :--------- | :-------------------------------------------------------------------------- |
          | id         | string     |                                                                             |
          | bot_id     | string     |                                                                             |
          | passage    | string     | platform to be integrated, availables: demo, web, line, telegram, messenger |
          | credential | credential | platform's credential data                                                  |

          credential(web)

          | Key          | Value Type | note |
          | :----------- | :--------- | :--- |
          | callback_url | string     |      |

          credential(telegram)

          | Key   | Value Type | note                  |
          | :---- | :--------- | :-------------------- |
          | token | string     | generated by Telegram |

          credential(messenger)

          | Key   | Value Type | note                   |
          | :---- | :--------- | :--------------------- |
          | token | string     | generated by Messenger |

          credential(line)

          | Key                  | Value Type | note |
          | :------------------- | :--------- | :--- |
          | channel_id           | string     |      |
          | channel_secret       | string     |      |
          | channel_access_token | string     |      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/integrations/caass8e7sasdas09c05c724653939e1eac4b9d2277==

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  response body

      {
        "status": "ok",
        "integration":{        
          "bot_id": "caass8e7sasdas09c05c724653939e1eac4b9d2277",
          "passage": "telegram",
          "credential": {
            "token": "cqwedq09c05c724653939e1eac4b9d2277..."
          }        
        }
      }

* **Notes:**
