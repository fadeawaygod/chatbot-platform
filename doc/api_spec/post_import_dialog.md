**Import Dialog**
----
import data 

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/import_dialog

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

  * **Body Parameters:**
    | Key  | Value        |
    | :--- | :----------- |
    | data | dialog array |

    dialog

    | Key        | Value           | Note                                                     |
    | :--------- | :-------------- | :------------------------------------------------------- |
    | name       | str             |                                                          |
    | utterances | utterance array |                                                          |
    | status     | str             | (optional)should be unlabeled(default), labeling or done |

    utterance

    | Key          | Value         | Note                                                                    |
    | :----------- | :------------ | :---------------------------------------------------------------------- |
    | from_user    | bool          | indicates whether this utterance was sent from user                     |
    | intent_names | string  array | these intent name should be consistent with name in collection "intent" |
    | text         | str           | the utterance text                                                      |
    | entities     | entity array  |                                                                         |
    | sent_time    | int           | UNIX timestamp millsecond,  e.g.1580439515000,                          |
    
    
    entity

    | Key            | Value Type | note                                                                                                                    |
    | :------------- | :--------- | :---------------------------------------------------------------------------------------------------------------------- |
    | entity_name    | string     | should be consistent with name in collection "entity", and the intent shoud contain entities which entity_id matched it |
    | text           | string     | entity value                                                                                                            |
    | start_position | int        | indicate the start position of this entity in utterance                                                                 |
    | len            | int        | indicate the len of this entity                                                                                         |



* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note        |
          | :----- | :--------- | :---------- |
          | status | string     | ok or error |
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/bots/bot_id/import

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok"
      }
  
  request body

      {
        "intents": [],
        "entities": [],
        "utterances": [],
        "flow": {},
      }

* **Notes:**
