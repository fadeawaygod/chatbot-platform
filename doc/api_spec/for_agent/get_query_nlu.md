**Query NLU**
----
This api will query nlu service and update last infered time of nlu resource

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/agent/query_nlu

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key          | Value            |
    | :----------- | :--------------- |
    | Content-Type | application/json |
    | token        | :nlu_token       |


  * **URL Parameters:**

    | Key     | Value | Note                                                            |
    | :------ | :---- | :-------------------------------------------------------------- |
    | q       | str   | query string                                                    |
    | msgid   | int   | unix time stamp                                                 |
    | bot_id  | str   |                                                                 |
    | user_id | str   |                                                                 |
    | pinyin  | int   | determine whether enable pinyin function, 1 is true, 0 is false |


* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note        |
          | :----- | :--------- | :---------- |
          | status | string     | ok or error |
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `

* **Example:**

  Post   http://127.0.0.1:8080/api/v1/agent/query_nlu?bot_id=qwtIXzOtSAnudUiPSoPCkFDDwONVnT8opu4sxZ5vehy&q=hello&msgid=1605494281330&user_id=editor-saen&pinyin=0

  request Header

  | Key   | Value    |
  | :---- | :------- |
  | token | xxxxxxxx |

  request body

      {
        "q": "hello",
        "msgid": 1605494281330,
        "bot_id"": "xxxxxx",
        "user_id": "editor-tom",
        "pinyin": 0
      }

* **Notes:**