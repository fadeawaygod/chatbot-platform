**Patch Entitys**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/entities/:entity_id

* **Method:**
  `Patch`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key        | Value Type   | note                                            |
    | :--------- | :----------- | :---------------------------------------------- |
    | name       | string       | optional, readable name                         |
    | entries    | entry array  | optional, examples of this entity               |
    | prompts    | prompt array | optional, sentences to ask users of this entity |
    | is_enabled | bool         | whether this entity is active                   |

    entry

    | Key      | Value Type   | note                  |
    | :------- | :----------- | :-------------------- |
    | value    | string       | entry value           |
    | synonyms | string array | synonym of this entry |

    prompt

    | Key  | Value Type | note |
    | :--- | :--------- | :--- |
    | text | string     |      |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Patch   http://127.0.0.1:8080/api/v1/editor/entities/c8e709c05c724653939e1eac4b9d2277

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {
        "name": "new entity name"
      }

  response body

      {
        "status": "ok"
      }

* **Notes:**
