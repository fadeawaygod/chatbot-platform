**Get Customer Service Auth**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/cs_auth

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters**
  
    | Key   | Value                                  |
    | :---- | :------------------------------------- |
    | role  | must in ['admin', 'employee']          |
    | skip  | optional, start position, default is 0 |
    | limit | optional, default is not limit         |
    
  
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key           | Value Type         | note                                  |
          | :------------ | :----------------- | :------------------------------------ |
          | status        | string             | ok or error                           |
          | error         | string             | show error message if status is error |
          | cs_auth_users | cs_auth_user array |                                       |
          | len           | int                | total count                           |

          
          cs_auth_user
          | Key  | Value Type | note |
          | :--- | :--------- | :--- |
          | uid  | string     |      |
          | name | string     |      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/bots/SOA2lyYByqUFqb6KzALnlf9FGdxMNxkXAVdAKSnbw1HMTU2Njk3OTMzNzgyOA/cs_auth?role=admin

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

    {
      "status": "ok",  
      "cs_auth_users": [
        {
        "uid": "xxx",  
        "name": "test222"
        }
      ]
    }    
      

* **Notes:**
