**Post Utterance**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/utterances

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key        | Value Type   | note     |
    | :--------- | :----------- | :------- |
    | intent_ids | string array |          |
    | text       | string       | sentence |
    | entities   | entity array |          |

    entity

    | Key            | Value Type | note                                                    |
    | :------------- | :--------- | :------------------------------------------------------ |
    | entity_id      | string     | id of entity                                            |
    | text           | string     | entity value                                            |
    | start_position | int        | indicate the start position of this entity in utterance |
    | len            | int        | indicate the len of this entity                         |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key          | Value Type | note                                  |
          | :----------- | :--------- | :------------------------------------ |
          | status       | string     | ok or error                           |
          | error        | string     | show error message if status is error |
          | utterance_id | string     | id of new utterance                   |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/utterances

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {        
        "intent_ids": ["caass8e7sasdas09c05c724653939e1eac4b9d2277"],
        "text": "我要去台北",
        "entities": [{
          "entity_id": "gff5sdfz3...",
          "text": "台北",
          "start_position": 3,
          "len": 2}
        ]
      }
  
  response body

      {
        "status": "ok",
        "utterance_id":"cqwedq09c05c724653939e1eac4b9d2277"          
      }

* **Notes:**
