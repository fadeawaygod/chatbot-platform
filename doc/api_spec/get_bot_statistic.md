**Get Bot Statistic**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/statistic

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key                | Value                                                                                                                                                       |
    | :----------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | start_time         | required, limit the first timestamp to query, use UNIX timestamp millsecond, e.g.1580439515000                                                              |
    | end_time           | required, limit the last timestamp to query, use UNIX timestamp millsecond,  e.g.1580439515000, end_time - start_time must less than 5356800000(two months) |
    | show_day_statistic | (optional) true or false, default is false show single day statistic by hour in every day selected.                                                         |
    | passage            | (optional) only do statisic on message of specified tunnel, e.g. web                                                                                        |
    | integration_name   | (optional)  only do statisic on message of specified integration, e.g. WEB1                                                                                 |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key       | Value Type    | note                                  |
          | :-------- | :------------ | :------------------------------------ |
          | status    | string        | ok or error                           |
          | error     | string        | show error message if status is error |
          | statistic | statistic obj |                                       |
          
          statistic

          | Key             | Value Type    | note                      |
          | :-------------- | :------------ | :------------------------ |
          | message_traffic | traffic array | daily message count       |
          | user_traffic    | traffic array | daily distinct user count |
          | user_count      | int           | total distinct user count |

          traffic
          
          | Key           | Value Type        | note                        |
          | :------------ | :---------------- | :-------------------------- |
          | date          | string            | in format: yyyy:MM:dd       |
          | count         | int               |                             |
          | day_statistic | day_statistic obj | hour is key, count is value |

  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:5000/api/v1/editor/bots/GM3fsHj8fP0Wy7CtZVrOahOg4LJwiHz8C24LhxEeXx1/statistic?start_time=1577776775000&end_time=1578035975000&show_day_statistic=true

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "statistic":[
          {
            "message_traffic": [
              {date: '2019/09/07' count:50},
              {date: '2019/09/08' count:150},
              {date: '2019/09/09' count:52},
              {date: '2019/09/10' count:50},
              {date: '2019/09/11' count:50},
              {date: '2019/09/12' count:55},
              {date: '2019/09/13' count:80},
            ],
            "user_traffic": [
              {date: '2019/09/07' count:1},
              {date: '2019/09/08' count:2},
              {date: '2019/09/09' count:3},
              {date: '2019/09/10' count:1},
              {date: '2019/09/11' count:1},
              {date: '2019/09/12' count:4},
              {date: '2019/09/13' count:5},
            ],
            "user_count": 5
          }
        ]
      }

* **Notes:**
