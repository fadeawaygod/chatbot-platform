**Send Reset Password Mail**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/reset_password_mail

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key          | Value                    |
    | :----------- | :----------------------- |
    | Content-Type | must be application/json |

  * **Body Parameters:**

    | Key          | Value  | Note                   |
    | :----------- | :----- | :--------------------- |
    | mail_address | string | required, mail address |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note        |
          | :----- | :--------- | :---------- |
          | status | string     | ok or error |
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/reset_password_mail

  request Header

  | Key          | Value            |
  | :----------- | :--------------- |
  | Content-Type | application/json |

  request body

      {
        "mail_address": "test@test.com",
      }

  response body

      {
        "status": "ok"
      }

* **Notes:**
