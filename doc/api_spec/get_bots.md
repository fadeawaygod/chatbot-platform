**Get Bots**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key      | Value                                                                                                                   |
    | :------- | :---------------------------------------------------------------------------------------------------------------------- |
    | skip     | optional, start position, default is 0                                                                                  |
    | limit    | optional, default is not limit                                                                                          |
    | keyword  | optional, search name for this keyword                                                                                  |
    | show_len | optional, indicate whether return len (true or false), default is false                                                 |
    | is_owner | optional,default is true if it's true, only show owner's bot, if it's false, only show not owner's(but accessible) bot. |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | bots   | bot array  |                                       |
          
          bot

          | Key                  | Value Type   | note                                                                               |
          | :------------------- | :----------- | :--------------------------------------------------------------------------------- |
          | id                   | string       | bot id                                                                             |
          | name                 | string       |                                                                                    |
          | owner                | string       | user id of owner                                                                   |
          | accessors            | accessor obj | indicate which users can view/ edit this bot                                       |
          | config               | config       | config for start a bot                                                             |
          | deploy_status        | string       | deploy status, should be one of the following: offline, deploying, ready, failed   |
          | train_status         | string       | train status, should be one of the following: untrained, training, trained, failed |
          | is_nlu_data_modified | bool         | true if intents, entities or utterances was modified after last training           |
          | last_failed_log      | string       | if status is failed, show the cause                                                |
          | nlu_id               | string       | generated after last training, can be used to query NLU train API                  |
          | nlu_token            | string       | generated after last training, can be used to query NLU infer API                  |
          | description          | string       |                                                                                    |
          | created              | string       | indicate the time of this bot be created(UNIX TIME STAMP MILLISECOND)              |
          
          accessor

          | Key  | Value Type     | note    |
          | :--- | :------------- | :------ |
          | :uid | permission obj | user id |

          permission

          | Key  | Value Type | note                                                                  |
          | :--- | :--------- | :-------------------------------------------------------------------- |
          | view | bool       | whether this user can view this bot(and it's intents, entities, etc.) |
          | edit | bool       | whether this user can edit this bot(and it's intents, entities, etc.) |
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/bots

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "bots":[
          {
            "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
            "name": "test_bot",
            "owner": "asassaew...",
            "config": {},
            "status": "offline",
            "description": "This is a bot for test."
          }
        ]
      }

* **Notes:**
