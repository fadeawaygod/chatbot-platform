**Import Bot**
----
import data 

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/import

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

  * **Body Parameters:**
    | Key        | Value           |
    | :--------- | :-------------- |
    | entities   | entity array    |
    | intents    | intent array    |
    | utterances | utterance array |
    | flow       | flow            |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note        |
          | :----- | :--------- | :---------- |
          | status | string     | ok or error |
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/bots/bot_id/import

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok"
      }
  
  request body

      {
        "intents": [],
        "entities": [],
        "utterances": [],
        "flow": {},
      }

* **Notes:**
