**Post Utterances auto labeling**
----
Label an utterance with NER service automatically.
It also create entity if the entity in labeled utterances not exsit.

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/auto_labeling

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key           | Value Type   | note                          |
    | :------------ | :----------- | :---------------------------- |
    | bot_id        | string       |                               |
    | utterance_ids | string array | the utterances need be labeld |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/auto_labeling

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      { 
        "bot_id": "asasacsg..."
        "utterance_ids": ["caass8e7sasdas09c05c724653939e1eac4b9d2277"],
      }
  
  response body

      {
        "status": "ok",
      }

* **Notes:**
