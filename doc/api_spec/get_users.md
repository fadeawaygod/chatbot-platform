**Get Users**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/users

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key      | Value                                                                   |
    | :------- | :---------------------------------------------------------------------- |
    | skip     | optional, start position, default is 0                                  |
    | limit    | optional, default is not limit                                          |
    | keyword  | optional, search name for this keyword                                  |
    | show_len | optional, indicate whether return len (true or false), default is false |
    | role     | optional, indicate specific role to search                              |
    | group    | optional, filter group(only working when user's role is admin)          |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | users  | user array |                                       |
          
          user

          | Key          | Value Type     | note                                                                  |
          | :----------- | :------------- | :-------------------------------------------------------------------- |
          | uid          | string         | user id                                                               |
          | name         | string         |                                                                       |
          | group        | string         | id of group that the user belong to                                   |
          | phone        | string         | phone number                                                          |
          | mail_address | string         | mail address                                                          |
          | avatar_url   | string         | URL of avatar image                                                   |
          | role         | list of string | every role should be one of the followings: group_admin, editor, user |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/users

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "users":[
          {
            "uid": "harry",
            "name": "harry",
            "mail_address": "xxx@silkrode.com.tw",
            "role": [
                "group_admin"
            ],
            "group": "silkrode",
            "phone": "0912123456",
            "avatar_url": "https://xxx.photo.jpg"
          }
        ]
      }

* **Notes:**
