**Get Bot Intent Usage**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/intent_usage

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key        | Value                                                                                                                                                       |
    | :--------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | start_time | required, limit the first timestamp to query, use UNIX timestamp millsecond, e.g.1580439515000                                                              |
    | end_time   | required, limit the last timestamp to query, use UNIX timestamp millsecond,  e.g.1580439515000, end_time - start_time must less than 5356800000(two months) |
    

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key          | Value Type             | note                                  |
          | :----------- | :--------------------- | :------------------------------------ |
          | status       | string                 | ok or error                           |
          | error        | string                 | show error message if status is error |
          | intent_usage | intent usage obj array | order by usage desc                   |
          
          intent usage

          | Key         | Value Type | note                      |
          | :---------- | :--------- | :------------------------ |
          | intent_name | str        |                           |
          | count       | int        | daily distinct user count |

          traffic
          
          | Key           | Value Type        | note                        |
          | :------------ | :---------------- | :-------------------------- |
          | date          | string            | in format: yyyy:MM:dd       |
          | count         | int               |                             |
          | day_statistic | day_statistic obj | hour is key, count is value |

  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:5000/api/v1/editor/bots/GM3fsHj8fP0Wy7CtZVrOahOg4LJwiHz8C24LhxEeXx1/intent_usage?start_time=1577776775000&end_time=1578035975000

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "intent_usage": [
            {
                "intent_name": "h_未到帳",
                "count": 240
            },
            {
                "intent_name": "h_充值",
                "count": 8
            },
            {
                "intent_name": "unknown",
                "count": 3
            },
            {
                "intent_name": "h_提供未到帳資訊",
                "count": 3
            }
        ]
      }

* **Notes:**
