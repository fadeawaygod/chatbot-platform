**Get Intent**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/intents/:intent_id

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key  | Value |
    | :--- | :---- |
    
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | intent | intent     |                                       |
          
          intent
          
          | Key      | type                                 | Note                                                                                         |
          | -------- | ------------------------------------ | -------------------------------------------------------------------------------------------- |
          | id       | string                               | UUID                                                                                         |
          | name     | string                               |                                                                                              |
          | priority | int                                  | if many intent are mapped with same probability, system will choose the highest priority one |
          | entities | string array, filled with entity ids |                                                                                              |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/intents/XDXD

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "intent":
        {
          "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
          "name": "test_intent",
          "bot_id": "asassaew...",
          "priority": 100,
          "entities": [
              "c8e709c0-5c72-4653-939e-1eac4b9d2277"
          ]
        }
        
      }

* **Notes:**
