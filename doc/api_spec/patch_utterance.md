**Patch Utterances**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/utterances/:utterance_id

* **Method:**
  `Patch`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key        | Value Type      | note               |
    | :--------- | :-------------- | :----------------- |
    | text       | string          | optional, sentence |
    | entities   | entity array    | optional,          |
    | intent_ids | array of string |                    |

    entity

    | Key            | Value Type | note                                                    |
    | :------------- | :--------- | :------------------------------------------------------ |
    | entity_id      | string     | id of entity                                            |
    | text           | string     | entity value                                            |
    | start_position | int        | indicate the start position of this entity in utterance |
    | len            | int        | indicate the len of this entity                         |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Patch   http://127.0.0.1:8080/api/v1/editor/utterances/fake_utterance_id

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {        
        "text": "我要去台南",
        "entities": [{
          "entity_id": "gff5sdfz3...",
          "text": "台南",
          "start_position": 3,
          "len": 2}
        ]
      }
  
  response body

      {
        "status": "ok"      
      }

* **Notes:**
