**Post Bots**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key         | Value Type | note                   |
    | :---------- | :--------- | :--------------------- |
    | name        | string     |                        |
    | config      | config     | config for start a bot |
    | description | string     |                        |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | bot_id | string     | new bot id                            |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/bots

  request Header

  | Key           | Value                    |
  | :------------ | :----------------------- |
  | Authorization | Bearer xxxxxxxx          |
  | Content-Type  | must be application/json |

  request body

      {
        "name": "test_bot",
        "config": "{}",
        "description": "This is a bot for test."        
      }

  response body

      {
        "status": "ok",
        "bot_id": "fdofi4fd..."
      }

* **Notes:**
