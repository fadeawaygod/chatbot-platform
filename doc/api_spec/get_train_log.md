**Get Train logs**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/train_log

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key        | Value                                                                                          |
    | :--------- | :--------------------------------------------------------------------------------------------- |
    | skip       | optional, start position, default is 0                                                         |
    | limit      | optional, default is not limit                                                                 |
    | show_len   | optional, indicate whether return len (true or false), default is false                        |
    | start_time | optional, limit the first timestamp to query, use UNIX timestamp millsecond, e.g.1580439515000 |
    | end_time   | optional, limit the last timestamp to query, use UNIX timestamp millsecond, e.g.1580439515000  |
    
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key       | Value Type | note                                  |
          | :-------- | :--------- | :------------------------------------ |
          | status    | string     | ok or error                           |
          | error     | string     | show error message if status is error |
          | train_log | log array  |                                       |
          | len       | int        | show total count if show_len is true  |
          
          log

          | Key          | Value Type       | note                                                                        |
          | :----------- | :--------------- | :-------------------------------------------------------------------------- |
          | status       | string           | deploy status, should be one of the following: start, ready, failed         |
          | train_detail | train detail obj | if, status is 'ready', record details of train result, e.g. intent_score... |
          | error        | string           | error message                                                               |
          | error_code   | int              | error code                                                                  |
          | updated      | string           | indicate the time of this record be recorded(UNIX TIME STAMP MILLISECOND)   |

          train detail obj
          | Key          | Type  | note |
          | :----------- | :---- | :--- |
          | intent_score | float |      |
          | entity_score | float |      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8000/api/v1/editor/bots/E2HN5AHxAGn1ceA5B2zFcbQEuoxfWaoPoxAEWV5PYku/train_log

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "train_log": [
            {
            "status": "failed",
            "error": "Agent service error:query bot_setup_service service fail:{\"status\": \"validation_fail\", \"message\": \"transitions of state(3ff4b612-1992-4831-905d-023e1abbfefd) must greater than or equal to 1\"}",
            "error_code": 30000,
            "updated": 1579053979804
            },
            {
                "status": "start",
                "updated": 1579053979004
            }
        ]        
      }

* **Notes:**
