**Patch Bots**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id

* **Method:**
  `Patch`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key              | Value Type       | note                                                         |
    | :--------------- | :--------------- | :----------------------------------------------------------- |
    | name             | string           | optional                                                     |
    | config           | config           | optional, config for start a bot                             |
    | description      | string           | optional                                                     |
    | accessors        | accessor obj     | indicate which users can view/ edit this bot                 |
    | intent_threshold | float            | between 0-1, set intent threshold when calling NLU query API |
    | nlu_endpoint     | nlu_endpoint obj | for default nlu endpoint query use                           |


    accessor

    | Key  | Value Type     | note    |
    | :--- | :------------- | :------ |
    | :uid | permission obj | user id |

    permission

    | Key  | Value Type | note                                                                  |
    | :--- | :--------- | :-------------------------------------------------------------------- |
    | view | bool       | whether this user can view this bot(and it's intents, entities, etc.) |
    | edit | bool       | whether this user can edit this bot(and it's intents, entities, etc.) |

    nlu_endpoint obj

    | Key           | Type | note            |
    | :------------ | :--- | :-------------- |
    | nlu_name      | str  | endpoin name    |
    | url           | str  | query url       |
    | http_type     | str  | POST, GET...etc |
    | header        | dict | key-value dict  |
    | parameter     | dict | key-value dict  |
    | body          | str  | json str        |
    | endpoint_type | str  | must be AI      |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Patch   http://127.0.0.1:8080/api/v1/editor/bots/asda25asdqw...

  request Header

  | Key           | Value                    |
  | :------------ | :----------------------- |
  | Authorization | Bearer xxxxxxxx          |
  | Content-Type  | must be application/json |

  request body

      {
        "name": "test_bot2"    
      }

  response body

      {
        "status": "ok",
      }

* **Notes:**
