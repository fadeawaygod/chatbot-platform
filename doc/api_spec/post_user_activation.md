**Post User Acitvation**
----
Activate an user by token

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/users

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key          | Value                    |
    | :----------- | :----------------------- |
    | Content-Type | must be application/json |

  * **Body Parameters:**

    | Key   | Value  | Note             |
    | :---- | :----- | :--------------- |
    | uid   | string | user id          |
    | token | string | activation token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/user_activation

  request Header

  | Key          | Value            |
  | :----------- | :--------------- |
  | Content-Type | application/json |

  request body

      {
        "uid": "test_user",
        "token":"84ec64d622e14daaaef8f23217289568"
      }

  response body

      {
        "status": "ok"
      }

* **Notes:**
