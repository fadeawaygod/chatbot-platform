**Get Intents**
----
  get nlu training data: intent

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/nlu_models/:nlu_model_id/intents

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key     | Value Type   | note                                  |
          | :------ | :----------- | :------------------------------------ |
          | status  | string       | ok or error                           |
          | error   | string       | show error message if status is error |
          | intents | intent array |                                       |
          
          intent

          | Key      | Value Type   | note                                                                 |
          | :------- | :----------- | :------------------------------------------------------------------- |
          | id       | string       | intent id                                                            |
          | name     | string       | readable name                                                        |
          | priority | int          | when many intent mapped with same score, return highest priority one |
          | entities | string array | entity ids                                                           |
    
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/nlu_models/fake_nlu_model_id/intents

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "intents":[
          {
            "id": "2905b8b9-ce1b-4c43-999a-e8fb6549882b",
            "name": "使用者問如何前往城市",
            "entities": [
              "451dada8-3e7a-459b-9672-bf6095ab9dc7"
            ],
            "priority": 100   
          }
        ]
      }

* **Notes:**
