**get Entities**
----
  get nlu training data: entity

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/nlu_models/:nlu_model_id/entities

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key      | Value Type   | note                                  |
          | :------- | :----------- | :------------------------------------ |
          | status   | string       | ok or error                           |
          | error    | string       | show error message if status is error |
          | entities | entity array |                                       |
          
          entity

          | Key     | Value Type  | note                    |
          | :------ | :---------- | :---------------------- |
          | id      | string      | entity id               |
          | name    | string      | readable name           |
          | entries | entry array | examples of this entity |

          entry

          | Key      | Value Type   | note                  |
          | :------- | :----------- | :-------------------- |
          | value    | string       | entry value           |
          | synonyms | string array | synonym of this entry |

          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/nlu_models/fake_nlu_model_id/entities

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "entities":[
          {
            "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
            "name": "city",
            "entries": [
              {
                "value": "台北",
                "synonyms": [
                  "臺北"
                ]
              },
              {
                "value": "高雄",
                "synonyms": [
                  "打狗"
                ]
              }
            ]
          }
        ]
      }

* **Notes:**
