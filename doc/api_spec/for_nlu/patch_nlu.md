**Partially Update NLU model**
----
  Update NLU model partially

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/nlu_models/:nlu_model_id

* **Method:**
  `PATCH`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |

  * **Body(json)**   

    | Key          | Type             | note                                                                          |
    | :----------- | :--------------- | :---------------------------------------------------------------------------- |
    | status       | string           | must be one of the following: data_fetched, trained, failed                   |
    | train_detail | train detail obj | if, status is 'trained', record details of train result, e.g. intent_score... |
    | note         | string           | optional, if status is failed, show error message                             |

    train detail obj

    | Key          | Type  | note |
    | :----------- | :---- | :--- |
    | intent_score | float |      |
    | entity_score | float |      |
    
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Type                                  | note |
          | :----- | :------------------------------------ | :--- |
          | status | ok or error                           |      |
          | error  | show error message if status is error |      |
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Patch   http://127.0.0.1:8080/api/v1/editor/nlu_models/test_nlu_model_id

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body    
      `
      {
        "status": "trained"
      }
      `
* **Notes:**
