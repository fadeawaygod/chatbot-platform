**Get Intent's Utterances**
----
  get nlu training data: intent

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/nlu_models/:nlu_model_id/utterances

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

  * **Url Parameters:**

    | Key       | Value                                                                             |
    | :-------- | :-------------------------------------------------------------------------------- |
    | intent_id | get utterances with specific intent                                               |
    | skip      | optional, start position, default is 0                                            |
    | limit     | optional, max query number of intents on offset, default is len(utterance)        |
    | show_len  | optional, indicate whether return len of utterances in this intent(true or false) |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key        | Value Type      | note                                              |
          | :--------- | :-------------- | :------------------------------------------------ |
          | status     | string          | ok or error                                       |
          | error      | string          | show error message if status is error             |
          | intent_id  | string          |                                                   |
          | utterances | utterance array |                                                   |
          | len        | int             | if show_len is true, show total len of utterances |
          
          utterance

          | Key      | Value Type   | note         |
          | :------- | :----------- | :----------- |
          | id       | string       | utterance id |
          | text     | string       | sentence     |
          | entities | entity array |              |

          entity

          | Key            | Value Type | note                                                    |
          | :------------- | :--------- | :------------------------------------------------------ |
          | entity_id      | string     |                                                         |
          | text           | string     | entity value                                            |
          | start_position | int        | indicate the start position of this entity in utterance |
          | len            | int        | indicate the len of this entity                         |
          
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/nlu_models/fake_nlu_model_id/utterances?intent_id=xxx&show_len=true

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "intent_id": "qweihjjkasd"
        "utterances": [
          {
            "id": "3e9eab6e-7c80-482a-b79a-825c17058cdb",
            "text": "怎樣才能去高雄呢"
            "entities":[
              {
                "entity_id": "451dada8-3e7a-459b-9672-bf6095ab9dc7",
                "text": "高雄",
                "start_position": 5,
                "len": 2                
              }
            ]
          }
        ],
        "len": 100
      }

* **Notes:**
