**Patch Dialog**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/dialogs/:dialog_id

* **Method:**
  `Patch`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Body Parameters:**

    | Key        | Value Type      | note                                  |
    | :--------- | :-------------- | :------------------------------------ |
    | name       | str             |                                       |
    | utterances | utterance array |                                       |
    | status     | str             | should be unlabeled, labeling or done |
    | labeler    | str             | uid of labeler                        |
   

    utterance

    | Key          | Value Type | note                                                 |
    | :----------- | :--------- | :--------------------------------------------------- |
    | utterance_id | string     | utterance id                                         |
    | from_user    | bool       | indicates whether this utterance was sent from user  |
    | sent_time    | int        | UNIX timestamp millsecond,  e.g.1580439515000,       |
    | commented    | bool       | commented means this utterance should be check later |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |

  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Patch   http://127.0.0.1:8080/api/v1/editor/dialogs/XXXXXX

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {
        "name": "new collection"
      }

  response body

      {
        "status": "ok",       
      }

* **Notes:**

