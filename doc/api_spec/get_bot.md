**Get Bots**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/bot_id

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | bot    | bot        |                                       |
          
          bot

          | Key                  | Value Type       | note                                                                             |
          | :------------------- | :--------------- | :------------------------------------------------------------------------------- |
          | id                   | string           | bot id                                                                           |
          | name                 | string           |                                                                                  |
          | owner                | string           | user id of owner                                                                 |
          | accessors            | accessor obj     | indicate which users can view/ edit this bot                                     |
          | config               | config           | config for start a bot                                                           |
          | status               | string           | deploy status, should be one of the following: offline, deploying, ready, failed |
          | last_failed_log      | string           | if status is failed, show the cause                                              |
          | is_nlu_data_modified | bool             | true if intents, entities or utterances was modified after last training         |
          | nlu_id               | string           | generated after last training, can be used to query NLU train API                |
          | nlu_token            | string           | generated after last training, can be used to query NLU infer API                |
          | train_detail         | train detail obj | record details of train result, e.g. intent_score...                             |
          | description          | string           |                                                                                  |
          | created              | string           | indicate the time of this bot be created(UNIX TIME STAMP MILLISECOND)            |
          | intent_threshold     | float            | between 0-1, set intent threshold when calling NLU query API                     |
          | nlu_endpoint         | nlu_endpoint obj | for default nlu endpoint query use                                               |

          accessor

          | Key  | Value Type     | note    |
          | :--- | :------------- | :------ |
          | :uid | permission obj | user id |

          permission

          | Key  | Value Type | note                                                                  |
          | :--- | :--------- | :-------------------------------------------------------------------- |
          | view | bool       | whether this user can view this bot(and it's intents, entities, etc.) |
          | edit | bool       | whether this can user edit this bot(and it's intents, entities, etc.) |

          train detail obj
          | Key          | Type  | note |
          | :----------- | :---- | :--- |
          | intent_score | float |      |
          | entity_score | float |      |

          nlu_endpoint obj

          | Key           | Type | note            |
          | :------------ | :--- | :-------------- |
          | nlu_name      | str  | endpoin name    |
          | url           | str  | query url       |
          | http_type     | str  | POST, GET...etc |
          | header        | dict | key-value dict  |
          | parameter     | dict | key-value dict  |
          | body          | str  | json str        |
          | endpoint_type | str  | must be AI      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/bots/bot_id

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "bots":{
            "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
            "name": "test_bot",
            "owner": "asassaew...",
            "config": {},
            "status": "offline",
            "description": "This is a bot for test."
          }
        
      }

* **Notes:**
