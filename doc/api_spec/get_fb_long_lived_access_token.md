**Get FB Long Lived Access Token**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/fb/long_lived_access_token

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key                                  | Value Type  | note                                  |
          | :----------------------------------- | :---------- | :------------------------------------ |
          | status                               | string      | ok or error                           |
          | error                                | string      | show error message if status is error |
          | long_lived_user_access_token         | string      |                                       |
          | long_lived_page_access_token_mapping | mapping obj |                                       |
          
          user

          | Key      | Value Type | note             |
          | :------- | :--------- | :--------------- |
          | :page_id | string     | id token mapping |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/fb/long_lived_access_token

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "long_lived_user_access_token": "EAAIXsvACy2QBAB0FszW6pZA2gEwT1EIzU3hhmpyNZA5W6Sa94PFF37uZBw34amoDe6ZAZBeqj6tnXdQd0OvrAnWSgFNGuqjOAcMYPKLM35Eq4e3YhWRYaETp5QJjIBn6aCFH0WmYx86kDQ705nnoIo8lXWOgGpwFjIoQMFyEG5wZDZD",
        "long_lived_page_access_token_mapping": {
        "109982897550965":  "EAAIXsvACy2QBAK486ckUC8pecOJzIWbzgRBIRdfyCwXz9f3GZAOGaraPYofKlg7mCcpaM5gW2Q4r0CZBNB9ggCMLCpOrndlO2h8UJV77GeUcGuLn0EZAWKpaPHU6MBbST1pKiaeyOmS0sYIRxlDF9ev9brWx6MJ9YZCwXR7OyAZDZD",
        "324276744624967": "EAAIXsvACy2QBAOdC5k3QeXRc3oM5ZCdaxcSXb2vSJqxvPHxdZCharIE1FdCQvC6ZALj0Ey7qhWob95TQMVZC3TgkE7coUCjm2IeK1Umro0QXpyze96y8ZA8oZBTauNZA0iIHXVblDzSynYmkY3uchdueLnvhwrLxCZChOZALFl4Tz0AZDZD"
    }
        
      }

* **Notes:**
