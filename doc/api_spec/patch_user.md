**Patch Users**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/users/:uid

* **Method:**
  `Patch`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |

  * **Body Parameters:**

    | Key          | Value          | Note                                                                                                                |
    | :----------- | :------------- | :------------------------------------------------------------------------------------------------------------------ |
    | name         | string         |                                                                                                                     |
    | group        | string         | should be identical with your group id if your are not admin                                                        |
    | password     | string         |                                                                                                                     |
    | phone        | string         | phone number                                                                                                        |
    | mail_address | string         | mail address                                                                                                        |
    | avatar_url   | string         | URL of avatar image                                                                                                 |
    | role         | list of string | every role should be one of the followings: group_admin(for group admin), editor(for group admin and editor), user, |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Patch   http://127.0.0.1:8080/api/v1/editor/users/tests_user_id

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {
        "name": "test_user_name",
        "nick_name": "apple"
      }

  response body

      {
        "status": "ok"
      }

* **Notes:**
