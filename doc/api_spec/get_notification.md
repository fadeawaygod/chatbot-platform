**Get Notification History**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/notification_history

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key        | Value                                                                                                                                                       |
    | :--------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | skip       | optional, start position, default is 0                                                                                                                      |
    | limit      | optional, default is not limit                                                                                                                              |
    | is_checked | optional, true or false, filter notifications by this column                                                                                                |
    | event      | optional, filter notifications by this column                                                                                                               |
    | start_time | optional, limit the first timestamp to query, use UNIX timestamp millsecond, e.g.1580439515000                                                              |
    | end_time   | optional, limit the last timestamp to query, use UNIX timestamp millsecond,  e.g.1580439515000, end_time - start_time must less than 5356800000(two months) |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key           | Value Type         | note                                  |
          | :------------ | :----------------- | :------------------------------------ |
          | status        | string             | ok or error                           |
          | error         | string             | show error message if status is error |
          | notifications | notification array |                                       |
          | len           | int                | total count                           |
          
          notification

          | Key        | Value Type | note                                               |
          | :--------- | :--------- | :------------------------------------------------- |
          | id         | string     | notification id, uuid v4                           |
          | bot_id     | string     |                                                    |
          | user_id    | string     |                                                    |
          | event      | string     | inducate the type of this notification             |
          | text       | string     | mail address                                       |
          | is_checked | bool       | if the notification is reviewed, it should be true |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/notification_history

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "notification_history":[
          {
            "id": "xxx",
            "name": "harry",
            "mail_address": "xxx@silkrode.com.tw",
            "role": [
                "group_admin"
            ],
            "group": "silkrode",
            "phone": "0912123456",
            "avatar_url": "https://xxx.photo.jpg"
          }
        ]
      }

* **Notes:**
