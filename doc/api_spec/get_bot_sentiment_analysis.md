**Get Bot Sentiment Analysis**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/sentiment_analysis

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key                  | Value                                                                                                                                                                                           |
    | :------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
    | start_date_timestamp | required, limit the first date timestamp to query(great or equal than), use UNIX timestamp millsecond, e.g.1580439515000                                                                        |
    | end_date_timestamp   | required, limit the last date timestamp to query(less than), use UNIX timestamp millsecond,  e.g.1580439515000, end_date_timestamp - start_date_timestamp must less than 5356800000(two months) |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key      | Value Type             | note                                                              |
          | :------- | :--------------------- | :---------------------------------------------------------------- |
          | status   | string                 | ok or error                                                       |
          | error    | string                 | show error message if status is error                             |
          | analysis | sentiment analysis obj | key: emotion type, value: score(float), it would between 0 and 1. |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:5000/api/v1/editor/bots/GM3fsHj8fP0Wy7CtZVrOahOg4LJwiHz8C24LhxEeXx1/sentiment_analysis?start_date_time=1577776775000&end_date_time=1578035975000

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "analysis":{
          "Angry": 0.048,
          "Compliment": 0.221,
          "Confused": 0.043,
          "Delight/Like": 0.023,
          "Disappointment": 0.311,
          "Tease/Ironic": 0.076,
          "Complain": 0.017
        }
      }

* **Notes:**
