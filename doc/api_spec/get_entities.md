**Get Entities**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/entities

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key      | Value                                                                   |
    | :------- | :---------------------------------------------------------------------- |
    | bot_id   | required                                                                |
    | skip     | optional, start position, default is 0                                  |
    | limit    | optional, default is not limit                                          |
    | keyword  | optional, search name for this keyword                                  |
    | show_len | optional, indicate whether return len (true or false), default is false |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key      | Value Type   | note                                  |
          | :------- | :----------- | :------------------------------------ |
          | status   | string       | ok or error                           |
          | error    | string       | show error message if status is error |
          | entities | entity array |                                       |

          entity

          | Key        | Value Type   | note                                            |
          | :--------- | :----------- | :---------------------------------------------- |
          | id         | string       | entity id                                       |
          | name       | string       | readable name                                   |
          | entries    | entry array  | examples of this entity                         |
          | prompts    | prompt array | optional, sentences to ask users of this entity |
          | is_enabled | bool         | whether this entity is active                   |

          entry

          | Key      | Value Type   | note                  |
          | :------- | :----------- | :-------------------- |
          | value    | string       | entry value           |
          | synonyms | string array | synonym of this entry |

          prompt

          | Key  | Value Type | note |
          | :--- | :--------- | :--- |
          | text | string     |      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/entities

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "entities":[
          {
            "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
            "name": "weather",
            "entries": [{
              "value": "雨天",
              "synonyms": ["下雨"]
            }]
          }
        ]
      }

* **Notes:**
