**Post Entitys**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/entities

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key    | Value Type | note          |
    | :----- | :--------- | :------------ |
    | name   | string     | readable name |
    | bot_id | string     |               |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key       | Value Type | note                                  |
          | :-------- | :--------- | :------------------------------------ |
          | status    | string     | ok or error                           |
          | error     | string     | show error message if status is error |
          | entity_id | string     | id of new entity                      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/entities

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {
        "name":"下雨",
        "bot_id":"qweqwec8e709..."
      }
  
  response body

    {
      "status": "ok",
      "entity_id":"asdasdc..."          
    }

* **Notes:**
