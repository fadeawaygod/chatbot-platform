**Get Dialog**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/dialogs

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key         | Value                                                                                                                  |
    | :---------- | :--------------------------------------------------------------------------------------------------------------------- |
    | bot_id      | required                                                                                                               |
    | skip        | optional, start position, default is 0                                                                                 |
    | limit       | optional, default is not limit                                                                                         |
    | keyword     | optional, search name for this keyword                                                                                 |
    | show_len    | optional, indicate whether return len (true or false), default is false                                                |
    | sort_by     | optional, return items with sorted by particular field(name, status, updated, labeling_total_time), default is updated |
    | is_asc      | optional, whether sort in ascending order(true or false), default is false                                             |
    | status      | optional, filter status                                                                                                |
    | keyword     | optional, search name for this keyword                                                                                 |
    | min_comment | optional, number, get dialogs that count of commented utterances exceed this amount                                    |
    
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key     | Value Type   | note                                  |
          | :------ | :----------- | :------------------------------------ |
          | status  | string       | ok or error                           |
          | error   | string       | show error message if status is error |
          | dialogs | dialog array |                                       |
          dialog

          | Key                 | Value           | Note                                                                                                        |
          | :------------------ | :-------------- | :---------------------------------------------------------------------------------------------------------- |
          | id                  | str             |                                                                                                             |
          | name                | str             |                                                                                                             |
          | utterances          | utterance array |                                                                                                             |
          | status              | str             | should be unlabeled, labeling or done                                                                       |
          | labeler             | str             | uid of labeler                                                                                              |
          | updated             | int             | UNIX timestamp millsecond,  e.g.1580439515000,                                                              |
          | start_labeling_time | int or none     | when the status turn into labeling, set this field to now .  UNIX timestamp millsecond,  e.g.1580439515000, |
          | labeling_total_time | int or none     | when the status turn into done,set this field to now - start_labeling_time in millsecond,  e.g.15000,       |
          | comment_count       | int             | number of utterances that commented is ture                                                                 |


          utterance

          | Key          | Value  | Note                                                 |
          | :----------- | :----- | :--------------------------------------------------- |
          | utterance_id | string | utterance id                                         |
          | from_user    | bool   | indicates whether this utterance was sent from user  |
          | sent_time    | int    | UNIX timestamp millsecond,  e.g.1580439515000,       |
          | commented    | bool   | commented means this utterance should be check later |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/dialogs?bot_id=XXXXXX

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "dialogs": [{
          "name": "collection 1",
          "status": done,
          "utterances": [{
            "utterance_id": "qyNI5VsSc8QfiQAkWZLSoo2Bh0NNTuDKzuBob4ZZy14",
            "from_user": false, "sent_time": 1585670400000
          }],
          "id": "yOpjeIRexF3tWMBu3NL2sp1oBbZaXjaG4v24gQCZ3TS",
          "updated": 1586159264673,
          "start_labeling_time": 1586159264673,
          "finish_labeling_time": 1586159264673
        }]    
      }


* **Notes:**


