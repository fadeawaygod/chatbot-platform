**Post Users**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/users

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |

  * **Body Parameters:**

    | Key          | Value          | Note                                                                  |
    | :----------- | :------------- | :-------------------------------------------------------------------- |
    | uid          | string         | unique id of a group                                                  |
    | name         | string         | optional                                                              |
    | group        | string         | id of group that the user belong to                                   |
    | password     | string         |                                                                       |
    | phone        | string         | optional, phone number                                                |
    | mail_address | string         | optional, mail address                                                |
    | avatar_url   | string         | optional, URL of avatar image                                         |
    | role         | list of string | every role should be one of the followings: group_admin, editor, user |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/users

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {
        "name": "test_user_name",
        "nick_name": "apple",
        "group": "dfsfsd4uy5bka...",
        "password": "xxxxxxxx",
        "phone": "+886912123456",
        "mail_address": "test@test.com",
        "avatar_url": "https://.....",
        "role": ["editor", "user"],
      }

  response body

      {
        "status": "ok"
      }

* **Notes:**
