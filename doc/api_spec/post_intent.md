**Post Intents**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/intents

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                    |
    | :------------ | :----------------------- |
    | Authorization | Bearer token             |
    | Content-Type  | must be application/json |
  * **Body Parameters:**

    | Key      | type   | Note                                                                                         |
    | -------- | ------ | -------------------------------------------------------------------------------------------- |
    | name     | string |                                                                                              |
    | bot_id   | string |                                                                                              |
    | priority | int    | if many intent are mapped with same probability, system will choose the highest priority one |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key       | Value Type | note                                  |
          | :-------- | :--------- | :------------------------------------ |
          | status    | string     | ok or error                           |
          | error     | string     | show error message if status is error |
          | intent_id | string     | id of new intent                      |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/intents

  request Header

  | Key           | Value            |
  | :------------ | :--------------- |
  | Authorization | Bearer xxxxxxxx  |
  | Content-Type  | application/json |

  request body

      {
        "name": "test intent",
        "bot_id":"caass8e7sasdas09c05c724653939e1eac4b9d2277"          
      }
  
  response body

      {
        "status": "ok",
        "intent_id":"c8e709c05c724653939e1eac4b9d2277" ,
        "priority": 10
      }

* **Notes:**
