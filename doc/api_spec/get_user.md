**Get Users**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/users/:user_id

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | user   | user       |                                       |
          
          user

          | Key          | Value Type     | note                                                                  |
          | :----------- | :------------- | :-------------------------------------------------------------------- |
          | uid          | string         | user id                                                               |
          | name         | string         |                                                                       |
          | group        | string         | id of group that the user belong to                                   |
          | phone        | string         | phone number                                                          |
          | mail_address | string         | mail address                                                          |
          | avatar_url   | string         | URL of avatar image                                                   |
          | role         | list of string | every role should be one of the followings: group_admin, editor, user |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/users/testid

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "user":{
            "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
            "name": "city"
          }
        
      }

* **Notes:**
