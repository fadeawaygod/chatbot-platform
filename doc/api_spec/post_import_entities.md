**Import Entities**
----
import entity

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/import_entities

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value            |
    | :------------ | :--------------- |
    | Authorization | Bearer token     |
    | Content-Type  | application/json |

  * **Body Parameters:**
    | Key  | Value        |
    | :--- | :----------- |
    | data | entity array |

    entity
    
    | Key     | Value       | Note        |
    | :------ | :---------- | :---------- |
    | name    | str         | entity name |
    | entries | entry array |             |
    | prompts | str array   |             |

    entry
    
    | Key      | Value     | Note        |
    | :------- | :-------- | :---------- |
    | value    | str       | entity name |
    | synonyms | str array |             |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note        |
          | :----- | :--------- | :---------- |
          | status | string     | ok or error |
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/bots/test_bot_id/import_entities

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok"
      }
  
  request body

      {
        "data":[
          {
            "name": "食物",
            "entries":[
              {
                "value": "水餃",
                "synonyms": [
                  "餃子"
                ]
              }
            ],
            "prompts":[]
          }
        ]
      }

* **Notes:**