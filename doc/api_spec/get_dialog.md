**Get Dialog**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/dialogs/:dialog_id

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | dialog | dialog     |                                       |
          dialog

          | Key                 | Value           | Note                                                                                                        |
          | :------------------ | :-------------- | :---------------------------------------------------------------------------------------------------------- |
          | id                  | str             |                                                                                                             |
          | name                | str             |                                                                                                             |
          | utterances          | utterance array |                                                                                                             |
          | status              | str             | should be unlabeled, labeling or done                                                                       |
          | labeler             | str             | uid of labeler                                                                                              |
          | updated             | int             | UNIX timestamp millsecond,  e.g.1580439515000,                                                              |
          | start_labeling_time | int or none     | when the status turn into labeling, set this field to now .  UNIX timestamp millsecond,  e.g.1580439515000, |
          | labeling_total_time | int or none     | when the status turn into done,set this field to now - start_labeling_time in millsecond,  e.g.15000,       |

          utterance

          | Key           | Value             | Note                                                 |
          | :------------ | :---------------- | :--------------------------------------------------- |
          | utterance_id  | string            | utterance id                                         |
          | from_user     | bool              | indicates whether this utterance was sent from user  |
          | sent_time     | int               | UNIX timestamp millsecond,  e.g.1580439515000,       |
          | commented     | bool              | commented means this utterance should be check later |
          | raw_utterance | raw_utterance obj | real utterance link by utterance_id                  |


          raw utterance

          | Key       | Value Type   | note                                                 |
          | :-------- | :----------- | :--------------------------------------------------- |
          | id        | string       | utterance id                                         |
          | text      | string       | sentence                                             |
          | entities  | entity array |                                                      |
          | from_user | bool         | indicates whether this utterance was sent from user  |
          | sent_time | int          | UNIX timestamp millsecond,  e.g.1580439515000,       |
          | commented | bool         | commented means this utterance should be check later |

          entity

          | Key            | Value Type | note                                                    |
          | :------------- | :--------- | :------------------------------------------------------ |
          | entity_id      | string     | id of entity                                            |
          | text           | string     | entity value                                            |
          | start_position | int        | indicate the start position of this entity in utterance |
          | len            | int        | indicate the len of this entity                         |

  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/dialogs/XXXXXX

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "dialog": {
          "bot_id": "L1hKlLUIB4HHsPVTgXKuD8BI9sI22P78ScsdEED1xSj",
          "name": "collection 1",
          "status": "done",
          "utterances": [
              {
                  "text": "點一杯卡布奇諾送到我家",
                  "from_user": false,
                  "sent_time": 1585670400000,
                  "entities": [
                      {
                          "text": "我家",
                          "start_position": 9,
                          "len": 2,
                          "entity_id": "f73988af84f9478d942dca0f07f9ce5a"
                      }
                  ],
                  "id": "vFvOcbUKYzTe3bc4YxyKf3cINuLYZaK26w4Oatj1PTv"
              }
          ],
          "id": "AoRwJ9S4OqetCLKAHKSavy9Lbaqnlm5p6WWgLkKtuH0",
          "updated": 1586315065431
        }
      }

* **Notes:**
