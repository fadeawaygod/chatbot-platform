**Get NLU Information**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/nlu_info

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key      | Value Type | note                                       |
          | :------- | :--------- | :----------------------------------------- |
          | status   | string     | ok or error                                |
          | error    | string     | show error message if status is error      |
          | nlu_info | nlu_info   | information of nlu service, e.g. query_url |
          
          nlu_info

          | Key       | Value Type | note              |
          | :-------- | :--------- | :---------------- |
          | query_url | string     | inference API url |


        
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/nlu_info

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "nlu_info":{
            "query_url": "http://10.205.50.2/api/query",
          }        
      }

* **Notes:**
