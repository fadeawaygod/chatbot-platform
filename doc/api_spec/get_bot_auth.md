**Get Bot's Auth**
----
 

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/auth

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key       | Value Type    | note                                  |
          | :-------- | :------------ | :------------------------------------ |
          | status    | string        | ok or error                           |
          | error     | string        | show error message if status is error |
          | accessors | accessors obj |                                       |
          
          accessors

          | Key  | Value Type     | note    |
          | :--- | :------------- | :------ |
          | :uid | permission obj | user id |

          permission

          | Key  | Value Type | note                                                                  |
          | :--- | :--------- | :-------------------------------------------------------------------- |
          | view | bool       | whether this user can view this bot(and it's intents, entities, etc.) |
          | edit | bool       | whether this can user edit this bot(and it's intents, entities, etc.) |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get  http://127.0.0.1:5000/api/v1/editor/bots/L1hKlLUIB4HHsPVTgXKuD8BI9sI22P78ScsdEED1xSj/auth

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "accessors": {
          "xxx":{
            "view": true,
            "edit": true
          }
        }
        
      }

* **Notes:**
