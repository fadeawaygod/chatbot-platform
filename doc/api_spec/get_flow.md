**Get Flow**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/flow

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key    | Value  |
    | :----- | :----- |
    | bot_id | bot id |
    
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | users  | user array |                                       |
          
          flow

          | Key     | Value Type | note        |
          | :------ | :--------- | :---------- |
          | bot_id  | string     | bot id      |
          | id      | string     | flow id     |
          | content | object     | flow config |
          
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/flow?bot_id=SOA2lyYByqUFqb6KzALnlf9FGdxMNxkXAVdAKSnbw1HMTU2Njk3OTMzNzgyOA==

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
      "status": "ok",  
      "flows": [
        {
            "_id": "xxx",  
            "bot_id": "xxx",  
            "id": "xxx",  
            "updated": 1568093543541,  
            "content": {
                "state": "welanvelnrmdiesmncvfje",
                "trans": "encmrmcjremc"
            }
        }
    ]
}

* **Notes:**
