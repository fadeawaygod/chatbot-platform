**get Export**
----
  export data

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/export

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | error  | string     | show error message if status is error |
          | data   | data obj   |                                       |

          data array

          | Key        | Value Type      | note     |
          | :--------- | :-------------- | :------- |
          | intents    | intent array    |          |
          | entities   | entity array    |          |
          | utterances | utterance array | examples |
          | flow       | flow array      |          |

          intent

          | Key      | Value Type   | note          |
          | :------- | :----------- | :------------ |
          | id       | string       |               |
          | name     | string       | readable name |
          | priority | int          |               |
          | entities | string array | entity ids    |

          entity

          | Key     | Value Type   | note                                            |
          | :------ | :----------- | :---------------------------------------------- |
          | id      | string       |                                                 |
          | name    | string       | readable name                                   |
          | entries | entry array  |                                                 |
          | prompts | prompt array | optional, sentences to ask users of this entity |

          entry

          | Key      | Value Type   | note                  |
          | :------- | :----------- | :-------------------- |
          | value    | string       | entry value           |
          | synonyms | string array | synonym of this entry |
          
          prompt

          | Key  | Value Type | note |
          | :--- | :--------- | :--- |
          | text | string     |      |

          utterance

          | Key        | Value Type             | note |
          | :--------- | :--------------------- | :--- |
          | intent_ids | string array           |      |
          | text       | string                 |      |
          | entities   | utterance entity array |      |

          utterance entity

          | Key            | Value Type | note          |
          | :------------- | :--------- | :------------ |
          | len            | int        |               |
          | start_position | int        |               |
          | entity_name    | string     | readable name |
          | text           | string     |               |

          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/bots/bot_id/export

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "data":{
          "entities":[],
          "intents":[],
          "utterances":[],
          "flow":[],
        }
      }

* **Notes:**
