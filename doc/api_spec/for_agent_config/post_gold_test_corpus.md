**Insert Gold test corpus**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/config_test_corpus/{bot_id}

* **Method:**
  `POST`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
    | Content-type | application/json |
    
  * **Body:**

    | Key           | Value        |
    | :------------ | :----------- |
    | corpus        | user: 你好\nbot: 你好 |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | message| string     | show error message if status is error |
          | data   | object     | inserted data object                  |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  POST   http://127.0.0.1:8080/api/v1/editor/config_test_corpus/123456

  request Header

  | Key           | Value            |
  | :------------ | :--------------  |
  | Authorization | Bearer xxxxxxxx  |
  | Content-type  | application/json |

  response body

      {
        "status": "ok",
        "message": "insert data success",
        "data": {
            "bot_id": "123456",
            "corpus": "user: 你好\nbot: 你好",
            "id": "RqcBPZQlMnQyA5oVHAv8OHzjrJF9jpqbnl513eyGgfc",
            "updated": 1573204047393,
            "created": 1573204047393,
            "_id": "5dc5304f3a6fae80f8d655d2"
        }
      }

* **Notes:**
