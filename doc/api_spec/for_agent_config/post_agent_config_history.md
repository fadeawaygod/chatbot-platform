**Save Agent config in history**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/config_test/{bot_id}

* **Method:**
  `POST`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
   
  * **Body:**

    | Key           | Value             |
    | :------------ | :-----------      |
    | flow          | Flow builder data |
    | config        | agent's config    |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | message| string     | show error message if status is error |
          | data   | object     | inserted object                       |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  POST   http://127.0.0.1:8080/api/v1/editor/config_test/123456

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "message": "insert data success",
        "data": {
            "bot_id": "123456",
            "create_time": 1573204936,
            "flow": "flow_maxson_test",
            "config": "config_maxson_test",
            "test_result": [
                {
                    "origin": "user: 你好\nbot: 你好",
                    "result": "user: 你好\nbot: 您好，我是华华",
                    "score": 0.2222222222222222,
                    "confuse": 0,
                    "pass": "FAIL"
                }
            ],
            "id": "60JWFZFlYH2NTCmF606287K45SNkmzzGOWr10DvRDTR",
            "updated": 1573204936067,
            "created": 1573204936067,
            "_id": "5dc533c83a6faed734d655d6"
        }
      }

* **Notes:**
