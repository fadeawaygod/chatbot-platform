**Get Agent config history**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/config_test/{bot_id}

* **Method:**
  `GET`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |

  * **Url Parameters:**

    | Key   | Value                                  |
    | :---- | :------------------------------------- |
    | skip  | optional, start position, default is 0 |
    | limit | optional, default is not limit         |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                            |
          | :----- | :--------- | :------------------------------ |
          | status | string     | ok or error                     |
          | count  | integer    | show how many row has been get. |
          | result | array      | history object array            |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  GET   http://127.0.0.1:8080/api/v1/editor/config_test/123456

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "count": 5,
        "result": [
            {
                "_id": "5dc532bf3a6fae7e9cd655d5",
                "bot_id": "123456",
                "create_time": 1573204671,
                "flow": "flow_maxson_test",
                "config": "config_maxson_test",
                "test_result": [
                    {
                        "origin": "user: 你好\nbot: 你好",
                        "result": "user: 你好\nbot: 您好，我是华华",
                        "score": 0.2222222222222222,
                        "confuse": 0,
                        "pass": "FAIL"
                    }
                ],
                "id": "ELGq75I1KCp2Wl6iOGLpyrye3G6RbsoMtjd8JsPipTW",
                "updated": 1573204671451,
                "created": 1573204671451
            },
            {
                "_id": "5dc532aa3a6fae27a2d655d4",
                "bot_id": "123456",
                "create_time": 1573204650,
                "flow": "flow_maxson_test",
                "config": "config_maxson_test",
                "test_result": [
                    {
                        "origin": "user: 你好\nbot: 你好",
                        "result": "user: 你好\nbot: 您好，我是华华",
                        "score": 0.2222222222222222,
                        "confuse": 0,
                        "pass": "FAIL"
                    }
                ],
                "id": "ZNHa7RbK9UsIDSXxfDS4vOo9ofVOi7hjrsb38Uyr6qk",
                "updated": 1573204650438,
                "created": 1573204650438
            }
        ]
    }

* **Notes:**
