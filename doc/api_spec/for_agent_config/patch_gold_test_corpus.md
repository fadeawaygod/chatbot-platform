**Patch Gold test corpus**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/config_test_corpus/{bot_id}

* **Method:**
  `POST`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
    | Content-type | application/json |
    
  * **Body:**

    | Key           | Value                  |
    | :------------ | :-----------           |
    | id            | corpus row id          |
    | corpus        | user: 你好\nbot: Hi你好 |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note                                  |
          | :----- | :--------- | :------------------------------------ |
          | status | string     | ok or error                           |
          | message| string     | show error message if status is error |
          | count   | integer     | affected row count                  |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  POST   http://127.0.0.1:8080/api/v1/editor/config_test_corpus/123456

  request Header

  | Key           | Value            |
  | :------------ | :--------------  |
  | Authorization | Bearer xxxxxxxx  |
  | Content-type  | application/json |

  response body

      {
        "id": "RqcBPZQlMnQyA5oVHAv8OHzjrJF9jpqbnl513eyGgfc",
        "corpus": "user: 你好\nbot: 你好"
      }

* **Notes:**
