**Analyze Utterance**
----
  This API analyze one utterance's intents and entities

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/nlu_services/analyze_utterance

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key          | Value                                  |
    | :----------- | :------------------------------------- |
    | query_string | required, utterance need by analyzed   |
    | nlu_token    | required, a token to specify nlu model |

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key      | Value Type   | note                                  |
          | :------- | :----------- | :------------------------------------ |
          | status   | string       | ok or error                           |
          | error    | string       | show error message if status is error |
          | analysis | analysis obj |                                       |

          analysis

          | Key             | Value Type       | note                                                      |
          | :-------------- | :--------------- | :-------------------------------------------------------- |
          | intents         | confidence array | show matched intent with confidence higher than threshold |
          | top5_intents    | confidence array | show top 5 confidence intent                              |
          | entities        | entity           | show matched intent entities                              |
          | combine_intents | confidence array | deprecated                                                |

          confidence

          | Key        | Value Type | note                                                                           |
          | :--------- | :--------- | :----------------------------------------------------------------------------- |
          | value      | string     | intent name                                                                    |
          | confidence | float      | the confidence of the utterance belonged to this intent, should be between 0~1 |
          
          entity

          | Key          | Value Type   | note                            |
          | :----------- | :----------- | :------------------------------ |
          | :entity name | string array | entries belonged to this entity |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/nlu_services/analyze_utterance?query_string=我想喝茉莉花茶&nlu_token=77ffd75c07494bdda9b2cd2b8c6eb2f8

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "analysis": {
            "intents": [
                {
                    "value": "點飲料",
                    "confidence": 0.52122
                }
            ],
            "top5_intents": [
                {
                    "value": "點飲料",
                    "confidence": 0.52122
                }
            ],
            "entities": {
                "飲料": [
                    "茉莉花茶"
                ]
            },
            "combine_intents": [
                {
                    "value": "點飲料",
                    "confidence": 0.52122
                }
            ]
        }
      }

* **Notes:**
