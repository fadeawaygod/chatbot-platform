**Import Intents**
----
import intent data 

please use .tsv file

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/bots/:bot_id/import_intents

* **Method:**
  `Post`  

* **Request**
  * **Header:**

    | Key           | Value                     |
    | :------------ | :------------------------ |
    | Authorization | Bearer token              |
    | Content-Type  | text/tab-separated-values |

  * **TSV format:**
    
    intent\tutterance
    intent1\ttterance1
    intent2\ttterance2
    intent2\ttterance3

* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key    | Value Type | note        |
          | :----- | :--------- | :---------- |
          | status | string     | ok or error |
          
  * **Error Response:**
    * **Code:** 400 Bad Request
      * **Header:** 

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
       * **Body:**
          `
          { "error" : "Invalid request format" }
          `
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is deprecated" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }
          `
* **Example:**

  Post   http://127.0.0.1:8080/api/v1/editor/bots/test_bot_id/import_intents

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok"
      }
  
  request body

      intent  utterance
      訂餐廳  我想訂餐廳
      訂餐廳  請幫我訂位

* **Notes:**

  - first line must be intent\tutterance
