**Get Utterance**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/utterances

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key       | Value                                                                   |
    | :-------- | :---------------------------------------------------------------------- |
    | intent_id | required                                                                |
    | skip      | optional, start position, default is 0                                  |
    | limit     | optional, default is not limit                                          |
    | keyword   | optional, search text for this keyword                                  |
    | show_len  | optional, indicate whether return len (true or false), default is false |
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key        | Value Type      | note                                  |
          | :--------- | :-------------- | :------------------------------------ |
          | status     | string          | ok or error                           |
          | error      | string          | show error message if status is error |
          | utterances | utterance array |                                       |
          
          utterance

          | Key        | Value Type   | note         |
          | :--------- | :----------- | :----------- |
          | id         | string       | utterance id |
          | text       | string       | sentence     |
          | entities   | entity array |              |
          | intent_ids | str array    |              |

          entity

          | Key            | Value Type | note                                                    |
          | :------------- | :--------- | :------------------------------------------------------ |
          | entity_id      | string     | id of entity                                            |
          | text           | string     | entity value                                            |
          | start_position | int        | indicate the start position of this entity in utterance |
          | len            | int        | indicate the len of this entity                         |
          
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/utterances

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "utterances":[
          {
            "id": "c8e709c05c724653939e1eac4b9d2277",
            "text": "我要去台北",
            "entities": [{
              "entity_id": "gff5sdfz3...",
              "text": "台北",
              "start_position": 3,
              "len": 2}
            ]
          }
        ]
      }

* **Notes:**
