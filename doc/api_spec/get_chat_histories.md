**Get Chat histories**
----
  

* **Host:**
  http://127.0.0.1:8080

* **Path:**
  api/v1/editor/chathistory

* **Method:**
  `Get`  

* **Request**
  * **Header:**

    | Key           | Value        |
    | :------------ | :----------- |
    | Authorization | Bearer token |
  * **Url Parameters:**

    | Key         | Value                                  |
    | :---------- | :------------------------------------- |
    | skip        | optional, start position, default is 0 |
    | limit       | optional, default is not limit         |
    | bot_id      | bot_id                                 |
    | user_id     | user id                                |
    | source_from | telegram、messenger、line、web、demo   |
    
* **Response**
  * **Success Response:**
      * **Code:** 200

          | Key       | Value Type         | note                                  |
          | :-------- | :----------------- | :------------------------------------ |
          | status    | string             | ok or error                           |
          | error     | string             | show error message if status is error |
          | histories | chat history array |                                       |
          
          histories

          | Key         | Value Type | note                                 |
          | :---------- | :--------- | :----------------------------------- |
          | id          | string     | chat history id                      |
          | source_from | string     | telegram, messenger, line, web, demo |
          | bot_id      | string     | id of bot                            |
          | user_id     | string     | id of user                           |
          | message     | string     | chat message                         |
          | from_user   | bool       | from user or from agent              |
          | updated     | timestamp  | chat time                            |
 
  * **Error Response:**
    * **Code:** 401 Unauthorized
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Invalid token or token is expired" }
          `
    * **Code:** 403 FORBIDDEN
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "Your token has no permission to do this operation" }

    * **Code:** 500 Server ERROR
      * **Header:**

          | Key          | Value            |
          | :----------- | :--------------- |
          | Content-Type | application/json |
      *  **Body:**
          `
          { "error" : "..." }
          `
* **Example:**

  Get   http://127.0.0.1:8080/api/v1/editor/chathistory?bot_id=test_bot&uesr_id=user1&source_from=telegram&skip=0&limit=100

  request Header

  | Key           | Value           |
  | :------------ | :-------------- |
  | Authorization | Bearer xxxxxxxx |

  response body

      {
        "status": "ok",
        "histories":[
          {
            "id": "c8e709c0-5c72-4653-939e-1eac4b9d2277",
            "source_from": "telegram",
            "bot_id": "test_bot",
            "user_id": "user1",
            "message": "hello world",
            "from_user": true,
            "updated": "1567557981"
          }
        ]
      }

* **Notes:**
