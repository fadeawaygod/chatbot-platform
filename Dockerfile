FROM python:3.7.3-alpine

LABEL Name=chatbotplatform Version=0.0.1
EXPOSE 5000

WORKDIR /app
COPY . /app


# Install linux dependencies.
RUN apk update && \
	apk add --virtual .build-deps gcc musl-dev libffi-dev && \
	apk del libressl-dev && \
	apk add openssl-dev && \
	apk add --update gcc && \
	apk add git

# Install python dependencies.
RUN cd /app \
  && python -m pip install --quiet --upgrade pip \
  && pip install --quiet -r requirements.txt \
  && rm -rf .cache/pip


CMD ["python", "tornado_server.py"]
